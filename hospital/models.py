from django.db import models
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords

# Create your models here.
def ids():
    no = Hospital.objects.count()
    if no == 0:
        no=1
        st=str(no)
        while len(st)<3:
            st= '0'+st
        st='1'+st
        return st
    else:
        no = Hospital.objects.last()
        i= int(no.code)+1
        st= str(i)
        while User.objects.filter(username=st).exists():
            i=i+1
            st=str(i)
        return st

class Ward(models.Model):
    name= models.CharField(max_length=20)

    def __str__(self):
      return self.name
    class Meta:
        db_table='wards'

class Hospital(models.Model):
    code = models.CharField(max_length=20, null=True, editable=False)
    name= models.CharField(max_length=50)
    phone = models.CharField(max_length=15,blank=True)
    location = models.CharField(max_length=50)
    ward = models.ForeignKey(Ward,on_delete=models.PROTECT,default=1)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    created_by = models.CharField(max_length=1)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1
    )
    history=HistoricalRecords()

    def __str__(self):
        return self.code

    def huid(self):
        return self.user.id

    def created(self):
        return self.created_at.strftime('%d %B, %Y')

    class Meta:
        db_table = "hospitals"

class UserBelongsToHospital(models.Model):
    hospital=models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    user=models.ForeignKey(
        User,
        on_delete=models.PROTECT
    )
    created_at = models.DateField(auto_now_add=True)
    class Meta:
        db_table="hospital_has_users"
