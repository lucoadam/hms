from django.shortcuts import render
from rest_framework import viewsets,generics,status,decorators,serializers,filters
from django.db.models.deletion import ProtectedError
from django.contrib.auth.models import User
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from .models import Hospital, Ward
from bill.models import Bill
from login.models import Year
from metas.models import Metas
from .serializers import HospitalSerializer, WardSerializer
from datetime import datetime
import urllib
import urllib3
import certifi
import json
from patients.models import Patients

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000
# Create your views here.
class HospitalViewSet(viewsets.ModelViewSet):
    serializer_class = HospitalSerializer
    queryset = Hospital.objects.all()
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['code','name','phone','location']

    @decorators.action(detail=False,methods=['GET'])
    def profile(self,request,*args,**kwargs):
        pharmacy = {
          'pan':'',
          'location':'',
          'phone':'',
          'name':''
        }
        if Hospital.objects.filter(user=request.user).exists():
            hospital = Hospital.objects.filter(user=request.user)[0]
            if hospital.pharmacy_set.exists():
              ph= hospital.pharmacy_set.first()
              pharmacy = {
                'pan':ph.pan,
                'location':ph.location,
                'phone':ph.phone,
                'name':ph.name
              }

            return Response({
                'name':hospital.name,
                'id':hospital.id,
                'user':request.user.id,
                'email':request.user.email,
                'phone':hospital.phone,
                'location':hospital.location,
                'pharmacy':pharmacy,
                'created_by':hospital.created_by
            })
        return Response({
            'name':'No name'
        })

    def destroy(self, request, *args, **kwargs):

      try:
        hos = Hospital.objects.filter(id=kwargs.get('pk'))
        if request.GET['remark']:
            hos.changeReason = request.GET['remark']
        if hos.exists():
          usr = hos[0].user
          hos.delete()
          if usr:
            usr.delete()
      except ProtectedError as e:
        return Response({
          'success':False,
          'message':'Deletion unsuccessful! Protected Data'
        })
      return Response({
        'success':True
      })


    def update(self, request, *args, **kwargs):
      partial = kwargs.pop('partial', False)
      instance = self.get_object()
      mydata = request.data
      if instance.pharmacy_set.exists():
        pharmacy = instance.pharmacy_set.first()
        pharmacy.pan = mydata['pharmacy']['pan']
        pharmacy.phone = mydata['pharmacy']['phone']
        pharmacy.location = mydata['pharmacy']['location']
        pharmacy.name = mydata['pharmacy']['name']
        pharmacy.save()
      else:
        instance.pharmacy_set.create(
          pan= mydata['pharmacy']['pan'],
          phone= mydata['pharmacy']['phone'],
          location= mydata['pharmacy']['location'],
          name= mydata['pharmacy']['name']
        )
      serializer = self.get_serializer(instance, data=request.data, partial=partial)
      serializer.is_valid(raise_exception=True)
      self.perform_update(serializer)

      if getattr(instance, '_prefetched_objects_cache', None):
        # If 'prefetch_related' has been applied to a queryset, we need to
        # forcibly invalidate the prefetch cache on the instance.
        instance._prefetched_objects_cache = {}

      return Response(serializer.data)

    def perform_update(self, serializer):
      serializer.save()

    def partial_update(self, request, *args, **kwargs):
      kwargs['partial'] = True
      return self.update(request, *args, **kwargs)

    # def update(self, request, *args, **kwargs):
    #     data = request.data
    #     return Response(data)

    # def update(self, request, *args, **kwargs):
    #     data = request.data
    #     hospital = Hospital.objects.filter(code=data['code'])[0]
    #     data['user']=User.objects.filter(username=data['user'])[0]
    #     print(data['user'])
    #     serializer = self.serializer_class(data=data)
    #     if(serializer.is_valid()):
    #         serializer.update(instance=hospital, validated_data=data)
    #         return Response(serializer.data,status=status.HTTP_201_CREATED)
    #     else:
    #         return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)


class WardViewSet(viewsets.ModelViewSet):
    permission_classes = []
    authentication_classes = []
    serializer_class = WardSerializer
    queryset = Ward.objects.all()

    @decorators.action(detail=False,methods=['POST'])
    def install(self,request,*args,**kwargs):
      meta = Metas.objects.filter(key='current_year')
      meta = int(meta.first().value) if meta.exists() else 77
      if 'noOfWards' in request.data and request.data['noOfWards'].isnumeric() and not Ward.objects.count():
        if meta == 77:
          Year.objects.create(year=77)
        else:
          Year.objects.create(year=77)
          Year.objects.create(year=meta)
        for i in range(int(request.data['noOfWards'])):
          Ward.objects.create(name='Ward No. '+str(i+1))
        return Response({'present':'true'})
      return Response(request.data)


    @decorators.action(detail=False,methods=['GET'])
    def sync_data(self,request,*args,**kwargs):
      meta = Metas.objects.filter(key='last_sync')
      data= {}
      if meta.exists():
        print(meta.first().value)
        date = datetime.strptime(meta.first().value,"%m/%d/%Y, %H:%M:%S")
      else:
        meta = Metas.objects.create(key='last_sync',value=datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
        date = False
      data = Hospital.objects.first().patients_set.all()
      if date:
        data = [{'bills':[{'bills':[{'bcode':b.bcode,'refunded':b.refunded,'user':{'first_name':b.user.first_name,'last_name':b.user.last_name,'password':b.user.password,'role':b.user.groups.values_list('name', flat=True)[0]},'created_at':b.created_at,'total':b.total,'year':b.year} for b in d.bills.all()]} for d in data],'pname':d.pname,'pcode':d.pcode,'page':d.page,'psex':d.psex,'paddress':d.paddress,'pphone':d.pphone,'pcreated_by':d.pcreated_by,'pcreated_at':d.pcreated_at,'referred_by':d.referred_by,'hospital_id':d.hospital.id} for d in data]
      else:
        data = [{'bills':[{'bills':[{'bcode':b.bcode,'refunded':b.refunded,'user':{'first_name':b.user.first_name,'last_name':b.user.last_name,'password':b.user.password,'role':b.user.groups.values_list('name', flat=True)[0]},'created_at':b.created_at,'total':b.total,'year':b.year} for b in d.bills.all()]} for d in data],'pname':d.pname,'pcode':d.pcode,'page':d.page,'psex':d.psex,'paddress':d.paddress,'pphone':d.pphone,'pcreated_by':d.pcreated_by,'pcreated_at':d.pcreated_at,'referred_by':d.referred_by,'hospital_id':d.hospital.id} for d in data]



      #code for returning data

      return Response({'success' : True,'data':data})

    @decorators.action(detail=False,methods=['GET'])
    def sync_patients(self,request,*args,**kwargs):
      meta = Metas.objects.filter(key='last_sync')
      data= {}
      if meta.exists():
        print(meta.first().value)
        date = datetime.strptime(meta.first().value,"%m/%d/%Y, %H:%M:%S")
      else:
        meta = Metas.objects.create(key='last_sync',value=datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
        date = False

      if date:
        data = Hospital.objects.first().patients_set.filter(pcreated_at__gt=date)
      else:
        data = Hospital.objects.first().patients_set.all()

      data = [{'pname':d.pname,'pcode':d.pcode,'page':d.page,'psex':d.psex,'paddress':d.paddress,'pphone':d.pphone,'pcreated_by':d.pcreated_by,'pcreated_at':d.pcreated_at,'referred_by':d.referred_by,'hospital_id':d.hospital.id} for d in data]

      result = urllib3
      #code for returning data

      return Response({'success' : True,'data':data})

    @decorators.action(detail=False,methods=['GET'])
    def sync_bills(self,request,*args,**kwargs):
      meta = Metas.objects.filter(key='last_sync')
      data= {}
      if meta.exists():
        print(meta.first().value)
        date = datetime.strptime(meta.first().value,"%m/%d/%Y, %H:%M:%S")
      else:
        meta = Metas.objects.create(key='last_sync',value=datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
        date = False

      if date:
        data = Bill.objects.filter(created_at__lt=date)
      else:
        data = Bill.objects.all()
      data = [{'bcode':b.bcode,'refunded':b.refunded,'user':{'first_name':b.user.first_name,'last_name':b.user.last_name,'password':b.user.password,'role':b.user.groups.values_list('name', flat=True)[0]},'created_at':b.created_at.strftime("%m/%d/%Y, %H:%M:%S"),'total':b.total,'year':b.year.year}  for b in data]
      #code for returning data

      http = urllib3.PoolManager(ca_certs=certifi.where())
      encoded_data = json.dumps(data).encode('utf-8')
      url = 'http://127.0.0.1:8000'
      resp = http.request(
        'POST',
        url+'/api/v1/ward/post_bills/',
        body=encoded_data,
        headers={'Content-Type': 'application/json'})

      data = json.loads(resp.data.decode('utf-8'))
      return Response(data)

    @decorators.action(detail=False,methods=['POST'])
    def post_bills(self,request,*args,**kwargs):
      # print(request.data,'billdata')
      datas = request.data
      for data in datas:
        print(data['bcode'])
      return Response({'success':True})

