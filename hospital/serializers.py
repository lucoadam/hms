from rest_framework import serializers
from . import models
from metas.models import Metas
from django.contrib.auth.models import User, Group
from django.contrib.auth.hashers import make_password

from .models import Hospital

# Create your models here.
def ids():
    no = Hospital.objects.count()
    if no == 0:
        no=1
        st=str(no)
        while len(st)<3:
            st= '0'+st
        st='1'+st
        return st
    else:
        no = Hospital.objects.last()
        i= int(no.code)+1
        st= str(i)
        while User.objects.filter(username=st).exists():
            i=i+1
            st=str(i)
        return st
class HospitalSerializer(serializers.ModelSerializer):
    # user = serializers.CharField(required=False)
    huid = serializers.IntegerField(required=False)
    id = serializers.IntegerField(required=False)

    class Meta:
        model = models.Hospital
        fields = "__all__"

    def create(self, validated_data):
        code = ids()
        hospital = models.Hospital.objects.create(
            code=code,
            user=User.objects.create(
              username=code,
              first_name=validated_data.get('name').split(maxsplit=1)[0],
              last_name=validated_data.get('name').split(maxsplit=1)[1] if 1 in validated_data.get('name').split(maxsplit=1) else "",
              password=make_password(validated_data.get('phone'))
            ),
            name=validated_data.get('name'),
            phone=validated_data.get('phone'),
            location=validated_data.get('location'),
            ward=validated_data.get('ward'),
            created_by=validated_data.get('created_by'),
        )
        if hospital.pk:
          group = Group.objects.filter(name='ROLE_HOSPITAL')[0] if Group.objects.filter(
            name='ROLE_HOSPITAL') else Group.objects.create(name='ROLE_HOSPITAL')
          group.user_set.add(hospital.user)
          dm= Metas.objects.filter(hospital__isnull=True)
          for meta in dm:
            Metas.objects.create(key=meta.key,value=meta.value if not meta.key=='address' else hospital.location,hospital=hospital)
          Metas.objects.create(key='application_name',value=hospital.name,hospital=hospital)

        return hospital


class WardSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Ward
        fields = "__all__"
