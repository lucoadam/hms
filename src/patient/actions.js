import { MUTATE } from './mutation-types';

/**
* @param { function } commit
* @param { string } data
*/
export function set({ commit }, { data }) {
    commit(MUTATE, { data });
}
