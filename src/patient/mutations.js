import { MUTATE } from './mutation-types';

export default {
    /**
    * @param { object } state
    * @param { string } data
    */
    [MUTATE](state, { data }) {
        state.data = data;
    },
};
