import conDate from './date'
import jsPDF from 'jspdf'
import 'jspdf-autotable'
export default {

  methods: {
    camelCase (str) {
      return str.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
        return index === 0 ? word.toLowerCase() : word.toUpperCase()
      }).replace(/\s+/g, '')
    },
    downloadDatas (name, headers, rows, header = '') {
      var columnHeaders = headers.map(each => {
        if (each.value !== 'actions') {
          return each.text
        }
      })
      let all = []
      let colspan = columnHeaders.indexOf('Amount (Rs.)')
      let total = 0
      rows.forEach((items, i) => {
         if ('day' in items) {

         }
      })
      rows.forEach((items, i) => {
        let row = []
        headers.forEach((each) => {
          if (each.value === 'id') {
            row.push(i + 1)
          } else {
            if (colspan !== -1 && each.value === 'amount') {
              total += items[each.value]
            }
            row.push(items[each.value])
          }
        })
        all.push(row)
      })
      if (colspan > 0) {
        all.push([
          {
            content: 'Total',
            colSpan: colspan,
            styles: { halign: 'center' }
          },
          total
        ])
      }
      if (all.length) {
        console.log(all)
        var doc = new jsPDF()
        doc.text(header, 20, 20)
        doc.autoTable(columnHeaders, all, { startY: 30 })
        // doc.text(header, 20, (all.length + 1)*18)
        doc.save(name + '.pdf')
      } else {
        alert('No data found')
      }
    },
    goTo (ref) {
      this.$nextTick(() => {
        console.log(this.$refs[ref])

        if (this.$refs[ref]) {
          if (Array.isArray(this.$refs[ref])) {
            this.$refs[ref][0].focus()
          } else {
            this.$refs[ref].focus()
          }
        }
      })
    },
    sumArray (d) {
      return d.reduce((a, b) => a + b, 0)
    },
    routePermissionExists (route) {
      let role = this.$auth.user().roleName
      if (route && role !== 'admin') {
        route = route.split(role + '.')[1]
        return route in this.$auth.user().permissions && this.$auth.user().permissions[route]
      } else {
        return true
      }
    },
    checkRouteExists (routeName) {
      var link = this.$router.resolve({ name: routeName })
      return link && link.href !== '#/'
    },
    formatDateComplete (date) {
      let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear()
      if (month.length < 2) {
        month = '0' + month
      }
      if (day.length < 2) {
        day = '0' + day
      }
      var converted = [year, month, day].join('/')
      return conDate[converted] !== '' ? conDate[converted] : [year, month, day].join('/')
    },

    convertDate (converted) {
      let key = Object.values(conDate).indexOf(converted)
      if (key > -1) {
        return Object.keys(conDate)[key]
      }
      return converted
    },
  }
}
