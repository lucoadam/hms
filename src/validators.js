import StudentApi from './api/students'
import UsersApi from './api/users'

export default {
  school_telephone: {
    getMessage (field, params, data) {
      return (data && data.message) || 'Telephone Already exists'
    },
    validate (value, args) {
      return UsersApi.checkUsernameExistsOrNot(value)
        .then((response) => {
          return {
            valid: response.body.status
          }
        })
    }
  },

  symbol_number: {
    getMessage (field, params, data) {
      return (data && data.message) || 'Symbol Number Already exists'
    },
    validate (value, args) {
      return StudentApi.checkSymbolNumberExistsOrNot(value, args[0] ? args[0] : undefined)
        .then((response) => {
          return {
            valid: response.body.status
          }
        })
    }
  }
}
