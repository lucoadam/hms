export default {
  doctor: [
    {
      title: 'Dashboard',
      route: 'doctor.dashboard.index',
      crud: ['index']
    },
    {
      title: 'Patients',
      route: 'doctor.patients.index'
    },
    {
      title: 'Services',
      route: 'doctor.services.index'
    },
    {
      title: 'Profile',
      route: 'doctor.profile'
    },
    {
      title: 'Downloads',
      crud: ['index'],
      route: 'doctor.downloads.index'
    }
  ],
  cashier: [
    {
      title: 'Account',
      route: 'cashier.account.index'
    },
    {
      title: 'Patients',
      route: 'cashier.patients.index'
    },
    {
      title: 'Services',
      route: 'cashier.services.index'
    }
  ],
  staff: [
    {
      title: 'Dashboard',
      route: 'staff.dashboard.index',
      crud: ['index']
    },
    {
      title: 'Accounts',
      route: 'staff.account.index',
      crud: ['index'],
      action: 'local_activity',
      childGroup: [
        {
          title: 'Income',
          active: true,
          children: [
            {
              title: 'Income',
              route: 'staff.incomes.index',
              crud: ['index', 'create', 'edit', 'delete']
            },
            {
              title: 'Income Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.incometypes.index'
            }
          ]
        },
        {
          title: 'Expenses',
          children: [
            {
              title: 'Expense',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.expenditures.index'
            },
            {
              title: 'Expense Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.expendituretypes.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Patients',
      crud: ['index', 'create', 'edit', 'delete', 'bill', 'search'],
      route: 'staff.patients.index'
    },
    {
      title: 'Pharmacy',
      action: 'local_activity',
      childGroup: [
        {
          title: 'Store',
          active: true,
          children: [
            {
              title: 'Store In',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.storein.index'
            },
            {
              title: 'Store Out',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.storeout.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Settings',
      crud: ['index'],
      route: 'staff.settings.index',
      action: 'local_activity',
      childGroup: [
        {
          title: 'Setup',
          active: true,
          children: [
            {
              title: 'Service Type',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.types.index'
            },
            {
              title: 'Services',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.services.index'
            },
            {
              title: 'Department',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.department.index'
            }
          ]
        },
        {
          title: 'User',
          children: [
            {
              title: 'User',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.users.index'
            },
            {
              title: 'Doctors',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.doctors.index'
            },
            {
              title: 'Staff Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.stafftypes.index'
            },
            {
              title: 'Staff',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'staff.staff.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Downloads',
      crud: ['index'],
      route: 'staff.downloads.index'
    },
    {
      title: 'Profile',
      route: 'staff.profile.index',
      crud: ['index']
    }
  ],
  gen: [
    {
      title: 'Account',
      route: 'admin.account.index'
    },

    {
      title: 'Patients',
      route: 'admin.patients.index'
    },
    {
      title: 'Services',
      route: 'admin.services.index'
    },
    {
      title: 'Services Types',
      route: 'admin.types.index'
    },
    {
      title: 'Income',
      route: 'admin.incomes.index'
    },
    {
      title: 'Income Types',
      route: 'admin.incometypes.index'
    },
    {
      title: 'Expense',
      route: 'admin.expenditures.index'
    },
    {
      title: 'Expense Types',
      route: 'admin.expendituretypes.index'
    },
    {
      title: 'Doctors',
      route: 'admin.doctors.index'
    },
    {
      title: 'Staff',
      route: 'admin.staff.index'
    },
    {
      title: 'Staff Types',
      route: 'admin.stafftypes.index'
    },
    {
      title: 'Users',
      route: 'admin.users.index'
    },
    {
      title: 'Departments',
      route: 'admin.department.index'
    },
    {
      title: 'Store In',
      route: 'admin.storein.index'
    },
    {
      title: 'Store Out',
      route: 'admin.storeout.index'
    },

    {
      title: 'Hospitals',
      route: 'admin.hospital.index'
    },

    {
      title: 'Settings',
      route: 'admin.settings'
    }
  ],
  general: [],
  // admin: [
  //   {
  //     title: 'Hospitals',
  //     route: 'admin.hospital.index'
  //   },
  //   {
  //     title: 'Accounts',
  //     route: 'admin.account.index',
  //     action: 'local_activity',
  //     childGroup: [
  //       {
  //         title: 'Income',
  //         active: true,
  //         children: [
  //           {
  //             title: 'Income',
  //             route: 'admin.incomes.index'
  //           },
  //           {
  //             title: 'Income Types',
  //             route: 'admin.incometypes.index'
  //           }
  //         ]
  //       },
  //       {
  //         title: 'Expenses',
  //         children: [
  //           {
  //             title: 'Expense',
  //             route: 'admin.expenditures.index'
  //           },
  //           {
  //             title: 'Expense Types',
  //             route: 'admin.expendituretypes.index'
  //           }
  //         ]
  //       }
  //     ]
  //   },
  //   {
  //     title: 'Patients',
  //     route: 'admin.patients.index'
  //   },
  //   {
  //     title: 'Pharmacy',
  //     action: 'local_activity',
  //     childGroup: [
  //       {
  //         title: 'Store',
  //         active: true,
  //         children: [
  //           {
  //             title: 'Store In',
  //             route: 'admin.storein.index'
  //           },
  //           {
  //             title: 'Store Out',
  //             route: 'admin.storeout.index'
  //           }
  //         ]
  //       }
  //     ]
  //   },
  //   {
  //     title: 'Settings',
  //     route: 'admin.settings.index',
  //     action: 'local_activity',
  //     childGroup: [
  //       {
  //         title: 'Setup',
  //         active: true,
  //         children: [
  //           {
  //             title: 'Service Type',
  //             route: 'admin.types.index'
  //           },
  //           {
  //             title: 'Services',
  //             route: 'admin.services.index'
  //           },
  //           {
  //             title: 'Department',
  //             route: 'admin.department.index'
  //           }
  //         ]
  //       },
  //       {
  //         title: 'User',
  //         children: [
  //           {
  //             title: 'User',
  //             route: 'admin.users.index'
  //           },
  //           {
  //             title: 'Role',
  //             route: 'admin.roles.index'
  //           },
  //           {
  //             title: 'Doctors',
  //             route: 'admin.doctors.index'
  //           },
  //           {
  //             title: 'Staff Types',
  //             route: 'admin.stafftypes.index'
  //           },
  //           {
  //             title: 'Staff',
  //             route: 'admin.staff.index'
  //           }
  //         ]
  //       }
  //     ]
  //   }
  // ],
  admin: [
    {
      title: 'Dashboard',
      route: 'admin.dashboard.index',
      crud: ['index']
    },
    {
      title: 'Hospitals',
      route: 'admin.hospital.index'
    },
    {
      title: 'Accounts',
      route: 'admin.account.index',
      crud: ['index'],
      action: 'local_activity',
      childGroup: [
        {
          title: 'Income',
          active: true,
          children: [
            {
              title: 'Income',
              route: 'admin.incomes.index',
              crud: ['index', 'create', 'edit', 'delete']
            },
            {
              title: 'Income Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.incometypes.index'
            }
          ]
        },
        {
          title: 'Expenses',
          children: [
            {
              title: 'Expense',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.expenditures.index'
            },
            {
              title: 'Expense Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.expendituretypes.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Patients',
      crud: ['index', 'create', 'edit', 'delete', 'bill','search'],
      route: 'admin.patients.index'
    },
    {
      title: 'Downloads',
      crud: ['index'],
      route: 'admin.downloads.index'
    },
    {
      title: 'Store',
      action: 'local_activity',
      childGroup: [
        {
          title: 'Setup',
          children: [
            {
              title: 'Product',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.product.index'
            },
            {
              title: 'Product Category',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.productCategory.index'
            }
          ]
        },
        {
          title: 'Store',
          active: true,
          children: [
            {
              title: 'Store In',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.product.storein.index'
            },
            {
              title: 'Store Out',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.product.storeout.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Pharmacy',
      action: 'local_activity',
      childGroup: [
        {
          title: 'Store',
          active: true,
          children: [
            {
              title: 'Store In',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.storein.index'
            },
            {
              title: 'Store Out',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.storeout.index'
            },
            {
              title: 'Medicine',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.medicine.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Request',
      crud: ['index', 'create', 'edit', 'delete'],
      route: 'admin.product.request.index'

    },
    {
      title: 'Settings',
      crud: ['index'],
      route: 'admin.settings.index',
      action: 'local_activity',
      childGroup: [
        {
          title: 'Setup',
          active: true,
          children: [
            {
              title: 'Service Type',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.types.index'
            },
            {
              title: 'Services',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.services.index'
            },
            {
              title: 'Department',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.department.index'
            }
          ]
        },
        {
          title: 'User',
          children: [
            {
              title: 'User',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.users.index'
            },
            {
              title: 'Doctors',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.doctors.index'
            },
            {
              title: 'Staff Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.stafftypes.index'
            },
            {
              title: 'Staff',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'admin.staff.index'
            },
            {
              title: 'Role',
              route: 'admin.roles.index',
              crud: ['index', 'edit']
            }
          ]
        }
      ]
    },
    {
      title: 'Profile',
      route: 'admin.profile.index',
      crud: ['index']
    }
  ],
  hospital: [
    {
      title: 'Dashboard',
      route: 'hospital.dashboard.index',
      crud: ['index']
    },
    {
      title: 'Accounts',
      route: 'hospital.account.index',
      crud: ['index'],
      action: 'local_activity',
      childGroup: [
        {
          title: 'Income',
          active: true,
          children: [
            {
              title: 'Income',
              route: 'hospital.incomes.index',
              crud: ['index', 'create', 'edit', 'delete']
            },
            {
              title: 'Income Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.incometypes.index'
            }
          ]
        },
        {
          title: 'Expenses',
          children: [
            {
              title: 'Expense',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.expenditures.index'
            },
            {
              title: 'Expense Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.expendituretypes.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Patients',
      crud: ['index', 'create', 'edit', 'delete', 'bill', 'search'],
      route: 'hospital.patients.index'
    },
    {
      title: 'Downloads',
      crud: ['index'],
      route: 'hospital.downloads.index'
    },
    {
      title: 'Store',
      action: 'local_activity',
      childGroup: [
        {
          title: 'Setup',
          children: [
            {
              title: 'Product',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.product.index'
            },
            {
              title: 'Product Category',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.productCategory.index'
            }
          ]
        },
        {
          title: 'Store',
          active: true,
          children: [
            {
              title: 'Store In',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.product.storein.index'
            },
            {
              title: 'Store Out',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.product.storeout.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Pharmacy',
      action: 'local_activity',
      childGroup: [
        {
          title: 'Store',
          active: true,
          children: [
            {
              title: 'Store In',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.storein.index'
            },
            {
              title: 'Store Out',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.storeout.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Settings',
      crud: ['index'],
      route: 'hospital.settings.index',
      action: 'local_activity',
      childGroup: [
        {
          title: 'Setup',
          active: true,
          children: [
            {
              title: 'Service Type',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.types.index'
            },
            {
              title: 'Services',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.services.index'
            },
            {
              title: 'Department',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.department.index'
            }
          ]
        },
        {
          title: 'User',
          children: [
            {
              title: 'User',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.users.index'
            },
            {
              title: 'Doctors',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.doctors.index'
            },
            {
              title: 'Staff Types',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.stafftypes.index'
            },
            {
              title: 'Staff',
              crud: ['index', 'create', 'edit', 'delete'],
              route: 'hospital.staff.index'
            }
          ]
        }
      ]
    },
    {
      title: 'Profile',
      route: 'hospital.profile.index',
      crud: ['index']
    }
  ]
}
