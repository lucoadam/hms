import Vuex from 'vuex'
import Vue from 'vue'
import core from './core'
import patient from './patient'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    core,
    patient
  },
  plugins: [createPersistedState({ storage: window.sessionStorage })]
})
