const state = {
  patients: []
}

const actions = {
  // The first argument is the vuex store, but we're using only the
  // dispatch function, which applies a mutation to the store,
  // and the current state of the stor
  loadPatient ({ commit }, param) {
    commit('updatePatients', param)
  }
}

const mutations = {
  clearAllPatients: function (state) {
    state.patients = []
  },

  updatePatients: function (state, patient) {
    state.patients = patient
  }
}

const getters = {}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
