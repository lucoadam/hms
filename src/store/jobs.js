import JobsApi from '../api/jobs'

const state = {
  jobs: [
    {
      title: 'Generate Slip Job',
      name: 'generateSlipJob',
      params: {},
      meta: {
        centre_based_entry: 'true'
      },
      progress: null,
      status: null,
      id: null,
      started: null,
      ended: null,
      percentage: null
    },

    {
      title: 'Populate Symbol Number Order By Name',
      name: 'generateSymbolNumberJob',
      params: { 'sortBy': 'name' },
      meta: {},
      progress: null,
      status: null,
      id: null,
      started: null,
      ended: null,
      percentage: null
    },

    {
      title: 'Populate Symbol Number Order By Id',
      name: 'generateSymbolNumberJob',
      params: { 'sortBy': 'id' },
      meta: {},
      progress: null,
      status: null,
      id: null,
      started: null,
      ended: null,
      percentage: null
    }
  ]
}

const actions = {
  // The first argument is the vuex store, but we're using only the
  // dispatch function, which applies a mutation to the store,
  // and the current state of the store
  loadJobsInfo ({ commit }) {
    JobsApi.getJobInfo().then((res) => {
      commit('updateFromJobInfo', res.body.jobInfos)
    })
  }
}

const mutations = {
  updateFromJobInfo (state, payload) {
    let mapped = {}
    for (let key in payload) {
      let obj = payload[key]
      mapped[ obj.jobName + (obj.sortBy ? obj.sortBy : '') ] = obj
    }
    console.log(mapped)
    state.jobs.forEach((item) => {
      let obj = mapped[item.name + (item.params.sortBy ? item.params.sortBy : '') ]
      console.log(obj, item.name + item.params.sortBy)
      if (obj) {
        item.status = obj.status
        item.total = obj.total
        item.processed = obj.processed
        item.started = obj.started
        item.ended = obj.end
        item.percentage = obj.completedPercentage
      }
    })
  }
}

const getters = {
  jobs (state, getters, rootState) {
    return state.jobs.filter((item) => {
      console.log('Hello', item)
      for (let key in item.meta) {
        console.log('Another Hello', key, rootState.core.metas.centre_based_entry)
        if (item.meta[key] && rootState.core.metas[key] !== item.meta[key]) { return false }
      }
      return true
    })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}



// WEBPACK FOOTER //
// ./src/store/jobs.js