import dateConvertor from 'ad-bs-converter'
import conDate from './date'
export default {
  numWords (num) {
    if ((num = num.toString()).length > 9) {
      throw new Error('overflow') // Does not support converting more than 9 digits yet
    }

    const regex = /^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/
    const n = ('000000000' + num).substr(-9).match(regex)
    if (!n) return

    let str = ''
    const a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen ']
    const b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety']

    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : ''
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : ''
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : ''
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : ''
    str += (n[5] != 0) ? ((str != '') ? 'And ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : ''

    return str.trim()
  },
  formatDate (date = false) {
    var d = date ? new Date(date) : new Date()
    var month = '' + (d.getMonth() + 1)
    var day = '' + d.getDate()
    var year = d.getFullYear()

    if (month.length < 2) { month = '0' + month }
    if (day.length < 2) { day = '0' + day }

    return this.dateADtoBS([year, month, day].join('/'))
  },
  sleep (ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
  },

  groupBy (xs, key) {
    return xs.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x)
      return rv
    }, {})
  },

  validateEmail (email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
  },

  dateWiseLock (date) {
    if (!date) return false
    let adVersion = dateConvertor.bs2ad(date)
    return (new Date(adVersion)) < (new Date())
  },

  dateConversion (bsDOB) {
    if (bsDOB && bsDOB.length === 10) {
      console.log(bsDOB)
      let converted = dateConvertor.bs2ad(bsDOB)
      return converted.year + '-' + (converted.month.toString().length === 1 ? '0' + converted.month.toString() : converted.month.toString()) + '-' + (converted.day.toString().length === 1 ? '0' + converted.day.toString() : converted.day.toString())
    }
  },
  dateADtoBS (bsDOB) {
    if (bsDOB && bsDOB.length === 10) {
      let converted = dateConvertor.ad2bs(bsDOB)
      return converted.en.year + '/' + (converted.en.month.toString().length === 1 ? '0' + converted.en.month.toString() : converted.en.month.toString()) + '/' + (converted.en.day.toString().length === 1 ? '0' + converted.en.day.toString() : converted.en.day.toString())
    }
  },
  formatDateComplete (date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear()
    if (month.length < 2) {
      month = '0' + month
    }
    if (day.length < 2) {
      day = '0' + day
    }
    var converted = [year, month, day].join('/')
    return conDate[converted] !== '' ? conDate[converted] : [year, month, day].join('/')
  },
  convertDate(date){
    return conDate[date]
  }
}
