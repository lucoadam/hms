// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import Vuetify from 'vuetify'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import './assets/style.css'
import 'vuetify/dist/vuetify.min.css'
import VeeValidate from 'vee-validate'
import Auth from '@websanova/vue-auth'
import Bus from './Bus'
import Utils from './utils'
import Mixin from './mixins'
import store from './store/index'
import Validators from './validators'
import auth from '@websanova/vue-auth/drivers/auth/bearer'
import authHttp from '@websanova/vue-auth/drivers/http/vue-resource.1.x.js'
import authRouter from './authRouter.js'
import VueHotkey from 'v-hotkey'

VeeValidate.Validator.extend('school_telephone', Validators.school_telephone)
VeeValidate.Validator.extend('hospital_telephone', Validators.school_telephone)
VeeValidate.Validator.extend('staff_telephone', Validators.school_telephone)
VeeValidate.Validator.extend('doctor_telephone', Validators.school_telephone)
VeeValidate.Validator.extend('admin_telephone', Validators.school_telephone)
VeeValidate.Validator.extend('symbol_number', Validators.symbol_number)

Vue.use(Vuetify)
Vue.use(VueResource)
Vue.use(VeeValidate)
Vue.use(VueHotkey)
Vue.router = router

Vue.mixin(Mixin)

Vue.use(Auth, {
  auth: {

    request: function (req, token) {
      this.options.http._setHeaders.call(this, req, {Authorization: 'Bearer ' + token});
    },

    response: function (res) {
      var headers = this.options.http._getHeaders.call(this, res),
        token = headers.Authorization || headers.authorization
      console.log(headers,token,'demo')
      if (token) {
        token = token.split(/Bearer\:?\s?/i)

        return token[token.length > 1 ? 1 : 0].trim()
      }

      if ('token' in res.data) {
        return res.data.token
      }
    }
  },
  http: authHttp,
  router: authRouter,
  parseUserData: (data) => {
    Utils.sleep(100).then(() => {
      Bus.$emit('user.fetched', data)
      Bus.$emit('load.metas')
    })
    return data
  },
  refreshData: {
    url: 'v1/refresh',
    method: 'GET',
    enabled: true,
    interval: 9,
    error: (err) => {
      Bus.$emit('error.on.refresh', err)
    }
  },
  impersonateData: {
    url: 'v1/auth/impersonate',
    method: 'POST'
  },

  unimpersonateData: {
    url: 'v1/auth/unimpersonate',
    method: 'POST',
    redirect: '/admin/dashboard',
    makeRequest: false
  },
  rolesVar: 'role',
  tokenStore: ['localStorage', 'cookie']
})
Vue.http.options.root = '/api'

Vue.config.productionTip = false
Vue.prototype.$bus = Bus

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  store: store,
  data () {
    return {
      goDark: true
    }
  },
  created () {
    console.log('created')
  },
  template: '<App/>',
  components: { App }
})
