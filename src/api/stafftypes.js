import Vue from 'vue'
export default {
  index () {
    return Vue.http.get('v1/stafftype')
  },

  save (stafftype) {
    return Vue.http.post('v1/stafftype/', stafftype)
  },

  update (stafftype) {
    return Vue.http.put('v1/stafftype/' + stafftype.id + '/', stafftype)
  },

  delete (stafftype, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/stafftype/' + stafftype + '/' + rmk)
  },
  findById (stafftype) {
    return Vue.http.get('v1/stafftype/' + stafftype)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/stafftype/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/stafftype/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }

}
