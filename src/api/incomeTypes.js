import Vue from 'vue';
export default {
  index(){
    return Vue.http.get('v1/incometype');
  },
  save (incometype) {
    return Vue.http.post('v1/incometype/', incometype)
  },
  update (incometype) {
    return Vue.http.put('v1/incometype/' + incometype.id+'/', incometype)
  },

  delete (incometype, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/incometype/' + incometype + '/' + rmk)
  },

  findById (incometype) {
    return Vue.http.get('v1/incometype/' + incometype )
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/incometype/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/incometype/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }

}
