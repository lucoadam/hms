import Vue from 'vue'

export default {
  index (query) {
    if (!query) {
      return Vue.http.get('/api/v1/hospital/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('/api/v1/hospital/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  },

  save (hospital) {
    return Vue.http.post('/api/v1/hospital/', hospital)
  },

  update (hospital) {
    return Vue.http.put('/api/v1/hospital/' + hospital.id + '/', hospital)
  },

  delete (hospital, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/hospital/' + hospital + '/' + rmk)
  },

  findById (hospitalId) {
    return Vue.http.get('/api/v1/hospital/' + hospitalId)
  },
  updateProfile (data) {
    return Vue.http.post('v1/hospital/profile', data)
  },

  getProfile () {
    return Vue.http.get('v1/hospital/profile/')
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/hospital/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/hospital/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }

}
