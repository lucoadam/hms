import Vue from 'vue'
export default {
  index () {
    return Vue.http.get('v1/departments/')
  },

  save (departments) {
    return Vue.http.post('v1/departments/', departments)
  },

  update (departments) {
    return Vue.http.put('v1/departments/' + departments.id + '/', departments)
  },

  delete (departments, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/departments/' + departments + '/' + rmk)
  },
  findById (departments) {
    return Vue.http.get('v1/departments/' + departments+'/')
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/departments/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/departments/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }

}
