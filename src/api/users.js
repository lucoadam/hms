import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/userview/users')
  },
  save (user) {
    return Vue.http.post('v1/userview/', user)
  },

  changePassword (data) {
    return Vue.http.put('v1/userview/' + data.id + '/', data)
  },

  delete (userview, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/userview/' + userview + '/' + rmk)
  },

  role (query = null) {
    if (!query) {
      return Vue.http.get('v1/roles/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/roles/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  },
  updateRoleById (role) {
    return Vue.http.put('v1/roles/' + role.id + '/', role)
  },
  findRoleById (role) {
    return Vue.http.get('v1/roles/' + role + '/')
  },
  findById (user) {
    return Vue.http.get('v1/userview/' + user)
  },

  findAllPermissions () {
    return Vue.http.get('v1/admin/users/permissions')
  },

  checkUsernameExistsOrNot (username) {
    return Vue.http.get('v1/admin/users/username-exists-or-not', { params: { username } })
  }
}
