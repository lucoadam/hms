import Vue from 'vue'
export default {
  index () {
    return Vue.http.get('v1/product-category')
  },

  save (productCategory) {
    return Vue.http.post('v1/product-category/', productCategory)
  },

  update (productCategory) {
    return Vue.http.put('v1/product-category/' + productCategory.id + '/', productCategory)
  },

  delete (productcategory, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/product-category/' + productcategory + '/' + rmk)
  },

  findById (productCategory) {
    return Vue.http.get('v1/product-category/' + productCategory)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/product-category/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/product-category/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }

}
