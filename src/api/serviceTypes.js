import Vue from 'vue';
export default {
  index(){
    return Vue.http.get('v1/type');
  },
  save (type) {
    return Vue.http.post('v1/type/', type)
  },
  update (type) {
    return Vue.http.put('v1/type/' + type.id+'/', type)
  },

  delete (type, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/type/' + type + '/' + rmk)
  },

  findById (type) {
    return Vue.http.get('v1/type/' + type )
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/type/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/type/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }

}
