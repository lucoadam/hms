import Vue from 'vue'

export default {
  index (query = null) {
    if (!query) {
      return Vue.http.get('v1/patients/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/patients/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  },
  search (patient) {
    return Vue.http.get('v1/psearch/?search=' + patient)
  },
  publicIndex () {
    return Vue.http.get('v1/public/patients')
  },
  update (patient) {
    return Vue.http.put('v1/patients/' + patient.id + '/', patient)
  },

  save (patient) {
    return Vue.http.post('v1/patients/', patient)
  },

  delete (patientId, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/patients/' + patientId + '/' + rmk)
  },

  findById (patientId) {
    return Vue.http.get('v1/patients/' + patientId)
  },

  age () {
    return Vue.http.get('v1/patients/age')
  },
  gender () {
    return Vue.http.get('v1/patients/gender')
  },
  servicebillamount () {
    return Vue.http.get('v1/patients/servicebillamount')
  },
  userbillamount () {
    return Vue.http.get('v1/patients/userbillamount')
  }
}
