import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/productservice')
  },

  publicIndex () {
    return Vue.http.get('v1/public/productservice')
  },
  update (productservice) {
    return Vue.http.put('v1/productservice/' + productservice.id + '/', productservice)
  },

  save (productservice) {
    return Vue.http.post('v1/productservice/', productservice)
  },

  delete (productservice, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/productservice/' + productservice + '/' + rmk)
  },

  findById (productserviceId) {
    return Vue.http.get('v1/productservice/' + productserviceId)
  }
}
