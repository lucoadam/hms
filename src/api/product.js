import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/product')
  },

  publicIndex () {
    return Vue.http.get('v1/public/product')
  },
  update (product) {
    return Vue.http.put('v1/product/'+product.id+'/', product)
  },

  save (product) {
    return Vue.http.post('v1/product/', product)
  },

  delete (product, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/product/' + product + '/' + rmk)
  },

  findById (productId) {
    return Vue.http.get('v1/product/' + productId)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/product/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/product/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
