import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/admin/userdetails')
  },

  publicIndex () {
    return Vue.http.get('v1/public/userdetails')
  },


  save (userdetail) {
    return Vue.http.post('v1/userdetails', userdetail)
  },

  delete (userdetails, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/userdetails/' + userdetails + '/' + rmk)
  },

  findById (userdetailId) {
    return Vue.http.get('v1/userdetails/' + userdetailId)
  }
}



// WEBPACK FOOTER //
// ./src/notices.js
