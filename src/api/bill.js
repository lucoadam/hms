import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/bill')
  },
  publicIndex () {
    return Vue.http.get('v1/public/bill')
  },
  update (patient) {
    return Vue.http.put('v1/bill/' + patient.id + '/', patient)
  },

  save (patient) {
    return Vue.http.post('v1/bill/', patient)
  },

  delete (patientId, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/bill/' + patientId + '/' + rmk)
  },

  findById (patientId) {
    return Vue.http.get('v1/bill/' + patientId + '/')
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/bill/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/bill/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
