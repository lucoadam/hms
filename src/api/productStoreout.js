import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/product-storeout')
  },

  publicIndex () {
    return Vue.http.get('v1/public/product-storeout')
  },
  update (product) {
    return Vue.http.put('v1/product-storeout/'+product.id+'/', product)
  },

  save (product) {
    return Vue.http.post('v1/product-storeout/', product)
  },

  delete (productstoreout, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/product-storeout/' + productstoreout + '/' + rmk)
  },

  findById (productId) {
    return Vue.http.get('v1/product-storeout/' + productId)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/product-storeout/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/product-storeout/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
