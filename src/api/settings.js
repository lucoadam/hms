import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/settings')
  },

  save (metas) {
    return Vue.http.post('v1/settings', metas)
  }
}



// WEBPACK FOOTER //
// ./src/settings.js
