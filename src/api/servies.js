import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/disciplines')
  },

  save (discipline) {
    return Vue.http.post('v1/disciplines', discipline)
  },

  delete (disciplines, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/disciplines/' + disciplines + '/' + rmk)
  },

  findById (discipline) {
    return Vue.http.get('v1/disciplines/' + discipline)
  }
}
