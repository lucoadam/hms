import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/storeout/')
  },

  publicIndex () {
    return Vue.http.get('v1/public/storeout')
  },
  update (storeout) {
    return Vue.http.put('v1/storeout/' + storeout.id + '/', storeout)
  },

  save (storeout) {
    return Vue.http.post('v1/storeout/', storeout)
  },

  delete (storeout, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/storeout/' + storeout + '/' + rmk)
  },
  findById (storeoutId) {
    return Vue.http.get('v1/storeout/' + storeoutId + '/')
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/storeout/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/storeout/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
