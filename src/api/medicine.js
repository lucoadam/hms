import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/medicine/')
  },

  publicIndex () {
    return Vue.http.get('v1/public/medicine')
  },
  update (medicine) {
    return Vue.http.put('v1/medicine/' + medicine.id + '/', medicine)
  },

  save (medicine) {
    return Vue.http.post('v1/medicine/', medicine)
  },

  delete (medicine, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/medicine/' + medicine + '/' + rmk)
  },
  findById (medicineId) {
    return Vue.http.get('v1/medicine/' + medicineId + '/')
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/medicine/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/medicine/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
