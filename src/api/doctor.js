import Vue from 'vue'

export default {
  index() {
    return Vue.http.get('v1/doctors/')
  },

  publicIndex() {
    return Vue.http.get('v1/public/doctors')
  },

  save(doctor) {
    return Vue.http.post('v1/doctors/', doctor)
  },

  update(doctor) {
    return Vue.http.put('v1/doctors/' + doctor.id + '/', doctor)
  },

  delete (doctors, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/doctors/' + doctors + '/' + rmk)
  },

  findById(doctorId) {
    return Vue.http.get('v1/doctors/' + doctorId + '/')
  },

  updateProfile(data) {
    return Vue.http.post('v1/doctors/profile', data)
  },

  getProfile() {
    return Vue.http.get('v1/doctors/profile/')
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/doctors/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/doctors/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
