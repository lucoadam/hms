import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/product-storein')
  },

  publicIndex () {
    return Vue.http.get('v1/public/product-storein')
  },
  update (product) {
    return Vue.http.put('v1/product-storein/'+product.id+'/', product)
  },

  save (product) {
    return Vue.http.post('v1/product-storein/', product)
  },

  delete (productstorein, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/product-storein/' + productstorein + '/' + rmk)
  },

  findById (productId) {
    return Vue.http.get('v1/product-storein/' + productId)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/product-storein/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/product-storein/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
