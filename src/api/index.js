import localforage from 'localforage'

localforage.config({
  driver: localforage.WEBSQL, // Force WebSQL; same as using setDriver()
  name: 'myApp',
  version: 1.0,
  size: 100000000, // Size of database, in bytes. WebSQL-only for now.
  storeName: 'localDatabase', // Should be alphanumeric, with underscores.
  description: 'some description'
})
const  localStore = localforage.createInstance();
export default localStore
