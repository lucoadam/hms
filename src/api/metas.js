import Vue from 'vue'
import localStore from './index'

export default {
  index () {
    localStore.getItem('metas')
      .then(value => {
        if (!value) {
          Vue.http.get('v1/metas/')
            // eslint-disable-next-line handle-callback-err
            .catch(err => {
              Vue.http.options.root = '/api'
            }).then(res => {
              localStore.setItem('metas', res.body).then(function () {
                return localStore.getItem('metas')
              })
                .then((value) => {
                  // we got our value
                  console.log(value, 'my meta values')
                  // eslint-disable-next-line handle-callback-err
                }).catch((err) => {
                // we got an error
                })
            })
        }
      })
      .catch(err => {
        console.log(err, 'met data not found')
      })
    // const metData = 1
    // if (metData) {
    //   return localStore.getItem('metas')
    // }
    return Vue.http.get('v1/metas/')
      // eslint-disable-next-line handle-callback-err
      .catch(err => {
        Vue.http.options.root = '/api'
      })
  },
  save (value) {
    return Vue.http.put('v1/metas/' + value.id + '/', value)
  }
}
