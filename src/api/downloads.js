import Vue from 'vue'
import jsPDF from 'jspdf'
import 'jspdf-autotable'

export default {
  downloadDashReport (parms = {}) {
    let newParams = parms
    return Vue.http.get('v1/patients/userbillamount/?from=' + newParams.fromDate + ';to=' + newParams.toDate + ';user=' + newParams.user)
  },
  downloadUserAccountReport (parms = {}) {
    let newParams = parms
    let name = parms.dtype.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
      return word.toUpperCase()
    }).replace(/\s+/g, ' ')

    // for (let kk in parms) {
    //   if (parms[kk]) { newParams[kk] = parms[kk] }
    // }
    Vue.http.get('v1/income/incomeexpensedetials/?dtype=' + newParams.dtype + ';from=' + newParams.fromDate + ';to=' + newParams.toDate + ';member=' + newParams.user + ';type=' + newParams.type)
      .then(res => {
        // eslint-disable-next-line new-cap
        var doc = new jsPDF()
        var data = res.body
        let dates = null
        let rows = []
        var total = 0
        let netTotal = 0
        let columnHeader = [
          'Date',
          name,
          'Amount(Rs)'
        ]
        doc.text('USER ' + parms.dtype.toUpperCase() + ' DETAILS', 20, 20)
        let length = 30
        let pdfName =
          'User' + parms.dtype + newParams.fromDate + '-' + newParams.toDate
        data.user.forEach(element => {
          netTotal = 0
          doc.setFontSize(9)
          if (length !== 30) {
            length = 30
            doc.addPage()
          }
          doc.text('User Name: ' + element, 20, length)
          // rows.push([
          //   {
          //     content: "User's name: " + element,
          //     colSpan: columnHeader.length,
          //     styles: { halign: 'center' }
          //   }
          // ])
          dates = Object.keys(data.data[element])
          dates.forEach(each => {
            // rows.push([
            //   {
            //     content: 'Date: ' + each,
            //     colSpan: columnHeader.length
            //   }
            // ])
            total = 0
            data.data[element][each].forEach(item => {
              total += item.amount
              netTotal += item.amount
              rows.push([
                each,
                item.type,
                item.amount
              ])
            })
            rows.push([
              {
                content: 'Total (Rs.)',
                colSpan: columnHeader.length - 1,
                styles: { halign: 'right' }
              },
              total
            ])
          })
          rows.push([
            {
              content: 'Net Total of '+parms.dtype+' amount for the range',
              colSpan: columnHeader.length - 1,
              styles: { halign: 'right' }
            },
            'Rs.' + netTotal
          ])
          length += 3
          doc.autoTable(columnHeader, rows, {
            startY: length
          })
          rows = []
        })
        doc.save(pdfName + '.pdf')
      })
  },
  downloadUserServiceReport (parms = {}) {
    let newParams = parms

    // for (let kk in parms) {
    //   if (parms[kk]) { newParams[kk] = parms[kk] }
    // }
    Vue.http.get('v1/patients/userserviceamountdetails/?from=' + newParams.fromDate + ';to=' + newParams.toDate + ';user=' + newParams.user + ';service=' + newParams.servicetype)
      .then(res => {
        // eslint-disable-next-line new-cap
        var doc = new jsPDF()
        var data = res.body
        let dates = null
        let rows = []
        var total = 0
        let netTotal = 0
        let columnHeader = [
          'Date',
          'Service',
          'Amount(Rs)'
        ]
        doc.text('User Service Details', 20, 20)
        let length = 30
        let pdfName =
          'UserServiceAmount' + newParams.fromDate + '-' + newParams.toDate
        data.user.forEach(element => {
          netTotal = 0
          doc.setFontSize(9)
          if (length !== 30) {
            length = 30
            doc.addPage()
          }
          doc.text('User Name: ' + element, 20, length)
          // rows.push([
          //   {
          //     content: "User's name: " + element,
          //     colSpan: columnHeader.length,
          //     styles: { halign: 'center' }
          //   }
          // ])
          dates = Object.keys(data.bill[element])
          dates.forEach(each => {
            // rows.push([
            //   {
            //     content: 'Date: ' + each,
            //     colSpan: columnHeader.length
            //   }
            // ])
            total = 0
            data.bill[element][each].forEach(item => {
              total += item.amount
              netTotal += item.amount
              rows.push([
                each,
                item.type,
                item.amount
              ])
            })
            rows.push([
              {
                content: 'Total (Rs.)',
                colSpan: columnHeader.length - 1,
                styles: { halign: 'right' }
              },
              total
            ])
          })
          rows.push([
            {
              content: 'Net Total of services amount for the range',
              colSpan: columnHeader.length - 1,
              styles: { halign: 'right' }
            },
            'Rs.' + netTotal
          ])
          length += 3
          doc.autoTable(columnHeader, rows, {
            startY: length
          })
          rows = []
        })
        doc.save(pdfName + '.pdf')
      })
  }

}
