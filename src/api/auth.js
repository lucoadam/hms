import Vue from 'vue'

export default {
  changePassword (data) {
    return Vue.http.put('v1/auth/change-password', data)
  }
}
