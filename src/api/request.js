import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/product-request')
  },

  publicIndex () {
    return Vue.http.get('v1/public/product-request')
  },
  update (product) {
    return Vue.http.put('v1/product-request/'+product.id+'/', product)
  },

  save (product) {
    return Vue.http.post('v1/product-request/', product)
  },

  delete (productrequest, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/product-request/' + productrequest + '/' + rmk)
  },

  findById (productId) {
    return Vue.http.get('v1/product-request/' + productId)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/product-request/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/product-request/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
