import Vue from 'vue'

export default {
  searchStudent (identifier) {
    return Vue.http.get('v1/admin/students/search', { params: { identifier } })
  },
  index (query) {
    return Vue.http.post('v1/school/students/index', query)
  },

  save (student) {
    return Vue.http.post('v1/school/students', student)
  },

  delete (student) {
    return Vue.http.delete('v1/school/students/' + student)
  },

  findById (student) {
    return Vue.http.get('v1/school/students/' + student)
  },

  history (student) {
    return Vue.http.get(`v1/school/students/${student}/history`)
  },

  generateNewSymbolNo (discipline) {
    return Vue.http.get('v1/school/students/generate-new-symbol-no', { params: { discipline } })
  },

  generateNewRegNo (discipline) {
    return Vue.http.get('v1/school/students/generate-new-reg-no', { params: { discipline } })
  },

  getFirstRegNo () {
    return Vue.http.get('v1/school/students/get-first-regno')
  },

  checkSymbolNumberExistsOrNot (symbolNo, regNo) {
    return Vue.http.get('v1/school/students/symbol-exists-or-not', { params: { symbolNo, regNo } })
  },

  failList () {
    return Vue.http.get('v1/school/students/fail-list')
  },

  updateShsList (shsList) {
    return Vue.http.post('v1/school/students/update-shs-list', shsList)
  }
}



// WEBPACK FOOTER //
// ./src/students.js
