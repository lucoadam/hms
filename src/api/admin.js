import Vue from 'vue'

export default {
  dashboard () {
    return Vue.http.get('v1/admin/dashboard/')
  }
}
