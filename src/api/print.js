import Vue from 'vue'

export default {
  admintCard (query) {
    return Vue.http.post('v1/school/admit-card/print', query)
  }
}



// WEBPACK FOOTER //
// ./src/print.js
