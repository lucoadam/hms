import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/wards')
  },

  save (ward) {
    return Vue.http.post('v1/wards', ward)
  },

  delete (wards, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/wards/' + wards + '/' + rmk)
  },

  findById (wardId) {
    return Vue.http.get('v1/wards/' + wardId)
  }
}



// WEBPACK FOOTER //
// ./src/wards.js
