import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/storein')
  },

  publicIndex () {
    return Vue.http.get('v1/public/storein')
  },
  update (storein) {
    return Vue.http.put('v1/storein/'+storein.id+'/', storein)
  },

  save (storein) {
    return Vue.http.post('v1/storein/', storein)
  },

  delete (storein, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/storein/' + storein + '/' + rmk)
  },
  findById (storeinId) {
    return Vue.http.get('v1/storein/' + storeinId)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/storein/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/storein/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
