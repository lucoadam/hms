import Vue from 'vue'
export default {
  index () {
    return Vue.http.get('v1/expendituretype')
  },

  save (expendituretype) {
    return Vue.http.post('v1/expendituretype/', expendituretype)
  },

  update (expendituretype) {
    return Vue.http.put('v1/expendituretype/' + expendituretype.id + '/', expendituretype)
  },

  delete (expendituretype, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/expendituretype/' + expendituretype + '/' + rmk)
  },

  findById (expendituretype) {
    return Vue.http.get('v1/expendituretype/' + expendituretype)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/expendituretype/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/expendituretype/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }

}
