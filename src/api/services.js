import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/services')
  },

  publicIndex () {
    return Vue.http.get('v1/public/services')
  },
  update (services) {
    return Vue.http.put('v1/services/'+services.id+'/', services)
  },

  save (services) {
    return Vue.http.post('v1/services/', services)
  },

  delete (services, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/services/' + services + '/' + rmk)
  },

  findById (servicesId) {
    return Vue.http.get('v1/services/' + servicesId)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/services/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/services/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
