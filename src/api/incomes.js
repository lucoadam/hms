import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/income')
  },

  acconts (hospital = '') {
    hospital = (hospital === '') ? '' : '?hospital=' + hospital
    return Vue.http.get('v1/accounts/' + hospital)
  },
  publicIndex () {
    return Vue.http.get('v1/public/income')
  },
  update (income) {
    return Vue.http.put('v1/income/'+income.id+'/', income)
  },

  save (income) {
    return Vue.http.post('v1/income/', income)
  },

  delete (income, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/income/' + income + '/' + rmk)
  },

  findById (incomeId) {
    return Vue.http.get('v1/income/' + incomeId)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/income/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/income/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  },
  downloadAcconts (query) {
    return Vue.http.get('v1/downloadAccounts', query)
  }
}

// WEBPACK FOOTER //
// ./src/notices.js
