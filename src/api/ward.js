import Vue from 'vue';

export default {
  index (query) {
    return Vue.http.get('/api/v1/ward')
  },

  install(data){
    return Vue.http.post('/api/v1/ward/install/', data)
  },

  save (ward) {
    return Vue.http.post('/api/v1/ward/', ward)
  },
   update (ward) {
    return Vue.http.put('/api/v1/ward/'+ward.id+'/', ward)
  },

  delete (ward, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/ward/' + ward + '/' + rmk)
  },


  findById (wardId) {
    return Vue.http.get('/api/v1/ward/' + wardId)
  },

}

