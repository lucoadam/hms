import Vue from 'vue'

export default {
  index() {
    return Vue.http.get('v1/staff/')
  },

  publicIndex() {
    return Vue.http.get('v1/public/staff')
  },

  save(staff) {
    return Vue.http.post('v1/staff/', staff)
  },

  update(staff) {
    return Vue.http.put('v1/staff/' + staff.id + '/', staff)
  },

  delete (staff, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/staff/' + staff + '/' + rmk)
  },

  findById(staffId) {
    return Vue.http.get('v1/staff/' + staffId + '/')
  },

  updateProfile(data) {
    return Vue.http.post('v1/staff/profile', data)
  },

  getProfile() {
    return Vue.http.get('v1/staff/profile/')
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/staff/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/staff/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
