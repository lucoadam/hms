import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/admin/notices')
  },

  publicIndex () {
    return Vue.http.get('v1/public/notices')
  },

  save (notice) {
    return Vue.http.post('v1/admin/notices', notice)
  },

  delete (admin, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/admin/notices/' + admin + '/' + rmk)
  },
  findById (noticeId) {
    return Vue.http.get('v1/admin/notices/' + noticeId)
  }
}
