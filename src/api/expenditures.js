import Vue from 'vue'

export default {
  index () {
    return Vue.http.get('v1/expenditure')
  },

  publicIndex () {
    return Vue.http.get('v1/public/expenditure')
  },

  update (expenditure) {
    return Vue.http.put('v1/expenditure/' + expenditure.id + '/', expenditure)
  },

  save (expenditure) {
    return Vue.http.post('v1/expenditure/', expenditure)
  },

  delete (expenditure, remark = '') {
    let rmk = remark === '' ? '' : '?remark=' + remark
    return Vue.http.delete('v1/expenditure/' + expenditure + '/' + rmk)
  },

  findById (expenditureId) {
    return Vue.http.get('v1/expenditure/' + expenditureId)
  },
  search (query = null) {
    if (!query) {
      return Vue.http.get('v1/expenditure/')
    } else {
      var search = query.search ? 'search=' + query.search : 'page=' + query.page
      search += '&ordering=' + (query.descending ? '-' : '') + query.sortBy
      return Vue.http.get('v1/expenditure/?' + search + '&page_size=' + query.rowsPerPage, query)
    }
  }
}
