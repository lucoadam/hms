import Vue from 'vue'
import Router from 'vue-router'

import adminRoutes from './admin'
import generalRoutes from './general'
import doctorRoutes from './doctor'
import cashierRoutes from './cashier'
import hospitalRoutes from './hospital'
import staffRoutes from './staff'

import store from '../store'

import ChangePassword from '../pages/auth/ChangePassword'

Vue.use(Router)
const router = new Router({
  routes: [

    ...adminRoutes,
    ...generalRoutes,
    ...cashierRoutes,
    ...doctorRoutes,
    ...hospitalRoutes,
    ...staffRoutes,

    {
      component: ChangePassword,
      meta: {
        auth: true
      },
      path: '/auth/change-password',
      name: 'auth.change-password'
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.middleware && to.meta.middleware.includes('changepassword')) {
    return changepassword(next, to)
  }

  routeAccess(next, to)
})
function comparer (otherArray) {
  return function (current) {
    return otherArray.filter(function (other) {
      return other.id === current.id
    }).length === 0
  }
}
function routeAccess (next, to) {
  console.log(navigator.onLine)
  // if (navigator.onLine) {
  //   Vue.http.options.root = 'https://hms.stsolutions.com.np/api'
  // } else {
  //   Vue.http.options.root = '/api'
  // }
  if (to.name.split('dashboard').length !== 2 && !routePermissionExists(to.name)) {
    return next({
      name: 'errors.403'
    })
  }
  next()
}

// TODO separate middleware in different file
function changepassword (next, to) {
  if (store.state.core.auth.user.forcePasswordChange && !localStorage.getItem('impersonate_auth_token')) {
    return next({
      name: 'auth.change-password'
    })
  }

  routeAccess(next, to)
}
function routePermissionExists (route) {
  let user = store.state.core.auth.user
  let d = route.split('.')[0]
  if (d !== 'errors' && d !== 'auth' && 'roleName' in user && route && user.roleName !== 'admin') {
    route = route.split(user.roleName + '.')[1]
    return route in user.permissions && user.permissions[route]
  } else {
    return true
  }
}
export default router
