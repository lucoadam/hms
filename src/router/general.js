import Login from '../pages/auth/Login'
import Error404 from '../pages/errors/404'
//
import Error403 from '../pages/errors/403'
import Dashboard from '../pages/Dashboard'
// import PublicNotice from '../pages/public/Notices'

let routes = [
  {
    path: '/login',
    name: 'auth.login',
    component: Login
  },

  {
    path: '/404',
    name: 'errors.404',
    component: Error404
  },

  {
    path: '/403',
    name: 'errors.403',
    component: Error403
  },

  {
    path: '',
    name: 'dashboard',
    component: Dashboard
  }

]

export default routes
