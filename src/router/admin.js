
import UserIndex from '../pages/admin/users/Index'
import RoleIndex from '../pages/admin/users/Role'
import PermissionIndex from '../pages/admin/users/Permissions'
import UserCreate from '../pages/admin/users/Create'
import UserEdit from '../pages/admin/users/ChangePassword'

import AdminDashboard from '../pages/admin/Dashboard'
import AccountIndex from '../pages/admin/accounts/Index'

import AdminSettings from '../pages/admin/settings/Index'

import ServiceIndex from '../pages/admin/services/Index'
import ServiceCreate from '../pages/admin/services/Create'
import ServiceEdit from '../pages/admin/services/Edit'

import DoctorIndex from '../pages/admin/doctors/Index'
import DoctorCreate from '../pages/admin/doctors/Create'
import DoctorEdit from '../pages/admin/doctors/Edit'

import StaffIndex from '../pages/admin/staff/Index'
import StaffCreate from '../pages/admin/staff/Create'
import StaffEdit from '../pages/admin/staff/Edit'

import StaffTypeIndex from '../pages/admin/stafftypes/Index'
import StaffTypeCreate from '../pages/admin/stafftypes/Create'
import StaffTypeEdit from '../pages/admin/stafftypes/Edit'

import PatientIndex from '../pages/admin/patients/Index'
import PatientCreate from '../pages/admin/patients/Create'
import PatientEdit from '../pages/admin/patients/Edit'
import PatientBill from '../pages/admin/patients/Bill'

import DownloadsIndex from '../pages/admin/downloads/Index'
import DownloadsCreate from '../pages/admin/downloads/Create'
import DownloadsEdit from '../pages/admin/downloads/Edit'
import DownloadsBill from '../pages/admin/downloads/Bill'

import TypeIndex from '../pages/admin/serviceTypes/Index'
import TypeCreate from '../pages/admin/serviceTypes/Create'
import TypeEdit from '../pages/admin/serviceTypes/Edit'

import IncomeTypeIndex from '../pages/admin/incomeTypes/Index'
import IncomeTypeCreate from '../pages/admin/incomeTypes/Create'
import IncomeTypeEdit from '../pages/admin/incomeTypes/Edit'

import ExpenditureTypeIndex from '../pages/admin/expenditureTypes/Index'
import ExpenditureTypeCreate from '../pages/admin/expenditureTypes/Create'
import ExpenditureTypeEdit from '../pages/admin/expenditureTypes/Edit'

import DepartmentIndex from '../pages/admin/department/Index'
import DepartmentCreate from '../pages/admin/department/Create'
import DepartmentEdit from '../pages/admin/department/Edit'

import HospitalIndex from '../pages/admin/hospital/Index'
import HospitalCreate from '../pages/admin/hospital/Create'
import HospitalEdit from '../pages/admin/hospital/Edit'

import Search from '../pages/admin/Search'

import IncomeIndex from '../pages/admin/incomes/Index'
import IncomeCreate from '../pages/admin/incomes/Create'
import IncomeEdit from '../pages/admin/incomes/Edit'

import ExpenditureIndex from '../pages/admin/expenditures/Index'
import ExpenditureCreate from '../pages/admin/expenditures/Create'
import ExpenditureEdit from '../pages/admin/expenditures/Edit'

import MedicineIndex from '../pages/admin/store/medicine/Index'
import MedicineCreate from '../pages/admin/store/medicine/Create'
import MedicineEdit from '../pages/admin/store/medicine/Edit'

import StoreInIndex from '../pages/admin/store/storein/Index'
import StoreInCreate from '../pages/admin/store/storein/Create'
import StoreInEdit from '../pages/admin/store/storein/Edit'

import StoreOutIndex from '../pages/admin/store/storeout/Index'
import StoreOutCreate from '../pages/admin/store/storeout/Create'
import StoreOutEdit from '../pages/admin/store/storeout/Edit'

import ProductIndex from '../pages/admin/products/product/Index'
import ProductCreate from '../pages/admin/products/product/Create'
import ProductEdit from '../pages/admin/products/product/Edit'

import productCategoryIndex from '../pages/admin/products/productCategory/Index'
import productCategoryCreate from '../pages/admin/products/productCategory/Create'
import productCategoryEdit from '../pages/admin/products/productCategory/Edit'

import ProductStoreInIndex from '../pages/admin/products/storein/Index'
import ProductStoreInCreate from '../pages/admin/products/storein/Create'
import ProductStoreInEdit from '../pages/admin/products/storein/Edit'

import ProductStoreOutIndex from '../pages/admin/products/storeout/Index'
import ProductStoreOutCreate from '../pages/admin/products/storeout/Create'
import ProductStoreOutEdit from '../pages/admin/products/storeout/Edit'

import ProductrequestIndex from '../pages/admin/products/request/Index'
import ProductrequestCreate from '../pages/admin/products/request/Create'
import ProductrequestEdit from '../pages/admin/products/request/Edit'

let routes = [
  {
    path: 'users',
    name: 'users.index',
    component: UserIndex
  },

  {
    path: 'roles',
    name: 'roles.index',
    component: RoleIndex
  },
  {
    path: 'roles/:id',
    name: 'roles.edit',
    component: PermissionIndex
  },
  {
    path: 'users/create',
    name: 'users.create',
    component: UserCreate
  },

  {
    path: 'users/:id/edit',
    name: 'users.edit',
    component: UserEdit
  },

  {
    path: 'dashboard',
    name: 'dashboard.index',
    component: AdminDashboard
  },

  {
    path: 'settings',
    name: 'settings.index',
    component: AdminSettings
  },

  {
    path: 'services',
    name: 'services.index',
    component: ServiceIndex
  },

  {
    path: 'services/create',
    name: 'services.create',
    component: ServiceCreate
  },

  {
    path: 'services/:id/edit',
    name: 'services.edit',
    component: ServiceEdit
  },
  {
    path: 'types',
    name: 'types.index',
    component: TypeIndex
  },

  {
    path: 'types/create',
    name: 'types.create',
    component: TypeCreate
  },

  {
    path: 'types/:id/edit',
    name: 'types.edit',
    component: TypeEdit
  },
  {
    path: 'incometype',
    name: 'incometypes.index',
    component: IncomeTypeIndex
  },

  {
    path: 'incometype/create',
    name: 'incometypes.create',
    component: IncomeTypeCreate
  },

  {
    path: 'incometype/:id/edit',
    name: 'incometypes.edit',
    component: IncomeTypeEdit
  },
  {
    path: 'expendituretype',
    name: 'expendituretypes.index',
    component: ExpenditureTypeIndex
  },

  {
    path: 'expendituretype/create',
    name: 'expendituretypes.create',
    component: ExpenditureTypeCreate
  },

  {
    path: 'expendituretype/:id/edit',
    name: 'expendituretypes.edit',
    component: ExpenditureTypeEdit
  },
  {
    path: 'department',
    name: 'department.index',
    component: DepartmentIndex
  },

  {
    path: 'department/create',
    name: 'department.create',
    component: DepartmentCreate
  },

  {
    path: 'department/:id/edit',
    name: 'department.edit',
    component: DepartmentEdit
  },

  {
    path: 'doctors',
    name: 'doctors.index',
    component: DoctorIndex
  },

  {
    path: 'doctors/create',
    name: 'doctors.create',
    component: DoctorCreate
  },

  {
    path: 'doctors/:id/edit',
    name: 'doctors.edit',
    component: DoctorEdit
  },
  {
    path: 'staff',
    name: 'staff.index',
    component: StaffIndex
  },

  {
    path: 'staff/create',
    name: 'staff.create',
    component: StaffCreate
  },

  {
    path: 'staff/:id/edit',
    name: 'staff.edit',
    component: StaffEdit
  },
  {
    path: 'stafftype',
    name: 'stafftypes.index',
    component: StaffTypeIndex
  },

  {
    path: 'stafftype/create',
    name: 'stafftypes.create',
    component: StaffTypeCreate
  },

  {
    path: 'stafftypes/:id/edit',
    name: 'stafftypes.edit',
    component: StaffTypeEdit
  },
  {
    path: 'patients',
    name: 'patients.index',
    component: PatientIndex
  },

  {
    path: 'patients/:id/bill',
    name: 'patients.bill',
    component: PatientBill
  },
  {
    path: 'patients/create',
    name: 'patients.create',
    component: PatientCreate
  },

  {
    path: 'patients/:id/bill/add',
    name: 'patients.edit',
    component: PatientEdit
  },
  {
    path: 'patient/:search',
    name: 'patients.search',
    component: Search
  },

  {
    path: 'downloads',
    name: 'downloads.index',
    component: DownloadsIndex
  },

  {
    path: 'downloads/:id/bill',
    name: 'downloads.bill',
    component: DownloadsBill
  },
  {
    path: 'downloads/create',
    name: 'downloads.create',
    component: DownloadsCreate
  },

  {
    path: 'downloads/:id/bill/add',
    name: 'downloads.edit',
    component: DownloadsEdit
  },
  {
    path: 'patient/:search',
    name: 'downloads.search',
    component: Search
  },
  {
    path: 'hospital',
    name: 'hospital.index',
    component: HospitalIndex
  },

  {
    path: 'hospital/create',
    name: 'hospital.create',
    component: HospitalCreate
  },

  {
    path: 'hospital/:id/edit',
    name: 'hospital.edit',
    component: HospitalEdit
  },
  {
    path: 'income',
    name: 'incomes.index',
    component: IncomeIndex
  },

  {
    path: 'income/create',
    name: 'incomes.create',
    component: IncomeCreate
  },

  {
    path: 'income/:id/edit',
    name: 'incomes.edit',
    component: IncomeEdit
  },
  {
    path: 'expenditure',
    name: 'expenditures.index',
    component: ExpenditureIndex
  },

  {
    path: 'expenditure/create',
    name: 'expenditures.create',
    component: ExpenditureCreate
  },

  {
    path: 'expenditure/:id/edit',
    name: 'expenditures.edit',
    component: ExpenditureEdit
  },
  {
    path: 'account',
    name: 'account.index',
    component: AccountIndex
  },
  {
    path: 'storein',
    name: 'storein.index',
    component: StoreInIndex
  },

  {
    path: 'storein/create',
    name: 'storein.create',
    component: StoreInCreate
  },

  {
    path: 'storein/:id/edit',
    name: 'storein.edit',
    component: StoreInEdit
  },
  {
    path: 'medicine',
    name: 'medicine.index',
    component: MedicineIndex
  },

  {
    path: 'medicine/create',
    name: 'medicine.create',
    component: MedicineCreate
  },

  {
    path: 'medicine/:id/edit',
    name: 'medicine.edit',
    component: MedicineEdit
  },
  {
    path: 'storeout',
    name: 'storeout.index',
    component: StoreOutIndex
  },

  {
    path: 'storeout/create',
    name: 'storeout.create',
    component: StoreOutCreate
  },

  {
    path: 'storeout/:id/edit',
    name: 'storeout.edit',
    component: StoreOutEdit
  },
  {
    path: 'product/storein',
    name: 'product.storein.index',
    component: ProductStoreInIndex
  },

  {
    path: 'product/storein/create',
    name: 'product.storein.create',
    component: ProductStoreInCreate
  },

  {
    path: 'product/storein/:id/edit',
    name: 'product.storein.edit',
    component: ProductStoreInEdit
  },
  {
    path: 'product',
    name: 'product.index',
    component: ProductIndex
  },

  {
    path: 'product/create',
    name: 'product.create',
    component: ProductCreate
  },

  {
    path: 'product/:id/edit',
    name: 'product.edit',
    component: ProductEdit
  },
  {
    path: 'productCategory',
    name: 'productCategory.index',
    component: productCategoryIndex
  },

  {
    path: 'productCategory/create',
    name: 'productCategory.create',
    component: productCategoryCreate
  },

  {
    path: 'productCategory/:id/edit',
    name: 'productCategory.edit',
    component: productCategoryEdit
  },
  {
    path: 'product/storeout',
    name: 'product.storeout.index',
    component: ProductStoreOutIndex
  },

  {
    path: 'product/storeout/create',
    name: 'product.storeout.create',
    component: ProductStoreOutCreate
  },

  {
    path: 'product/storeout/:id/edit',
    name: 'product.storeout.edit',
    component: ProductStoreOutEdit
  },
  {
    path: 'product/request',
    name: 'product.request.index',
    component: ProductrequestIndex
  },

  {
    path: 'product/request/create',
    name: 'product.request.create',
    component: ProductrequestCreate
  },

  {
    path: 'product/request/:id/edit',
    name: 'product.request.edit',
    component: ProductrequestEdit
  }

]

for (let index in routes) {
  routes[index].path = '/admin/' + routes[index].path
  routes[index].name = 'admin.' + routes[index].name
  routes[index].meta = { auth: ['ROLE_ADMIN'], middleware: ['changepassword'] }
}

export default routes
