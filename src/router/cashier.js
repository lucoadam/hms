
import UserIndex from '../pages/admin/users/Index'
import UserCreate from '../pages/admin/users/Create'
import UserEdit from '../pages/admin/users/ChangePassword'

import AdminDashboard from '../pages/admin/Dashboard'

import AdminSettings from '../pages/admin/settings/Index'

import ServiceIndex from '../pages/admin/services/Index'
import ServiceCreate from '../pages/admin/services/Create'
import ServiceEdit from '../pages/admin/services/Edit'

import DoctorIndex from '../pages/admin/doctors/Index'
import DoctorCreate from '../pages/admin/doctors/Create'
import DoctorEdit from '../pages/admin/doctors/Edit'

import PatientIndex from '../pages/admin/patients/Index'
import PatientCreate from '../pages/admin/patients/Create'
import PatientEdit from '../pages/admin/patients/Edit'
import PatientBill from '../pages/admin/patients/Bill'

import TypeIndex from '../pages/admin/serviceTypes/Index'
import TypeCreate from '../pages/admin/serviceTypes/Create'
import TypeEdit from '../pages/admin/serviceTypes/Edit'

import Search from '../pages/admin/Search'

let routes = [
  {
    path: 'users',
    name: 'users.index',
    component: UserIndex
  },

  {
    path: 'users/create',
    name: 'users.create',
    component: UserCreate
  },

  {
    path: 'users/:id/edit',
    name: 'users.edit',
    component: UserEdit
  },

  {
    path: 'dashboard',
    name: 'dashboard.index',
    component: AdminDashboard
  },

  {
    path: 'settings',
    name: 'settings',
    component: AdminSettings
  },

  {
    path: 'services',
    name: 'services.index',
    component: ServiceIndex
  },

  {
    path: 'services/create',
    name: 'services.create',
    component: ServiceCreate
  },

  {
    path: 'services/:id/edit',
    name: 'services.edit',
    component: ServiceEdit
  },
  {
    path: 'types',
    name: 'types.index',
    component: TypeIndex
  },

  {
    path: 'types/create',
    name: 'types.create',
    component: TypeCreate
  },

  {
    path: 'types/:id/edit',
    name: 'types.edit',
    component: TypeEdit
  },

  {
    path: 'doctors',
    name: 'doctors.index',
    component: DoctorIndex
  },

  {
    path: 'doctors/create',
    name: 'doctors.create',
    component: DoctorCreate
  },

  {
    path: 'doctors/:id/edit',
    name: 'doctors.edit',
    component: DoctorEdit
  },

  {
    path: 'patients',
    name: 'patients.index',
    component: PatientIndex
  },

  {
    path: 'patients/create',
    name: 'patients.create',
    component: PatientCreate
  },

  {
    path: 'patients/:id/edit',
    name: 'patients.edit',
    component: PatientEdit
  },
  {
    path: 'patients/:id/bill',
    name: 'patients.bill',
    component: PatientBill
  },

  {
    path: 'search/:search',
    name: 'search',
    component: Search
  }
]

for (let index in routes) {
  routes[index].path = '/cashier/' + routes[index].path
  routes[index].name = 'cashier.' + routes[index].name
  routes[index].meta = { auth: ['ROLE_CASHIER'], middleware: ['changepassword'] }
}

export default routes
