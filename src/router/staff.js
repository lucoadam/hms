
import UserIndex from '../pages/admin/users/Index'
import UserCreate from '../pages/admin/users/Create'
import UserEdit from '../pages/admin/users/ChangePassword'
import AccountIndex from '../pages/admin/accounts/Index'
import AdminDashboard from '../pages/admin/Dashboard'

import AdminSettings from '../pages/admin/settings/Index'

import ServiceIndex from '../pages/admin/services/Index'
import ServiceCreate from '../pages/admin/services/Create'
import ServiceEdit from '../pages/admin/services/Edit'

import DoctorIndex from '../pages/admin/doctors/Index'
import DoctorCreate from '../pages/admin/doctors/Create'
import DoctorEdit from '../pages/admin/doctors/Edit'

import StaffIndex from '../pages/admin/staff/Index'
import StaffCreate from '../pages/admin/staff/Create'
import StaffEdit from '../pages/admin/staff/Edit'

import StaffTypeIndex from '../pages/admin/stafftypes/Index'
import StaffTypeCreate from '../pages/admin/stafftypes/Create'
import StaffTypeEdit from '../pages/admin/stafftypes/Edit'

import PatientIndex from '../pages/admin/patients/Index'
import PatientCreate from '../pages/admin/patients/Create'
import PatientEdit from '../pages/admin/patients/Edit'
import PatientBill from '../pages/admin/patients/Bill'

import TypeIndex from '../pages/admin/serviceTypes/Index'
import TypeCreate from '../pages/admin/serviceTypes/Create'
import TypeEdit from '../pages/admin/serviceTypes/Edit'

import IncomeTypeIndex from '../pages/admin/incomeTypes/Index'
import IncomeTypeCreate from '../pages/admin/incomeTypes/Create'
import IncomeTypeEdit from '../pages/admin/incomeTypes/Edit'

import IncomeIndex from '../pages/admin/incomes/Index'
import IncomeCreate from '../pages/admin/incomes/Create'
import IncomeEdit from '../pages/admin/incomes/Edit'

import ExpenditureIndex from '../pages/admin/expenditures/Index'
import ExpenditureCreate from '../pages/admin/expenditures/Create'
import ExpenditureEdit from '../pages/admin/expenditures/Edit'

import ExpenditureTypeIndex from '../pages/admin/expenditureTypes/Index'
import ExpenditureTypeCreate from '../pages/admin/expenditureTypes/Create'
import ExpenditureTypeEdit from '../pages/admin/expenditureTypes/Edit'

import DepartmentIndex from '../pages/admin/department/Index'
import DepartmentCreate from '../pages/admin/department/Create'
import DepartmentEdit from '../pages/admin/department/Edit'

import HospitalIndex from '../pages/admin/hospital/Index'
import HospitalCreate from '../pages/admin/hospital/Create'
import HospitalEdit from '../pages/admin/hospital/Edit'
import HospitalProfile from '../pages/staff/Profile'

import StoreInIndex from '../pages/admin/store/storein/Index'
import StoreInCreate from '../pages/admin/store/storein/Create'
import StoreInEdit from '../pages/admin/store/storein/Edit'

import StoreOutIndex from '../pages/admin/store/storeout/Index'
import StoreOutCreate from '../pages/admin/store/storeout/Create'
import StoreOutEdit from '../pages/admin/store/storeout/Edit'

import Search from '../pages/admin/Search'

import DownloadsIndex from '../pages/admin/downloads/Index'

let routes = [
  {
    path: 'users',
    name: 'users.index',
    component: UserIndex
  },
  {
    path: 'downloads',
    name: 'downloads.index',
    component: DownloadsIndex
  },


  {
    path: 'users/create',
    name: 'users.create',
    component: UserCreate
  },

  {
    path: 'users/:id/edit',
    name: 'users.edit',
    component: UserEdit
  },

  {
    path: 'dashboard',
    name: 'dashboard.index',
    component: AdminDashboard
  },
  {
    path: 'patient/:search',
    name: 'patients.search',
    component: Search
  },
  {
    path: 'settings',
    name: 'settings.index',
    component: AdminSettings
  },

  {
    path: 'services',
    name: 'services.index',
    component: ServiceIndex
  },

  {
    path: 'services/create',
    name: 'services.create',
    component: ServiceCreate
  },

  {
    path: 'services/:id/edit',
    name: 'services.edit',
    component: ServiceEdit
  },
  {
    path: 'types',
    name: 'types.index',
    component: TypeIndex
  },

  {
    path: 'types/create',
    name: 'types.create',
    component: TypeCreate
  },

  {
    path: 'types/:id/edit',
    name: 'types.edit',
    component: TypeEdit
  },

  {
    path: 'department',
    name: 'department.index',
    component: DepartmentIndex
  },

  {
    path: 'department/create',
    name: 'department.create',
    component: DepartmentCreate
  },

  {
    path: 'department/:id/edit',
    name: 'department.edit',
    component: DepartmentEdit
  },

  {
    path: 'doctors',
    name: 'doctors.index',
    component: DoctorIndex
  },

  {
    path: 'doctors/create',
    name: 'doctors.create',
    component: DoctorCreate
  },

  {
    path: 'doctors/:id/edit',
    name: 'doctors.edit',
    component: DoctorEdit
  },
  {
    path: 'staff',
    name: 'staff.index',
    component: StaffIndex
  },

  {
    path: 'staff/create',
    name: 'staff.create',
    component: StaffCreate
  },

  {
    path: 'staff/:id/edit',
    name: 'staff.edit',
    component: StaffEdit
  },
  {
    path: 'stafftype',
    name: 'stafftypes.index',
    component: StaffTypeIndex
  },

  {
    path: 'stafftype/create',
    name: 'stafftypes.create',
    component: StaffTypeCreate
  },

  {
    path: 'stafftype/:id/edit',
    name: 'stafftypes.edit',
    component: StaffTypeEdit
  },
  {
    path: 'patients',
    name: 'patients.index',
    component: PatientIndex
  },

  {
    path: 'patients/create',
    name: 'patients.create',
    component: PatientCreate
  },

  {
    path: 'patients/:id/bill/add',
    name: 'patients.edit',
    component: PatientEdit
  },
  {
    path: 'patients/:id/bill',
    name: 'patients.bill',
    component: PatientBill
  },

  {
    path: 'search/:search',
    name: 'search',
    component: Search
  },

  {
    path: 'hospital',
    name: 'hospital.index',
    component: HospitalIndex
  },

  {
    path: 'hospital/create',
    name: 'hospital.create',
    component: HospitalCreate
  },

  {
    path: 'hospital/:id/edit',
    name: 'hospital.edit',
    component: HospitalEdit
  },
  {
    path: 'incometype',
    name: 'incometypes.index',
    component: IncomeTypeIndex
  },

  {
    path: 'incometype/create',
    name: 'incometypes.create',
    component: IncomeTypeCreate
  },

  {
    path: 'incometype/:id/edit',
    name: 'incometypes.edit',
    component: IncomeTypeEdit
  },
  {
    path: 'expendituretype',
    name: 'expendituretypes.index',
    component: ExpenditureTypeIndex
  },

  {
    path: 'expendituretype/create',
    name: 'expendituretypes.create',
    component: ExpenditureTypeCreate
  },

  {
    path: 'expendituretype/:id/edit',
    name: 'expendituretypes.edit',
    component: ExpenditureTypeEdit
  },
  {
    path: 'income',
    name: 'incomes.index',
    component: IncomeIndex
  },

  {
    path: 'income/create',
    name: 'incomes.create',
    component: IncomeCreate
  },

  {
    path: 'income/:id/edit',
    name: 'incomes.edit',
    component: IncomeEdit
  },
  {
    path: 'expenditure',
    name: 'expenditures.index',
    component: ExpenditureIndex
  },

  {
    path: 'expenditure/create',
    name: 'expenditures.create',
    component: ExpenditureCreate
  },

  {
    path: 'expenditure/:id/edit',
    name: 'expenditures.edit',
    component: ExpenditureEdit
  },
  {
    path: 'storein',
    name: 'storein.index',
    component: StoreInIndex
  },

  {
    path: 'storein/create',
    name: 'storein.create',
    component: StoreInCreate
  },

  {
    path: 'storein/:id/edit',
    name: 'storein.edit',
    component: StoreInEdit
  },
  {
    path: 'storeout',
    name: 'storeout.index',
    component: StoreOutIndex
  },

  {
    path: 'storeout/create',
    name: 'storeout.create',
    component: StoreOutCreate
  },

  {
    path: 'storeout/:id/edit',
    name: 'storeout.edit',
    component: StoreOutEdit
  },
  {
    path: 'account',
    name: 'account.index',
    component: AccountIndex
  },
  {
    path: 'profile',
    name: 'profile.index',
    component: HospitalProfile
  },
  {
    path: 'settings',
    name: 'settings',
    component: AdminSettings
  }
]

for (let index in routes) {
  routes[index].path = '/staff/' + routes[index].path
  routes[index].name = 'staff.' + routes[index].name
  routes[index].meta = { auth: ['ROLE_STAFF'], middleware: ['changepassword'] }
}

export default routes
