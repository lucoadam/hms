from django.apps import AppConfig


class IncometypeConfig(AppConfig):
    name = 'incometype'
