from rest_framework import serializers
from . import models


class IncomeTypeSerializer(serializers.ModelSerializer):
    hospital_name = serializers.CharField(required=False)

    class Meta:
        model = models.IncomeType
        fields = "__all__"

class IncomeTypeHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.IncomeTypeHistory
        fields = "__all__"

