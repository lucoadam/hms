from django.db import models
from hospital.models import Hospital
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords

# Create your models here.
def ids():
    no = IncomeType.objects.count()
    if no == 0:
        return '001'
    else:
        no = IncomeType.objects.last()
        i = hex(int('0x' + no.code, 16) + 1).split('0x')[1]
        while len(i) < 3:
            i = '0' + i

        return i


class IncomeType(models.Model):
    tname = models.CharField(max_length=50)
    code = models.CharField(max_length=3, null=True)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.CharField(null=True, max_length=50)
    updated_by = models.CharField(null=True, max_length=50)
    history=HistoricalRecords()
    def __str__(self):
        return self.tname

    def hospital_name(self):
        return self.hospital.name

    class Meta:
        db_table = "income_type"
        unique_together = ('tname', 'hospital')

class IncomeTypeHistory(models.Model):
    tname = models.CharField(max_length=50)
    code = models.CharField(max_length=3, null=True)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    ) 
    action=models.CharField(max_length=20)
    actionBy=models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )
    actionDate=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.actionBy.username
    