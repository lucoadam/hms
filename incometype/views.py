from rest_framework import viewsets, response, status,filters
from rest_framework.pagination import PageNumberPagination
from . import serializers
from .models import IncomeType,IncomeTypeHistory
from hospital.models import Hospital
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
def ownGenerator(type):
  typ= Hospital.objects.filter(id=type)[0].code if Hospital.objects.filter(id=type).exists() else  '000'
  no = IncomeType.objects.count()
  if no == 0:
    return '001'
  else:
    s = IncomeType.objects.filter(hospital_id=type).count()
    if(s == 0):
      return '001'
    else:
      a='000'
      st = str(s+1)
      i = len(st)
      while i< 3:
        st='0'+st
        i=i+1
      return st

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000


class IncomeTypeViewSet(viewsets.ModelViewSet):
    queryset = IncomeType.objects.all()
    serializer_class = serializers.IncomeTypeSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['tname']
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        hos = Hospital.objects.filter(user=self.request.user)
        queryset = IncomeType.objects.filter(hospital=hos[0]) if hos.exists() else IncomeType.objects.all()
        return queryset

    def create(self, request, *args, **kwargs):
      data=request.data
      data['code']=ownGenerator(data['hospital'])
      print(data)
      temp=IncomeTypeHistory()
      temp.tname=data['tname']
      temp.code=ownGenerator(data['hospital'])
      temp.hospital=Hospital.objects.get(id=data['hospital'])
      temp.action='create'
      temp.actionBy=request.user
      temp.save()
      serializer = self.serializer_class(data=data) 
      if(serializer.is_valid()):
        serializer.save()
        return response.Response(serializer.data,status=status.HTTP_201_CREATED)
      else:
        return response.Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    # def destroy(self, request, *args, **kwargs):
    #     if not Services.objects.filter(stype=kwargs.get('pk')).exists():
    #         Type.objects.filter(id=kwargs.get('pk')).delete()
    #
    #     return Response({'success': True}) if not Services.objects.filter(
    #         stype=kwargs.get('pk')).exists() else Response({'success': False})

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        tempInc=IncomeType.objects.get(id=kwargs.get('pk'))
        temp=IncomeTypeHistory()
        temp.tname=tempInc.tname
        temp.code=tempInc.code
        temp.hospital=tempInc.hospital
        temp.action='edit'
        temp.actionBy=request.user
        temp.save()
        serializer.save()
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.GET['remark']:
            instance.changeReason = request.GET['remark']
        self.perform_destroy(instance)
        return Response({'success':True})

    def perform_destroy(self, instance):
        instance.delete()


class IncomeTypeHistoryViewSet(viewsets.ModelViewSet):
    queryset=IncomeTypeHistory.objects.all()
    serializer_class=serializers.IncomeTypeHistorySerializer