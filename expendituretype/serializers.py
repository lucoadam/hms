from rest_framework import serializers
from . import models


class ExpenditureTypeSerializer(serializers.ModelSerializer):
    hospital_name = serializers.CharField(required=False)

    class Meta:
        model = models.ExpenditureType
        fields = "__all__"



class ExpenditureTypeHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model=models.ExpenditureTypeHistory
        fields="__all__"