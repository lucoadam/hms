from django.apps import AppConfig


class ExpendituretypeConfig(AppConfig):
    name = 'expendituretype'
