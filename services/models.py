from django.db import models
from datetime import date
import datetime
from login.models import Year
from type.models import Type
from hospital.models import Hospital
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords

def ftn():
    no = Services.objects.count()
    if no == None:
        return 1
    else:
        return 'S' + str(str(datetime.date.today().year)) + str(datetime.date.today().month).zfill(2)+str(no+1)

# Create your models here.
class Services(models.Model):
    scode = models.CharField(max_length = 20)
    sname = models.CharField(max_length=50)
    sprice = models.FloatField()

    stype = models.ForeignKey(Type, on_delete=models.CASCADE)
    screated = models.DateTimeField( auto_now_add=True)
    screated_by = models.CharField(null=True,max_length=50)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    history=HistoricalRecords()
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)

    def __str__(self):
        return self.sname

    def screated_at(self):
        return self.screated.strftime('%d %B, %Y')

    def type(self):
        return self.stype.tname

    def hospital_name(self):
        return self.hospital.name


    class Meta:
        db_table = "Services"
        unique_together = ('sname','sprice','stype','hospital')

class ServicesHistory(models.Model):
    scode = models.CharField(max_length = 20)
    sname = models.CharField(max_length=50)
    sprice = models.FloatField()
    stype = models.ForeignKey(Type, on_delete=models.CASCADE)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    action=models.CharField(max_length=20)
    actionBy=models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )
    actionDate=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.actionBy.username
