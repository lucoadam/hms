from rest_framework import serializers
from . import models


class ServicesSerializer(serializers.ModelSerializer):
    type= serializers.StringRelatedField()
    hospital_name = serializers.CharField(required=False)
    #
    # def __init__(self,data,context):
    #     if(context.get('request').method == 'POST'):
    #          data['scode']=ownGenerator(data['stype'])
    #     super(ServicesSerializer, self).__init__(data=data,context=context)

    # def create(self, validated_data):
    #     return validated_data
    class Meta:
        model = models.Services
        fields = [
            'id',
            'scode',
            'sname',
            'sprice',
            'screated_by',
            'stype',
            'type',
            'hospital',
            'hospital_name',
            'year',
        ]
class ServicesHistorySerializers(serializers.ModelSerializer):
    class Meta:
        model=models.ServicesHistory
        fields=("__all__")
