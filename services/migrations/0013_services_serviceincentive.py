# Generated by Django 3.0.1 on 2020-07-14 07:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0012_remove_servicehasincentive_service'),
    ]

    operations = [
        migrations.AddField(
            model_name='services',
            name='serviceincentive',
            field=models.ManyToManyField(to='services.ServiceHasIncentive'),
        ),
    ]
