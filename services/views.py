from django.shortcuts import render
from rest_framework import viewsets,status,filters
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from . import serializers
from .models import Services,ServicesHistory
from bill.models import BillService
from hospital.models import Hospital
from rest_framework.permissions import IsAuthenticated

from type.models import Type


def ownGenerator(type):
    typ= Type.objects.filter(id=type)[0].code if Type.objects.filter(id=type).exists() else  '000'
    no = Services.objects.count()
    if no == 0:
        return 'S'+typ+'001'
    else:
        s = Services.objects.filter(stype_id=type).count()
        if(s == 0):
            return 'S'+typ + '001'
        else:
            a='000'
            st = str(int(Services.objects.filter(stype_id=type).last().scode[6:])+1)
            i = len(st)
            while i< 3:
                st='0'+st
                i=i+1

            return 'S'+typ+st

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000
# Create your views here.
class ServicesViewset(viewsets.ModelViewSet):
    queryset = Services.objects.all()
    serializer_class = serializers.ServicesSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['sname']
    #permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = Services.objects.all()
        return queryset
        hos= Hospital.objects.filter(user=self.request.user)
        if hos.exists():
            queryset= Services.objects.filter(hospital=hos[0])
        return queryset

    def create(self, request, *args, **kwargs):
        data = request.data
        data['scode']=ownGenerator(data['stype'])
        serializer = self.serializer_class(data=data)
        if(serializer.is_valid()):
            temp=ServicesHistory()
            temp.scode=ownGenerator(data['stype'])
            temp.sname=data['sname']
            temp.sprice=data['sprice']
            temp.stype=Type.objects.get(id=data['stype'])
            temp.hospital=Hospital.objects.get(id=data['hospital'])
            temp.action='create'
            temp.actionBy=request.user
            temp.save()
            serializer.save()
            # if(serializer.save())
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        tempSer=Services.objects.get(id=kwargs.get('pk'))
        temp=ServicesHistory()
        temp.scode=tempSer.scode
        temp.sname=tempSer.sname
        temp.sprice=tempSer.sprice
        temp.stype=tempSer.stype
        temp.hospital=tempSer.hospital
        temp.action='edit'
        temp.actionBy=request.user
        temp.save()
        serializer.save()
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):

        if not BillService.objects.filter(service=kwargs.get('pk')).exists():
            tempSer=Services.objects.get(id=kwargs.get('pk'))
            temp=ServicesHistory()
            temp.scode=tempSer.scode
            temp.sname=tempSer.sname
            temp.sprice=tempSer.sprice
            temp.stype=tempSer.stype
            temp.hospital=tempSer.hospital
            temp.action='delete'
            temp.actionBy=request.user
            temp.save()
            Services.objects.filter(id=kwargs.get('pk')).delete()

        return Response({'success':True}) if not BillService.objects.filter(service=kwargs.get('pk')).exists() else  Response({'success':False})

class ServicesHistoryViewset(viewsets.ModelViewSet):
    queryset=ServicesHistory.objects.all()
    serializer_class=serializers.ServicesHistorySerializers
