
from django.urls import include, path
from rest_framework.routers import DefaultRouter
from . import views


urlpatterns = [
    path('',views.show,name='show'),
    path('api/user',views.find,name='find'),
    path('api/v1/auth/impersonate',views.GetUserView.as_view())
   
]
