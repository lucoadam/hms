from rest_framework import serializers
from django.contrib.auth.models import User,Group
from django.contrib.auth.hashers import make_password
from hospital.models import Hospital,UserBelongsToHospital
from .models import RolePermissions
import json



class UserSerializer(serializers.ModelSerializer):
    role = serializers.IntegerField(min_value=0,max_value=100,required=False)
    hospital_id=serializers.IntegerField(required=False,default=0)
    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data.get('username'),
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
            password=make_password(validated_data.get('password'))
        )

        if(Group.objects.filter(id=validated_data.get('role'))):
            group = Group.objects.get(pk=validated_data.get('role'))
            group.user_set.add(user)
        else:
            group = Group.objects.filter(name='ROLE_STAFF')[0] if Group.objects.filter(name='ROLE_STAFF') else Group.objects.create(name='ROLE_STAFF')
            group.user_set.add(user)
        #validated_data['password'] = make_password(validated_data['password'])
        hos = Hospital.objects.filter(id=validated_data.get('hospital_id'))
        if (hos.exists()):
            UserBelongsToHospital.objects.create(
                hospital=hos[0],
                user=user
            )
        return user
        ##return super(UserSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        instance.password = make_password(validated_data.get('password'))
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','password','role','hospital_id')



class GroupListSerializer(serializers.ModelSerializer):
  class Meta:
    model = Group
    fields = ('id','name')

class GroupSerializer(serializers.ModelSerializer):
    permissions = serializers.JSONField(required=False)
    class Meta:
        model = Group
        fields = ("id", "name","permissions")
    def update(self, instance, validated_data):
      rolepermission = RolePermissions.objects.filter(group=instance.id)[0] if RolePermissions.objects.filter(group=instance).exists() else  RolePermissions(group=instance)
      rolepermission.permissions = json.dumps(validated_data.get('permissions'))
      rolepermission.save()
      return validated_data



class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "password")


class ChangePasswordSerializer(serializers.Serializer):
    model = User

    """
    Serializer for password change endpoint.
    """
    currentPassword = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
