from django.db import models
from simple_history.models import HistoricalRecords
from django.contrib.auth.models import Group
# from django.contrib.auth.models import User
# # Create your models here.

# class (models.Model):
#     user=models.OneToOneField(User,on_delete=models.CASCADE,related_name="profile")
#     description=models.TextField(blank=True,null=True)
class Year(models.Model):
  year = models.IntegerField(primary_key=True)
  history=HistoricalRecords()

  class Meta:
    db_table = "years"

class RolePermissions(models.Model):
  group = models.OneToOneField(Group,on_delete=models.CASCADE,related_name='role')
  permissions = models.TextField()
  history=HistoricalRecords()




#     def __str__(self):
#         return self.user.username
