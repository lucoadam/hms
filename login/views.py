from django.shortcuts import render
from django.template.defaultfilters import lower
from rest_framework import viewsets,filters
from rest_framework.pagination import PageNumberPagination
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from django.contrib.auth.models import User
from . import serializers, models
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.decorators import action
from hospital.models import Hospital,UserBelongsToHospital
import json
from django.utils import timezone
SESSION_KEY = '_auth_user_id'
BACKEND_SESSION_KEY = '_auth_user_backend'
HASH_SESSION_KEY = '_auth_user_hash'
REDIRECT_FIELD_NAME = 'next'

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = serializers.GroupSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['name']
    def create(self, request, *args, **kwargs):
      self.serializer_class = serializers.GroupListSerializer
      serializer = self.get_serializer(data=request.data)
      serializer.is_valid(raise_exception=True)
      self.perform_create(serializer)
      headers = self.get_success_headers(serializer.data)
      return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
      self.serializer_class = serializers.GroupListSerializer
      queryset = self.filter_queryset(self.get_queryset())
      page = self.paginate_queryset(queryset)
      if page is not None:
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

      serializer = self.get_serializer(queryset, many=True)
    #   print(serializer.data)
      return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
      self.serializer_class = serializers.GroupListSerializer
      instance = self.get_object()
      serializer = self.get_serializer(instance)
      data = serializer.data
      data['permissions'] = json.loads(models.RolePermissions.objects.filter(group=instance)[0].permissions if models.RolePermissions.objects.filter(group=instance).exists() else "{}")
      return Response(data)


class UserViewSet(viewsets.ModelViewSet):
    queryset=User.objects.all()
    serializer_class= serializers.UserSerializer
    # permission_classes = (IsAuthenticated,)
    @action(detail=False, methods=['GET'])
    def years(self,request):
      years = models.Year.objects.all()
      return Response([year.year for year in years])
    @action(detail=False,methods=['GET'])
    def users(self,request):
        user = User.objects.exclude(username='superuser')
        if not request.user.is_anonymous:
            hos = Hospital.objects.filter(user=request.user)
            if (hos.exists()):
                user=[usr.user for usr in UserBelongsToHospital.objects.filter(hospital_id=hos[0].id)]
                user.append(hos.last().user)
        use = [{"id":usr.id,"username":usr.username,"name":usr.first_name+" "+usr.last_name,"role":usr.groups.values_list('name', flat=True)[0],'last_login': usr.last_login.strftime('%Y/%m/%d %I:%M %p') if not usr.last_login is None else '-','hospital':Hospital.objects.filter(user=usr)[0].name if Hospital.objects.filter(user=usr).exists() else ''} if usr.groups.values_list('name', flat=True)[0]=='ROLE_HOSPITAL' else {"id":usr.id,"username":usr.username,"name":usr.first_name+" "+usr.last_name,"role":usr.groups.values_list('name', flat=True)[0],'last_login': usr.last_login.strftime('%Y/%m/%d  %I:%M %p') if not usr.last_login is None else '-','hospital':UserBelongsToHospital.objects.filter(user=usr)[0].hospital.name if UserBelongsToHospital.objects.filter(user=usr).exists() else ''} for usr in user]

        return Response(status=200,data=use)

    def destroy(self, request, *args, **kwargs):
        user=User.objects.filter(id=kwargs.get('pk'))[0]
        print(user);
        if UserBelongsToHospital.objects.filter(user=user).exists():
            UserBelongsToHospital.objects.filter(user=user).delete();
        user.delete();
        return Response(status=200,data={'success':True})

def show(request):
    return render(request,"index.html")

def impersonate(request):
    print(request.POST);
    return render(request,"index.html")
def find(request):
    print(request.user)
    return render(request,"index.html")

class LoginViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.LoginSerializer

    def create(self, request):
        print("new request")
        try:
            username = request.data.get('username')
            password = request.data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:

                response = Response(status=200,headers={'Auth':True},data={"success": True})
                response['Origin'] = 'google.com'
                return response
            else:
                return Response(status=404,data={"success": False})
        except Exception as e:
            print(e)
            return Response(status=501,data={"msg": "Problem while logging in user."})

    def list(self, request):
        print(request)
        return Response(status=403,data={"msg": "API not allowed."})

    def retrieve(self, request, pk=None):
        return Response(status=403,data={"msg": "API not allowed."})

    def update(self, request, pk=None):
        return Response(status=403,data={"msg": "API not allowed."})

    def partial_update(self, request, pk=None):
        return Response(status=403,data={"msg": "API not allowed."})

    def destroy(self, request, pk=None):
        return Response(status=403,data={"msg": "API not allowed."})



class ChangePasswordView(generics.UpdateAPIView):
        """
        An endpoint for changing password.
        """
        serializer_class = serializers.ChangePasswordSerializer
        model = User
        permission_classes = (IsAuthenticated,)

        def get_object(self, queryset=None):
            obj = self.request.user
            return obj

        def update(self, request, *args, **kwargs):
            self.object = self.get_object()
            serializer = self.get_serializer(data=request.data)

            if serializer.is_valid():
                # Check old password
                if not self.object.check_password(serializer.data.get("currentPassword")):
                    return Response({"currentPassword": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
                # set_password also hashes the password that the user will get
                self.object.set_password(serializer.data.get("password"))
                self.object.save()
                refresh = RefreshToken.for_user(self.object)
                response = {
                    'status': 'success',
                    'code': status.HTTP_200_OK,
                    'message': 'Password updated successfully',
                    'refresh': str(refresh),
                    'access': str(refresh.access_token),
                    'user': {
                        'id':self.object.id,
                        'username':self.object.username,
                        'role':self.object.groups.values_list('name', flat=True)[0],
                        'roleName':lower(self.object.groups.values_list('name', flat=True)[0].split('_')[1])
                    },
                    'data': []
                }

                return Response(response)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class RefreshTokenView(generics.GenericAPIView):
        model = User
        permission_classes = (IsAuthenticated,)

        def get_object(self, queryset=None):
            obj = self.request.user
            return obj

        def get(self, request, *args, **kwargs):
            self.object = self.get_object()

            refresh = RefreshToken.for_user(self.object)
            response = {
                'success': True
            }

            return Response(response,headers={
              'Authorization':'Bearer '+ str(refresh.access_token)
            })

# class ImpersonateUser(generics.CreateAPIView):
#     model= User
#     permission_classes = (IsAuthenticated,)
#     def create(self, request, *args, **kwargs):
#         print(request.data)
#         if request.data.get('type') == 'hospital':
#             hospital=Hospital.objects.filter(id=request.data.get('id'))
#             if hospital.exists():
#                 hospital=hospital[0]
#                 refresh = RefreshToken.for_user(hospital.user)
#                 response = {
#                     'refresh': str(refresh),
#                     'access': str(refresh.access_token),
#                 }
#                 print(response)
#                 #print(authenticate(request=request,username=hospital.user.username))
#                 #request.
#                 # request.user=hospital.user
#                 # return Response({
#                 #     "success": True
#                 # }, status=200)
#         return Response({
#             "success":False
#         },status=400)

class GetUserView(generics.ListCreateAPIView):
        """
        An endpoint for changing password.
        """
        model = User

        permission_classes = (IsAuthenticated,)

        def create(self, request, *args, **kwargs):
            user=User.objects.filter(id=request.data.get('id'))
            if user.exists():
                refresh=RefreshToken.for_user(user[0])
                response = {
                  "success": True,
                }
                return Response(response,headers={
                  'Authorization':'Bearer '+ str(refresh.access_token)
                })

            return Response({
                "success": False
            }, status=400)


        def get_object(self, queryset=None):
            obj = self.request.user
            return obj

        def get(self, request, *args, **kwargs):
            self.object = self.get_object()
            role=self.object.groups.values_list('name', flat=True)[0]
            roleId=self.object.groups.values_list('id', flat=True)[0]
            roleName=lower(role.split('_')[1])




            #refresh = RefreshToken.for_user(self.object)
            response = {

                    'id':self.object.id,
                    'username':self.object.username,
                    'role':role,
                    'roleName':roleName

            }
            forcePasswordChange = False


            self.object.last_login = timezone.now()
            self.object.save(update_fields=['last_login'])
            response['permissions'] = json.loads(models.RolePermissions.objects.filter(group_id=roleId)[0].permissions if models.RolePermissions.objects.filter(group_id=roleId).exists() else "{}")
            if roleName == 'hospital' and self.object.hospital_set.exists():
              forcePasswordChange = self.object.check_password(self.object.hospital_set.all()[0].phone)
              response['hospital_id']=self.object.hospital_set.all()[0].id
            elif roleName != 'admin' and self.object.userbelongstohospital_set.exists():
              response['hospital_id']=self.object.userbelongstohospital_set.all()[0].hospital.id
              if roleName == 'staff':
                forcePasswordChange = self.object.check_password(self.object.staff_set.all()[0].sphone)
                response['permissions'] = json.loads(self.object.staff_set.all()[0].stype.permissions if self.object.staff_set.exists() else "{}")

            if forcePasswordChange:
              response['forcePasswordChange'] = True
            return Response(response)



