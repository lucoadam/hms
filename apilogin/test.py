# from services.models import Services
#
#
# def changetothree(string):
#   s = str(string)
#   i = len(s)
#   while i < 3:
#     s = '0' + s
#     i += 1
#   return s
#
# stype={}
#
# for service in Services.objects.all().order_by('scode'):
#    scode = 'S'+service.stype.code + service.scode[-3:]
#    if service.stype.code not in stype:
#      stype[service.stype.code] = 1
#    else:
#      stype[service.stype.code] += 1
#    service.scode = 'S'+service.stype.code + changetothree(stype[service.stype.code])
#    service.save()
#    print(service.stype.code,service.scode[1:6],service.scode[-3:])
#
def converttothree(string, int=3):
  s = str(string)
  i = len(s)
  while i < int:
    s = '0' + s
    i += 1
  return s


from patients.models import Patients

code = {}
for patient in Patients.objects.order_by('id'):
  cod = patient.pcode[-4:]
  codd = patient.pcode[:5]
  co = patient.pcode[:3]
  if co == 'P77':
    if codd in code:
      code[codd] += 1
    else:
      code[codd] = int(cod)
    patient.pcode = codd + converttothree(code[codd], 4)
    patient.save()
    print(codd, cod, converttothree(code[codd], 4), patient.pcode)

#
#
# from type.models import Type
# hospital = {}
# for type in Type.objects.filter(id__gte=10):
#   typ = type.code[:2]
#   if typ not in hospital:
#     hospital[typ] = 10
#   else:
#     hospital[typ] +=1
#   val = converttothree(hospital[typ])
#   type.code = typ + val
#   type.save()
#   print(typ+val,type.code)
import datetime, os, json

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
with open(os.path.join(BASE_DIR, 'apilogin', 'date.json')) as json_data:
  datemap = json.load(json_data)


def returnCode():
  dateKey = datetime.datetime.now().strftime('%Y/%m/%d')
  if (dateKey in datemap) and (datemap[dateKey] != ''):
    converted = datemap[dateKey]
    each_date = converted.split('/')
    year = each_date[0]
    month = each_date[1]
    return year[-2:] + month
