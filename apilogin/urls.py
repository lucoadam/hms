from django.urls import path,include
from django.http import HttpResponse
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import response,status
from django.contrib import admin
from . import api
from login.views import  ChangePasswordView,GetUserView,RefreshTokenView
from income.views import DataForAccount,DownloadDataForAccount
from patients.views import PatientListView
from django.conf import settings
from rest_framework_simplejwt.exceptions import TokenError,InvalidToken
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        # data['refresh'] = str(refresh)
        data['token'] = str(refresh.access_token)
        return data


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
      serializer = self.get_serializer(data=request.data)

      try:
        serializer.is_valid(raise_exception=True)
      except TokenError as e:
        raise InvalidToken(e.args[0])
      return response.Response(serializer.validated_data,headers={
        'Authorization':'Bearer '+ serializer.validated_data.get('token')
      }, status=status.HTTP_200_OK)


def metaView(request):
    return HttpResponse('[{"key":"municipal_name","value":"Hospital Management System"},{"key":"municipal_office_name","value":"Melamchi Muncipality, Nepal"}]')

urlpatterns = [
    path('admin/', admin.site.urls),
    path("services/", include("services.urls")),
    path("patients/", include("patients.urls")),
    path("doctors/", include("doctor.urls")),
    # path("api/v1/user/", include("userdetail.urls")),
    path("departments/",include("department.urls")),
    path('api/v1/auth/login/',MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/v1/refresh',RefreshTokenView.as_view()),
    path('api/v1/auth/change-password', ChangePasswordView.as_view()),
    path('api/auth/user',GetUserView.as_view()),
    path('api/v1/accounts/',DataForAccount.as_view()),
    path('api/v1/downloadAccounts/',DownloadDataForAccount.as_view()),
    path('',include("login.urls")),
    path('userview/',include("login.urls")),
    path('api/v1/metas/',include("metas.urls")),
    path('api/v1/public/metas',metaView,name='metaView'),
    path('api/v1/', include(api.router.urls)),
    path('api/v1/psearch/',PatientListView.as_view(),name='patientSearch'),
    path('api/v1/print/',include('store.urls'))
]
