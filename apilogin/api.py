from rest_framework import routers
from services.views import ServicesViewset,ServicesHistoryViewset
from patients.views import PatientsViewset
from doctor.views import DoctorsViewset,DoctorsHistoryViewset
from staff.views import StaffViewset,StaffHistoryViewset
from login.views import  LoginViewSet,UserViewSet,GroupViewSet
from department.views import DepartmentsViewset,DepartmentsHistoryViewset
from bill.views import BillViewset,BillServiceViewset
from type.views import TypeViewset
from staffType.views import StaffTypeViewSet,StaffTypeHistoryViewset
from incometype.views import IncomeTypeViewSet,IncomeTypeHistoryViewSet
from income.views import IncomeViewSet,IncomeHistoryViewset
from expendituretype.views import ExpenditureTypeViewSet,ExpenditureTypeHistoryViewset
from expenditure.views import ExpenditureViewSet,ExpenditureHistoryViewset
from productstore.views import ProductCategoryViewset,ProductStoreInViewset,ProductStoreOutViewset,ProductViewset,RequestViewSet
from hospital.views import HospitalViewSet,WardViewSet
from store.views import StoreInViewset,StoreOutViewset,ProductServiceViewSet,MedicineViewSet,StoreInHistoryViewset,StoreOutHistoryViewset
# from login.views import LoginViewSet


router = routers.DefaultRouter()
router.register(r'services', ServicesViewset)
router.register(r'product-category', ProductCategoryViewset)
router.register(r'product-storein', ProductStoreInViewset)
router.register(r'product-storeout', ProductStoreOutViewset)
router.register(r'product', ProductViewset)
router.register(r'product-request', RequestViewSet)
router.register(r'services-history', ServicesHistoryViewset)
router.register(r'patients', PatientsViewset)
router.register(r'doctors', DoctorsViewset)
router.register(r'doctors-history', DoctorsHistoryViewset)
router.register(r'staff', StaffViewset)
router.register(r'staff-history', StaffHistoryViewset)
router.register(r'bill',BillViewset)
router.register(r'login',LoginViewSet)
router.register(r'departments',DepartmentsViewset)
router.register(r'departments-history',DepartmentsHistoryViewset)
router.register(r'billservice',BillServiceViewset)
router.register(r'userview',UserViewSet),
router.register(r'roles',GroupViewSet),
router.register(r'type',TypeViewset)
router.register(r'stafftype',StaffTypeViewSet)
router.register(r'stafftype-history',StaffTypeHistoryViewset)
router.register(r'income',IncomeViewSet)
router.register(r'income-history',IncomeHistoryViewset)
router.register(r'incometype',IncomeTypeViewSet)
router.register(r'incometype-history',IncomeTypeHistoryViewSet)
router.register(r'expendituretype',ExpenditureTypeViewSet)
router.register(r'expendituretype-history',ExpenditureTypeHistoryViewset)
router.register(r'expenditure',ExpenditureViewSet)
router.register(r'expenditure-history',ExpenditureHistoryViewset)
router.register(r'hospital',HospitalViewSet)
router.register(r'ward',WardViewSet)
router.register(r'storein',StoreInViewset)
router.register(r'storeout',StoreOutViewset)
router.register(r'storein-history',StoreInHistoryViewset)
router.register(r'storeout-history',StoreOutHistoryViewset)
router.register(r'productservice',ProductServiceViewSet)
router.register(r'medicine',MedicineViewSet)
# router.register(r'psearch',PatientListView)


# router.register('plist',PatientList)
