"""
WSGI config for apilogin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
import django
from django.core.handlers.wsgi import WSGIHandler
from dj_static import Cling



def get_wsgi_application():
  """
  The public interface to Django's WSGI support. Return a WSGI callable.

  Avoids making django.core.handlers.WSGIHandler a public API, in case the
  internal WSGI implementation changes or moves in the future.
  """
  django.setup(set_prefix=False)
  return WSGIHandler()

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'apilogin.settings')
application =Cling(get_wsgi_application())

