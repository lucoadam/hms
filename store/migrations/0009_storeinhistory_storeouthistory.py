# Generated by Django 3.0.1 on 2020-07-10 11:39

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0004_auto_20200628_1022'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('hospital', '0005_auto_20200617_1154'),
        ('store', '0008_auto_20200703_0709'),
    ]

    operations = [
        migrations.CreateModel(
            name='StoreOutHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('buyer', models.CharField(default='', max_length=50, null=True)),
                ('action', models.CharField(max_length=20)),
                ('actionDate', models.DateTimeField(auto_now_add=True)),
                ('actionBy', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('hospital', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='hospital.Hospital')),
                ('orders', models.ManyToManyField(to='store.ProductService')),
                ('year', models.ForeignKey(default=77, on_delete=django.db.models.deletion.PROTECT, to='login.Year')),
            ],
        ),
        migrations.CreateModel(
            name='StoreInHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('quantity', models.IntegerField()),
                ('unit_price', models.FloatField()),
                ('batch', models.CharField(max_length=15, null=True)),
                ('packs', models.CharField(max_length=10, null=True)),
                ('manufacture_date', models.DateField(null=True)),
                ('expiry_date', models.DateField(null=True)),
                ('action', models.CharField(max_length=20)),
                ('actionDate', models.DateTimeField(auto_now_add=True)),
                ('actionBy', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('hospital', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='hospital.Hospital')),
                ('year', models.ForeignKey(default=77, on_delete=django.db.models.deletion.PROTECT, to='login.Year')),
            ],
        ),
    ]
