# Generated by Django 3.0.1 on 2020-07-10 11:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0009_storeinhistory_storeouthistory'),
    ]

    operations = [
        migrations.RenameField(
            model_name='storein',
            old_name='packs',
            new_name='pack',
        ),
        migrations.RenameField(
            model_name='storeinhistory',
            old_name='packs',
            new_name='pack',
        ),
    ]
