from rest_framework import serializers
from rest_framework.serializers import raise_errors_on_nested_writes, model_meta
import traceback
from . import models


class StoreInServiceSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    created = serializers.StringRelatedField(required=False)
    product_name =  serializers.StringRelatedField(required=False)

    def create(self, validated_data):
      """
      We have a bit of extra checking around this in order to provide
      descriptive messages when something goes wrong, but this method is
      essentially just:

          return ExampleModel.objects.create(**validated_data)

      If there are many to many fields present on the instance then they
      cannot be set until the model is instantiated, in which case the
      implementation is like so:

          example_relationship = validated_data.pop('example_relationship')
          instance = ExampleModel.objects.create(**validated_data)
          instance.example_relationship = example_relationship
          return instance

      The default implementation also does not handle nested relationships.
      If you want to support writable nested relationships you'll need
      to write an explicit `.create()` method.
      """
      raise_errors_on_nested_writes('create', self, validated_data)
      validated_data['remaining_quantity'] = validated_data['quantity']

      ModelClass = self.Meta.model

      # Remove many-to-many relationships from validated_data.
      # They are not valid arguments to the default `.create()` method,
      # as they require that the instance has already been saved.
      info = model_meta.get_field_info(ModelClass)
      many_to_many = {}
      for field_name, relation_info in info.relations.items():
        if relation_info.to_many and (field_name in validated_data):
          many_to_many[field_name] = validated_data.pop(field_name)

      try:
        instance = ModelClass._default_manager.create(**validated_data)
      except TypeError:
        tb = traceback.format_exc()
        msg = (
          'Got a `TypeError` when calling `%s.%s.create()`. '
          'This may be because you have a writable field on the '
          'serializer class that is not a valid argument to '
          '`%s.%s.create()`. You may need to make the field '
          'read-only, or override the %s.create() method to handle '
          'this correctly.\nOriginal exception was:\n %s' %
          (
            ModelClass.__name__,
            ModelClass._default_manager.name,
            ModelClass.__name__,
            ModelClass._default_manager.name,
            self.__class__.__name__,
            tb
          )
        )
        raise TypeError(msg)

      # Save many-to-many relationships after the instance is created.
      if many_to_many:
        for field_name, value in many_to_many.items():
          field = getattr(instance, field_name)
          field.set(value)

      return instance
    class Meta:
        model = models.ProductService
        fields = "__all__"


class MedicineSerializer(serializers.ModelSerializer):
  created = serializers.StringRelatedField(required=False)
  class Meta:
      model = models.Medicine
      fields = "__all__"

class StoreInSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    created = serializers.StringRelatedField(required=False)
    hospital_name = serializers.StringRelatedField(required=False)

    #service =  serializers.StringRelatedField(required=True)
    class Meta:
        model = models.StoreIn
        fields = "__all__"

class StoreOutSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    sname = serializers.StringRelatedField(required=False)
    hospital_name = serializers.StringRelatedField(required=False)
    created = serializers.StringRelatedField(required=False)
    orders = StoreInServiceSerializer(many =True)
    # buyer = serializers.StringRelatedField(required=False)
    created_by_name =  serializers.StringRelatedField(required=False)
    buyer_name =  serializers.StringRelatedField(required=False)
    def _user(self):
      request = getattr(self.context, 'request', None)
      if request:
        return request.user

    def create(self, validated_data):
      print(validated_data.get("buyer"))
      if validated_data.get('buyer') != '':
        storeout = models.StoreOut(buyer=validated_data.get('buyer'),hospital=validated_data.get('hospital'),created_by=validated_data.get('created_by'))
      else:
        storeout = models.StoreOut(hospital=validated_data.get('hospital'),created_by=validated_data.get('created_by'))
      storeout.save()
      orders = validated_data.get('orders')
      for order in orders:
        if storeout.orders.create(quantity=order['quantity'],unit_price=order['unit_price'],product=order['product']):
          product = order['product']
          if product.remaining_quantity == -1:
            product.remaining_quantity = product.quantity - int(order['quantity'])
          else:
            product.remaining_quantity = product.remaining_quantity - int(order['quantity'])
          product.save()
      return storeout

    def update(self, instance, validated_data):
      print(validated_data)
      if validated_data.get('buyer') != '':
        instance.buyer = validated_data.get('buyer')
      orders = validated_data.get('orders')
      presOder = []
      ord = instance.orders.all()
      for order in orders:
        if 'id' in order:
          each = models.ProductService.objects.filter(id=order['id'])
          if each.exists():
            each =each[0]
            presOder.append(order['id'])
            prevQuantity = each.quantity
            each.quantity=order['quantity']
            each.product = order['product']
            print(each.quantity)
            print(order['quantity'])
            each.save()
            product = each.product
            print('inner')
            print(prevQuantity)
            if prevQuantity > order['quantity']:
              product.remaining_quantity =product.remaining_quantity+ prevQuantity - order['quantity']
            else:
              product.remaining_quantity =product.remaining_quantity- (order['quantity'] - prevQuantity)
            product.save()
        else:
          d = instance.orders.create(quantity=order['quantity'],unit_price=order['unit_price'],product=order['product'])
          if d:
            product = order['product']
            if product.remaining_quantity == -1:
              product.remaining_quantity = product.quantity - int(order['quantity'])
            else:
              product.remaining_quantity = product.remaining_quantity - int(order['quantity'])
            product.save()
          presOder.append(d.id)
      for ora in ord:
        if not ora.id in presOder:
          ora.delete()
      return instance

    class Meta:
        model = models.StoreOut
        fields = "__all__"
        # fields = [
        #     "id",
        #     "sprice",
        #     "squantity",
        #     "service",
        #     'sname',
        # ]
        # read_only_fields =  ('bill',)

# class BillSerializer(serializers.ModelSerializer):
#     id = serializers.IntegerField(required=False)
#     services = BillSerivceSerializer(many=True)
#     created = serializers.StringRelatedField()
#     created_by = serializers.StringRelatedField()
#     posts = serializers.StringRelatedField()
#     class Meta:
#         model = models.Bill
#         fields = [
#             "id",
#             "bcode",
#             "user",
#             "created_at",
#             "services",
#             "total",
#             'created',
#             'created_by',
#             'posts'
#         ]
#         read_only_fields =  ('patient',)



# class NewServiceSerializers(serializers.ModelSerializer):
#     choices = BillSerializer(many=True)
#     class Meta:
#         model=models.Services
#         fields=["scode",
#                 "sname",
#                 "sprice",
#                 "stype",
#                 "choices",
#         ]

#     def create(self,**validated_data):
#         choices = validated_data.pop('choices')
#         services = Services.objects.create(**validate_data)
#         for choice in choices:
#             choice.objects.create(**choice,service=service)
#         return service

#     def update(self,instance,**validated_data):
#         choices = validated_data.pop('choices')
#         instance.sname = validated_data.get("sname",instance.sname)
#         instance.save()
#         keep_choices =[]
#         existing_scodes = [c.scode for c in instances.choices]
#         for choice in choices:
#             if "scode" in choice.key():
#                 if choice.objects.filter(id=choice["scode"]).exists():
#                     c=choice.objects.get(scode=choice["scode"])
#                     c.text = choice.get('text',c.text)
#                     c.save()
#                     keep_choices.append(c)
#                 else:
#                     continue
#             else:
#                 c= choice.objects.create(**choice,service=service)
#                 keep_choices.append(c.scode)

#         for choice in instance.choices:
#             if choice.scode not in keep_choices:
#                 choice.delete()

#         return instances





class StoreInHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model=models.StoreInHistory
        fields="__all__"

class StoreOutHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model=models.StoreOutHistory
        fields="__all__"
