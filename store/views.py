from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from rest_framework import viewsets, generics,filters
from hospital.models import Hospital
from rest_framework.pagination import PageNumberPagination
from . import serializers, models
from rest_framework import response
from rest_framework.decorators import action
from rest_framework import parsers
from django_filters.rest_framework import DjangoFilterBackend
from django.views.generic import TemplateView
from rest_framework.permissions import IsAuthenticated
from rest_framework.settings import api_settings 
from rest_framework.response import Response
from rest_framework import status

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 20
  page_size_query_param = 'page_size'
  max_page_size = 1000

class StoreInViewset(viewsets.ModelViewSet):
    queryset = models.StoreIn.objects.all()
    serializer_class = serializers.StoreInSerializer
    filter_backends = (DjangoFilterBackend,)
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['name']
    def get_queryset(self):
        queryset = models.StoreIn.objects.all()
        hos= Hospital.objects.filter(user=self.request.user)
        if hos.exists():
            queryset= models.StoreIn.objects.filter(hospital=hos[0])
        return queryset
    # permission_classes = (IsAuthenticated,)
    #filterset_fields = ('bcode','patient__pname','patient__pcode','services__scode','services__sname')
    def create(self, request, *args, **kwargs):
        data=request.data
        storeinhis=models.StoreInHistory(
          name = data['name'],
          quantity = data['quantity'],
          unit_price = data['unit_price'],
          batch = data['batch'],
          pack = data['pack'],
          manufacture_date = data['manufacture_date'],
          expiry_date = data['expiry_date'],
          hospital = Hospital.objects.get(id=data['hospital']),
          action='create',
          actionBy=request.user
        )
        storeinhis.save()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


    def update(self, request, *args, **kwargs):
        storeindata=models.StoreIn.objects.get(id=kwargs.get('pk'))
        storeinhis=models.StoreInHistory(
          name = storeindata.name,
          quantity = storeindata.quantity,
          unit_price = storeindata.unit_price,
          batch = storeindata.batch,
          pack = storeindata.pack,
          manufacture_date = storeindata.manufacture_date,
          expiry_date = storeindata.expiry_date,
          hospital = storeindata.hospital,
          action='edit',
          actionBy=request.user
        )
        storeinhis.save()
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


    def destroy(self, request, *args, **kwargs):
        storeindata=models.StoreIn.objects.get(id=kwargs.get('pk'))
        storeinhis=models.StoreInHistory(
          name = storeindata.name,
          quantity = storeindata.quantity,
          unit_price = storeindata.unit_price,
          batch = storeindata.batch,
          pack = storeindata.pack,
          manufacture_date = storeindata.manufacture_date,
          expiry_date = storeindata.expiry_date,
          hospital = storeindata.hospital,
          action='delete',
          actionBy=request.user
        )
        storeinhis.save()
        instance = self.get_object()
        instance.delete()
        return Response({'success':True})



class StoreOutViewset(viewsets.ModelViewSet):
    queryset = models.StoreOut.objects.all()
    serializer_class = serializers.StoreOutSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['buyer']
    def get_queryset(self):
      queryset = models.StoreOut.objects.all()
      hos= Hospital.objects.filter(user=self.request.user)
      if hos.exists():
        queryset= models.StoreOut.objects.filter(hospital=hos[0])
      return queryset
      

class ProductServiceViewSet(viewsets.ModelViewSet):
    queryset = models.ProductService.objects.all()
    serializer_class = serializers.StoreInServiceSerializer

class MedicineViewSet(viewsets.ModelViewSet):
  queryset = models.Medicine.objects.all()
  serializer_class = serializers.MedicineSerializer
  filter_backends = (filters.SearchFilter,filters.OrderingFilter)
  pagination_class = StandardResultsSetPagination
  search_fields = ['name']



class GetPrintForBill(TemplateView):
  template_name = "bill.html"

  def get(self, request, *args, **kwargs):
    context = self.get_context_data(**kwargs)
    context['message'] = 'some information'
    return self.render_to_response(context)





class StoreInHistoryViewset(viewsets.ModelViewSet):
    queryset=models.StoreInHistory.objects.all()
    serializer_class=serializers.StoreInHistorySerializer

class StoreOutHistoryViewset(viewsets.ModelViewSet):
    queryset=models.StoreOutHistory.objects.all()
    serializer_class=serializers.StoreOutHistorySerializer