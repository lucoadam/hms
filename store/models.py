from django.db import models
from hospital.models import Hospital
from django.contrib.auth.models import User
from login.models import Year
from simple_history.models import HistoricalRecords

class Pharmacy(models.Model):
  pan = models.CharField(max_length=10)
  phone = models.CharField(max_length=15,blank=True)
  location = models.CharField(max_length=50)
  name= models.CharField(max_length=50)
  hospital = models.ForeignKey(
    Hospital,
    on_delete=models.PROTECT
  )
  created_at = models.DateField(auto_now_add=True)
  updated_at = models.DateField(auto_now=True)
  created_by = models.CharField(max_length=1)

  user = models.ForeignKey(
    User,
    on_delete=models.CASCADE,
    default=1
  )
  history=HistoricalRecords()
  class Meta:
    db_table= "pharmacies"

# Create your models here.
class StoreIn(models.Model):
    name = models.CharField(max_length=50)
    quantity = models.IntegerField()
    remaining_quantity = models.IntegerField(default=-1)
    unit_price = models.FloatField()
    batch = models.CharField(max_length=15,null=True)
    pack = models.CharField(max_length=10,null=True)
    manufacture_date = models.DateField(null=True)
    expiry_date = models.DateField(null=True)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.PROTECT
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    history=HistoricalRecords()
    def created(self):
        return self.created_at.strftime('%Y/%m/%d')
    def hospital_name(self):
        return self.hospital.name
    class Meta:
        db_table = 'storein'

class Medicine(models.Model):
  name = models.TextField(null=True)
  quantity = models.IntegerField(default=1)
  batch = models.CharField(max_length=15,null=True)
  code = models.CharField(max_length=15,null=True)
  pack_unit = models.CharField(max_length=5)
  created_by = models.ForeignKey(
    User,
    on_delete=models.PROTECT,
    default=1
  )
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)
  history=HistoricalRecords()
  def created(self):
    return self.created_at.strftime('%Y/%m/%d')

  class Meta:
    db_table = 'medicines'


class ProductService(models.Model):
    product = models.ForeignKey(
        StoreIn,
        on_delete=models.PROTECT
    )
    quantity = models.IntegerField(default=1)
    unit_price = models.FloatField()
    batch = models.CharField(max_length=15,null=True)
    packs = models.CharField(max_length=10,null=True)
    manufacture_date = models.DateField(null=True)
    expiry_date = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    history=HistoricalRecords()

    def product_name(self):
        return self.product.name


class StoreOut(models.Model):
    buyer = models.CharField(max_length=50,default='',null=True)
    orders = models.ManyToManyField(ProductService)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.PROTECT
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    history=HistoricalRecords()


    def created(self):
        return self.created_at.strftime('%Y/%m/%d')
    def hospital_name(self):
        return self.hospital.name
    def created_by_name(self):
        return self.created_by.username
    def buyer_name(self):
        return self.buyer
    class Meta:
        db_table = 'storeout'

class StoreInHistory(models.Model):
    name = models.CharField(max_length=50)
    quantity = models.IntegerField()
    unit_price = models.FloatField()
    batch = models.CharField(max_length=15,null=True)
    pack = models.CharField(max_length=10,null=True)
    manufacture_date = models.DateField(null=True)
    expiry_date = models.DateField(null=True)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    action=models.CharField(max_length=20)
    actionBy=models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )
    actionDate=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.actionBy.username


class StoreOutHistory(models.Model):
    buyer = models.CharField(max_length=50,default='',null=True)
    orders = models.ManyToManyField(ProductService)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    action=models.CharField(max_length=20)
    actionBy=models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )
    actionDate=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.actionBy.username
