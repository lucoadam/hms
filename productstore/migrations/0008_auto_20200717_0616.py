# Generated by Django 3.0.1 on 2020-07-17 06:16

from django.db import migrations, models


class Migration(migrations.Migration):
  dependencies = [
    ('productstore', '0007_auto_20200717_0610'),
  ]

  operations = [
    migrations.AlterField(
      model_name='productstorein',
      name='code',
      field=models.CharField(blank=True, default=None, max_length=10, null=True),
    ),
  ]
