from django.apps import AppConfig


class ProductstoreConfig(AppConfig):
    name = 'productstore'
