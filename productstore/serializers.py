from rest_framework import serializers
from . import models


class ProductCategorySerializer(serializers.ModelSerializer):
  hospital_name=serializers.StringRelatedField(required=False)
  class Meta:
    model = models.ProductCategory
    fields = "__all__"


class ProductSerializer(serializers.ModelSerializer):
  category_name=serializers.StringRelatedField(required=False)
  hospital_name=serializers.StringRelatedField(required=False)
  class Meta:
    model = models.Product
    fields = "__all__"


class SellerDetails(serializers.ModelSerializer):


  class Meta:
    model = models.SellerDetails
    fields = "__all__"


class ProductStoreInSerializer(serializers.ModelSerializer):
  seller_name = serializers.StringRelatedField(required=False)

  class Meta:
    model = models.ProductStoreIn
    fields = "__all__"


class ProductStoreOutSerializer(serializers.ModelSerializer):
  class Meta:
    model= models.ProductStoreOut
    fields = "__all__"


class StoreOutHasProductSerializer(serializers.ModelSerializer):
  class Meta:
    model = models.StoreOutHasProduct
    fields = "__all__"


class RequestProductSerializer(serializers.ModelSerializer):
  class Meta:
    model = models.RequestProduct
    fields = "__all__"


class RequestFormSerializer(serializers.ModelSerializer):
  class Meta:
    model = models.RequestForm
    fields = "__all__"
