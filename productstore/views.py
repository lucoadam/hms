from django.shortcuts import render
from django.contrib.auth.models import User
# Create your views here.
from django.shortcuts import render
from rest_framework import viewsets, generics,filters
from hospital.models import Hospital
from rest_framework.pagination import PageNumberPagination
from . import serializers, models
from rest_framework import response
from rest_framework.decorators import action
from rest_framework import parsers
from django_filters.rest_framework import DjangoFilterBackend
from django.views.generic import TemplateView
from rest_framework.permissions import IsAuthenticated
from rest_framework.settings import api_settings
from rest_framework import status
from . import models
from rest_framework.response import Response

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 20
  page_size_query_param = 'page_size'
  max_page_size = 1000

class ProductCategoryViewset(viewsets.ModelViewSet):
  queryset = models.ProductCategory.objects.all()
  serializer_class = serializers.ProductCategorySerializer
  filter_backends = (DjangoFilterBackend,)
  pagination_class = StandardResultsSetPagination
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name']
  def get_queryset(self):
    queryset =  models.ProductCategory.objects.all()
    hos= Hospital.objects.filter(user=self.request.user)
    if hos.exists():
      queryset= models.ProductCategory.objects.filter(hospital=hos[0])
    return queryset
  def create(self, request, *args, **kwargs):
        data=request.data
        print(data)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()
        promodel=models.ProductCategory(
          name=data['name'],
          hospital=Hospital.objects.get(id=data["hospital"]),
          created_by=request.user.id
        )
        promodel.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

  def update(self, request, *args, **kwargs):
        data=request.data
        print(data)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        old=models.ProductCategory.objects.get(id=kwargs.get('pk'))
        # old=models.ProductCategory()
        old.name=data['name']
        old.code=data['code']
        old.hospital=Hospital.objects.get(id=data['hospital'])
        old.created_at=data['created_at']
        old.updated_at=data['updated_at']
        old.created_by=data['created_by']
        old.save()
        # instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.data)
  def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.GET['remark']:
            instance.changeReason = request.GET['remark']
        self.perform_destroy(instance)
        return Response({'success':True})

  def perform_destroy(self, instance):
      instance.delete()





class ProductViewset(viewsets.ModelViewSet):
  queryset = models.Product.objects.all()
  serializer_class = serializers.ProductSerializer
  filter_backends = (DjangoFilterBackend,)
  pagination_class = StandardResultsSetPagination
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name']
  def get_queryset(self):
    queryset =  models.Product.objects.all()
    hos= Hospital.objects.filter(user=self.request.user)
    if hos.exists():
      queryset= models.Product.objects.filter(hospital=hos[0])
    return queryset
  def update(self, request, *args, **kwargs):
        data=request.data
        print(data)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        old=models.Product.objects.get(id=kwargs.get('pk'))
        # old=models.Product()
        old.name=data['name']
        old.rate=data['rate']
        old.code=data['code']
        old.hospital=Hospital.objects.get(id=data['hospital'])
        old.created_at=data['created_at']
        old.updated_at=data['updated_at']
        old.created_by=data['created_by']
        old.type=data['type']
        old.category=models.ProductCategory.objects.get(id=data['category'])
        old.unit=data['unit']
        old.quantity=data['quantity']
        old.specification=data['specification']
        old.save()
        # instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
  def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.GET['remark']:
            instance.changeReason = request.GET['remark']
        instance.delete()
        return Response({'success':True})



class ProductStoreInViewset(viewsets.ModelViewSet):
  queryset = models.ProductStoreIn.objects.all()
  serializer_class = serializers.ProductStoreInSerializer
  filter_backends = (DjangoFilterBackend,)
  pagination_class = StandardResultsSetPagination
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name']
  def get_queryset(self):
    queryset =  models.ProductStoreIn.objects.all()
    hos= Hospital.objects.filter(user=self.request.user)
    if hos.exists():
      queryset= models.ProductStoreIn.objects.filter(hospital=hos[0])
    return queryset

  def create(self, request, *args, **kwargs):
    data=request.data
    print(data)
    tempseller = models.SellerDetails()
    tempseller.name = data['seller_name']
    tempseller.address = data['seller_address']
    tempseller.registration_number = data['seller_registration_number']
    tempseller.pan_number = data['seller_pan_number']
    tempseller.phone_number = data['seller_phone_number']
    tempseller.save()

    tempStorin = models.ProductStoreIn()
    tempStorin.seller_details = tempseller
    tempStorin.purchase_number = data['purchase_number']
    tempStorin.purchase_date = data['purchase_date']
    tempStorin.purchase_decision_number = data['purchase_decision_number']
    tempStorin.purchase_decision_date = data['purchase_decision_date']
    tempStorin.total = data['total']
    tempStorin.save()

    for i in range(len(data['products'])):
      tempstoreinHas = models.StoreInHasProducts()
      tempstoreinHas.product_commodity_number = data['products'][i]['product_commodity_number']
      tempstoreinHas.product = models.Product.objects.get(id=data['products'][i]['product'])
      tempstoreinHas.store_in = tempStorin
      tempstoreinHas.quantity = data['products'][i]['quantity']
      tempstoreinHas.rate = data['products'][i]['rate']
      tempstoreinHas.remarks = data['products'][i]['remarks']
      tempstoreinHas.save()
    return Response(status=status.HTTP_201_CREATED)

  # def get_success_headers(self, data):
  #     try:
  #         return {'Location': str(data[api_settings.URL_FIELD_NAME])}
  #     except (TypeError, KeyError):
  #         return {}

class ProductStoreOutViewset(viewsets.ModelViewSet):
  queryset = models.ProductStoreOut.objects.all()
  serializer_class = serializers.ProductStoreOutSerializer
  filter_backends = (DjangoFilterBackend,)
  pagination_class = StandardResultsSetPagination
  filter_backends = [filters.SearchFilter,filters.OrderingFilter]
  search_fields = ['name']
  def get_queryset(self):
    queryset =  models.ProductStoreOut.objects.all()
    hos= Hospital.objects.filter(user=self.request.user)
    if hos.exists():
      queryset= models.ProductStoreOut.objects.filter(hospital=hos[0])
    return queryset

  def create(self, request, *args, **kwargs):
    data = request.data
    print(data)
    tempout = models.ProductStoreOut()
    tempout.invoice_out_number = data['invoice_out_number']
    tempout.created_by = request.user.id
    tempout.save()

    for i in range(len(data['products'])):
      tempstoreoutHas = models.StoreOutHasProduct()
      tempstoreoutHas.product_commodity_number = data['products'][i]['product_commodity_number']
      tempstoreoutHas.product = models.Product.objects.get(id=data['products'][i]['product'])
      tempstoreoutHas.store_out = tempout
      tempstoreoutHas.quantity = data['products'][i]['quantity']
      tempstoreoutHas.rate = data['products'][i]['rate']
      tempstoreoutHas.remarks = data['products'][i]['remarks']
      tempstoreoutHas.save()
    return Response(status=status.HTTP_201_CREATED)


class RequestViewSet(viewsets.ModelViewSet):
  queryset=models.RequestForm.objects.all()
  serializer_class=serializers.RequestFormSerializer


  def create(self, request, *args, **kwargs):
    data = request.data
    temprequest=models.RequestForm()
    temprequest.request_number=data['request_number']
    temprequest.request_aawa=data['request_aawa']
    # temprequest.hospital=data['hospital']
    temprequest.created_by=data['created_by']
    temprequest.save()
    for i in range(len(data['products'])):
      temprequestproduct=models.RequestProduct()
      temprequestproduct.name=data['products'][i]['name']
      temprequestproduct.unit=data['products'][i]['unit']
      temprequestproduct.quantity=data['products'][i]['quantity']
      temprequestproduct.specification=data['products'][i]['specification']
      temprequestproduct.remarks=data['products'][i]['remarks']
      temprequestproduct.save()
      temprequest.product.add(temprequestproduct)
    return Response(status=status.HTTP_201_CREATED)

