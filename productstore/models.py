from django.db import models
from hospital.models import Hospital
from django.contrib.auth.models import User
# Create your models here.
from simple_history.models import HistoricalRecords

class ProductCategory(models.Model):
  name = models.CharField(max_length=150)
  code = models.CharField(max_length=20,default='')
  hospital = models.ForeignKey(
    Hospital,
    on_delete=models.PROTECT,
    default=None
  )
  created_at = models.DateField(auto_now_add=True)
  updated_at = models.DateField(auto_now=True)
  created_by = models.CharField(max_length=6)
  history=HistoricalRecords()
  def hospital_name(self):
    return self.hospital.name

class Product(models.Model):
  code = models.CharField(max_length=20,default='')
  name = models.CharField(max_length=150)
  type = models.CharField(max_length=150)
  category = models.ForeignKey(ProductCategory,on_delete=models.PROTECT)
  unit = models.CharField(max_length=10)
  rate=models.IntegerField(default=0)
  quantity = models.IntegerField(default=0)
  specification = models.TextField()
  hospital = models.ForeignKey(
    Hospital,
    on_delete=models.PROTECT,
    default=None
  )
  created_at = models.DateField(auto_now_add=True)
  updated_at = models.DateField(auto_now=True)
  created_by = models.CharField(max_length=6)
  history=HistoricalRecords()

  def __str__(self):
      return self.name
  def category_name(self):
    return self.category.name
  def hospital_name(self):
    return self.hospital.name


class SellerDetails(models.Model):
  name = models.CharField(max_length=150)
  address = models.CharField(max_length=150)
  registration_number = models.CharField(max_length=20)
  pan_number = models.CharField(max_length=20)
  phone_number = models.CharField(max_length=20)
  # hospital = models.ForeignKey(
  #   Hospital,
  #   on_delete=models.PROTECT,
  #   default=None,
  #   blank=True, null=True
  # )
  created_at = models.DateField(auto_now_add=True)
  updated_at = models.DateField(auto_now=True)
  history=HistoricalRecords()

  # created_by = models.CharField(max_length=6,default=None)

  def __str__(self):
      return self.name


class ProductStoreIn(models.Model):
  code = models.CharField(max_length=10, default=None, blank=True, null=True)
  seller_details = models.ForeignKey(
    SellerDetails,
    on_delete=models.PROTECT,
    default=None
  )
  purchase_number = models.CharField(max_length=20)
  purchase_date = models.DateField(default=None)
  purchase_decision_number = models.CharField(max_length=20)
  purchase_decision_date = models.DateField(default=None)
  history=HistoricalRecords()
  total = models.FloatField()
  # hospital = models.ForeignKey(
  #   Hospital,
  #   on_delete=models.PROTECT,
  #   default=None,
  #   blank=True, null=True
  # )
  created_at = models.DateField(auto_now_add=True)
  updated_at = models.DateField(auto_now=True)

  # created_by = models.CharField(max_length=6,default=None)

  def seller_name(self):
    return self.seller_details.name


class StoreInHasProducts(models.Model):
  product_commodity_number = models.CharField(max_length=20)
  product = models.ForeignKey(
    Product,
    on_delete=models.CASCADE
  )
  store_in = models.ForeignKey(
    ProductStoreIn,
    on_delete=models.CASCADE
  )
  quantity = models.FloatField()
  rate = models.FloatField()
  remarks = models.CharField(max_length=150)
  history=HistoricalRecords()
class ProductStoreOut(models.Model):
  invoice_out_number = models.CharField(max_length=20)
  # hospital = models.ForeignKey(
  #   Hospital,
  #   on_delete=models.PROTECT,
  #   default=None
  # )
  created_at = models.DateField(auto_now_add=True)
  updated_at = models.DateField(auto_now=True)
  history=HistoricalRecords()
  created_by = models.CharField(max_length=6)

class StoreOutHasProduct(models.Model):
  product_commodity_number = models.CharField(max_length=20)
  product = models.ForeignKey(
    Product,
    on_delete=models.CASCADE
  )
  store_out = models.ForeignKey(
    ProductStoreOut,
    on_delete=models.CASCADE
  )
  quantity = models.FloatField()
  rate = models.FloatField()
  history=HistoricalRecords()
  remarks = models.CharField(max_length=150)


class RequestProduct(models.Model):
  name = models.CharField(max_length=150)
  unit = models.CharField(max_length=10)
  quantity = models.IntegerField(default=0)
  specification = models.TextField()
  history=HistoricalRecords()
  remarks = models.CharField(max_length=150)

class RequestForm(models.Model):
  product = models.ManyToManyField(RequestProduct)
  request_number=models.IntegerField(default=0)
  request_aawa=models.CharField(max_length=150,default=None)
  # hospital = models.ForeignKey(
  #   Hospital,
  #   on_delete=models.PROTECT,
  #   default=None
  # )
  created_at = models.DateField(auto_now_add=True)
  updated_at = models.DateField(auto_now=True)
  history=HistoricalRecords()
  created_by = models.CharField(max_length=6)
