from django.shortcuts import render
from rest_framework import viewsets,decorators,filters
from rest_framework.pagination import PageNumberPagination
from . import serializers
from .models import Doctors,DoctorsHistory
from hospital.models import Hospital
from rest_framework.permissions import IsAuthenticated
from rest_framework.settings import api_settings
from rest_framework import status
from django.contrib.auth.models import User,Group
from rest_framework.response import Response
from department.models import Departments

def ownGenerator(type):
  typ= Hospital.objects.filter(id=type)[0].code if Hospital.objects.filter(id=type).exists() else  '0001'
  typ = typ[2:]
  no = Doctors.objects.count()
  if no == 0:
    return 'D'+typ+'0001'
  else:
    s = Doctors.objects.filter(hospital_id=type).count()
    if(s == 0):
      return 'D'+typ+'0001'
    else:
      a='000'
      st = str(s+1)
      i = len(st)
      while i< 4:
        st='0'+st
        i=i+1

      return 'D'+typ+st

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000

# Create your views here.
class DoctorsViewset(viewsets.ModelViewSet):
    queryset = Doctors.objects.all()
    serializer_class = serializers.DoctorsSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['dname']
    #permission_classes = (IsAuthenticated,)
    def get_queryset(self):
        queryset = Doctors.objects.all()
        if not self.request.user.is_anonymous:
            hos = Hospital.objects.filter(user=self.request.user)
            if (hos.exists()):
                queryset = Doctors.objects.filter(hospital_id=hos[0].id)
        return queryset
    # @decorators.action(detail=False,methods=['GET'])

    def create(self, request, *args, **kwargs):
        data = request.data
        data['code'] = ownGenerator(data['hospital'])
        print(data)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer,request)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer,request):
        data=request.data
        print(data)
        temp=DoctorsHistory()
        temp.hospital=Hospital.objects.get(id=data['hospital'])
        temp.dname=data['dname']
        temp.dnmc=data['dnmc']
        temp.dphone=data['dphone']
        # temp.year=data['year']
        temp.action="create"
        temp.actionBy=request.user
        temp.ddepart=Departments.objects.get(id=data['ddepart'])
        temp.dcode=data['dcode']
        temp.save()
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


    def profile(self,request,*args,**kwargs):
        if Doctors.objects.filter(user=request.user).exists():
            hospital = Doctors.objects.filter(user=request.user)[0]
            return Response({
                'dname':hospital.dname,
                'ddepart':hospital.ddepart.id,
                'id':hospital.id,
                'user':request.user.id,
                'demail':hospital.demail,
                'dnmc':hospital.dnmc,
                'dphone':hospital.dphone,
                'hospital':hospital.hospital.id,
                # 'location':hospital.location,
                'created_by':hospital.dcreated_by
            })
        return Response({
            'name':'Alish'
        })

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        tempdoc=Doctors.objects.get(id=kwargs.get('pk'))
        temp=DoctorsHistory()
        temp.hospital=tempdoc.hospital
        temp.dname=tempdoc.dname
        temp.dnmc=tempdoc.dnmc
        temp.dphone=tempdoc.dphone
        # temp.year=data['year']
        temp.action="edit"
        temp.actionBy=request.user
        temp.ddepart=tempdoc.ddepart
        temp.dcode=tempdoc.dcode
        temp.save()
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)



    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.GET['remark']:
            instance.changeReason = request.GET['remark']
        tempdoc=Doctors.objects.get(id=kwargs.get('pk'))
        temp=DoctorsHistory()
        temp.hospital=tempdoc.hospital
        temp.dname=tempdoc.dname
        temp.dnmc=tempdoc.dnmc
        temp.dphone=tempdoc.dphone
        # temp.year=data['year']
        temp.action="delete"
        temp.actionBy=request.user
        temp.ddepart=tempdoc.ddepart
        temp.dcode=tempdoc.dcode
        temp.save()
        instance.delete()
        return Response({'success':True})



class DoctorsHistoryViewset(viewsets.ModelViewSet):
    queryset=DoctorsHistory.objects.all()
    serializer_class=serializers.DoctorsHistorySerializer
