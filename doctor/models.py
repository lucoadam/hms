from django.db import models
from django.contrib.auth.models import User
from department.models import Departments
from login.models import Year
from datetime import date
import datetime
from hospital.models import Hospital
from simple_history.models import HistoricalRecords

def ids():
    no = Doctors.objects.count()
    if no == None:
        return 1
    else:
        return 'D' + str(str(datetime.date.today().year)) + str(datetime.date.today().month).zfill(2)+str(no+1)
# Create your models here.
class Doctors(models.Model):

    dcode = models.CharField(max_length = 20,null=True,editable=False)
    dname = models.CharField(max_length=50)
    ddepart = models.ForeignKey(Departments,on_delete=models.CASCADE,default=1)
    dnmc = models.CharField(max_length=50,unique=True)
    dphone = models.CharField(max_length=15,blank=True)
    demail = models.EmailField()
    dcreated = models.DateTimeField( auto_now_add=True)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT,
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1
    )
    history=HistoricalRecords()
    # year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    def __str__(self):
        return self.dname

    def dcreated_at(self):
        return self.dcreated.strftime('%d %B, %Y')

    def department(self):
        return self.ddepart.dname

    def docuid(self):
        return self.user.id

    def hospital_name(self):
        return self.hospital.name

    class Meta:
        db_table = "Doctors"

class DoctorsHistory(models.Model):
    dcode = models.CharField(max_length = 20, default = ids, editable=False)
    dname = models.CharField(max_length=50)
    ddepart = models.ForeignKey(Departments,on_delete=models.CASCADE,default=1)
    dnmc = models.CharField(max_length=50)
    dphone = models.CharField(max_length=15,blank=True)
    demail = models.EmailField()
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT,
    )
    # year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77,blank=True, null=True)
    action=models.CharField(max_length=20)
    actionBy=models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name='action_user'
    )
    actionDate=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.dname

