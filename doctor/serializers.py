from rest_framework import serializers
from . import models
from staffType.models import StaffType
from staff.models import Staff
from django.contrib.auth.models import User,Group
from django.contrib.auth.hashers import make_password
from hospital.models import UserBelongsToHospital

class DoctorsSerializer(serializers.ModelSerializer):
    # user = serializers.CharField(required=False)
    department = serializers.CharField(required=False)
    docuid= serializers.IntegerField(required=False)
    id= serializers.IntegerField(required=False)
    hospital_name = serializers.CharField(required=False)
    code = serializers.CharField(required=False)
    class Meta:
        model = models.Doctors
        fields = "__all__"

    # def create(self, validated_data):
    #     user = User.objects.create(
    #         username=validated_data.get('demail').split('@')[0],
    #         first_name=validated_data.get('dname').split()[0],
    #         last_name=validated_data.get('dname').split()[1],
    #         password=make_password(validated_data.get('dphone'))
    #     )
    #     group = Group.objects.filter(name='ROLE_DOCTOR')[0] if Group.objects.filter(name='ROLE_DOCTOR') else Group.objects.create(name='ROLE_STAFF')
    #     group.user_set.add(user)
    #     doctor = models.Doctors.objects.create(
    #         user=user,
    #         ddepart=validated_data.get('ddepart'),
    #         dname=validated_data.get('dname'),
    #         dnmc=validated_data.get('dnmc'),
    #         dphone=validated_data.get('dphone'),
    #         demail=validated_data.get('demail'),
    #         hospital=validated_data.get('hospital')
    #     )
    #     UserBelongsToHospital.objects.create(
    #         hospital=validated_data.get('hospital'),
    #         user=user
    #     )
    #     return doctor

    def create(self, validated_data):
        request = getattr(self.context, 'request', None)
        doctor = models.Doctors.objects.create(
          ddepart=validated_data.get('ddepart'),
          dcode = validated_data.get('code'),
          dname=validated_data.get('dname'),
          dnmc=validated_data.get('dnmc'),
          dphone=validated_data.get('dphone'),
          demail=validated_data.get('demail'),
          hospital=validated_data.get('hospital'),
        #   year=validated_data.get('year'),
          # dcreated_by = request.user.id if request else '',
          user=User.objects.create(
            username=validated_data.get('code'),
            first_name=validated_data.get('dname').split()[0],
            last_name=validated_data.get('dname').split()[1],
            password=make_password(validated_data.get('dphone'))
          )
        )
        group = Group.objects.filter(name='ROLE_DOCTOR')[0] if Group.objects.filter(name='ROLE_DOCTOR') else Group.objects.create(name='ROLE_DOCTOR')
        group.user_set.add(doctor.user)
        doctor.save()
        UserBelongsToHospital.objects.create(
            hospital=validated_data.get('hospital'),
            user=doctor.user
        )
        return doctor

class DoctorsHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model=models.DoctorsHistory
        fields="__all__"
