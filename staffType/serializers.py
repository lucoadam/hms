from rest_framework import serializers
from .models import StaffType,StaffTypeHistory
import json
class StaffTypeSerializer(serializers.ModelSerializer):
    permissions = serializers.JSONField()
    hospital_name = serializers.CharField(required=False)
    class Meta:
        model = StaffType
        fields="__all__"
    
    def create(self, validated_data):
        st=StaffType(
            tname=validated_data.get('tname'),
            hospital=validated_data.get('hospital'), 
            permissions= json.dumps(validated_data.get('permissions'))
            )
        
        st.save()
        return st

    def update(self, instance: StaffType, validated_data):
      instance.tname = validated_data.get('tname')
      instance.permissions = json.dumps(validated_data.get('permissions'))
      instance.save()
      return instance

class StaffTypeHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model=StaffTypeHistory
        fields="__all__"