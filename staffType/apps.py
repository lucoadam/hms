from django.apps import AppConfig


class StafftypeConfig(AppConfig):
    name = 'staffType'
