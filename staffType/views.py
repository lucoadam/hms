from rest_framework import viewsets,response,filters
from . import serializers
from .models import StaffType,StaffTypeHistory
from rest_framework.pagination import PageNumberPagination
from staff.models import Staff
from hospital.models import Hospital
from rest_framework.permissions import IsAuthenticated
import json
from rest_framework.settings import api_settings
from rest_framework.response import Response
from rest_framework import status


class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000


class StaffTypeViewSet(viewsets.ModelViewSet):
    queryset = StaffType.objects.all()
    serializer_class = serializers.StaffTypeSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['tname']
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        hos = Hospital.objects.filter(user=self.request.user)
        queryset = StaffType.objects.filter(hospital=hos[0]) if hos.exists() else StaffType.objects.all()
        return queryset

    def create(self, request, *args, **kwargs):
        data=request.data
        sttype=StaffTypeHistory(
          tname=data['tname'],
          hospital=Hospital.objects.get(id=data['hospital']), 
          permissions= json.dumps(data['permissions']),
          action='create',
          actionBy=request.user
        )
        sttype.save()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

    def update(self, request, *args, **kwargs):
        stypeOld=StaffType.objects.get(id=kwargs.get('pk'))
        sttype=StaffTypeHistory(
          tname=stypeOld.tname,
          code=stypeOld.code,
          hospital=stypeOld.hospital, 
          permissions= stypeOld.permissions,
          action='edit',
          actionBy=request.user
        )
        sttype.save()
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


    def retrieve(self, request, *args, **kwargs):
      instance = self.get_object()
      serializer = self.get_serializer(instance)
      data = serializer.data
      data['permissions']= json.loads(data['permissions'])
      return response.Response(data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.GET['remark']:
            instance.changeReason = request.GET['remark']
        self.perform_destroy(instance)
        return Response({'success':True})

    def perform_destroy(self, instance):
        instance.delete()


class StaffTypeHistoryViewset(viewsets.ModelViewSet):
    queryset=StaffTypeHistory.objects.all()
    serializer_class=serializers.StaffTypeHistorySerializer