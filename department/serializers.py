from rest_framework import serializers
from . import models


class DepartmentsSerializer(serializers.ModelSerializer):

    hospital_name = serializers.CharField(required=False)

    class Meta:
        model = models.Departments
        fields = "__all__"

class DepartmentHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model=models.DepartmentHistory
        fields="__all__"