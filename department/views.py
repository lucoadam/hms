from django.shortcuts import render
from rest_framework.pagination import PageNumberPagination
from rest_framework import viewsets,filters
from rest_framework.response import Response
from login.serializers import GroupListSerializer
from . import serializers
from django.contrib.auth.models import User,Group
from .models import Departments,DepartmentHistory
from doctor.models import Doctors
from hospital.models import Hospital
from rest_framework.permissions import IsAuthenticated
from rest_framework import response
from rest_framework import status
from rest_framework.settings import api_settings

# Create your views here.
SESSION_KEY = '_auth_user_id'
BACKEND_SESSION_KEY = '_auth_user_backend'
HASH_SESSION_KEY = '_auth_user_hash'
REDIRECT_FIELD_NAME = 'next'

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000

class DepartmentsViewset(viewsets.ModelViewSet):
    queryset = Departments.objects.all()
    serializer_class = serializers.DepartmentsSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['dname']
    # permission_classes = (IsAuthenticated,)
    def get_queryset(self,*args, **kwargs):
        queryset = Departments.objects.all()
        # print(self.request.query_params)
        hos= Hospital.objects.filter(user=self.request.user)
        if hos.exists():
            queryset= Departments.objects.filter(hospital=hos[0])
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data=request.data
        depHist=DepartmentHistory()
        depHist.dname=data['dname']
        depHist.hospital=Hospital.objects.get(id=data['hospital'])
        depHist.action='create'
        depHist.actionBy=request.user
        depHist.save()
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        department=Departments.objects.get(id=kwargs.get('pk'))
        depHist=DepartmentHistory()
        depHist.dname=department.dname
        depHist.hospital=department.hospital
        depHist.action='edit'
        depHist.actionBy=request.user
        depHist.save()
        print(DepartmentHistory.objects.all())
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.GET['remark']:
            instance.changeReason = request.GET['remark']
        if not Doctors.objects.filter(ddepart=kwargs.get('pk')).exists():
            department=Departments.objects.get(id=kwargs.get('pk'))
            depHist=DepartmentHistory()
            depHist.dname=department.dname
            depHist.hospital=department.hospital
            depHist.action='delete'
            depHist.actionBy=request.user
            depHist.save()
            instance.delete()
        return Response({'success':True}) if not Doctors.objects.filter(ddepart=kwargs.get('pk')).exists()  else  Response({'success':False,'message':'Linked with Doctor'})




    def list(self, request, *args, **kwargs):
      self.serializer_class = serializers.DepartmentsSerializer
      queryset = self.filter_queryset(self.get_queryset())

      page = self.paginate_queryset(queryset)
      if page is not None:
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

      serializer = self.get_serializer(queryset, many=True)
    #   print(serializer.data)
      return Response(serializer.data)

class DepartmentsHistoryViewset(viewsets.ModelViewSet):
    queryset=DepartmentHistory.objects.all()
    serializer_class=serializers.DepartmentHistorySerializer