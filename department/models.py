from django.db import models
from hospital.models import Hospital
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords

# Create your models here.
class Departments(models.Model):
    dname = models.CharField(max_length=50)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    history=HistoricalRecords()
    # dcode = models.CharField(max_length=50)
 
    def __str__(self):
        return self.dname

    def hospital_name(self):
        return self.hospital.name

    class Meta:
        db_table="departments"
        unique_together=('dname','hospital')

class DepartmentHistory(models.Model):
    dname = models.CharField(max_length=50,default='skt')
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT,
    )   
    action=models.CharField(max_length=20)
    actionBy=models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )
    actionDate=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.actionBy.username
    