
from rest_framework import viewsets,filters
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from . import serializers,models
from expendituretype.models import ExpenditureType
from .models import Expenditure,ExpenditureHistory
# from bill.models import BillService
from hospital.models import Hospital
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.settings import api_settings

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000

# Create your views here.
class ExpenditureViewSet(viewsets.ModelViewSet):
    queryset = Expenditure.objects.all()
    serializer_class = serializers.ExpenditureSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['paid_by']

    def get_queryset(self):
        queryset = Expenditure.objects.all()
        hos= Hospital.objects.filter(user=self.request.user)
        if hos.exists():
            queryset= Expenditure.objects.filter(hospital=hos[0])
        return queryset

    def create(self, request, *args, **kwargs):
        data=request.data
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        tempExp=ExpenditureHistory()
        tempExp.action='create'
        tempExp.actionBy=request.user
        tempExp.amount=data['amount']
        tempExp.hospital=Hospital.objects.get(id=data['hospital'])
        tempExp.itype=ExpenditureType.objects.get(id=data['itype'])
        tempExp.paid_by=data['paid_by']
        tempExp.save()
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        exptype=Expenditure.objects.get(id=kwargs['pk'])
        tempExp=ExpenditureHistory()
        tempExp.action='edit'
        tempExp.actionBy=request.user
        tempExp.amount=exptype.amount
        tempExp.hospital=exptype.hospital
        tempExp.itype=exptype.itype
        tempExp.paid_by=exptype.paid_by
        tempExp.save()
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.GET['remark']:
            instance.changeReason = request.GET['remark']
        self.perform_destroy(instance)
        return Response({'success':True})

    def perform_destroy(self, instance):
        instance.delete()

class ExpenditureHistoryViewset(viewsets.ModelViewSet):
    queryset=ExpenditureHistory.objects.all()
    serializer_class=serializers.ExpenditureHistorySerializer