from rest_framework import serializers
from . import models
from expendituretype.models import ExpenditureType


def ownGenerator(type):
    typ= ExpenditureType.objects.filter(id=type)[0].code if ExpenditureType.objects.filter(id=type).exists() else  '000'
    no = models.Services.objects.count()
    if no == 0:
        return 'S'+typ+'001'
    else:
        s = models.Services.objects.filter(stype_id=type).count()
        if(s == 0):
            return 'S'+typ + '001'
        else:
            a='000'
            st = str(int(models.Services.objects.filter(stype_id=22)[0].scode[4:])+1)
            i = len(st)
            while i< 3:
                st='0'+st
                i=i+1

            return 'S'+typ+st


class ExpenditureSerializer(serializers.ModelSerializer):
    type= serializers.StringRelatedField()
    hospital_name = serializers.CharField(required=False)
    screated_at = serializers.CharField(required=False)


    # def __init__(self,data,context):
    #     if(context.get('request').method == 'POST'):
    #          data['scode']=ownGenerator(data['stype'])
    #     super(ServicesSerializer, self).__init__(data=data,context=context)

    # def create(self, validated_data):
    #     return validated_data
    class Meta:
        model = models.Expenditure
        fields = [
            'id',
            'itype',
            'paid_by',
            'amount',
            'created_by',
            'itype',
            'type',
            'hospital',
            'hospital_name',
            'year',
          'remarks',
          'screated_at'
        ]

class ExpenditureHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model=models.ExpenditureHistory
        fields="__all__"
