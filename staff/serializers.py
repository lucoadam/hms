from rest_framework import serializers
from . import models
from department.models import Departments
from django.contrib.auth.models import User,Group
from django.contrib.auth.hashers import make_password
from hospital.models import UserBelongsToHospital

class StaffSerializer(serializers.ModelSerializer):
    # user = serializers.CharField(required=False)
    # department = serializers.CharField(required=False)
    # socuid= serializers.IntegerField(required=False)
    # id= serializers.IntegerField(required=False)
    # type= serializers.StringRelatedField()
    hospital_name = serializers.CharField(required=False)
    department= serializers.CharField(required=False)
    type=serializers.CharField(required=False)

    class Meta:
        model = models.Staff
        fields = (
            'id',
            "scode",
            "stype",
            'sname',
            'sdepart',
            'sphone',
            'semail',
            'hospital',
            'hospital_name',
            'department',
            'type',
            'user',
            'year'
        )

    def create(self, validated_data):
        staffHistory=models.StaffHistory(
            sdepart=validated_data.get('sdepart'),
            stype=validated_data.get('stype'),
            sname=validated_data.get('sname'),
            sphone=validated_data.get('sphone'),
            semail=validated_data.get('semail'),
            hospital=validated_data.get('hospital'),
            action='create'

        )
        staff = models.Staff(
          sdepart=validated_data.get('sdepart'),
          stype=validated_data.get('stype'),
          sname=validated_data.get('sname'),
          sphone=validated_data.get('sphone'),
          semail=validated_data.get('semail'),
          hospital=validated_data.get('hospital')
        )

        user = User.objects.create(
            username=staff.scode,
            first_name=validated_data.get('sname').split()[0],
            last_name=validated_data.get('sname').split()[1],
            password=make_password(validated_data.get('sphone'))
        )
        group = Group.objects.filter(name='ROLE_STAFF')[0] if Group.objects.filter(
          name='ROLE_STAFF') else Group.objects.create(name='ROLE_STAFF')
        group.user_set.add(user)
        staff.user = user
        staffHistory.actionBy=user

        UserBelongsToHospital.objects.create(
            hospital=validated_data.get('hospital'),
            user=user
        )
        staffHistory.save()
        staff.save()
        return staff


class StaffHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model=models.StaffHistory
        fields="__all__"