from rest_framework.pagination import PageNumberPagination
from django.shortcuts import render
from rest_framework import viewsets,decorators,response,filters
from . import serializers
from .models import Staff,StaffHistory
from hospital.models import Hospital,UserBelongsToHospital
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework import status

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000
# Create your views here.

class StaffViewset(viewsets.ModelViewSet):
    queryset = Staff.objects.all()
    serializer_class = serializers.StaffSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['sname']
    #permission_classes = (IsAuthenticated,)
    def get_queryset(self):
        queryset = Staff.objects.all()
        if not self.request.user.is_anonymous:
            hos = Hospital.objects.filter(user=self.request.user)
            if (hos.exists()):
                queryset = Staff.objects.filter(hospital_id=hos[0].id)
            else:
                usr = UserBelongsToHospital.objects.filter(user=self.request.user)
                if usr.exists():
                  queryset = Staff.objects.filter(hospital=usr[0].hospital)

        return queryset
    # @decorators.action(detail=False,methods=['GET'])

    def update(self, request, *args, **kwargs):
        staffhist=Staff.objects.get(id=kwargs.get('pk'))
        temp=StaffHistory(
            scode=staffhist.scode,
            sdepart=staffhist.sdepart,
            stype=staffhist.stype,
            sname=staffhist.sname,
            sphone=staffhist.sphone,
            semail=staffhist.semail,
            hospital=staffhist.hospital,
            action='edit',
            actionBy=staffhist.user
        )
        temp.save()
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.GET['remark']:
            instance.changeReason = request.GET['remark']
        self.perform_destroy(instance)
        return Response({'success':True})

    def perform_destroy(self, instance):
        instance.delete()

    @decorators.action(detail=False,methods=['GET'])
    def profile(self,request,*args,**kwargs):

        print(request.user)
        if Staff.objects.filter(user=request.user).exists():
            hospital = Staff.objects.filter(user=request.user)[0]
            return response.Response({
                'sname':hospital.sname,
                'sdepart':hospital.sdepart.id,
                'stype':hospital.stype.id,
                'id':hospital.id,
                'user':request.user.id,
                'semail':hospital.semail,
                'sphone':hospital.sphone,
                'hospital':hospital.hospital.id,
                # 'location':hospital.location,
                'created_by':hospital.screated_by
            })
        return response.Response({
            'sname':'No name'
        })
class StaffHistoryViewset(viewsets.ModelViewSet):
    queryset=StaffHistory.objects.all()
    serializer_class=serializers.StaffHistorySerializer
