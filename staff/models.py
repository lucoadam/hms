from django.db import models
from django.contrib.auth.models import User
from department.models import Departments
from login.models import Year
from staffType.models import StaffType
import datetime
from hospital.models import Hospital
from simple_history.models import HistoricalRecords

def ids():
    no = Staff.objects.count()
    if no == None:
        return 1
    else:
        return str(str(datetime.date.today().year)) + str(datetime.date.today().month).zfill(2)+str(no+1)
# Create your models here.
class Staff(models.Model):

    scode = models.CharField(max_length = 20, default = ids, editable=False)
    stype= models.ForeignKey(StaffType,on_delete=models.CASCADE)
    sname = models.CharField(max_length=50)
    sdepart = models.ForeignKey(Departments,on_delete=models.CASCADE,default=1)
    sphone = models.CharField(max_length=15,blank=True)
    semail = models.EmailField()
    screated = models.DateTimeField( auto_now_add=True)
    screated_by = models.CharField(null=True,max_length=50)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT,
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1
    )
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    history=HistoricalRecords()
    def __str__(self):
        return self.sname

    def dcreated_at(self):
        return self.dcreated.strftime('%d %B, %Y')

    def department(self):
        return self.sdepart.dname

    def docuid(self):
        return self.user.id
    def type(self):
        return self.stype

    def hospital_name(self):
        return self.hospital.name

    class Meta:
        db_table = "Staff"

class StaffHistory(models.Model):
    scode = models.CharField(max_length = 20, default = ids, editable=False)
    stype= models.ForeignKey(StaffType,on_delete=models.CASCADE)
    sname = models.CharField(max_length=50)
    sdepart = models.ForeignKey(Departments,on_delete=models.CASCADE,default=1)
    sphone = models.CharField(max_length=15,blank=True)
    semail = models.EmailField()
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT,
    )
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
  
    action=models.CharField(max_length=20)
    actionBy=models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )
    actionDate=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.actionBy.username
    