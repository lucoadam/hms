from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from rest_framework import viewsets, generics,response,status
from . import serializers
from .models import Bill,BillService
from rest_framework import response
from rest_framework.decorators import action
from rest_framework import parsers
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated


class BillViewset(viewsets.ModelViewSet):
    queryset = Bill.objects.all()
    serializer_class = serializers.BillSerializer
    filter_backends = (DjangoFilterBackend,)
    # permission_classes = (IsAuthenticated,)
    #filterset_fields = ('bcode','patient__pname','patient__pcode','services__scode','services__sname')

    def destroy(self, request, *args, **kwargs):
      instance = self.get_object()
      if request.GET['remark']:
        instance.changeReason = request.GET['remark']
      self.perform_destroy(instance)
      return response.Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
      data = {}
      data['refunded'] = request.data.get('refunded')
      partial = True
      instance = self.get_object()
      serializer = self.get_serializer(instance, data=data, partial=partial)
      serializer.is_valid(raise_exception=True)
      self.perform_update(serializer)
      if getattr(instance, '_prefetched_objects_cache', None):
        # If 'prefetch_related' has been applied to a queryset, we need to
        # forcibly invalidate the prefetch cache on the instance.
        instance._prefetched_objects_cache = {}

      return response.Response(serializer.data)

class BillServiceViewset(viewsets.ModelViewSet):
    queryset = BillService.objects.all()
    serializer_class = serializers.BillSerivceSerializer












