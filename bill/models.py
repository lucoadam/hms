from django.db import models
from services.models import Services
from patients.models import Patients
from login.models import Year
import datetime
from django.core import serializers
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords
# Create your models here.
def xyz():
    no = Bill.objects.count()
    if no == None:
        return 1
    else:
        return 'B' + str(str(datetime.date.today().year)) + str(datetime.date.today().month).zfill(2)+str(no+1)

class Bill(models.Model):

    bcode =  models.CharField(max_length = 20, default = xyz, editable=False)
    patient = models.ForeignKey(Patients,related_name="bills" ,on_delete=models.CASCADE)
    refunded = models.BooleanField(default=False)
    user = models.ForeignKey(User,on_delete=models.CASCADE,default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    total = models.IntegerField(default=0)
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    #bservices = models.ManyToManyField('BillService')
    history = HistoricalRecords()
    def __str__(self):
        return self.bcode
    def created(self):
        return self.created_at.strftime('%Y/%m/%d')
    def created_by(self):
        return self.user.first_name+' '+self.user.last_name;
    def posts(self):
        return self.user.groups.values_list('name', flat=True)[0].split('_')[1]+'('+str(self.user.id)+')'
    @property
    def services(self):
        return BillService.objects.filter(bill=self)

    class Meta:
        db_table = "bills"
        ordering = ["-id"]

class BillService(models.Model):
    service = models.ForeignKey(Services,on_delete=models.CASCADE)
    bill = models.ForeignKey(Bill,on_delete=models.CASCADE)
    sprice = models.IntegerField(null=True)
    squantity = models.IntegerField(null=True)
    history = HistoricalRecords()
    def __str__(self):
        return str(self.id)
    def sname(self):
        return self.service

    class Meta:
        db_table = "bill_services"
        ordering = ["-id"]
