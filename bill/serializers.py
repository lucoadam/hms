from rest_framework import serializers
from services.models import Services
from . import models

class BillSerivceSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    sname = serializers.StringRelatedField(required=False)

    #service =  serializers.StringRelatedField(required=True)
    class Meta:
        model = models.BillService
        # fields = "__all__"
        fields = [
            "id",
            "sprice",
            "squantity",
            "service",
            'sname',
        ]
        read_only_fields =  ('bill',)

class BillSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    services = BillSerivceSerializer(many=True)
    created = serializers.StringRelatedField()
    created_by = serializers.StringRelatedField()
    posts = serializers.StringRelatedField()
    class Meta:
        model = models.Bill
        fields = [
            "id",
            "bcode",
            "user",
            "created_at",
            "services",
            "total",
            'created',
            'created_by',
            'posts',
            'year',
            'refunded'
        ]
        read_only_fields = ('patient',)



# class NewServiceSerializers(serializers.ModelSerializer):
#     choices = BillSerializer(many=True)
#     class Meta:
#         model=models.Services
#         fields=["scode",
#                 "sname",
#                 "sprice",
#                 "stype",
#                 "choices",
#         ]

#     def create(self,**validated_data):
#         choices = validated_data.pop('choices')
#         services = Services.objects.create(**validate_data)
#         for choice in choices:
#             choice.objects.create(**choice,service=service)
#         return service

#     def update(self,instance,**validated_data):
#         choices = validated_data.pop('choices')
#         instance.sname = validated_data.get("sname",instance.sname)
#         instance.save()
#         keep_choices =[]
#         existing_scodes = [c.scode for c in instances.choices]
#         for choice in choices:
#             if "scode" in choice.key():
#                 if choice.objects.filter(id=choice["scode"]).exists():
#                     c=choice.objects.get(scode=choice["scode"])
#                     c.text = choice.get('text',c.text)
#                     c.save()
#                     keep_choices.append(c)
#                 else:
#                     continue
#             else:
#                 c= choice.objects.create(**choice,service=service)
#                 keep_choices.append(c.scode)

#         for choice in instance.choices:
#             if choice.scode not in keep_choices:
#                 choice.delete()

#         return instances




