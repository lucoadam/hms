--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Ubuntu 12.2-4)
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Doctors; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public."Doctors" (
    id integer NOT NULL,
    dcode character varying(20),
    dname character varying(50) NOT NULL,
    dnmc character varying(50) NOT NULL,
    dphone character varying(15) NOT NULL,
    demail character varying(254) NOT NULL,
    dcreated timestamp with time zone NOT NULL,
    ddepart_id integer NOT NULL,
    hospital_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public."Doctors" OWNER TO hos;

--
-- Name: Doctors_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public."Doctors_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Doctors_id_seq" OWNER TO hos;

--
-- Name: Doctors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public."Doctors_id_seq" OWNED BY public."Doctors".id;


--
-- Name: Patient; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public."Patient" (
    id integer NOT NULL,
    pcode character varying(20) NOT NULL,
    pname character varying(50) NOT NULL,
    page integer NOT NULL,
    psex character varying(6) NOT NULL,
    paddress character varying(100) NOT NULL,
    pphone bigint,
    pcreated_by character varying(50),
    pcreated_at timestamp with time zone NOT NULL,
    referred_by integer NOT NULL,
    hospital_id integer NOT NULL,
    year_id integer NOT NULL
);


ALTER TABLE public."Patient" OWNER TO hos;

--
-- Name: Patient_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public."Patient_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Patient_id_seq" OWNER TO hos;

--
-- Name: Patient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public."Patient_id_seq" OWNED BY public."Patient".id;


--
-- Name: Services; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public."Services" (
    id integer NOT NULL,
    scode character varying(20) NOT NULL,
    sname character varying(50) NOT NULL,
    sprice double precision NOT NULL,
    screated timestamp with time zone NOT NULL,
    screated_by character varying(50),
    hospital_id integer NOT NULL,
    stype_id integer NOT NULL,
    year_id integer NOT NULL
);


ALTER TABLE public."Services" OWNER TO hos;

--
-- Name: Services_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public."Services_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Services_id_seq" OWNER TO hos;

--
-- Name: Services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public."Services_id_seq" OWNED BY public."Services".id;


--
-- Name: Staff; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public."Staff" (
    id integer NOT NULL,
    scode character varying(20) NOT NULL,
    sname character varying(50) NOT NULL,
    sphone character varying(15) NOT NULL,
    semail character varying(254) NOT NULL,
    screated timestamp with time zone NOT NULL,
    screated_by character varying(50),
    hospital_id integer NOT NULL,
    sdepart_id integer NOT NULL,
    stype_id integer NOT NULL,
    user_id integer NOT NULL,
    year_id integer NOT NULL
);


ALTER TABLE public."Staff" OWNER TO hos;

--
-- Name: Staff_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public."Staff_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Staff_id_seq" OWNER TO hos;

--
-- Name: Staff_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public."Staff_id_seq" OWNED BY public."Staff".id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO hos;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO hos;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO hos;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO hos;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO hos;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO hos;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO hos;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO hos;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO hos;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO hos;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO hos;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO hos;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: bill_historicalbill; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.bill_historicalbill (
    id integer NOT NULL,
    bcode character varying(20) NOT NULL,
    refunded boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    total integer NOT NULL,
    history_id integer NOT NULL,
    history_date timestamp with time zone NOT NULL,
    history_change_reason character varying(100),
    history_type character varying(1) NOT NULL,
    history_user_id integer,
    patient_id integer,
    user_id integer,
    year_id integer
);


ALTER TABLE public.bill_historicalbill OWNER TO hos;

--
-- Name: bill_historicalbill_history_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.bill_historicalbill_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bill_historicalbill_history_id_seq OWNER TO hos;

--
-- Name: bill_historicalbill_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.bill_historicalbill_history_id_seq OWNED BY public.bill_historicalbill.history_id;


--
-- Name: bill_historicalbillservice; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.bill_historicalbillservice (
    id integer NOT NULL,
    sprice integer,
    squantity integer,
    history_id integer NOT NULL,
    history_date timestamp with time zone NOT NULL,
    history_change_reason character varying(100),
    history_type character varying(1) NOT NULL,
    bill_id integer,
    history_user_id integer,
    service_id integer
);


ALTER TABLE public.bill_historicalbillservice OWNER TO hos;

--
-- Name: bill_historicalbillservice_history_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.bill_historicalbillservice_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bill_historicalbillservice_history_id_seq OWNER TO hos;

--
-- Name: bill_historicalbillservice_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.bill_historicalbillservice_history_id_seq OWNED BY public.bill_historicalbillservice.history_id;


--
-- Name: bill_services; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.bill_services (
    id integer NOT NULL,
    sprice integer,
    squantity integer,
    bill_id integer NOT NULL,
    service_id integer NOT NULL
);


ALTER TABLE public.bill_services OWNER TO hos;

--
-- Name: bill_services_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.bill_services_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bill_services_id_seq OWNER TO hos;

--
-- Name: bill_services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.bill_services_id_seq OWNED BY public.bill_services.id;


--
-- Name: bills; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.bills (
    id integer NOT NULL,
    bcode character varying(20) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    total integer NOT NULL,
    patient_id integer NOT NULL,
    user_id integer NOT NULL,
    year_id integer NOT NULL,
    refunded boolean NOT NULL
);


ALTER TABLE public.bills OWNER TO hos;

--
-- Name: bills_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.bills_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bills_id_seq OWNER TO hos;

--
-- Name: bills_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.bills_id_seq OWNED BY public.bills.id;


--
-- Name: department_departmenthistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.department_departmenthistory (
    id integer NOT NULL,
    dname character varying(50) NOT NULL,
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL
);


ALTER TABLE public.department_departmenthistory OWNER TO hos;

--
-- Name: department_departmenthistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.department_departmenthistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.department_departmenthistory_id_seq OWNER TO hos;

--
-- Name: department_departmenthistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.department_departmenthistory_id_seq OWNED BY public.department_departmenthistory.id;


--
-- Name: departments; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.departments (
    id integer NOT NULL,
    dname character varying(50) NOT NULL,
    hospital_id integer NOT NULL
);


ALTER TABLE public.departments OWNER TO hos;

--
-- Name: departments_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.departments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departments_id_seq OWNER TO hos;

--
-- Name: departments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.departments_id_seq OWNED BY public.departments.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO hos;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO hos;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO hos;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO hos;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO hos;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO hos;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO hos;

--
-- Name: doctor_doctorshistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.doctor_doctorshistory (
    id integer NOT NULL,
    dcode character varying(20) NOT NULL,
    dname character varying(50) NOT NULL,
    dnmc character varying(50) NOT NULL,
    dphone character varying(15) NOT NULL,
    demail character varying(254) NOT NULL,
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    ddepart_id integer NOT NULL,
    hospital_id integer NOT NULL
);


ALTER TABLE public.doctor_doctorshistory OWNER TO hos;

--
-- Name: doctor_doctorshistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.doctor_doctorshistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.doctor_doctorshistory_id_seq OWNER TO hos;

--
-- Name: doctor_doctorshistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.doctor_doctorshistory_id_seq OWNED BY public.doctor_doctorshistory.id;


--
-- Name: expenditure_expenditurehistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.expenditure_expenditurehistory (
    id integer NOT NULL,
    amount double precision NOT NULL,
    paid_by character varying(50),
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL,
    itype_id integer NOT NULL
);


ALTER TABLE public.expenditure_expenditurehistory OWNER TO hos;

--
-- Name: expenditure_expenditurehistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.expenditure_expenditurehistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expenditure_expenditurehistory_id_seq OWNER TO hos;

--
-- Name: expenditure_expenditurehistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.expenditure_expenditurehistory_id_seq OWNED BY public.expenditure_expenditurehistory.id;


--
-- Name: expenditure_type; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.expenditure_type (
    id integer NOT NULL,
    tname character varying(50) NOT NULL,
    code character varying(3),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_by character varying(50),
    updated_by character varying(50),
    hospital_id integer NOT NULL
);


ALTER TABLE public.expenditure_type OWNER TO hos;

--
-- Name: expenditure_type_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.expenditure_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expenditure_type_id_seq OWNER TO hos;

--
-- Name: expenditure_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.expenditure_type_id_seq OWNED BY public.expenditure_type.id;


--
-- Name: expendituretype_expendituretypehistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.expendituretype_expendituretypehistory (
    id integer NOT NULL,
    tname character varying(50) NOT NULL,
    code character varying(3),
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL
);


ALTER TABLE public.expendituretype_expendituretypehistory OWNER TO hos;

--
-- Name: expendituretype_expendituretypehistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.expendituretype_expendituretypehistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expendituretype_expendituretypehistory_id_seq OWNER TO hos;

--
-- Name: expendituretype_expendituretypehistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.expendituretype_expendituretypehistory_id_seq OWNED BY public.expendituretype_expendituretypehistory.id;


--
-- Name: expenses; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.expenses (
    id integer NOT NULL,
    amount double precision NOT NULL,
    paid_by character varying(50),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_by character varying(50),
    updated_by character varying(50),
    hospital_id integer NOT NULL,
    itype_id integer NOT NULL,
    year_id integer NOT NULL,
    remarks text
);


ALTER TABLE public.expenses OWNER TO hos;

--
-- Name: expenses_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.expenses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expenses_id_seq OWNER TO hos;

--
-- Name: expenses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.expenses_id_seq OWNED BY public.expenses.id;


--
-- Name: hospital_has_users; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.hospital_has_users (
    id integer NOT NULL,
    created_at date NOT NULL,
    hospital_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.hospital_has_users OWNER TO hos;

--
-- Name: hospital_has_users_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.hospital_has_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hospital_has_users_id_seq OWNER TO hos;

--
-- Name: hospital_has_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.hospital_has_users_id_seq OWNED BY public.hospital_has_users.id;


--
-- Name: hospitals; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.hospitals (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    phone character varying(15) NOT NULL,
    location character varying(50) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    created_by character varying(1) NOT NULL,
    user_id integer NOT NULL,
    code character varying(20),
    ward_id integer NOT NULL
);


ALTER TABLE public.hospitals OWNER TO hos;

--
-- Name: hospitals_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.hospitals_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hospitals_id_seq OWNER TO hos;

--
-- Name: hospitals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.hospitals_id_seq OWNED BY public.hospitals.id;


--
-- Name: income_incomehistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.income_incomehistory (
    id integer NOT NULL,
    amount double precision NOT NULL,
    received_by character varying(50),
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL,
    itype_id integer NOT NULL
);


ALTER TABLE public.income_incomehistory OWNER TO hos;

--
-- Name: income_incomehistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.income_incomehistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.income_incomehistory_id_seq OWNER TO hos;

--
-- Name: income_incomehistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.income_incomehistory_id_seq OWNED BY public.income_incomehistory.id;


--
-- Name: income_type; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.income_type (
    id integer NOT NULL,
    tname character varying(50) NOT NULL,
    code character varying(3),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_by character varying(50),
    updated_by character varying(50),
    hospital_id integer NOT NULL
);


ALTER TABLE public.income_type OWNER TO hos;

--
-- Name: income_type_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.income_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.income_type_id_seq OWNER TO hos;

--
-- Name: income_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.income_type_id_seq OWNED BY public.income_type.id;


--
-- Name: incomes; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.incomes (
    id integer NOT NULL,
    amount double precision NOT NULL,
    received_by character varying(50),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_by character varying(50),
    updated_by character varying(50),
    hospital_id integer NOT NULL,
    itype_id integer NOT NULL,
    year_id integer NOT NULL,
    remarks text
);


ALTER TABLE public.incomes OWNER TO hos;

--
-- Name: incomes_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.incomes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.incomes_id_seq OWNER TO hos;

--
-- Name: incomes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.incomes_id_seq OWNED BY public.incomes.id;


--
-- Name: incometype_incometypehistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.incometype_incometypehistory (
    id integer NOT NULL,
    tname character varying(50) NOT NULL,
    code character varying(3),
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL
);


ALTER TABLE public.incometype_incometypehistory OWNER TO hos;

--
-- Name: incometype_incometypehistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.incometype_incometypehistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.incometype_incometypehistory_id_seq OWNER TO hos;

--
-- Name: incometype_incometypehistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.incometype_incometypehistory_id_seq OWNED BY public.incometype_incometypehistory.id;


--
-- Name: login_rolepermissions; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.login_rolepermissions (
    id integer NOT NULL,
    permissions text NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.login_rolepermissions OWNER TO hos;

--
-- Name: login_rolepermissions_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.login_rolepermissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.login_rolepermissions_id_seq OWNER TO hos;

--
-- Name: login_rolepermissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.login_rolepermissions_id_seq OWNED BY public.login_rolepermissions.id;


--
-- Name: medicines; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.medicines (
    id integer NOT NULL,
    name text,
    quantity integer NOT NULL,
    pack_unit character varying(5) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_by_id integer NOT NULL,
    batch character varying(15),
    code character varying(15)
);


ALTER TABLE public.medicines OWNER TO hos;

--
-- Name: medicines_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.medicines_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.medicines_id_seq OWNER TO hos;

--
-- Name: medicines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.medicines_id_seq OWNED BY public.medicines.id;


--
-- Name: metas_metas; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.metas_metas (
    id integer NOT NULL,
    key character varying(20) NOT NULL,
    value character varying(50) NOT NULL,
    hospital_id integer
);


ALTER TABLE public.metas_metas OWNER TO hos;

--
-- Name: metas_metas_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.metas_metas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metas_metas_id_seq OWNER TO hos;

--
-- Name: metas_metas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.metas_metas_id_seq OWNED BY public.metas_metas.id;


--
-- Name: patients_historicalpatients; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.patients_historicalpatients (
    id integer NOT NULL,
    pcode character varying(20) NOT NULL,
    pname character varying(50) NOT NULL,
    page integer NOT NULL,
    psex character varying(6) NOT NULL,
    paddress character varying(100) NOT NULL,
    pphone bigint,
    pcreated_by character varying(50),
    pcreated_at timestamp with time zone NOT NULL,
    referred_by integer NOT NULL,
    history_id integer NOT NULL,
    history_date timestamp with time zone NOT NULL,
    history_change_reason character varying(100),
    history_type character varying(1) NOT NULL,
    history_user_id integer,
    hospital_id integer,
    year_id integer
);


ALTER TABLE public.patients_historicalpatients OWNER TO hos;

--
-- Name: patients_historicalpatients_history_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.patients_historicalpatients_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.patients_historicalpatients_history_id_seq OWNER TO hos;

--
-- Name: patients_historicalpatients_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.patients_historicalpatients_history_id_seq OWNED BY public.patients_historicalpatients.history_id;


--
-- Name: pharmacies; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.pharmacies (
    id integer NOT NULL,
    pan character varying(10) NOT NULL,
    phone character varying(15) NOT NULL,
    location character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    created_by character varying(1) NOT NULL,
    hospital_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.pharmacies OWNER TO hos;

--
-- Name: pharmacies_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.pharmacies_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pharmacies_id_seq OWNER TO hos;

--
-- Name: pharmacies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.pharmacies_id_seq OWNED BY public.pharmacies.id;


--
-- Name: productstore_product; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_product (
    id integer NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(150) NOT NULL,
    type character varying(150) NOT NULL,
    unit character varying(10) NOT NULL,
    quantity integer NOT NULL,
    specification text NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    created_by character varying(6) NOT NULL,
    category_id integer NOT NULL,
    hospital_id integer NOT NULL,
    rate integer NOT NULL
);


ALTER TABLE public.productstore_product OWNER TO hos;

--
-- Name: productstore_product_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_product_id_seq OWNER TO hos;

--
-- Name: productstore_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_product_id_seq OWNED BY public.productstore_product.id;


--
-- Name: productstore_productcategory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_productcategory (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    code character varying(20) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    created_by character varying(6) NOT NULL,
    hospital_id integer NOT NULL
);


ALTER TABLE public.productstore_productcategory OWNER TO hos;

--
-- Name: productstore_productcategory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_productcategory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_productcategory_id_seq OWNER TO hos;

--
-- Name: productstore_productcategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_productcategory_id_seq OWNED BY public.productstore_productcategory.id;


--
-- Name: productstore_productstorein; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_productstorein (
    id integer NOT NULL,
    code character varying(10),
    purchase_number character varying(20) NOT NULL,
    purchase_date date NOT NULL,
    purchase_decision_number character varying(20) NOT NULL,
    purchase_decision_date date NOT NULL,
    total double precision NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    seller_details_id integer NOT NULL
);


ALTER TABLE public.productstore_productstorein OWNER TO hos;

--
-- Name: productstore_productstorein_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_productstorein_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_productstorein_id_seq OWNER TO hos;

--
-- Name: productstore_productstorein_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_productstorein_id_seq OWNED BY public.productstore_productstorein.id;


--
-- Name: productstore_productstoreout; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_productstoreout (
    id integer NOT NULL,
    invoice_out_number character varying(20) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    created_by character varying(6) NOT NULL
);


ALTER TABLE public.productstore_productstoreout OWNER TO hos;

--
-- Name: productstore_productstoreout_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_productstoreout_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_productstoreout_id_seq OWNER TO hos;

--
-- Name: productstore_productstoreout_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_productstoreout_id_seq OWNED BY public.productstore_productstoreout.id;


--
-- Name: productstore_requestform; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_requestform (
    id integer NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL,
    created_by character varying(6) NOT NULL,
    request_aawa character varying(150) NOT NULL,
    request_number integer NOT NULL
);


ALTER TABLE public.productstore_requestform OWNER TO hos;

--
-- Name: productstore_requestform_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_requestform_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_requestform_id_seq OWNER TO hos;

--
-- Name: productstore_requestform_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_requestform_id_seq OWNED BY public.productstore_requestform.id;


--
-- Name: productstore_requestform_product; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_requestform_product (
    id integer NOT NULL,
    requestform_id integer NOT NULL,
    requestproduct_id integer NOT NULL
);


ALTER TABLE public.productstore_requestform_product OWNER TO hos;

--
-- Name: productstore_requestform_product_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_requestform_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_requestform_product_id_seq OWNER TO hos;

--
-- Name: productstore_requestform_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_requestform_product_id_seq OWNED BY public.productstore_requestform_product.id;


--
-- Name: productstore_requestproduct; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_requestproduct (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    unit character varying(10) NOT NULL,
    quantity integer NOT NULL,
    specification text NOT NULL,
    remarks character varying(150) NOT NULL
);


ALTER TABLE public.productstore_requestproduct OWNER TO hos;

--
-- Name: productstore_requestproduct_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_requestproduct_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_requestproduct_id_seq OWNER TO hos;

--
-- Name: productstore_requestproduct_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_requestproduct_id_seq OWNED BY public.productstore_requestproduct.id;


--
-- Name: productstore_sellerdetails; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_sellerdetails (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    address character varying(150) NOT NULL,
    registration_number character varying(20) NOT NULL,
    pan_number character varying(20) NOT NULL,
    phone_number character varying(20) NOT NULL,
    created_at date NOT NULL,
    updated_at date NOT NULL
);


ALTER TABLE public.productstore_sellerdetails OWNER TO hos;

--
-- Name: productstore_sellerdetails_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_sellerdetails_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_sellerdetails_id_seq OWNER TO hos;

--
-- Name: productstore_sellerdetails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_sellerdetails_id_seq OWNED BY public.productstore_sellerdetails.id;


--
-- Name: productstore_storeinhasproducts; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_storeinhasproducts (
    id integer NOT NULL,
    product_commodity_number character varying(20) NOT NULL,
    quantity double precision NOT NULL,
    rate double precision NOT NULL,
    remarks character varying(150) NOT NULL,
    product_id integer NOT NULL,
    store_in_id integer NOT NULL
);


ALTER TABLE public.productstore_storeinhasproducts OWNER TO hos;

--
-- Name: productstore_storeinhasproducts_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_storeinhasproducts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_storeinhasproducts_id_seq OWNER TO hos;

--
-- Name: productstore_storeinhasproducts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_storeinhasproducts_id_seq OWNED BY public.productstore_storeinhasproducts.id;


--
-- Name: productstore_storeouthasproduct; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.productstore_storeouthasproduct (
    id integer NOT NULL,
    product_commodity_number character varying(20) NOT NULL,
    quantity double precision NOT NULL,
    rate double precision NOT NULL,
    remarks character varying(150) NOT NULL,
    product_id integer NOT NULL,
    store_out_id integer NOT NULL
);


ALTER TABLE public.productstore_storeouthasproduct OWNER TO hos;

--
-- Name: productstore_storeouthasproduct_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.productstore_storeouthasproduct_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productstore_storeouthasproduct_id_seq OWNER TO hos;

--
-- Name: productstore_storeouthasproduct_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.productstore_storeouthasproduct_id_seq OWNED BY public.productstore_storeouthasproduct.id;


--
-- Name: service_type; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.service_type (
    id integer NOT NULL,
    tname character varying(50) NOT NULL,
    hospital_id integer NOT NULL,
    code character varying(5),
    is_incentive boolean NOT NULL
);


ALTER TABLE public.service_type OWNER TO hos;

--
-- Name: services_serviceshistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.services_serviceshistory (
    id integer NOT NULL,
    scode character varying(20) NOT NULL,
    sname character varying(50) NOT NULL,
    sprice double precision NOT NULL,
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL,
    stype_id integer NOT NULL
);


ALTER TABLE public.services_serviceshistory OWNER TO hos;

--
-- Name: services_serviceshistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.services_serviceshistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.services_serviceshistory_id_seq OWNER TO hos;

--
-- Name: services_serviceshistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.services_serviceshistory_id_seq OWNED BY public.services_serviceshistory.id;


--
-- Name: staffType_stafftypehistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public."staffType_stafftypehistory" (
    id integer NOT NULL,
    tname character varying(50) NOT NULL,
    code character varying(3) NOT NULL,
    permissions text NOT NULL,
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL
);


ALTER TABLE public."staffType_stafftypehistory" OWNER TO hos;

--
-- Name: staffType_stafftypehistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public."staffType_stafftypehistory_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."staffType_stafftypehistory_id_seq" OWNER TO hos;

--
-- Name: staffType_stafftypehistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public."staffType_stafftypehistory_id_seq" OWNED BY public."staffType_stafftypehistory".id;


--
-- Name: staff_staffhistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.staff_staffhistory (
    id integer NOT NULL,
    scode character varying(20) NOT NULL,
    sname character varying(50) NOT NULL,
    sphone character varying(15) NOT NULL,
    semail character varying(254) NOT NULL,
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL,
    sdepart_id integer NOT NULL,
    stype_id integer NOT NULL,
    year_id integer NOT NULL
);


ALTER TABLE public.staff_staffhistory OWNER TO hos;

--
-- Name: staff_staffhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.staff_staffhistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staff_staffhistory_id_seq OWNER TO hos;

--
-- Name: staff_staffhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.staff_staffhistory_id_seq OWNED BY public.staff_staffhistory.id;


--
-- Name: staff_type; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.staff_type (
    id integer NOT NULL,
    tname character varying(50) NOT NULL,
    code character varying(3) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_by character varying(50),
    updated_by character varying(50),
    hospital_id integer NOT NULL,
    permissions text NOT NULL
);


ALTER TABLE public.staff_type OWNER TO hos;

--
-- Name: staff_type_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.staff_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staff_type_id_seq OWNER TO hos;

--
-- Name: staff_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.staff_type_id_seq OWNED BY public.staff_type.id;


--
-- Name: store_productservice; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.store_productservice (
    id integer NOT NULL,
    quantity integer NOT NULL,
    unit_price double precision NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    product_id integer NOT NULL,
    batch character varying(15),
    expiry_date date,
    manufacture_date date,
    packs character varying(10)
);


ALTER TABLE public.store_productservice OWNER TO hos;

--
-- Name: store_productservice_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.store_productservice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.store_productservice_id_seq OWNER TO hos;

--
-- Name: store_productservice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.store_productservice_id_seq OWNED BY public.store_productservice.id;


--
-- Name: store_storeinhistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.store_storeinhistory (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    quantity integer NOT NULL,
    unit_price double precision NOT NULL,
    batch character varying(15),
    pack character varying(10),
    manufacture_date date,
    expiry_date date,
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL,
    year_id integer NOT NULL
);


ALTER TABLE public.store_storeinhistory OWNER TO hos;

--
-- Name: store_storeinhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.store_storeinhistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.store_storeinhistory_id_seq OWNER TO hos;

--
-- Name: store_storeinhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.store_storeinhistory_id_seq OWNED BY public.store_storeinhistory.id;


--
-- Name: store_storeouthistory; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.store_storeouthistory (
    id integer NOT NULL,
    buyer character varying(50),
    action character varying(20) NOT NULL,
    "actionDate" timestamp with time zone NOT NULL,
    "actionBy_id" integer NOT NULL,
    hospital_id integer NOT NULL,
    year_id integer NOT NULL
);


ALTER TABLE public.store_storeouthistory OWNER TO hos;

--
-- Name: store_storeouthistory_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.store_storeouthistory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.store_storeouthistory_id_seq OWNER TO hos;

--
-- Name: store_storeouthistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.store_storeouthistory_id_seq OWNED BY public.store_storeouthistory.id;


--
-- Name: store_storeouthistory_orders; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.store_storeouthistory_orders (
    id integer NOT NULL,
    storeouthistory_id integer NOT NULL,
    productservice_id integer NOT NULL
);


ALTER TABLE public.store_storeouthistory_orders OWNER TO hos;

--
-- Name: store_storeouthistory_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.store_storeouthistory_orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.store_storeouthistory_orders_id_seq OWNER TO hos;

--
-- Name: store_storeouthistory_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.store_storeouthistory_orders_id_seq OWNED BY public.store_storeouthistory_orders.id;


--
-- Name: storein; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.storein (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    quantity integer NOT NULL,
    unit_price double precision NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_by_id integer NOT NULL,
    hospital_id integer NOT NULL,
    year_id integer NOT NULL,
    batch character varying(15),
    expiry_date date,
    manufacture_date date,
    pack character varying(10)
);


ALTER TABLE public.storein OWNER TO hos;

--
-- Name: storein_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.storein_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.storein_id_seq OWNER TO hos;

--
-- Name: storein_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.storein_id_seq OWNED BY public.storein.id;


--
-- Name: storeout; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.storeout (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    created_by_id integer NOT NULL,
    hospital_id integer NOT NULL,
    buyer character varying(50),
    year_id integer NOT NULL
);


ALTER TABLE public.storeout OWNER TO hos;

--
-- Name: storeout_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.storeout_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.storeout_id_seq OWNER TO hos;

--
-- Name: storeout_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.storeout_id_seq OWNED BY public.storeout.id;


--
-- Name: storeout_orders; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.storeout_orders (
    id integer NOT NULL,
    storeout_id integer NOT NULL,
    productservice_id integer NOT NULL
);


ALTER TABLE public.storeout_orders OWNER TO hos;

--
-- Name: storeout_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.storeout_orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.storeout_orders_id_seq OWNER TO hos;

--
-- Name: storeout_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.storeout_orders_id_seq OWNED BY public.storeout_orders.id;


--
-- Name: type_servicehasincentive; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.type_servicehasincentive (
    id integer NOT NULL,
    percentage integer NOT NULL,
    stafftype_id integer NOT NULL,
    type_id integer NOT NULL
);


ALTER TABLE public.type_servicehasincentive OWNER TO hos;

--
-- Name: type_servicehasincentive_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.type_servicehasincentive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_servicehasincentive_id_seq OWNER TO hos;

--
-- Name: type_servicehasincentive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.type_servicehasincentive_id_seq OWNED BY public.type_servicehasincentive.id;


--
-- Name: type_type_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.type_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_type_id_seq OWNER TO hos;

--
-- Name: type_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.type_type_id_seq OWNED BY public.service_type.id;


--
-- Name: wards; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.wards (
    id integer NOT NULL,
    name character varying(20) NOT NULL
);


ALTER TABLE public.wards OWNER TO hos;

--
-- Name: wards_id_seq; Type: SEQUENCE; Schema: public; Owner: hos
--

CREATE SEQUENCE public.wards_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wards_id_seq OWNER TO hos;

--
-- Name: wards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hos
--

ALTER SEQUENCE public.wards_id_seq OWNED BY public.wards.id;


--
-- Name: years; Type: TABLE; Schema: public; Owner: hos
--

CREATE TABLE public.years (
    year integer NOT NULL
);


ALTER TABLE public.years OWNER TO hos;

--
-- Name: Doctors id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Doctors" ALTER COLUMN id SET DEFAULT nextval('public."Doctors_id_seq"'::regclass);


--
-- Name: Patient id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Patient" ALTER COLUMN id SET DEFAULT nextval('public."Patient_id_seq"'::regclass);


--
-- Name: Services id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Services" ALTER COLUMN id SET DEFAULT nextval('public."Services_id_seq"'::regclass);


--
-- Name: Staff id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Staff" ALTER COLUMN id SET DEFAULT nextval('public."Staff_id_seq"'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: bill_historicalbill history_id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_historicalbill ALTER COLUMN history_id SET DEFAULT nextval('public.bill_historicalbill_history_id_seq'::regclass);


--
-- Name: bill_historicalbillservice history_id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_historicalbillservice ALTER COLUMN history_id SET DEFAULT nextval('public.bill_historicalbillservice_history_id_seq'::regclass);


--
-- Name: bill_services id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_services ALTER COLUMN id SET DEFAULT nextval('public.bill_services_id_seq'::regclass);


--
-- Name: bills id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bills ALTER COLUMN id SET DEFAULT nextval('public.bills_id_seq'::regclass);


--
-- Name: department_departmenthistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.department_departmenthistory ALTER COLUMN id SET DEFAULT nextval('public.department_departmenthistory_id_seq'::regclass);


--
-- Name: departments id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.departments ALTER COLUMN id SET DEFAULT nextval('public.departments_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: doctor_doctorshistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.doctor_doctorshistory ALTER COLUMN id SET DEFAULT nextval('public.doctor_doctorshistory_id_seq'::regclass);


--
-- Name: expenditure_expenditurehistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_expenditurehistory ALTER COLUMN id SET DEFAULT nextval('public.expenditure_expenditurehistory_id_seq'::regclass);


--
-- Name: expenditure_type id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_type ALTER COLUMN id SET DEFAULT nextval('public.expenditure_type_id_seq'::regclass);


--
-- Name: expendituretype_expendituretypehistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expendituretype_expendituretypehistory ALTER COLUMN id SET DEFAULT nextval('public.expendituretype_expendituretypehistory_id_seq'::regclass);


--
-- Name: expenses id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenses ALTER COLUMN id SET DEFAULT nextval('public.expenses_id_seq'::regclass);


--
-- Name: hospital_has_users id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.hospital_has_users ALTER COLUMN id SET DEFAULT nextval('public.hospital_has_users_id_seq'::regclass);


--
-- Name: hospitals id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.hospitals ALTER COLUMN id SET DEFAULT nextval('public.hospitals_id_seq'::regclass);


--
-- Name: income_incomehistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_incomehistory ALTER COLUMN id SET DEFAULT nextval('public.income_incomehistory_id_seq'::regclass);


--
-- Name: income_type id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_type ALTER COLUMN id SET DEFAULT nextval('public.income_type_id_seq'::regclass);


--
-- Name: incomes id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incomes ALTER COLUMN id SET DEFAULT nextval('public.incomes_id_seq'::regclass);


--
-- Name: incometype_incometypehistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incometype_incometypehistory ALTER COLUMN id SET DEFAULT nextval('public.incometype_incometypehistory_id_seq'::regclass);


--
-- Name: login_rolepermissions id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.login_rolepermissions ALTER COLUMN id SET DEFAULT nextval('public.login_rolepermissions_id_seq'::regclass);


--
-- Name: medicines id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.medicines ALTER COLUMN id SET DEFAULT nextval('public.medicines_id_seq'::regclass);


--
-- Name: metas_metas id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.metas_metas ALTER COLUMN id SET DEFAULT nextval('public.metas_metas_id_seq'::regclass);


--
-- Name: patients_historicalpatients history_id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.patients_historicalpatients ALTER COLUMN history_id SET DEFAULT nextval('public.patients_historicalpatients_history_id_seq'::regclass);


--
-- Name: pharmacies id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.pharmacies ALTER COLUMN id SET DEFAULT nextval('public.pharmacies_id_seq'::regclass);


--
-- Name: productstore_product id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_product ALTER COLUMN id SET DEFAULT nextval('public.productstore_product_id_seq'::regclass);


--
-- Name: productstore_productcategory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_productcategory ALTER COLUMN id SET DEFAULT nextval('public.productstore_productcategory_id_seq'::regclass);


--
-- Name: productstore_productstorein id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_productstorein ALTER COLUMN id SET DEFAULT nextval('public.productstore_productstorein_id_seq'::regclass);


--
-- Name: productstore_productstoreout id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_productstoreout ALTER COLUMN id SET DEFAULT nextval('public.productstore_productstoreout_id_seq'::regclass);


--
-- Name: productstore_requestform id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestform ALTER COLUMN id SET DEFAULT nextval('public.productstore_requestform_id_seq'::regclass);


--
-- Name: productstore_requestform_product id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestform_product ALTER COLUMN id SET DEFAULT nextval('public.productstore_requestform_product_id_seq'::regclass);


--
-- Name: productstore_requestproduct id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestproduct ALTER COLUMN id SET DEFAULT nextval('public.productstore_requestproduct_id_seq'::regclass);


--
-- Name: productstore_sellerdetails id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_sellerdetails ALTER COLUMN id SET DEFAULT nextval('public.productstore_sellerdetails_id_seq'::regclass);


--
-- Name: productstore_storeinhasproducts id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_storeinhasproducts ALTER COLUMN id SET DEFAULT nextval('public.productstore_storeinhasproducts_id_seq'::regclass);


--
-- Name: productstore_storeouthasproduct id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_storeouthasproduct ALTER COLUMN id SET DEFAULT nextval('public.productstore_storeouthasproduct_id_seq'::regclass);


--
-- Name: service_type id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.service_type ALTER COLUMN id SET DEFAULT nextval('public.type_type_id_seq'::regclass);


--
-- Name: services_serviceshistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.services_serviceshistory ALTER COLUMN id SET DEFAULT nextval('public.services_serviceshistory_id_seq'::regclass);


--
-- Name: staffType_stafftypehistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."staffType_stafftypehistory" ALTER COLUMN id SET DEFAULT nextval('public."staffType_stafftypehistory_id_seq"'::regclass);


--
-- Name: staff_staffhistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_staffhistory ALTER COLUMN id SET DEFAULT nextval('public.staff_staffhistory_id_seq'::regclass);


--
-- Name: staff_type id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_type ALTER COLUMN id SET DEFAULT nextval('public.staff_type_id_seq'::regclass);


--
-- Name: store_productservice id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_productservice ALTER COLUMN id SET DEFAULT nextval('public.store_productservice_id_seq'::regclass);


--
-- Name: store_storeinhistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeinhistory ALTER COLUMN id SET DEFAULT nextval('public.store_storeinhistory_id_seq'::regclass);


--
-- Name: store_storeouthistory id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory ALTER COLUMN id SET DEFAULT nextval('public.store_storeouthistory_id_seq'::regclass);


--
-- Name: store_storeouthistory_orders id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory_orders ALTER COLUMN id SET DEFAULT nextval('public.store_storeouthistory_orders_id_seq'::regclass);


--
-- Name: storein id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storein ALTER COLUMN id SET DEFAULT nextval('public.storein_id_seq'::regclass);


--
-- Name: storeout id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout ALTER COLUMN id SET DEFAULT nextval('public.storeout_id_seq'::regclass);


--
-- Name: storeout_orders id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout_orders ALTER COLUMN id SET DEFAULT nextval('public.storeout_orders_id_seq'::regclass);


--
-- Name: type_servicehasincentive id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.type_servicehasincentive ALTER COLUMN id SET DEFAULT nextval('public.type_servicehasincentive_id_seq'::regclass);


--
-- Name: wards id; Type: DEFAULT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.wards ALTER COLUMN id SET DEFAULT nextval('public.wards_id_seq'::regclass);


--
-- Data for Name: Doctors; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public."Doctors" (id, dcode, dname, dnmc, dphone, demail, dcreated, ddepart_id, hospital_id, user_id) FROM stdin;
1	D010001	DR. BINOD K.C	17559	9841002454	bnods2454@gmail.com	2020-07-14 16:02:39.819305+05:45	3	1	6
2	D010002	DR. ANISH KUMAR SHRESTHA	26596	9818022698	shresthaanish02@gmail.com	2020-07-14 16:03:24.144407+05:45	3	1	7
3	D010003	DR. SISTU K.C	26444	9847073524	melamchiphc@gmail.com	2020-07-14 16:04:24.661225+05:45	3	1	8
\.


--
-- Data for Name: Patient; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public."Patient" (id, pcode, pname, page, psex, paddress, pphone, pcreated_by, pcreated_at, referred_by, hospital_id, year_id) FROM stdin;
4	P2020071	RAMJI BHANDARI	26	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 11:13:46.886461+05:45	1	1	77
7	P2020074	AASHIKA SHRESTHA	27	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 11:39:48.5544+05:45	1	1	77
8	P2020075	TUNA KARKI	55	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 11:54:49.4396+05:45	2	1	77
9	P2020076	RAM K.THAPA	19	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 12:06:56.275753+05:45	2	1	77
10	P2020077	PUTALI RIJAL	50	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 12:35:01.519929+05:45	2	1	77
13	P20200710	ANISHA TAMANG	17	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 12:38:27.320702+05:45	0	1	77
14	P20200711	TULASA KHADKA	67	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 12:40:54.081925+05:45	0	1	77
16	P2020078	RAMRI TAMANG	27	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 14:17:26.969944+05:45	1	1	77
17	P2020079	NISHA KOIRALA	24	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 14:24:04.929821+05:45	0	1	77
19	P20200711	APSARA KARKI	34	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 14:49:38.512405+05:45	0	1	77
21	P20200713	BINITA TAMANG	26	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 15:14:16.73843+05:45	0	1	77
22	P20200714	SUREN GAUTAM	17	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 15:32:51.802631+05:45	0	1	77
24	P20200716	TUNA BAHADUR KARKI	28	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 15:56:18.308234+05:45	1	1	77
25	P20200717	URMILA KHADKA	25	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 16:06:02.979355+05:45	1	1	77
26	P20200718	ANISHA TAMANG	25	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-16 16:11:28.16788+05:45	1	1	77
27	P20200716	DOLMA TAMANG	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 10:41:50.789815+05:45	0	1	77
28	P20200717	BHIM KUMARI PURI	46	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 10:52:52.197288+05:45	3	1	77
29	P20200718	SARITA GHIMIRE	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 11:25:35.760373+05:45	2	1	77
30	P20200719	SABINA MOKTAN	25	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 11:27:58.127662+05:45	2	1	77
31	P20200720	RINA TAMANG	21	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 11:37:15.665414+05:45	2	1	77
32	P20200721	SAJAN THAPA	33	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 11:59:47.897719+05:45	0	1	77
33	P20200722	GOMA ACHARYA	29	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 12:33:03.742639+05:45	2	1	77
34	P20200723	SARMILA SHRESTHA	38	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 12:48:52.380206+05:45	0	1	77
35	P20200724	KALIKA DULAL	43	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 13:34:34.787105+05:45	2	1	77
36	P20200725	ROSHAN SHIVABHAKTI	7	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 14:24:16.599535+05:45	2	1	77
37	P20200726	KUMARI CHAIPAGAN	34	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 14:33:01.498139+05:45	2	1	77
38	P20200727	TILAK SHIVABHAKTI	27	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-17 14:39:06.638469+05:45	2	1	77
39	P20200728	CHINIMAYA TAMANG	25	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 10:23:31.517302+05:45	2	1	77
40	P20200729	RADHA THAPA	47	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 10:26:48.684944+05:45	2	1	77
41	P20200730	MINA DEUJA	53	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 10:30:54.323311+05:45	2	1	77
42	P20200731	AMBAR TAMANG	60	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 10:32:50.598597+05:45	2	1	77
43	P20200732	GOMA KHANAL	25	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 10:35:04.730414+05:45	2	1	77
44	P20200733	ARCHANA BASNET	26	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 10:46:08.767792+05:45	2	1	77
45	P20200734	SHOVA TAMANG	54	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 10:56:08.708867+05:45	2	1	77
46	P20200735	RADHIKA BHARTI	28	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 10:58:34.044421+05:45	2	1	77
47	P20200736	NIRMALA TAMANG	30	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:00:33.002899+05:45	0	1	77
48	P20200737	BINDA KHADKA	39	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:02:56.326613+05:45	2	1	77
49	P20200738	DOLMA DANUWAR	25	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:04:45.89678+05:45	2	1	77
50	P20200739	BEN THAPAMAGAR	49	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:06:33.589685+05:45	0	1	77
51	P20200740	FULMAYA TAMANG	25	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:20:45.729557+05:45	2	1	77
52	P20200741	SUNTALI KATWAL	40	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:22:38.887041+05:45	2	1	77
53	P20200742	THILI BISHANK	53	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:25:32.696576+05:45	2	1	77
54	P20200743	SARITA TAMANG	20	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:27:12.915937+05:45	2	1	77
55	P20200744	CHANAMTI CHALISE	70	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:34:44.785+05:45	2	1	77
56	P20200745	LAXMI TAMANG	24	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 11:57:28.016683+05:45	2	1	77
57	P20200746	RITA KHATI	26	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 12:03:55.816709+05:45	2	1	77
58	P20200747	KOPILA SHRESTHA	38	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 12:10:05.040604+05:45	2	1	77
59	P20200748	LALIMAYA TAMANG	24	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 12:15:07.799477+05:45	2	1	77
60	P20200749	SIRJANA KATWAL	20	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 12:17:27.274923+05:45	2	1	77
61	P20200750	CHIRA K.DHITAL	75	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 12:42:31.241998+05:45	2	1	77
62	P20200751	ROJINA TAMANG	9	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 12:46:16.526175+05:45	2	1	77
63	P20200752	SHANTA GHORASANI	50	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 12:54:08.7744+05:45	2	1	77
64	P20200753	MIN  BDR.NEWAR	74	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 12:56:14.798451+05:45	2	1	77
65	P20200754	PARNISHA LAMA	68	FEMALE	PACHPOKHARI	\N	3	2020-07-19 14:29:25.986211+05:45	2	1	77
66	P20200755	NADA K.NEPALI	65	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-19 15:29:05.51674+05:45	0	1	77
67	P20200756	ASMITA CHALISE	21	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 10:28:54.654424+05:45	2	1	77
68	P20200757	PRAKASH KHADKA	26	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 10:43:04.521092+05:45	3	1	77
69	P20200758	SISIR MIJAR	5	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 11:10:35.142503+05:45	2	1	77
70	P20200759	SABITA MAJHI	27	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 11:26:46.51936+05:45	2	1	77
71	P20200760	BIMALA SHRESTHA	22	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 11:28:49.210398+05:45	2	1	77
72	P20200761	SUNTALI MIJAR	50	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 11:30:33.013+05:45	2	1	77
73	P20200762	SIRJANA DULAL	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 11:43:17.96757+05:45	2	1	77
74	P20200763	SABINA KHATIWADA	25	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 11:45:59.117934+05:45	2	1	77
75	P20200764	LALIMAI TAMANG	20	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 11:58:03.014714+05:45	2	1	77
76	P20200765	SITA NEPALI	29	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 12:02:41.247774+05:45	2	1	77
77	P20200766	KABITA GHORASANI	22	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 12:06:35.233868+05:45	2	1	77
78	P20200767	BIMALA	20	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 14:18:11.442912+05:45	2	1	77
79	P20200768	INDRA BDR.RIJAL	40	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-20 14:31:18.563355+05:45	2	1	77
80	P20200769	ANJANA GIRI	10	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 10:29:40.741856+05:45	2	1	77
81	P77040769	SHOVA ADHAKARI	35	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 10:50:01.048069+05:45	2	1	77
82	P77040769	JANUKA SAPKOTA	50	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 10:56:49.283372+05:45	2	1	77
83	P77040769	GOMA KARKI	28	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 11:25:56.982493+05:45	2	1	77
84	P77040769	PRATIMA SHRESTHA	20	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 11:31:40.338504+05:45	2	1	77
85	P77040769	BHUPENDRA SONI	36	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 11:44:19.841515+05:45	2	1	77
86	P77040769	SUNTALI MIJAR	49	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:01:25.75055+05:45	1	1	77
87	P77040769	SANGITA LAMICHHANE	22	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:06:57.654316+05:45	2	1	77
88	P77040769	SHREERAM KARAKI	28	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:15:19.020283+05:45	1	1	77
89	P77040769	CHAMATI CHALISE	70	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:42:35.195075+05:45	2	1	77
90	P77040769	SABITRI DULAL	37	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:53:11.714225+05:45	2	1	77
91	P77040769	RITA DAHAL	45	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 13:06:54.676476+05:45	0	1	77
92	P77040769	THULIMAYA TAMANG	40	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 14:12:45.071668+05:45	2	1	77
93	P77040769	BINAD DULAL	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 14:15:02.207901+05:45	2	1	77
94	P77040769	ANITA TAMANG	18	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 14:23:10.629375+05:45	2	1	77
95	P77040769	FAUTHA K KARAKI	60	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:19:59.309899+05:45	2	1	77
96	P77040769	PURANAMAYA JYOTI	28	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:22:40.75743+05:45	2	1	77
97	P77040769	DAWA TAMANG	36	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:24:50.916253+05:45	2	1	77
98	P77040769	NETRA K.ACHARYA	60	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:28:03.719641+05:45	2	1	77
99	P77040769	SANGITA DULAL	21	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:29:39.642151+05:45	2	1	77
100	P77040769	BINITA PANDIT	18	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:31:12.97996+05:45	2	1	77
101	P77040769	SITA THAPA	40	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:32:44.185358+05:45	2	1	77
102	P77040769	CHUSANG LAMA	36	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:35:07.632445+05:45	0	1	77
103	P77040769	KOPILA BHUJEL	20	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:38:15.670105+05:45	2	1	77
104	P77040769	NASIR TAMANG	18	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:52:09.019402+05:45	2	1	77
105	P77040769	RAMMAYA GADAILI	22	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 11:14:12.290171+05:45	2	1	77
106	P77040769	RENU K.C	26	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 11:17:53.448708+05:45	2	1	77
107	P77040769	SUNITA THAPA	29	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:38:56.792386+05:45	2	1	77
108	P77040769	RAJ TAMANG	21	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:40:09.106723+05:45	2	1	77
109	P77040769	PRATIGYA TIWARI	26	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:41:37.105162+05:45	2	1	77
110	P77040769	MINA TAMANG	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:43:36.751012+05:45	2	1	77
111	P77040769	BIMALA TAMANG	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:44:41.475424+05:45	2	1	77
112	P77040769	SITA TAMANAG	28	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:47:05.057496+05:45	2	1	77
113	P77040769	SOM BDR,TAMANG	28	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:49:01.385972+05:45	2	1	77
114	P77040769	JIT BDR MIJAR	37	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:52:34.233359+05:45	2	1	77
115	P77040769	SHYAM BDR ACHARYA	69	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:55:03.149097+05:45	2	1	77
116	P77040769	DINESH TAMANG	28	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 13:01:04.032367+05:45	2	1	77
\.


--
-- Data for Name: Services; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public."Services" (id, scode, sname, sprice, screated, screated_by, hospital_id, stype_id, year_id) FROM stdin;
1	S01001001	HAND AP/LAT/OB	350	2020-07-14 16:26:58.557577+05:45	\N	1	1	77
16	S01001002	PELVIS	350	2020-07-14 16:32:16.628878+05:45	\N	1	1	77
17	S01001003	FEMUR-AP/LT	350	2020-07-14 16:32:26.156715+05:45	\N	1	1	77
18	S01001004	KNEE-AP/LAT	350	2020-07-14 16:33:02.689735+05:45	\N	1	1	77
19	S01001005	KNEE-SKYLINE	350	2020-07-14 16:33:13.754414+05:45	\N	1	1	77
20	S01001006	TIBIA-FIBULA-AP/LT	350	2020-07-14 16:34:59.930197+05:45	\N	1	1	77
21	S01001007	ANKLE-AP/LT/MORTISE	350	2020-07-14 16:35:33.297191+05:45	\N	1	1	77
22	S01001008	FOOT-AP/OB	350	2020-07-14 16:35:52.82267+05:45	\N	1	1	77
2	S01001009	WRIST AP/LAT	350	2020-07-14 16:27:40.907854+05:45	\N	1	1	77
3	S01001010	FOREARM AP/LT	350	2020-07-14 16:28:18.206356+05:45	\N	1	1	77
4	S01001011	ELBOW AP/LT	350	2020-07-14 16:28:30.713913+05:45	\N	1	1	77
5	S01001012	HUMERUS AP/LT	350	2020-07-14 16:28:42.625008+05:45	\N	1	1	77
6	S01001013	SHOULDER AP/LAT	350	2020-07-14 16:29:02.694783+05:45	\N	1	1	77
7	S01001014	CLAVICLE	350	2020-07-14 16:29:09.850687+05:45	\N	1	1	77
8	S01001015	CHEST-PA/AP	350	2020-07-14 16:29:33.16757+05:45	\N	1	1	77
9	S01001016	SKULL-LT/PA	350	2020-07-14 16:30:18.311489+05:45	\N	1	1	77
14	S01001017	ABDOMEN-ERECT/SUPINE	350	2020-07-14 16:31:36.903894+05:45	\N	1	1	77
15	S01001018	DORSO-LUMBAR-AP/LT	350	2020-07-14 16:32:03.197995+05:45	\N	1	1	77
10	S01001019	PNS-OM/TOWNE	350	2020-07-14 16:30:34.004939+05:45	\N	1	1	77
11	S01001020	CERVICAL SPINE-AP/LT	350	2020-07-14 16:30:48.544344+05:45	\N	1	1	77
12	S01001021	LUMBAR-AP/LT	350	2020-07-14 16:30:58.740189+05:45	\N	1	1	77
13	S01001022	KUB	350	2020-07-14 16:31:15.377267+05:45	\N	1	1	77
46	S01002002	URINE RE/ME	100	2020-07-14 16:46:50.260684+05:45	\N	1	2	77
47	S01002003	STOOL RE/ME	100	2020-07-14 16:46:59.693118+05:45	\N	1	2	77
48	S01002004	UPT	100	2020-07-14 16:47:06.900827+05:45	\N	1	2	77
49	S01002005	ANC PACKAGE	500	2020-07-14 16:47:16.94949+05:45	\N	1	2	77
50	S01002006	TROP I	350	2020-07-14 16:47:27.06766+05:45	\N	1	2	77
51	S01002007	H.PYLORI STOOL AG	250	2020-07-14 16:47:38.224267+05:45	\N	1	2	77
52	S01002008	LIPID PROFILE	650	2020-07-14 16:47:46.108993+05:45	\N	1	2	77
53	S01002009	TRIGLYCERIDE	150	2020-07-14 16:48:00.318923+05:45	\N	1	2	77
55	S01002010	LFT	450	2020-07-14 16:49:04.421263+05:45	\N	1	2	77
56	S01002011	RFT	400	2020-07-14 16:49:21.76527+05:45	\N	1	2	77
57	S01002012	STOOL OCCULT BLOOD TEST	150	2020-07-14 16:49:39.784844+05:45	\N	1	2	77
58	S01002013	URINE KETONE	100	2020-07-14 16:50:05.77176+05:45	\N	1	2	77
59	S01002014	AST/ALT	150	2020-07-14 16:51:35.460181+05:45	\N	1	2	77
54	S01002015	TOTAL CHOLESTEROL	150	2020-07-14 16:48:50.748074+05:45	\N	1	2	77
24	S01002016	TOTAL WBC COUNT	50	2020-07-14 16:40:51.912429+05:45	\N	1	2	77
25	S01002017	DIFFERENTIAL COUNT	50	2020-07-14 16:41:34.844142+05:45	\N	1	2	77
26	S01002018	RBC COUNT	150	2020-07-14 16:41:59.993301+05:45	\N	1	2	77
27	S01002019	PLATELET COUNT	150	2020-07-14 16:42:13.622261+05:45	\N	1	2	77
28	S01002020	ESR	50	2020-07-14 16:42:21.742568+05:45	\N	1	2	77
29	S01002021	BLOOD GROUPING	100	2020-07-14 16:42:35.29597+05:45	\N	1	2	77
30	S01002022	CRP	150	2020-07-14 16:42:41.632423+05:45	\N	1	2	77
31	S01002023	RBS	50	2020-07-14 16:42:58.448345+05:45	\N	1	2	77
32	S01002024	FBS/PP	100	2020-07-14 16:43:18.315512+05:45	\N	1	2	77
33	S01002025	WIDAL	150	2020-07-14 16:43:47.123208+05:45	\N	1	2	77
34	S01002026	ASO	150	2020-07-14 16:43:57.270747+05:45	\N	1	2	77
35	S01002027	BILIRUBIN	150	2020-07-14 16:44:04.489392+05:45	\N	1	2	77
37	S01002029	UREA	150	2020-07-14 16:44:56.884041+05:45	\N	1	2	77
38	S01002030	CREATININE	150	2020-07-14 16:45:09.871629+05:45	\N	1	2	77
39	S01002031	URIC ACID	150	2020-07-14 16:45:18.815303+05:45	\N	1	2	77
40	S01002032	MP SEROLOGY	0	2020-07-14 16:45:48.373183+05:45	\N	1	2	77
41	S01002033	HBSAG	250	2020-07-14 16:46:03.413555+05:45	\N	1	2	77
42	S01002034	VDRL	150	2020-07-14 16:46:10.63743+05:45	\N	1	2	77
43	S01002035	HCV	250	2020-07-14 16:46:18.900671+05:45	\N	1	2	77
44	S01002036	HIV	0	2020-07-14 16:46:23.876171+05:45	\N	1	2	77
45	S01002037	SPUTUM AFB	0	2020-07-14 16:46:37.833316+05:45	\N	1	2	77
60	S01004001	SIMPLE DRESSING	25	2020-07-14 16:54:26.523069+05:45	\N	1	4	77
62	S01004002	I AND D-KETAMINE	200	2020-07-14 16:56:38.219431+05:45	\N	1	4	77
63	S01004003	SUTURE	100	2020-07-14 16:57:40.539586+05:45	\N	1	4	77
64	S01004004	SYRINGING-1 SIDE	50	2020-07-14 16:58:12.437728+05:45	\N	1	4	77
65	S01004005	EAR/NASAL PACKING	50	2020-07-14 16:58:28.858451+05:45	\N	1	4	77
66	S01004006	EXCISION-LIPOMA/CYST	250	2020-07-14 16:58:51.304587+05:45	\N	1	4	77
67	S01004007	EXCISION- INGROWING TOE NAIL	250	2020-07-14 16:59:14.875497+05:45	\N	1	4	77
68	S01004008	EXCISION-CORN	250	2020-07-14 16:59:39.141814+05:45	\N	1	4	77
69	S01004009	NEBULIZATION	25	2020-07-14 16:59:51.814587+05:45	\N	1	4	77
70	S01004010	ECG	250	2020-07-14 17:00:00.349793+05:45	\N	1	4	77
71	S01004011	USG-ANC	0	2020-07-14 17:00:59.400562+05:45	\N	1	4	77
72	S01004012	INJ. TT	50	2020-07-14 17:01:09.000557+05:45	\N	1	4	77
75	S01004014	PLASTER CAST-HAND	400	2020-07-14 17:01:48.287291+05:45	\N	1	4	77
76	S01004015	PLASTER CAST-LEG	500	2020-07-14 17:02:23.371213+05:45	\N	1	4	77
77	S01004016	SAFE ABORTION SERVICE	1000	2020-07-14 17:02:42.883593+05:45	\N	1	4	77
78	S01004017	PLASTER REMOVAL	100	2020-07-14 17:03:01.488469+05:45	\N	1	4	77
79	S01004018	INTRA-ARTICULAR INJECTION	50	2020-07-14 17:03:15.895436+05:45	\N	1	4	77
80	S01004019	INTRA-LESIONAL INJECTION	50	2020-07-14 17:04:10.075712+05:45	\N	1	4	77
74	S01004020	PLASTER SLAB	300	2020-07-14 17:01:33.427009+05:45	\N	1	4	77
61	S01004021	I AND D-LA	75	2020-07-14 16:56:26.796844+05:45	\N	1	4	77
82	S01006001	RAPE CASE	300	2020-07-14 17:05:48.504221+05:45	\N	1	6	77
23	S01002001	HEMOGLOBIN(HB)	50	2020-07-14 16:39:55.093645+05:45	\N	1	2	77
36	S01002028	RA FACTOR	150	2020-07-14 16:44:45.506608+05:45	\N	1	2	77
86	S01003001	EMERGENCY TICKET	50	2020-07-14 17:08:31.506734+05:45	\N	1	3	77
73	S01004013	ADMISSION CHARGE	50	2020-07-14 17:01:22.279754+05:45	\N	1	4	77
81	S01010001	WOUND EXAMINATION	300	2020-07-14 17:05:26.5424+05:45	\N	1	10	77
87	S01005001	OPD	15	2020-07-14 17:08:49.445839+05:45	\N	1	5	77
83	S01006002	BIRTH CERTIFICATE	50	2020-07-14 17:06:12.904932+05:45	\N	1	6	77
85	S01012001	USG-NON-ROUTINE	250	2020-07-14 17:08:07.421323+05:45	\N	1	12	77
84	S01010002	MEDICAL CERTIFICATE	500	2020-07-14 17:07:19.349635+05:45	\N	1	10	77
\.


--
-- Data for Name: Staff; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public."Staff" (id, scode, sname, sphone, semail, screated, screated_by, hospital_id, sdepart_id, stype_id, user_id, year_id) FROM stdin;
2	2020072	BINDESHWORI GYALANG	9845149081	melamchiphc@gmail.com	2020-07-14 15:55:21.610175+05:45	\N	1	2	2	4	77
3	2020073	JAY PRAKASH SHAH	9840729198	shahjayprakash978@gmail.com	2020-07-14 15:56:35.157073+05:45	\N	1	2	2	5	77
1	2020071	DIPESH DHITAL	9860279876	melamchiphc@gmail.com	2020-07-14 15:54:38.021863+05:45	\N	1	1	1	3	77
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.auth_group (id, name) FROM stdin;
1	ROLE_ADMIN
2	ROLE_HOSPITAL
3	ROLE_STAFF
4	ROLE_CASHIER
5	ROLE_DOCTOR
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add year	7	add_year
26	Can change year	7	change_year
27	Can delete year	7	delete_year
28	Can view year	7	view_year
29	Can add role permissions	8	add_rolepermissions
30	Can change role permissions	8	change_rolepermissions
31	Can delete role permissions	8	delete_rolepermissions
32	Can view role permissions	8	view_rolepermissions
33	Can add departments	9	add_departments
34	Can change departments	9	change_departments
35	Can delete departments	9	delete_departments
36	Can view departments	9	view_departments
37	Can add department history	10	add_departmenthistory
38	Can change department history	10	change_departmenthistory
39	Can delete department history	10	delete_departmenthistory
40	Can view department history	10	view_departmenthistory
41	Can add services	11	add_services
42	Can change services	11	change_services
43	Can delete services	11	delete_services
44	Can view services	11	view_services
45	Can add services history	12	add_serviceshistory
46	Can change services history	12	change_serviceshistory
47	Can delete services history	12	delete_serviceshistory
48	Can view services history	12	view_serviceshistory
49	Can add service has incentive	13	add_servicehasincentive
50	Can change service has incentive	13	change_servicehasincentive
51	Can delete service has incentive	13	delete_servicehasincentive
52	Can view service has incentive	13	view_servicehasincentive
53	Can add patients	14	add_patients
54	Can change patients	14	change_patients
55	Can delete patients	14	delete_patients
56	Can view patients	14	view_patients
57	Can add doctors history	15	add_doctorshistory
58	Can change doctors history	15	change_doctorshistory
59	Can delete doctors history	15	delete_doctorshistory
60	Can view doctors history	15	view_doctorshistory
61	Can add doctors	16	add_doctors
62	Can change doctors	16	change_doctors
63	Can delete doctors	16	delete_doctors
64	Can view doctors	16	view_doctors
65	Can add hospital	17	add_hospital
66	Can change hospital	17	change_hospital
67	Can delete hospital	17	delete_hospital
68	Can view hospital	17	view_hospital
69	Can add user belongs to hospital	18	add_userbelongstohospital
70	Can change user belongs to hospital	18	change_userbelongstohospital
71	Can delete user belongs to hospital	18	delete_userbelongstohospital
72	Can view user belongs to hospital	18	view_userbelongstohospital
73	Can add ward	19	add_ward
74	Can change ward	19	change_ward
75	Can delete ward	19	delete_ward
76	Can view ward	19	view_ward
77	Can add bill	20	add_bill
78	Can change bill	20	change_bill
79	Can delete bill	20	delete_bill
80	Can view bill	20	view_bill
81	Can add bill service	21	add_billservice
82	Can change bill service	21	change_billservice
83	Can delete bill service	21	delete_billservice
84	Can view bill service	21	view_billservice
85	Can add type	22	add_type
86	Can change type	22	change_type
87	Can delete type	22	delete_type
88	Can view type	22	view_type
89	Can add metas	23	add_metas
90	Can change metas	23	change_metas
91	Can delete metas	23	delete_metas
92	Can view metas	23	view_metas
93	Can add expenditure type	24	add_expendituretype
94	Can change expenditure type	24	change_expendituretype
95	Can delete expenditure type	24	delete_expendituretype
96	Can view expenditure type	24	view_expendituretype
97	Can add expenditure type history	25	add_expendituretypehistory
98	Can change expenditure type history	25	change_expendituretypehistory
99	Can delete expenditure type history	25	delete_expendituretypehistory
100	Can view expenditure type history	25	view_expendituretypehistory
101	Can add income type	26	add_incometype
102	Can change income type	26	change_incometype
103	Can delete income type	26	delete_incometype
104	Can view income type	26	view_incometype
105	Can add income type history	27	add_incometypehistory
106	Can change income type history	27	change_incometypehistory
107	Can delete income type history	27	delete_incometypehistory
108	Can view income type history	27	view_incometypehistory
109	Can add income	28	add_income
110	Can change income	28	change_income
111	Can delete income	28	delete_income
112	Can view income	28	view_income
113	Can add income history	29	add_incomehistory
114	Can change income history	29	change_incomehistory
115	Can delete income history	29	delete_incomehistory
116	Can view income history	29	view_incomehistory
117	Can add expenditure	30	add_expenditure
118	Can change expenditure	30	change_expenditure
119	Can delete expenditure	30	delete_expenditure
120	Can view expenditure	30	view_expenditure
121	Can add expenditure history	31	add_expenditurehistory
122	Can change expenditure history	31	change_expenditurehistory
123	Can delete expenditure history	31	delete_expenditurehistory
124	Can view expenditure history	31	view_expenditurehistory
125	Can add product service	32	add_productservice
126	Can change product service	32	change_productservice
127	Can delete product service	32	delete_productservice
128	Can view product service	32	view_productservice
129	Can add store out	33	add_storeout
130	Can change store out	33	change_storeout
131	Can delete store out	33	delete_storeout
132	Can view store out	33	view_storeout
133	Can add store in	34	add_storein
134	Can change store in	34	change_storein
135	Can delete store in	34	delete_storein
136	Can view store in	34	view_storein
137	Can add pharmacy	35	add_pharmacy
138	Can change pharmacy	35	change_pharmacy
139	Can delete pharmacy	35	delete_pharmacy
140	Can view pharmacy	35	view_pharmacy
141	Can add medicine	36	add_medicine
142	Can change medicine	36	change_medicine
143	Can delete medicine	36	delete_medicine
144	Can view medicine	36	view_medicine
145	Can add store out history	37	add_storeouthistory
146	Can change store out history	37	change_storeouthistory
147	Can delete store out history	37	delete_storeouthistory
148	Can view store out history	37	view_storeouthistory
149	Can add store in history	38	add_storeinhistory
150	Can change store in history	38	change_storeinhistory
151	Can delete store in history	38	delete_storeinhistory
152	Can view store in history	38	view_storeinhistory
153	Can add staff	39	add_staff
154	Can change staff	39	change_staff
155	Can delete staff	39	delete_staff
156	Can view staff	39	view_staff
157	Can add staff history	40	add_staffhistory
158	Can change staff history	40	change_staffhistory
159	Can delete staff history	40	delete_staffhistory
160	Can view staff history	40	view_staffhistory
161	Can add staff type	41	add_stafftype
162	Can change staff type	41	change_stafftype
163	Can delete staff type	41	delete_stafftype
164	Can view staff type	41	view_stafftype
165	Can add staff type history	42	add_stafftypehistory
166	Can change staff type history	42	change_stafftypehistory
167	Can delete staff type history	42	delete_stafftypehistory
168	Can view staff type history	42	view_stafftypehistory
169	Can add product	43	add_product
170	Can change product	43	change_product
171	Can delete product	43	delete_product
172	Can view product	43	view_product
173	Can add product store in	44	add_productstorein
174	Can change product store in	44	change_productstorein
175	Can delete product store in	44	delete_productstorein
176	Can view product store in	44	view_productstorein
177	Can add product store out	45	add_productstoreout
178	Can change product store out	45	change_productstoreout
179	Can delete product store out	45	delete_productstoreout
180	Can view product store out	45	view_productstoreout
181	Can add request product	46	add_requestproduct
182	Can change request product	46	change_requestproduct
183	Can delete request product	46	delete_requestproduct
184	Can view request product	46	view_requestproduct
185	Can add store out has product	47	add_storeouthasproduct
186	Can change store out has product	47	change_storeouthasproduct
187	Can delete store out has product	47	delete_storeouthasproduct
188	Can view store out has product	47	view_storeouthasproduct
189	Can add store in has products	48	add_storeinhasproducts
190	Can change store in has products	48	change_storeinhasproducts
191	Can delete store in has products	48	delete_storeinhasproducts
192	Can view store in has products	48	view_storeinhasproducts
193	Can add seller details	49	add_sellerdetails
194	Can change seller details	49	change_sellerdetails
195	Can delete seller details	49	delete_sellerdetails
196	Can view seller details	49	view_sellerdetails
197	Can add request form	50	add_requestform
198	Can change request form	50	change_requestform
199	Can delete request form	50	delete_requestform
200	Can view request form	50	view_requestform
201	Can add product category	51	add_productcategory
202	Can change product category	51	change_productcategory
203	Can delete product category	51	delete_productcategory
204	Can view product category	51	view_productcategory
205	Can add service has incentive	52	add_servicehasincentive
206	Can change service has incentive	52	change_servicehasincentive
207	Can delete service has incentive	52	delete_servicehasincentive
208	Can view service has incentive	52	view_servicehasincentive
209	Can add historical patients	53	add_historicalpatients
210	Can change historical patients	53	change_historicalpatients
211	Can delete historical patients	53	delete_historicalpatients
212	Can view historical patients	53	view_historicalpatients
213	Can add historical bill service	54	add_historicalbillservice
214	Can change historical bill service	54	change_historicalbillservice
215	Can delete historical bill service	54	delete_historicalbillservice
216	Can view historical bill service	54	view_historicalbillservice
217	Can add historical bill	55	add_historicalbill
218	Can change historical bill	55	change_historicalbill
219	Can delete historical bill	55	delete_historicalbill
220	Can view historical bill	55	view_historicalbill
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
5	pbkdf2_sha256$180000$QPgCLW7AhOBS$ebAThXiQ1mydzawstAvbl41lWEPBP2/zN7F9W660SF8=	2020-07-21 15:19:15.273729+05:45	f	2020073	JAY	PRAKASH		f	t	2020-07-14 15:56:35.144671+05:45
3	pbkdf2_sha256$180000$s9RG3grn2PLW$4dzhNqa6G8dMT2BwjP/yKUhTFv1lt5U9vjYhB0+jQGQ=	2020-07-22 12:37:55.105381+05:45	f	2020071	DIPESH	DHITAL		f	t	2020-07-14 15:54:38.004269+05:45
2	pbkdf2_sha256$180000$u5K5UEYx8X1t$dVIq5oGWsxrH7wwL9urHB47QQg0S0ntVpXL7ohFXN/A=	2020-07-21 11:04:57.683957+05:45	f	1001	Melamchi	Primary Health Care Center		f	t	2020-07-14 15:44:00.312143+05:45
4	pbkdf2_sha256$180000$RWNyERxtpUWc$I/+Ey0vaLS03T7BZXG5y0dakoP2wp4R/uIJ1CLTd404=	\N	f	2020072	BINDESHWORI	GYALANG		f	t	2020-07-14 15:55:21.588852+05:45
6	pbkdf2_sha256$180000$WVJj39mZnFql$owvuQtMaL1lY9T//lWdvipBRzSzxuzQD/H+2krGqZNU=	\N	f	D010001	DR.	BINOD		f	t	2020-07-14 16:02:39.816876+05:45
7	pbkdf2_sha256$180000$8tjcrUzZB8xv$haBzHSib+XIyELe3JBb0G/scNprpY7zvqili8V3q7+U=	\N	f	D010002	DR.	ANISH		f	t	2020-07-14 16:03:24.139705+05:45
8	pbkdf2_sha256$180000$hPMi19DTXo5V$EAjjtPz8ODNIWo4czneoTv1TfRVk5sRljuCPjKti9ns=	\N	f	D010003	DR.	SISTU		f	t	2020-07-14 16:04:24.657174+05:45
1	pbkdf2_sha256$180000$eqwy3HncLFiC$JPSU0hjpAQ5aTUkywoPc8ZqnlNIf903216P2zkkuZ6c=	2020-07-21 13:04:14.407874+05:45	t	superuser	Super	User		t	t	2020-03-25 23:43:25.751+05:45
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
1	1	1
2	2	2
3	3	3
4	4	3
5	5	3
6	6	5
7	7	5
8	8	5
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: bill_historicalbill; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.bill_historicalbill (id, bcode, refunded, created_at, total, history_id, history_date, history_change_reason, history_type, history_user_id, patient_id, user_id, year_id) FROM stdin;
92	B20200780	f	2020-07-21 10:50:01.058199+05:45	100	1	2020-07-21 10:50:01.060725+05:45	\N	+	3	81	3	77
93	B20200781	f	2020-07-21 10:56:49.289446+05:45	500	2	2020-07-21 10:56:49.291046+05:45	\N	+	3	82	3	77
94	B20200782	f	2020-07-21 11:25:56.987939+05:45	100	3	2020-07-21 11:25:56.989575+05:45	\N	+	3	83	3	77
95	B20200783	f	2020-07-21 11:31:40.344414+05:45	500	4	2020-07-21 11:31:40.346343+05:45	\N	+	3	84	3	77
96	B20200784	f	2020-07-21 11:44:19.85639+05:45	750	5	2020-07-21 11:44:19.858776+05:45	\N	+	3	85	3	77
97	B20200785	f	2020-07-21 12:01:25.758846+05:45	350	6	2020-07-21 12:01:25.760733+05:45	\N	+	3	86	3	77
98	B20200786	f	2020-07-21 12:06:57.660116+05:45	500	7	2020-07-21 12:06:57.661746+05:45	\N	+	3	87	3	77
99	B20200787	f	2020-07-21 12:15:19.028382+05:45	50	8	2020-07-21 12:15:19.03038+05:45	\N	+	3	88	3	77
100	B20200788	f	2020-07-21 12:42:35.202428+05:45	100	9	2020-07-21 12:42:35.2043+05:45	\N	+	3	89	3	77
101	B20200789	f	2020-07-21 12:53:11.722312+05:45	350	10	2020-07-21 12:53:11.72418+05:45	\N	+	3	90	3	77
102	B20200790	f	2020-07-21 13:06:54.682732+05:45	800	11	2020-07-21 13:06:54.68557+05:45	\N	+	3	91	3	77
96	B20200784	t	2020-07-21 11:44:19.85639+05:45	750	12	2020-07-21 13:10:16.504942+05:45	\N	~	3	85	3	77
96	B20200784	f	2020-07-21 11:44:19.85639+05:45	750	13	2020-07-21 13:10:18.856538+05:45	\N	~	3	85	3	77
103	B20200791	f	2020-07-21 13:12:09.704305+05:45	500	14	2020-07-21 13:12:09.706323+05:45	\N	+	3	85	3	77
104	B20200792	f	2020-07-21 14:12:45.077685+05:45	100	15	2020-07-21 14:12:45.079402+05:45	\N	+	3	92	3	77
105	B20200793	f	2020-07-21 14:15:02.266326+05:45	100	16	2020-07-21 14:15:02.276353+05:45	\N	+	3	93	3	77
106	B20200794	f	2020-07-21 14:23:10.642615+05:45	100	17	2020-07-21 14:23:10.646238+05:45	\N	+	3	94	3	77
107	B20200795	f	2020-07-22 10:19:59.317797+05:45	50	18	2020-07-22 10:19:59.319359+05:45	\N	+	3	95	3	77
108	B20200796	f	2020-07-22 10:22:40.764716+05:45	500	19	2020-07-22 10:22:40.766478+05:45	\N	+	3	96	3	77
109	B20200797	f	2020-07-22 10:24:50.921782+05:45	700	20	2020-07-22 10:24:50.92329+05:45	\N	+	3	97	3	77
110	B20200798	f	2020-07-22 10:28:03.727448+05:45	700	21	2020-07-22 10:28:03.729191+05:45	\N	+	3	98	3	77
111	B20200799	f	2020-07-22 10:29:39.648004+05:45	100	22	2020-07-22 10:29:39.649334+05:45	\N	+	3	99	3	77
112	B202007100	f	2020-07-22 10:31:12.985771+05:45	350	23	2020-07-22 10:31:12.987627+05:45	\N	+	3	100	3	77
113	B202007101	f	2020-07-22 10:32:44.19158+05:45	100	24	2020-07-22 10:32:44.193079+05:45	\N	+	3	101	3	77
114	B202007102	f	2020-07-22 10:35:07.638577+05:45	250	25	2020-07-22 10:35:07.640333+05:45	\N	+	3	102	3	77
115	B202007103	f	2020-07-22 10:38:15.676906+05:45	100	26	2020-07-22 10:38:15.678817+05:45	\N	+	3	103	3	77
116	B202007104	f	2020-07-22 10:52:09.026309+05:45	25	27	2020-07-22 10:52:09.028488+05:45	\N	+	3	104	3	77
117	B202007105	f	2020-07-22 11:11:50.233269+05:45	300	28	2020-07-22 11:11:50.235725+05:45	\N	+	3	102	3	77
118	B202007106	f	2020-07-22 11:14:12.297493+05:45	100	29	2020-07-22 11:14:12.29922+05:45	\N	+	3	105	3	77
119	B202007107	f	2020-07-22 11:17:53.456093+05:45	350	30	2020-07-22 11:17:53.458076+05:45	\N	+	3	106	3	77
120	B202007108	f	2020-07-22 12:38:56.800073+05:45	350	31	2020-07-22 12:38:56.802579+05:45	\N	+	3	107	3	77
121	B202007109	f	2020-07-22 12:40:09.117199+05:45	350	32	2020-07-22 12:40:09.119168+05:45	\N	+	3	108	3	77
122	B202007110	f	2020-07-22 12:41:37.113409+05:45	100	33	2020-07-22 12:41:37.11627+05:45	\N	+	3	109	3	77
123	B202007111	f	2020-07-22 12:43:36.759099+05:45	500	34	2020-07-22 12:43:36.761387+05:45	\N	+	3	110	3	77
124	B202007112	f	2020-07-22 12:44:41.483397+05:45	500	35	2020-07-22 12:44:41.485418+05:45	\N	+	3	111	3	77
125	B202007113	f	2020-07-22 12:47:05.064115+05:45	100	36	2020-07-22 12:47:05.065783+05:45	\N	+	3	112	3	77
126	B202007114	f	2020-07-22 12:49:01.392957+05:45	50	37	2020-07-22 12:49:01.395003+05:45	\N	+	3	113	3	77
127	B202007115	f	2020-07-22 12:52:34.245865+05:45	350	38	2020-07-22 12:52:34.247482+05:45	\N	+	3	114	3	77
128	B202007116	f	2020-07-22 12:55:03.155434+05:45	100	39	2020-07-22 12:55:03.157019+05:45	\N	+	3	115	3	77
129	B202007117	f	2020-07-22 13:01:04.040182+05:45	50	40	2020-07-22 13:01:04.042278+05:45	\N	+	3	116	3	77
\.


--
-- Data for Name: bill_historicalbillservice; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.bill_historicalbillservice (id, sprice, squantity, history_id, history_date, history_change_reason, history_type, bill_id, history_user_id, service_id) FROM stdin;
123	100	1	1	2020-07-21 10:50:01.072774+05:45	\N	+	92	3	48
124	50	1	2	2020-07-21 10:56:49.295009+05:45	\N	+	93	3	31
125	50	1	3	2020-07-21 10:56:49.29862+05:45	\N	+	93	3	24
126	150	1	4	2020-07-21 10:56:49.301471+05:45	\N	+	93	3	37
127	250	1	5	2020-07-21 10:56:49.306423+05:45	\N	+	93	3	70
128	100	1	6	2020-07-21 11:25:56.99363+05:45	\N	+	94	3	48
129	500	1	7	2020-07-21 11:31:40.350071+05:45	\N	+	95	3	49
130	250	1	8	2020-07-21 11:44:19.864005+05:45	\N	+	96	3	51
131	150	1	9	2020-07-21 11:44:19.868252+05:45	\N	+	96	3	30
132	350	1	10	2020-07-21 11:44:19.871914+05:45	\N	+	96	3	8
133	350	1	11	2020-07-21 12:01:25.765811+05:45	\N	+	97	3	6
134	500	1	12	2020-07-21 12:06:57.665356+05:45	\N	+	98	3	49
135	50	1	13	2020-07-21 12:15:19.036902+05:45	\N	+	99	3	72
136	100	1	14	2020-07-21 12:42:35.209182+05:45	\N	+	100	3	32
137	350	1	15	2020-07-21 12:53:11.727986+05:45	\N	+	101	3	22
138	350	1	16	2020-07-21 13:06:54.690512+05:45	\N	+	102	3	8
139	100	1	17	2020-07-21 13:06:54.694754+05:45	\N	+	102	3	46
140	50	1	18	2020-07-21 13:06:54.698593+05:45	\N	+	102	3	24
141	150	1	19	2020-07-21 13:06:54.701647+05:45	\N	+	102	3	30
142	50	1	20	2020-07-21 13:06:54.704459+05:45	\N	+	102	3	23
143	50	1	21	2020-07-21 13:06:54.707222+05:45	\N	+	102	3	24
144	50	1	22	2020-07-21 13:06:54.709975+05:45	\N	+	102	3	25
145	350	1	23	2020-07-21 13:12:09.710772+05:45	\N	+	103	3	8
146	150	1	24	2020-07-21 13:12:09.714667+05:45	\N	+	103	3	30
147	100	1	25	2020-07-21 14:12:45.083326+05:45	\N	+	104	3	48
148	100	1	26	2020-07-21 14:15:02.294095+05:45	\N	+	105	3	46
149	100	1	27	2020-07-21 14:23:10.656479+05:45	\N	+	106	3	46
150	50	1	28	2020-07-22 10:19:59.323146+05:45	\N	+	107	3	31
151	350	1	29	2020-07-22 10:22:40.770195+05:45	\N	+	108	3	8
152	50	1	30	2020-07-22 10:22:40.773484+05:45	\N	+	108	3	24
153	50	1	31	2020-07-22 10:22:40.77614+05:45	\N	+	108	3	25
154	50	1	32	2020-07-22 10:22:40.7791+05:45	\N	+	108	3	23
155	350	1	33	2020-07-22 10:24:50.926797+05:45	\N	+	109	3	8
156	350	1	34	2020-07-22 10:24:50.930107+05:45	\N	+	109	3	12
157	350	1	35	2020-07-22 10:28:03.73436+05:45	\N	+	110	3	14
158	250	1	36	2020-07-22 10:28:03.738364+05:45	\N	+	110	3	85
159	100	1	37	2020-07-22 10:28:03.741588+05:45	\N	+	110	3	46
160	100	1	38	2020-07-22 10:29:39.652346+05:45	\N	+	111	3	48
161	350	1	39	2020-07-22 10:31:12.991232+05:45	\N	+	112	3	4
162	100	1	40	2020-07-22 10:32:44.19749+05:45	\N	+	113	3	48
163	100	1	41	2020-07-22 10:35:07.646562+05:45	\N	+	114	3	46
164	50	1	42	2020-07-22 10:35:07.650958+05:45	\N	+	114	3	24
165	50	1	43	2020-07-22 10:35:07.659065+05:45	\N	+	114	3	25
166	50	1	44	2020-07-22 10:35:07.669256+05:45	\N	+	114	3	23
167	100	1	45	2020-07-22 10:38:15.683618+05:45	\N	+	115	3	46
168	25	1	46	2020-07-22 10:52:09.034396+05:45	\N	+	116	3	60
169	150	1	47	2020-07-22 11:11:50.242113+05:45	\N	+	117	3	30
170	150	1	48	2020-07-22 11:11:50.245868+05:45	\N	+	117	3	33
171	100	1	49	2020-07-22 11:14:12.303039+05:45	\N	+	118	3	46
172	250	1	50	2020-07-22 11:17:53.462934+05:45	\N	+	119	3	85
173	100	1	51	2020-07-22 11:17:53.466992+05:45	\N	+	119	3	48
174	350	1	52	2020-07-22 12:38:56.808917+05:45	\N	+	120	3	8
175	350	1	53	2020-07-22 12:40:09.128989+05:45	\N	+	121	3	8
176	100	1	54	2020-07-22 12:41:37.122278+05:45	\N	+	122	3	48
177	500	1	55	2020-07-22 12:43:36.76677+05:45	\N	+	123	3	49
178	500	1	56	2020-07-22 12:44:41.489752+05:45	\N	+	124	3	49
179	100	1	57	2020-07-22 12:47:05.070411+05:45	\N	+	125	3	48
180	50	1	58	2020-07-22 12:49:01.400093+05:45	\N	+	126	3	83
181	350	1	59	2020-07-22 12:52:34.251608+05:45	\N	+	127	3	8
182	100	1	60	2020-07-22 12:55:03.161883+05:45	\N	+	128	3	46
183	50	1	61	2020-07-22 13:01:04.047263+05:45	\N	+	129	3	83
\.


--
-- Data for Name: bill_services; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.bill_services (id, sprice, squantity, bill_id, service_id) FROM stdin;
6	250	1	5	70
9	500	1	8	49
10	50	1	9	23
11	50	1	9	25
12	50	1	9	24
13	350	1	10	18
14	50	1	11	72
17	100	1	14	46
18	100	1	15	46
21	100	1	17	48
22	100	1	17	46
23	500	1	18	49
26	50	1	20	24
29	100	1	22	48
30	25	1	23	60
32	100	1	25	46
33	450	1	26	55
34	350	1	27	16
35	350	1	27	8
36	350	1	27	11
37	350	1	28	8
38	350	1	29	1
39	25	1	30	60
40	250	1	31	85
41	100	1	32	46
42	100	1	33	48
43	100	1	34	48
44	100	1	35	48
45	100	1	36	46
46	100	1	37	46
47	150	1	38	38
48	100	1	38	46
49	50	1	38	24
50	50	1	38	31
51	100	1	39	46
52	100	1	40	46
53	450	1	40	55
54	150	1	40	38
55	100	1	40	48
56	250	1	40	85
57	100	1	41	46
58	50	1	42	72
59	350	1	42	18
60	100	1	43	48
61	50	1	44	31
62	100	1	44	46
63	500	1	45	49
64	350	1	46	8
65	50	1	47	31
66	150	1	48	35
67	100	1	49	48
68	100	1	50	46
69	350	1	51	21
70	500	1	52	49
71	250	1	53	85
72	100	1	53	48
73	100	1	54	46
74	500	1	55	49
75	50	1	56	31
76	150	1	57	37
77	500	1	58	49
78	100	1	59	48
79	100	1	60	46
80	500	1	61	49
81	50	1	62	31
82	250	1	62	70
83	100	1	63	48
84	350	1	64	1
85	350	1	65	22
86	500	1	66	49
87	50	1	67	31
88	50	1	67	23
89	350	1	68	12
90	50	1	68	31
91	350	1	69	8
92	50	1	70	23
93	50	1	70	24
94	150	1	70	27
95	350	1	71	8
96	350	1	72	20
97	200	1	73	62
98	50	1	74	31
99	350	1	75	8
100	150	1	75	30
101	150	1	75	37
102	150	1	75	35
103	250	1	75	70
104	250	1	76	85
105	350	1	77	4
106	50	1	78	72
107	50	1	79	31
108	50	1	80	72
109	25	1	80	60
110	500	1	81	49
111	500	1	82	49
112	200	1	83	62
113	500	1	84	49
114	350	1	85	11
115	100	1	86	48
116	100	1	86	46
117	100	1	87	48
118	50	1	88	83
119	100	1	89	46
120	100	1	90	46
121	250	1	90	85
122	100	1	91	47
123	100	1	92	48
124	50	1	93	31
125	50	1	93	24
126	150	1	93	37
127	250	1	93	70
128	100	1	94	48
129	500	1	95	49
130	250	1	96	51
131	150	1	96	30
132	350	1	96	8
133	350	1	97	6
134	500	1	98	49
135	50	1	99	72
136	100	1	100	32
137	350	1	101	22
138	350	1	102	8
139	100	1	102	46
140	50	1	102	24
141	150	1	102	30
142	50	1	102	23
143	50	1	102	24
144	50	1	102	25
145	350	1	103	8
146	150	1	103	30
147	100	1	104	48
148	100	1	105	46
149	100	1	106	46
150	50	1	107	31
151	350	1	108	8
152	50	1	108	24
153	50	1	108	25
154	50	1	108	23
155	350	1	109	8
156	350	1	109	12
157	350	1	110	14
158	250	1	110	85
159	100	1	110	46
160	100	1	111	48
161	350	1	112	4
162	100	1	113	48
163	100	1	114	46
164	50	1	114	24
165	50	1	114	25
166	50	1	114	23
167	100	1	115	46
168	25	1	116	60
169	150	1	117	30
170	150	1	117	33
171	100	1	118	46
172	250	1	119	85
173	100	1	119	48
174	350	1	120	8
175	350	1	121	8
176	100	1	122	48
177	500	1	123	49
178	500	1	124	49
179	100	1	125	48
180	50	1	126	83
181	350	1	127	8
182	100	1	128	46
183	50	1	129	83
\.


--
-- Data for Name: bills; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.bills (id, bcode, created_at, total, patient_id, user_id, year_id, refunded) FROM stdin;
92	B20200780	2020-07-21 10:50:01.058199+05:45	100	81	3	77	f
93	B20200781	2020-07-21 10:56:49.289446+05:45	500	82	3	77	f
94	B20200782	2020-07-21 11:25:56.987939+05:45	100	83	3	77	f
5	B2020071	2020-07-16 11:13:46.89023+05:45	250	4	3	77	f
8	B2020074	2020-07-16 11:39:48.557257+05:45	500	7	3	77	f
9	B2020075	2020-07-16 11:54:49.443651+05:45	150	8	3	77	f
10	B2020076	2020-07-16 12:06:56.282234+05:45	350	9	3	77	f
11	B2020077	2020-07-16 12:35:01.530213+05:45	50	10	3	77	f
14	B20200710	2020-07-16 12:38:27.326142+05:45	100	13	3	77	f
15	B20200711	2020-07-16 12:40:54.087666+05:45	100	14	3	77	f
18	B2020079	2020-07-16 14:24:04.938912+05:45	500	17	3	77	f
20	B20200711	2020-07-16 14:49:38.516451+05:45	50	19	3	77	f
22	B20200713	2020-07-16 15:14:16.743891+05:45	100	21	3	77	f
23	B20200714	2020-07-16 15:32:51.806016+05:45	25	22	3	77	f
25	B20200716	2020-07-16 15:56:18.31328+05:45	100	24	3	77	f
26	B20200717	2020-07-16 15:58:39.501983+05:45	450	4	3	77	f
27	B20200718	2020-07-16 16:00:58.290857+05:45	1050	4	3	77	f
28	B20200719	2020-07-16 16:04:33.244259+05:45	350	24	3	77	f
29	B20200720	2020-07-16 16:06:02.987105+05:45	350	25	3	77	f
30	B20200721	2020-07-16 16:07:37.610811+05:45	25	25	3	77	f
31	B20200722	2020-07-16 16:11:28.176122+05:45	250	26	3	77	f
95	B20200783	2020-07-21 11:31:40.344414+05:45	500	84	3	77	f
17	B2020078	2020-07-16 14:17:26.97411+05:45	200	16	3	77	f
32	B20200720	2020-07-17 10:41:50.793828+05:45	100	27	3	77	f
33	B20200721	2020-07-17 10:52:52.202586+05:45	100	28	3	77	f
34	B20200722	2020-07-17 11:25:35.764651+05:45	100	29	3	77	f
35	B20200723	2020-07-17 11:27:58.131157+05:45	100	30	3	77	f
36	B20200724	2020-07-17 11:37:15.668972+05:45	100	31	3	77	f
37	B20200725	2020-07-17 11:51:20.374062+05:45	100	30	3	77	f
38	B20200726	2020-07-17 11:59:47.901402+05:45	500	32	3	77	f
39	B20200727	2020-07-17 12:33:03.747023+05:45	100	33	3	77	f
40	B20200728	2020-07-17 12:48:52.384095+05:45	1050	34	3	77	f
41	B20200729	2020-07-17 13:34:34.80462+05:45	100	35	3	77	f
42	B20200730	2020-07-17 14:24:16.603085+05:45	425	36	3	77	f
43	B20200731	2020-07-17 14:33:01.503231+05:45	100	37	3	77	f
44	B20200732	2020-07-17 14:39:06.642491+05:45	400	38	3	77	t
45	B20200733	2020-07-19 10:23:31.52148+05:45	500	39	3	77	f
46	B20200734	2020-07-19 10:26:48.688345+05:45	350	40	3	77	f
47	B20200735	2020-07-19 10:30:54.329172+05:45	50	41	3	77	f
48	B20200736	2020-07-19 10:32:50.601875+05:45	150	42	3	77	f
49	B20200737	2020-07-19 10:35:04.737029+05:45	100	43	3	77	f
50	B20200738	2020-07-19 10:46:08.77123+05:45	100	44	3	77	f
51	B20200739	2020-07-19 10:56:08.711969+05:45	350	45	3	77	f
52	B20200740	2020-07-19 10:58:34.048798+05:45	500	46	3	77	f
53	B20200741	2020-07-19 11:00:33.006788+05:45	350	47	3	77	f
54	B20200742	2020-07-19 11:02:56.32994+05:45	100	48	3	77	f
55	B20200743	2020-07-19 11:04:45.90113+05:45	500	49	3	77	f
56	B20200744	2020-07-19 11:06:33.593604+05:45	50	50	3	77	f
57	B20200745	2020-07-19 11:11:58.585899+05:45	150	40	3	77	f
58	B20200746	2020-07-19 11:20:45.73291+05:45	500	51	3	77	f
59	B20200747	2020-07-19 11:22:38.890993+05:45	100	52	3	77	f
60	B20200748	2020-07-19 11:25:32.700594+05:45	100	53	3	77	f
61	B20200749	2020-07-19 11:27:12.919748+05:45	500	54	3	77	f
62	B20200750	2020-07-19 11:34:44.788608+05:45	300	55	3	77	f
63	B20200751	2020-07-19 11:57:28.020645+05:45	100	56	3	77	f
64	B20200752	2020-07-19 12:03:55.821519+05:45	350	57	3	77	f
65	B20200753	2020-07-19 12:10:05.201639+05:45	350	58	3	77	f
66	B20200754	2020-07-19 12:15:07.810517+05:45	500	59	3	77	f
67	B20200755	2020-07-19 12:17:27.335967+05:45	100	60	3	77	f
68	B20200756	2020-07-19 12:24:15.134506+05:45	400	53	3	77	f
69	B20200757	2020-07-19 12:42:31.24613+05:45	350	61	3	77	f
70	B20200758	2020-07-19 12:46:16.53052+05:45	250	62	3	77	f
71	B20200759	2020-07-19 12:54:08.778224+05:45	350	63	3	77	f
72	B20200760	2020-07-19 12:56:14.801872+05:45	350	64	3	77	f
73	B20200761	2020-07-19 14:29:25.990937+05:45	200	65	3	77	f
74	B20200762	2020-07-19 14:54:01.53618+05:45	50	64	5	77	f
75	B20200763	2020-07-19 15:29:05.520256+05:45	1050	66	3	77	f
76	B20200764	2020-07-19 15:49:20.158177+05:45	250	66	3	77	f
77	B20200765	2020-07-20 10:28:54.658102+05:45	350	67	3	77	f
78	B20200766	2020-07-20 10:43:04.52542+05:45	50	68	3	77	f
79	B20200767	2020-07-20 10:47:20.843962+05:45	50	68	3	77	f
80	B20200768	2020-07-20 11:10:35.177166+05:45	75	69	3	77	f
81	B20200769	2020-07-20 11:26:46.526815+05:45	500	70	3	77	f
82	B20200770	2020-07-20 11:28:49.215367+05:45	500	71	3	77	f
83	B20200771	2020-07-20 11:30:33.017749+05:45	200	72	3	77	f
84	B20200772	2020-07-20 11:43:17.974189+05:45	500	73	3	77	f
85	B20200773	2020-07-20 11:45:59.123217+05:45	350	74	3	77	f
86	B20200774	2020-07-20 11:58:03.019668+05:45	200	75	3	77	f
87	B20200775	2020-07-20 12:02:41.252144+05:45	100	76	3	77	f
88	B20200776	2020-07-20 12:06:35.23751+05:45	50	77	3	77	f
89	B20200777	2020-07-20 14:18:11.448633+05:45	100	78	3	77	f
90	B20200778	2020-07-20 14:31:18.566723+05:45	350	79	3	77	f
91	B20200779	2020-07-21 10:29:40.746169+05:45	100	80	3	77	f
97	B20200785	2020-07-21 12:01:25.758846+05:45	350	86	3	77	f
98	B20200786	2020-07-21 12:06:57.660116+05:45	500	87	3	77	f
99	B20200787	2020-07-21 12:15:19.028382+05:45	50	88	3	77	f
100	B20200788	2020-07-21 12:42:35.202428+05:45	100	89	3	77	f
101	B20200789	2020-07-21 12:53:11.722312+05:45	350	90	3	77	f
102	B20200790	2020-07-21 13:06:54.682732+05:45	800	91	3	77	f
106	B20200794	2020-07-21 14:23:10.642615+05:45	100	94	3	77	f
96	B20200784	2020-07-21 11:44:19.85639+05:45	750	85	3	77	f
103	B20200791	2020-07-21 13:12:09.704305+05:45	500	85	3	77	f
104	B20200792	2020-07-21 14:12:45.077685+05:45	100	92	3	77	f
105	B20200793	2020-07-21 14:15:02.266326+05:45	100	93	3	77	f
107	B20200795	2020-07-22 10:19:59.317797+05:45	50	95	3	77	f
108	B20200796	2020-07-22 10:22:40.764716+05:45	500	96	3	77	f
109	B20200797	2020-07-22 10:24:50.921782+05:45	700	97	3	77	f
110	B20200798	2020-07-22 10:28:03.727448+05:45	700	98	3	77	f
111	B20200799	2020-07-22 10:29:39.648004+05:45	100	99	3	77	f
112	B202007100	2020-07-22 10:31:12.985771+05:45	350	100	3	77	f
113	B202007101	2020-07-22 10:32:44.19158+05:45	100	101	3	77	f
114	B202007102	2020-07-22 10:35:07.638577+05:45	250	102	3	77	f
115	B202007103	2020-07-22 10:38:15.676906+05:45	100	103	3	77	f
116	B202007104	2020-07-22 10:52:09.026309+05:45	25	104	3	77	f
117	B202007105	2020-07-22 11:11:50.233269+05:45	300	102	3	77	f
118	B202007106	2020-07-22 11:14:12.297493+05:45	100	105	3	77	f
119	B202007107	2020-07-22 11:17:53.456093+05:45	350	106	3	77	f
120	B202007108	2020-07-22 12:38:56.800073+05:45	350	107	3	77	f
121	B202007109	2020-07-22 12:40:09.117199+05:45	350	108	3	77	f
122	B202007110	2020-07-22 12:41:37.113409+05:45	100	109	3	77	f
123	B202007111	2020-07-22 12:43:36.759099+05:45	500	110	3	77	f
124	B202007112	2020-07-22 12:44:41.483397+05:45	500	111	3	77	f
125	B202007113	2020-07-22 12:47:05.064115+05:45	100	112	3	77	f
126	B202007114	2020-07-22 12:49:01.392957+05:45	50	113	3	77	f
127	B202007115	2020-07-22 12:52:34.245865+05:45	350	114	3	77	f
128	B202007116	2020-07-22 12:55:03.155434+05:45	100	115	3	77	f
129	B202007117	2020-07-22 13:01:04.040182+05:45	50	116	3	77	f
\.


--
-- Data for Name: department_departmenthistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.department_departmenthistory (id, dname, action, "actionDate", "actionBy_id", hospital_id) FROM stdin;
1	BILL COUNTER	create	2020-07-14 15:52:47.122864+05:45	2	1
2	ACCOUNT	create	2020-07-14 15:52:52.887195+05:45	2	1
3	ACCOUNT	edit	2020-07-14 15:53:42.038921+05:45	2	1
4	GENERAL PRACTICE	create	2020-07-14 16:00:37.665883+05:45	2	1
5	GENERAL PRACTICE	edit	2020-07-14 16:01:11.737053+05:45	2	1
\.


--
-- Data for Name: departments; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.departments (id, dname, hospital_id) FROM stdin;
1	BILL COUNTER	1
2	ADMINISTRATION AND ACCOUNT DEPARTMENT	1
3	PHYSICIAN	1
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	login	year
8	login	rolepermissions
9	department	departments
10	department	departmenthistory
11	services	services
12	services	serviceshistory
13	services	servicehasincentive
14	patients	patients
15	doctor	doctorshistory
16	doctor	doctors
17	hospital	hospital
18	hospital	userbelongstohospital
19	hospital	ward
20	bill	bill
21	bill	billservice
22	type	type
23	metas	metas
24	expendituretype	expendituretype
25	expendituretype	expendituretypehistory
26	incometype	incometype
27	incometype	incometypehistory
28	income	income
29	income	incomehistory
30	expenditure	expenditure
31	expenditure	expenditurehistory
32	store	productservice
33	store	storeout
34	store	storein
35	store	pharmacy
36	store	medicine
37	store	storeouthistory
38	store	storeinhistory
39	staff	staff
40	staff	staffhistory
41	staffType	stafftype
42	staffType	stafftypehistory
43	productstore	product
44	productstore	productstorein
45	productstore	productstoreout
46	productstore	requestproduct
47	productstore	storeouthasproduct
48	productstore	storeinhasproducts
49	productstore	sellerdetails
50	productstore	requestform
51	productstore	productcategory
52	type	servicehasincentive
53	patients	historicalpatients
54	bill	historicalbillservice
55	bill	historicalbill
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-07-14 13:33:57.848549+05:45
2	auth	0001_initial	2020-07-14 13:33:57.892978+05:45
3	admin	0001_initial	2020-07-14 13:33:57.957549+05:45
4	admin	0002_logentry_remove_auto_add	2020-07-14 13:33:57.974624+05:45
5	admin	0003_logentry_add_action_flag_choices	2020-07-14 13:33:57.983933+05:45
6	contenttypes	0002_remove_content_type_name	2020-07-14 13:33:58.004239+05:45
7	auth	0002_alter_permission_name_max_length	2020-07-14 13:33:58.010728+05:45
8	auth	0003_alter_user_email_max_length	2020-07-14 13:33:58.02038+05:45
9	auth	0004_alter_user_username_opts	2020-07-14 13:33:58.029323+05:45
10	auth	0005_alter_user_last_login_null	2020-07-14 13:33:58.038526+05:45
11	auth	0006_require_contenttypes_0002	2020-07-14 13:33:58.041545+05:45
12	auth	0007_alter_validators_add_error_messages	2020-07-14 13:33:58.053802+05:45
13	auth	0008_alter_user_username_max_length	2020-07-14 13:33:58.0674+05:45
14	auth	0009_alter_user_last_name_max_length	2020-07-14 13:33:58.077157+05:45
15	auth	0010_alter_group_name_max_length	2020-07-14 13:33:58.08749+05:45
16	auth	0011_update_proxy_permissions	2020-07-14 13:33:58.098298+05:45
17	login	0001_initial	2020-07-14 13:33:58.106775+05:45
18	login	0002_auto_20200625_0950	2020-07-14 13:33:58.111473+05:45
19	login	0003_rolepermissions	2020-07-14 13:33:58.129981+05:45
20	login	0004_auto_20200628_1022	2020-07-14 13:33:58.142882+05:45
21	hospital	0001_initial	2020-07-14 13:33:58.175301+05:45
22	type	0001_initial	2020-07-14 13:33:58.205031+05:45
23	services	0001_initial	2020-07-14 13:33:58.229477+05:45
24	patients	0001_initial	2020-07-14 13:33:58.255082+05:45
25	bill	0001_initial	2020-07-14 13:33:58.291548+05:45
26	bill	0002_bill_year	2020-07-14 13:33:58.329685+05:45
27	hospital	0002_auto_20200401_1333	2020-07-14 13:33:58.357766+05:45
28	hospital	0003_hospital_ward	2020-07-14 13:33:58.372992+05:45
29	hospital	0004_auto_20200402_1147	2020-07-14 13:33:58.405544+05:45
30	hospital	0005_auto_20200617_1154	2020-07-14 13:33:58.419574+05:45
31	department	0001_initial	2020-07-14 13:33:58.444517+05:45
32	department	0002_departmenthistory	2020-07-14 13:33:58.471769+05:45
33	doctor	0001_initial	2020-07-14 13:33:58.524427+05:45
34	expendituretype	0001_initial	2020-07-14 13:33:58.620627+05:45
35	expendituretype	0002_auto_20200630_1510	2020-07-14 13:33:58.647944+05:45
36	expenditure	0001_initial	2020-07-14 13:33:58.676054+05:45
37	expenditure	0002_expenditure_year	2020-07-14 13:33:58.710659+05:45
38	expenditure	0003_expenditurehistory	2020-07-14 13:33:58.743137+05:45
39	expenditure	0004_expenditure_remarks	2020-07-14 13:33:58.776698+05:45
40	expendituretype	0003_expendituretypehistory	2020-07-14 13:33:58.8009+05:45
41	incometype	0001_initial	2020-07-14 13:33:58.832795+05:45
42	incometype	0002_auto_20200630_1510	2020-07-14 13:33:58.858854+05:45
43	income	0001_initial	2020-07-14 13:33:58.883862+05:45
44	income	0002_income_year	2020-07-14 13:33:58.919945+05:45
45	income	0003_incomehistory	2020-07-14 13:33:58.949832+05:45
46	income	0004_income_remarks	2020-07-14 13:33:58.986119+05:45
47	incometype	0003_incometypehistory	2020-07-14 13:33:59.01401+05:45
48	metas	0001_initial	2020-07-14 13:33:59.059525+05:45
49	patients	0002_patients_year	2020-07-14 13:33:59.099702+05:45
50	productstore	0001_initial	2020-07-14 13:33:59.542875+05:45
51	productstore	0002_auto_20200711_0739	2020-07-14 13:33:59.64255+05:45
52	staffType	0001_initial	2020-07-14 13:33:59.682567+05:45
53	staffType	0002_stafftype_permissions	2020-07-14 13:33:59.733192+05:45
54	staffType	0003_stafftypehistory	2020-07-14 13:33:59.778894+05:45
55	type	0002_auto_20200325_1854	2020-07-14 13:33:59.871299+05:45
56	type	0003_type_code	2020-07-14 13:33:59.900616+05:45
57	type	0004_auto_20200619_0716	2020-07-14 13:33:59.92915+05:45
58	services	0002_auto_20200325_1854	2020-07-14 13:33:59.966332+05:45
59	services	0003_auto_20200402_1159	2020-07-14 13:33:59.995993+05:45
60	services	0004_auto_20200402_1211	2020-07-14 13:34:00.045773+05:45
61	services	0005_auto_20200402_1212	2020-07-14 13:34:00.07408+05:45
62	services	0006_auto_20200608_0808	2020-07-14 13:34:00.117309+05:45
63	services	0007_auto_20200612_0706	2020-07-14 13:34:00.264035+05:45
64	services	0008_auto_20200617_1020	2020-07-14 13:34:00.306584+05:45
65	services	0009_auto_20200630_1510	2020-07-14 13:34:00.370564+05:45
66	services	0010_serviceshistory	2020-07-14 13:34:00.410899+05:45
67	services	0011_auto_20200714_0725	2020-07-14 13:34:00.48402+05:45
68	services	0012_remove_servicehasincentive_service	2020-07-14 13:34:00.529197+05:45
69	services	0013_services_serviceincentive	2020-07-14 13:34:00.568968+05:45
70	services	0014_auto_20200714_0733	2020-07-14 13:34:00.642313+05:45
71	sessions	0001_initial	2020-07-14 13:34:00.657546+05:45
72	staff	0001_initial	2020-07-14 13:34:00.708389+05:45
73	staff	0002_auto_20200706_1326	2020-07-14 13:34:00.807889+05:45
74	staff	0003_remove_staff_snmc	2020-07-14 13:34:00.858912+05:45
75	staff	0004_auto_20200706_1408	2020-07-14 13:34:00.999009+05:45
76	staff	0005_auto_20200706_1416	2020-07-14 13:34:01.033006+05:45
77	staff	0006_remove_staff_snmc	2020-07-14 13:34:01.064458+05:45
78	staff	0007_staffhistory	2020-07-14 13:34:01.108509+05:45
79	store	0001_initial	2020-07-14 13:34:01.300088+05:45
80	store	0002_auto_20200622_0604	2020-07-14 13:34:01.355483+05:45
81	store	0003_auto_20200624_1233	2020-07-14 13:34:01.368625+05:45
82	store	0004_auto_20200630_1510	2020-07-14 13:34:01.466722+05:45
83	store	0005_pharmacy	2020-07-14 13:34:01.515982+05:45
84	store	0006_auto_20200702_0955	2020-07-14 13:34:01.61525+05:45
85	store	0007_medicine	2020-07-14 13:34:01.744819+05:45
86	store	0008_auto_20200703_0709	2020-07-14 13:34:01.77967+05:45
87	store	0009_storeinhistory_storeouthistory	2020-07-14 13:34:01.836255+05:45
88	store	0010_auto_20200710_1156	2020-07-14 13:34:01.912654+05:45
89	type	0005_auto_20200706_1221	2020-07-14 13:34:01.931072+05:45
90	bill	0003_bill_refunded	2020-07-16 02:58:18.675696+05:45
91	patients	0003_auto_20200715_0920	2020-07-16 02:58:18.709671+05:45
92	productstore	0003_product_rate	2020-07-16 02:58:18.746604+05:45
93	services	0015_auto_20200715_0751	2020-07-16 02:58:18.785514+05:45
94	type	0006_auto_20200715_0751	2020-07-16 02:58:18.900413+05:45
95	patients	0004_patientshistory	2020-07-20 17:56:30.387171+05:45
96	patients	0005_delete_patientshistory	2020-07-20 17:56:30.402393+05:45
97	patients	0006_historicalpatients	2020-07-20 17:56:30.479706+05:45
98	patients	0007_auto_20200719_1041	2020-07-20 17:56:30.58477+05:45
99	patients	0008_auto_20200719_1044	2020-07-20 17:56:30.724624+05:45
100	bill	0004_auto_20200719_1044	2020-07-20 17:56:30.862472+05:45
101	bill	0005_auto_20200719_1051	2020-07-20 17:56:30.966254+05:45
102	patients	0009_auto_20200719_1051	2020-07-20 17:56:31.032577+05:45
103	productstore	0004_auto_20200717_0509	2020-07-20 17:56:31.091423+05:45
104	productstore	0005_auto_20200717_0556	2020-07-20 17:56:31.296282+05:45
105	productstore	0006_auto_20200717_0609	2020-07-20 17:56:31.385325+05:45
106	productstore	0007_auto_20200717_0610	2020-07-20 17:56:31.398437+05:45
107	productstore	0008_auto_20200717_0616	2020-07-20 17:56:31.406854+05:45
108	productstore	0009_remove_productstoreout_hospital	2020-07-20 17:56:31.457491+05:45
109	productstore	0010_auto_20200719_0721	2020-07-20 17:56:31.542323+05:45
110	productstore	0011_remove_requestform_hospital	2020-07-20 17:56:31.588152+05:45
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: doctor_doctorshistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.doctor_doctorshistory (id, dcode, dname, dnmc, dphone, demail, action, "actionDate", "actionBy_id", ddepart_id, hospital_id) FROM stdin;
1		DR. BINOD K.C	17559	9841002454		create	2020-07-14 16:02:39.658574+05:45	2	3	1
2		DR. ANISH KUMAR SHRESTHA	26596	9818022698		create	2020-07-14 16:03:24.014909+05:45	2	3	1
3		DR. SISTU K.C	26444	9847073524		create	2020-07-14 16:04:24.535902+05:45	2	3	1
\.


--
-- Data for Name: expenditure_expenditurehistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.expenditure_expenditurehistory (id, amount, paid_by, action, "actionDate", "actionBy_id", hospital_id, itype_id) FROM stdin;
3	2415	DIPESH DHITAL	create	2020-07-20 10:24:11.988946+05:45	5	1	6
4	2000	DIPESH DHITAL	create	2020-07-21 11:19:09.259235+05:45	5	1	7
\.


--
-- Data for Name: expenditure_type; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.expenditure_type (id, tname, code, created_at, updated_at, created_by, updated_by, hospital_id) FROM stdin;
1	STATIONARY	001	2020-07-14 18:25:40.131998+05:45	2020-07-14 18:25:40.132021+05:45	\N	\N	1
2	FURNITURE	002	2020-07-14 18:25:45.570541+05:45	2020-07-14 18:25:45.570568+05:45	\N	\N	1
3	ELECTRICITY	003	2020-07-14 18:25:55.546833+05:45	2020-07-14 18:25:55.546856+05:45	\N	\N	1
4	STAFF SALARY	004	2020-07-15 14:01:37.49853+05:45	2020-07-15 14:01:37.498556+05:45	\N	\N	1
5	STAFF INCENTIVE	005	2020-07-15 14:03:09.256642+05:45	2020-07-15 14:03:09.256672+05:45	\N	\N	1
7	COMMUNICATION	007	2020-07-15 14:05:10.330111+05:45	2020-07-15 14:05:10.330135+05:45	\N	\N	1
6	OFFICE MANAGEMENT	006	2020-07-15 14:04:56.737267+05:45	2020-07-20 10:21:58.147856+05:45	\N	\N	1
\.


--
-- Data for Name: expendituretype_expendituretypehistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.expendituretype_expendituretypehistory (id, tname, code, action, "actionDate", "actionBy_id", hospital_id) FROM stdin;
1	STATIONARY	001	create	2020-07-14 18:25:40.128003+05:45	2	1
2	FURNITURE	002	create	2020-07-14 18:25:45.567728+05:45	2	1
3	ELECTRICITY	003	create	2020-07-14 18:25:55.543964+05:45	2	1
4	STAFF SALARY	004	create	2020-07-15 14:01:37.495123+05:45	2	1
5	STAFF INCENTIVE	005	create	2020-07-15 14:03:09.252966+05:45	2	1
6	OFFICE MANAGEMENT	006	create	2020-07-15 14:04:56.73322+05:45	2	1
7	COMMUNICATION	007	create	2020-07-15 14:05:10.326173+05:45	2	1
8	OFFICE MANAGEMENT	006	edit	2020-07-20 10:21:58.144835+05:45	5	1
\.


--
-- Data for Name: expenses; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.expenses (id, amount, paid_by, created_at, updated_at, created_by, updated_by, hospital_id, itype_id, year_id, remarks) FROM stdin;
2	2415	DIPESH DHITAL	2020-07-20 10:24:11.993421+05:45	2020-07-20 10:24:11.993449+05:45	\N	\N	1	6	77	\N
3	2000	DIPESH DHITAL	2020-07-21 11:19:09.262965+05:45	2020-07-21 11:19:09.263002+05:45	\N	\N	1	7	77	telephone bill
\.


--
-- Data for Name: hospital_has_users; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.hospital_has_users (id, created_at, hospital_id, user_id) FROM stdin;
1	2020-07-14	1	3
2	2020-07-14	1	4
3	2020-07-14	1	5
4	2020-07-14	1	6
5	2020-07-14	1	7
6	2020-07-14	1	8
\.


--
-- Data for Name: hospitals; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.hospitals (id, name, phone, location, created_at, updated_at, created_by, user_id, code, ward_id) FROM stdin;
1	Melamchi Primary Health Care Center	9849595565	MELAMCHI, SINDHUPALCHOwK	2020-07-14	2020-07-17	1	2	1001	11
\.


--
-- Data for Name: income_incomehistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.income_incomehistory (id, amount, received_by, action, "actionDate", "actionBy_id", hospital_id, itype_id) FROM stdin;
3	1185	DIPESH DHITAL	create	2020-07-17 15:05:13.724352+05:45	2	1	2
4	1055	DIPESH DHITAL	create	2020-07-20 10:19:31.013442+05:45	5	1	2
5	380	DIPESH DHITAL	create	2020-07-20 15:24:22.190884+05:45	5	1	2
6	900	DIPESH DHITAL	create	2020-07-20 15:37:20.097581+05:45	5	1	3
7	1225	DIPESH DHITAL	create	2020-07-20 15:42:31.474842+05:45	5	1	4
8	400	DIPESH DHITAL	create	2020-07-21 15:20:13.74692+05:45	5	1	3
\.


--
-- Data for Name: income_type; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.income_type (id, tname, code, created_at, updated_at, created_by, updated_by, hospital_id) FROM stdin;
1	MUNICIPALITY	001	2020-07-14 18:00:13.199945+05:45	2020-07-15 14:01:13.312352+05:45	\N	\N	1
2	OPD TICKET	002	2020-07-17 15:04:21.037324+05:45	2020-07-17 15:04:21.037349+05:45	\N	\N	1
3	EMERGENCY TICKET	003	2020-07-20 15:20:52.276747+05:45	2020-07-20 15:22:05.446895+05:45	\N	\N	1
4	ER PROCEDURE	004	2020-07-20 15:41:25.393354+05:45	2020-07-20 15:41:25.393379+05:45	\N	\N	1
\.


--
-- Data for Name: incomes; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.incomes (id, amount, received_by, created_at, updated_at, created_by, updated_by, hospital_id, itype_id, year_id, remarks) FROM stdin;
2	1185	DIPESH DHITAL	2020-07-17 15:05:13.875958+05:45	2020-07-17 15:05:13.875985+05:45	\N	\N	1	2	77	\N
3	1055	DIPESH DHITAL	2020-07-20 10:19:31.038363+05:45	2020-07-20 10:19:31.038394+05:45	\N	\N	1	2	77	\N
4	380	DIPESH DHITAL	2020-07-20 15:24:22.1954+05:45	2020-07-20 15:24:22.195424+05:45	\N	\N	1	2	77	\N
5	900	DIPESH DHITAL	2020-07-20 15:37:20.1008+05:45	2020-07-20 15:37:20.100823+05:45	\N	\N	1	3	77	\N
6	1225	DIPESH DHITAL	2020-07-20 15:42:31.478546+05:45	2020-07-20 15:42:31.478574+05:45	\N	\N	1	4	77	\N
7	400	DIPESH DHITAL	2020-07-21 15:20:13.753014+05:45	2020-07-21 15:20:13.753043+05:45	\N	\N	1	3	77	\N
\.


--
-- Data for Name: incometype_incometypehistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.incometype_incometypehistory (id, tname, code, action, "actionDate", "actionBy_id", hospital_id) FROM stdin;
1	OPD TICKET	001	create	2020-07-14 18:00:13.192208+05:45	5	1
2	OPD TICKET	001	edit	2020-07-15 14:01:13.307483+05:45	2	1
3	OPD TICKET 	002	create	2020-07-17 15:04:21.022493+05:45	2	1
4	EMERGENCY	003	create	2020-07-20 15:20:52.268231+05:45	5	1
5	EMERGENCY	004	create	2020-07-20 15:20:52.40458+05:45	5	1
6	EMERGENCY	003	edit	2020-07-20 15:22:05.443377+05:45	5	1
7	ER PROCEDURE	004	create	2020-07-20 15:41:25.386241+05:45	5	1
\.


--
-- Data for Name: login_rolepermissions; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.login_rolepermissions (id, permissions, group_id) FROM stdin;
1	{"dashboard.index": true, "account.index": true, "incomes.index": true, "incomes.create": true, "incomes.edit": true, "incomes.delete": true, "incometypes.index": true, "incometypes.create": true, "incometypes.edit": true, "incometypes.delete": true, "expenditures.index": true, "expenditures.create": true, "expenditures.edit": true, "expenditures.delete": true, "expendituretypes.index": true, "expendituretypes.create": true, "expendituretypes.edit": true, "expendituretypes.delete": true, "patients.index": true, "patients.create": true, "patients.edit": true, "patients.delete": true, "patients.bill": true, "patients.search": true, "downloads.index": true, "product.index": false, "product.create": false, "product.edit": false, "storein.index": true, "storein.create": true, "storein.edit": true, "storein.delete": true, "storeout.index": true, "storeout.create": true, "storeout.edit": true, "storeout.delete": true, "medicine.index": true, "medicine.create": true, "medicine.edit": true, "medicine.delete": true, "settings.index": true, "types.index": true, "types.create": true, "types.edit": true, "types.delete": true, "services.index": true, "services.create": true, "services.edit": true, "services.delete": true, "department.index": true, "department.create": true, "department.edit": true, "department.delete": true, "users.index": true, "users.create": true, "users.edit": true, "users.delete": true, "doctors.index": true, "doctors.create": true, "doctors.edit": true, "doctors.delete": true, "stafftypes.index": true, "stafftypes.create": true, "stafftypes.edit": true, "stafftypes.delete": true, "staff.index": true, "staff.create": true, "staff.edit": true, "staff.delete": true, "roles.index": true, "roles.edit": true, "profile.index": true}	2
\.


--
-- Data for Name: medicines; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.medicines (id, name, quantity, pack_unit, created_at, updated_at, created_by_id, batch, code) FROM stdin;
\.


--
-- Data for Name: metas_metas; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.metas_metas (id, key, value, hospital_id) FROM stdin;
4	country	Nepal	\N
6	bill_font_size	10	\N
3	province	Bagmati Province	\N
2	municipality_name	Melamchi Municipality	\N
1	current_year	77	\N
5	address	Melamchi, Sindupalchok	\N
13	application_name	Melamchi Primary Health Care Center	1
8	country	Nepal	1
11	current_year	77	1
10	bill_font_size	10	1
7	province	Bagmati Province	1
12	municipality_name	Melamchi Municipality	1
9	address	Melamchi, Sindhupalchok	1
\.


--
-- Data for Name: patients_historicalpatients; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.patients_historicalpatients (id, pcode, pname, page, psex, paddress, pphone, pcreated_by, pcreated_at, referred_by, history_id, history_date, history_change_reason, history_type, history_user_id, hospital_id, year_id) FROM stdin;
81	P77040769	SHOVA ADHAKARI	35	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 10:50:01.048069+05:45	2	1	2020-07-21 10:50:01.052118+05:45	\N	+	3	1	77
82	P77040769	JANUKA SAPKOTA	50	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 10:56:49.283372+05:45	2	2	2020-07-21 10:56:49.285824+05:45	\N	+	3	1	77
83	P77040769	GOMA KARKI	28	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 11:25:56.982493+05:45	2	3	2020-07-21 11:25:56.984788+05:45	\N	+	3	1	77
84	P77040769	PRATIMA SHRESTHA	20	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 11:31:40.338504+05:45	2	4	2020-07-21 11:31:40.340968+05:45	\N	+	3	1	77
85	P77040769	BHUPENDRA SONI	36	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 11:44:19.841515+05:45	2	5	2020-07-21 11:44:19.846181+05:45	\N	+	3	1	77
86	P77040769	SUNTALI MIJAR	49	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:01:25.75055+05:45	1	6	2020-07-21 12:01:25.754087+05:45	\N	+	3	1	77
87	P77040769	SANGITA LAMICHHANE	22	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:06:57.654316+05:45	2	7	2020-07-21 12:06:57.656667+05:45	\N	+	3	1	77
88	P77040769	SHREERAM KARAKI	28	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:15:19.020283+05:45	1	8	2020-07-21 12:15:19.024406+05:45	\N	+	3	1	77
89	P77040769	CHAMATI CHALISE	70	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:42:35.195075+05:45	2	9	2020-07-21 12:42:35.198424+05:45	\N	+	3	1	77
90	P77040769	SABITRI DULAL	37	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 12:53:11.714225+05:45	2	10	2020-07-21 12:53:11.716906+05:45	\N	+	3	1	77
91	P77040769	RITA DAHAL	45	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 13:06:54.676476+05:45	0	11	2020-07-21 13:06:54.67831+05:45	\N	+	3	1	77
92	P77040769	THULIMAYA TAMANG	40	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 14:12:45.071668+05:45	2	12	2020-07-21 14:12:45.074141+05:45	\N	+	3	1	77
93	P77040769	BINAD DULAL	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 14:15:02.207901+05:45	2	13	2020-07-21 14:15:02.26001+05:45	\N	+	3	1	77
94	P77040769	ANITA TAMANG	18	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-21 14:23:10.629375+05:45	2	14	2020-07-21 14:23:10.632265+05:45	\N	+	3	1	77
95	P77040769	FAUTHA K KARAKI	60	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:19:59.309899+05:45	2	15	2020-07-22 10:19:59.314652+05:45	\N	+	3	1	77
96	P77040769	PURANAMAYA JYOTI	28	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:22:40.75743+05:45	2	16	2020-07-22 10:22:40.760859+05:45	\N	+	3	1	77
97	P77040769	DAWA TAMANG	36	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:24:50.916253+05:45	2	17	2020-07-22 10:24:50.918619+05:45	\N	+	3	1	77
98	P77040769	NETRA K.ACHARYA	60	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:28:03.719641+05:45	2	18	2020-07-22 10:28:03.724083+05:45	\N	+	3	1	77
99	P77040769	SANGITA DULAL	21	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:29:39.642151+05:45	2	19	2020-07-22 10:29:39.644408+05:45	\N	+	3	1	77
100	P77040769	BINITA PANDIT	18	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:31:12.97996+05:45	2	20	2020-07-22 10:31:12.982533+05:45	\N	+	3	1	77
101	P77040769	SITA THAPA	40	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:32:44.185358+05:45	2	21	2020-07-22 10:32:44.188521+05:45	\N	+	3	1	77
102	P77040769	CHUSANG LAMA	36	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:35:07.632445+05:45	0	22	2020-07-22 10:35:07.635333+05:45	\N	+	3	1	77
103	P77040769	KOPILA BHUJEL	20	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:38:15.670105+05:45	2	23	2020-07-22 10:38:15.672854+05:45	\N	+	3	1	77
104	P77040769	NASIR TAMANG	18	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 10:52:09.019402+05:45	2	24	2020-07-22 10:52:09.021984+05:45	\N	+	3	1	77
105	P77040769	RAMMAYA GADAILI	22	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 11:14:12.290171+05:45	2	25	2020-07-22 11:14:12.294069+05:45	\N	+	3	1	77
106	P77040769	RENU K.C	26	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 11:17:53.448708+05:45	2	26	2020-07-22 11:17:53.451449+05:45	\N	+	3	1	77
107	P77040769	SUNITA THAPA	29	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:38:56.792386+05:45	2	27	2020-07-22 12:38:56.796187+05:45	\N	+	3	1	77
108	P77040769	RAJ TAMANG	21	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:40:09.106723+05:45	2	28	2020-07-22 12:40:09.112203+05:45	\N	+	3	1	77
109	P77040769	PRATIGYA TIWARI	26	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:41:37.105162+05:45	2	29	2020-07-22 12:41:37.108813+05:45	\N	+	3	1	77
110	P77040769	MINA TAMANG	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:43:36.751012+05:45	2	30	2020-07-22 12:43:36.753861+05:45	\N	+	3	1	77
111	P77040769	BIMALA TAMANG	23	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:44:41.475424+05:45	2	31	2020-07-22 12:44:41.478047+05:45	\N	+	3	1	77
112	P77040769	SITA TAMANAG	28	FEMALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:47:05.057496+05:45	2	32	2020-07-22 12:47:05.060397+05:45	\N	+	3	1	77
113	P77040769	SOM BDR,TAMANG	28	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:49:01.385972+05:45	2	33	2020-07-22 12:49:01.388272+05:45	\N	+	3	1	77
114	P77040769	JIT BDR MIJAR	37	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:52:34.233359+05:45	2	34	2020-07-22 12:52:34.240907+05:45	\N	+	3	1	77
115	P77040769	SHYAM BDR ACHARYA	69	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 12:55:03.149097+05:45	2	35	2020-07-22 12:55:03.152304+05:45	\N	+	3	1	77
116	P77040769	DINESH TAMANG	28	MALE	MELAMCHI,SINDHUPALCHOK	\N	3	2020-07-22 13:01:04.032367+05:45	2	36	2020-07-22 13:01:04.036504+05:45	\N	+	3	1	77
\.


--
-- Data for Name: pharmacies; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.pharmacies (id, pan, phone, location, name, created_at, updated_at, created_by, hospital_id, user_id) FROM stdin;
1	12345	9849595565	Melamchi, Sindhupalchowk	Melamchi Primary health care	2020-07-14	2020-07-17		1	1
\.


--
-- Data for Name: productstore_product; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_product (id, code, name, type, unit, quantity, specification, created_at, updated_at, created_by, category_id, hospital_id, rate) FROM stdin;
\.


--
-- Data for Name: productstore_productcategory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_productcategory (id, name, code, created_at, updated_at, created_by, hospital_id) FROM stdin;
\.


--
-- Data for Name: productstore_productstorein; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_productstorein (id, code, purchase_number, purchase_date, purchase_decision_number, purchase_decision_date, total, created_at, updated_at, seller_details_id) FROM stdin;
\.


--
-- Data for Name: productstore_productstoreout; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_productstoreout (id, invoice_out_number, created_at, updated_at, created_by) FROM stdin;
\.


--
-- Data for Name: productstore_requestform; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_requestform (id, created_at, updated_at, created_by, request_aawa, request_number) FROM stdin;
\.


--
-- Data for Name: productstore_requestform_product; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_requestform_product (id, requestform_id, requestproduct_id) FROM stdin;
\.


--
-- Data for Name: productstore_requestproduct; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_requestproduct (id, name, unit, quantity, specification, remarks) FROM stdin;
\.


--
-- Data for Name: productstore_sellerdetails; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_sellerdetails (id, name, address, registration_number, pan_number, phone_number, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: productstore_storeinhasproducts; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_storeinhasproducts (id, product_commodity_number, quantity, rate, remarks, product_id, store_in_id) FROM stdin;
\.


--
-- Data for Name: productstore_storeouthasproduct; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.productstore_storeouthasproduct (id, product_commodity_number, quantity, rate, remarks, product_id, store_out_id) FROM stdin;
\.


--
-- Data for Name: service_type; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.service_type (id, tname, hospital_id, code, is_incentive) FROM stdin;
1	X-RAY	1	01001	f
2	LAB	1	01002	f
4	PROCEDURE	1	01004	f
6	MEDICOLEGAL SECTION	1	01006	f
9	DELIVERY	1	01009	f
3	EMERGENCY TICKET	1	01003	f
5	OPD TICKET	1	01005	f
10	MEDICAL CERTIFICATE	1	01010	f
11	MATERNITY WAITING HOME	1	01011	f
12	USG	1	01012	f
13	AMBULANCE	1	01013	f
\.


--
-- Data for Name: services_serviceshistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.services_serviceshistory (id, scode, sname, sprice, action, "actionDate", "actionBy_id", hospital_id, stype_id) FROM stdin;
1	S01001001	HAND AP/LAT/OB	350	create	2020-07-14 16:26:58.55446+05:45	2	1	1
2	S01001002	WRIST AP/LAT	350	create	2020-07-14 16:27:40.9034+05:45	2	1	1
3	S01001002	FOREARM AP/LT 	350	create	2020-07-14 16:28:18.202323+05:45	2	1	1
4	S01001002	ELBOW AP/LT	350	create	2020-07-14 16:28:30.709733+05:45	2	1	1
5	S01001002	HUMERUS AP/LT	350	create	2020-07-14 16:28:42.622137+05:45	2	1	1
6	S01001002	SHOULDER AP/LAT	350	create	2020-07-14 16:29:02.691707+05:45	2	1	1
7	S01001002	CLAVICLE	350	create	2020-07-14 16:29:09.846279+05:45	2	1	1
8	S01001002	CHEST-PA/AP	350	create	2020-07-14 16:29:33.164533+05:45	2	1	1
9	S01001002	SKULL-LT/PA	350	create	2020-07-14 16:30:18.308669+05:45	2	1	1
10	S01001002	PNS-OM/TOWNE	350	create	2020-07-14 16:30:34.000946+05:45	2	1	1
11	S01001002	CERVICAL SPINE-AP/LT	350	create	2020-07-14 16:30:48.541683+05:45	2	1	1
12	S01001002	LUMBAR-AP/LT	350	create	2020-07-14 16:30:58.737311+05:45	2	1	1
13	S01001002	KUB	350	create	2020-07-14 16:31:15.373893+05:45	2	1	1
14	S01001002	ABDOMEN-ERECT/SUPINE	350	create	2020-07-14 16:31:36.900961+05:45	2	1	1
15	S01001002	DORSO-LUMBAR-AP/LT	350	create	2020-07-14 16:32:03.194508+05:45	2	1	1
16	S01001002	PELVIS	350	create	2020-07-14 16:32:16.625901+05:45	2	1	1
17	S01001002	FEMUR-AP/LT	350	create	2020-07-14 16:32:26.153288+05:45	2	1	1
18	S01001002	KNEE-AP/LAT	350	create	2020-07-14 16:33:02.685915+05:45	2	1	1
19	S01001002	KNEE-SKYLINE	350	create	2020-07-14 16:33:13.750283+05:45	2	1	1
20	S01001002	TIBIA-FIBULA-AP/LT	350	create	2020-07-14 16:34:59.924984+05:45	2	1	1
21	S01001002	ANKLE-AP/LT/MORTISE	350	create	2020-07-14 16:35:33.294259+05:45	2	1	1
22	S01001002	FOOT-AP/OB	350	create	2020-07-14 16:35:52.819734+05:45	2	1	1
23	S01002001	HEMOGLOBIN(HB)	50	create	2020-07-14 16:39:55.090466+05:45	2	1	2
24	S01002002	TOTAL WBC COUNT	50	create	2020-07-14 16:40:51.908774+05:45	2	1	2
25	S01002002	DIFFERENTIAL COUNT	50	create	2020-07-14 16:41:34.841044+05:45	2	1	2
26	S01002002	RBC COUNT	150	create	2020-07-14 16:41:59.990324+05:45	2	1	2
27	S01002002	PLATELET COUNT	150	create	2020-07-14 16:42:13.619681+05:45	2	1	2
28	S01002002	ESR	50	create	2020-07-14 16:42:21.740012+05:45	2	1	2
29	S01002002	BLOOD GROUPING	100	create	2020-07-14 16:42:35.293362+05:45	2	1	2
30	S01002002	CRP	150	create	2020-07-14 16:42:41.62948+05:45	2	1	2
31	S01002002	RBS	50	create	2020-07-14 16:42:58.442033+05:45	2	1	2
32	S01002002	FBS/PP	100	create	2020-07-14 16:43:18.312593+05:45	2	1	2
33	S01002002	WIDAL	150	create	2020-07-14 16:43:47.119141+05:45	2	1	2
34	S01002002	ASO	150	create	2020-07-14 16:43:57.268379+05:45	2	1	2
35	S01002002	BILIRUBIN	150	create	2020-07-14 16:44:04.48399+05:45	2	1	2
36	S01002002	RA FACTOR	150	create	2020-07-14 16:44:45.502865+05:45	2	1	2
37	S01002002	UREA	150	create	2020-07-14 16:44:56.881203+05:45	2	1	2
38	S01002002	CREATININE	150	create	2020-07-14 16:45:09.866998+05:45	2	1	2
39	S01002002	URIC ACID	150	create	2020-07-14 16:45:18.812499+05:45	2	1	2
40	S01002002	MP SEROLOGY	0	create	2020-07-14 16:45:48.37016+05:45	2	1	2
41	S01002002	HBSAG	250	create	2020-07-14 16:46:03.410164+05:45	2	1	2
42	S01002002	VDRL	150	create	2020-07-14 16:46:10.633842+05:45	2	1	2
43	S01002002	HCV	250	create	2020-07-14 16:46:18.897709+05:45	2	1	2
44	S01002002	HIV	0	create	2020-07-14 16:46:23.873411+05:45	2	1	2
45	S01002002	SPUTUM AFB	0	create	2020-07-14 16:46:37.830481+05:45	2	1	2
46	S01002002	URINE RE/ME	100	create	2020-07-14 16:46:50.257652+05:45	2	1	2
47	S01002002	STOOL RE/ME	100	create	2020-07-14 16:46:59.690255+05:45	2	1	2
48	S01002002	UPT	100	create	2020-07-14 16:47:06.898147+05:45	2	1	2
49	S01002002	ANC PACKAGE	500	create	2020-07-14 16:47:16.946334+05:45	2	1	2
50	S01002002	TROP I	350	create	2020-07-14 16:47:27.060853+05:45	2	1	2
51	S01002002	H.PYLORI STOOL AG	250	create	2020-07-14 16:47:38.220923+05:45	2	1	2
52	S01002002	LIPID PROFILE	650	create	2020-07-14 16:47:46.105347+05:45	2	1	2
53	S01002002	TRIGLYCERIDE	150	create	2020-07-14 16:48:00.314571+05:45	2	1	2
54	S01002002	TOTAL CHOLESTEROL	150	create	2020-07-14 16:48:50.744483+05:45	2	1	2
55	S01002002	LFT	450	create	2020-07-14 16:49:04.416923+05:45	2	1	2
56	S01002002	RFT	400	create	2020-07-14 16:49:21.762721+05:45	2	1	2
57	S01002002	STOOL OCCULT BLOOD TEST	150	create	2020-07-14 16:49:39.781102+05:45	2	1	2
58	S01002002	URINE KETONE	100	create	2020-07-14 16:50:05.769069+05:45	2	1	2
59	S01002002	AST/ALT	150	create	2020-07-14 16:51:35.454706+05:45	2	1	2
60	S01004001	SIMPLE DRESSING	25	create	2020-07-14 16:54:26.505448+05:45	2	1	4
61	S01004002	I AND D-LA	75	create	2020-07-14 16:56:26.793885+05:45	2	1	4
62	S01004002	I AND D-KETAMINE	200	create	2020-07-14 16:56:38.215959+05:45	2	1	4
63	S01004002	SUTURE	100	create	2020-07-14 16:57:40.535982+05:45	2	1	4
64	S01004002	SYRINGING-1 SIDE	50	create	2020-07-14 16:58:12.4337+05:45	2	1	4
65	S01004002	EAR/NASAL PACKING	50	create	2020-07-14 16:58:28.855515+05:45	2	1	4
66	S01004002	EXCISION-LIPOMA/CYST	250	create	2020-07-14 16:58:51.300958+05:45	2	1	4
67	S01004002	EXCISION- INGROWING TOE NAIL	250	create	2020-07-14 16:59:14.867449+05:45	2	1	4
68	S01004002	EXCISION-CORN	250	create	2020-07-14 16:59:39.130072+05:45	2	1	4
69	S01004002	NEBULIZATION	25	create	2020-07-14 16:59:51.81147+05:45	2	1	4
70	S01004002	ECG	250	create	2020-07-14 17:00:00.347125+05:45	2	1	4
71	S01004002	USG-ANC	0	create	2020-07-14 17:00:59.397416+05:45	2	1	4
72	S01004002	INJ. TT	50	create	2020-07-14 17:01:08.997116+05:45	2	1	4
73	S01004002	ADMISSION CHARGE	50	create	2020-07-14 17:01:22.277314+05:45	2	1	4
74	S01004002	PLASTER SLAB	300	create	2020-07-14 17:01:33.42422+05:45	2	1	4
75	S01004002	PLASTER CAST-HAND	400	create	2020-07-14 17:01:48.283489+05:45	2	1	4
76	S01004002	PLASTER CAST-LEG	500	create	2020-07-14 17:02:23.368374+05:45	2	1	4
77	S01004002	SAFE ABORTION SERVICE	1000	create	2020-07-14 17:02:42.880603+05:45	2	1	4
78	S01004002	PLASTER REMOVAL	100	create	2020-07-14 17:03:01.485405+05:45	2	1	4
79	S01004002	INTRA-ARTICULAR INJECTION	50	create	2020-07-14 17:03:15.89305+05:45	2	1	4
80	S01004002	INTRA-LESIONAL INJECTION	50	create	2020-07-14 17:04:10.072639+05:45	2	1	4
81	S01004002	WOUND EXAMINATION	300	create	2020-07-14 17:05:26.537627+05:45	2	1	4
82	S01006001	RAPE CASE	300	create	2020-07-14 17:05:48.501283+05:45	2	1	6
83	S01006002	BIRTH CERTIFICATE	50	create	2020-07-14 17:06:12.899589+05:45	2	1	6
84	S01004002	WOUND EXAMINATION	300	edit	2020-07-14 17:06:42.570823+05:45	2	1	4
85	S01010003	MEDICAL CERTIFICATE	500	create	2020-07-14 17:07:19.345817+05:45	2	1	10
86	S01010001	USG-NON-ROUTINE	250	create	2020-07-14 17:08:07.417569+05:45	2	1	12
87	S01003001	EMERGENCY TICKET	50	create	2020-07-14 17:08:31.502651+05:45	2	1	3
88	S01005001	OPD 	15	create	2020-07-14 17:08:49.442051+05:45	2	1	5
\.


--
-- Data for Name: staffType_stafftypehistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public."staffType_stafftypehistory" (id, tname, code, permissions, action, "actionDate", "actionBy_id", hospital_id) FROM stdin;
1	CASHIER	001	{"dashboard.index": true, "account.index": false, "patients.index": true, "patients.create": true, "patients.edit": true, "patients.delete": false, "patients.search": true, "patients.bill": true, "downloads.index": true, "types.index": true, "types.create": true, "types.edit": true, "services.index": true, "services.create": true, "services.edit": true, "department.index": true, "department.create": true, "department.edit": true, "profile.index": true}	create	2020-07-14 15:50:43.101992+05:45	2	1
2	ACCOUNTANT 	002	{"dashboard.index": true, "account.index": true, "incomes.index": true, "incomes.create": true, "incomes.edit": true, "incomes.delete": false, "incometypes.index": true, "incometypes.create": true, "incometypes.edit": true, "incometypes.delete": false, "expenditures.index": true, "expenditures.create": true, "expenditures.edit": true, "expendituretypes.index": true, "expendituretypes.create": true, "expendituretypes.edit": true, "patients.index": true, "patients.create": true, "patients.edit": true, "patients.delete": false, "patients.bill": true, "patients.search": true, "downloads.index": true, "storein.index": false, "storein.create": false, "storein.edit": false, "storein.delete": false, "types.index": true, "types.create": true, "types.edit": true, "services.index": true, "services.create": true, "services.edit": true, "department.index": true, "department.create": true, "department.edit": true, "profile.index": true}	create	2020-07-14 15:51:34.972859+05:45	2	1
3	CASHIER	001	{"dashboard.index": true, "account.index": false, "patients.index": true, "patients.create": true, "patients.edit": true, "patients.delete": false, "patients.search": true, "patients.bill": true, "downloads.index": true, "types.index": true, "types.create": true, "types.edit": true, "services.index": true, "services.create": true, "services.edit": true, "department.index": true, "department.create": true, "department.edit": true, "profile.index": true}	edit	2020-07-14 16:00:01.036243+05:45	2	1
4	DOCTOR 	003	{"dashboard.index": true, "account.index": true, "incomes.index": true, "incomes.create": true, "incomes.edit": true, "incomes.delete": true, "incometypes.index": true, "incometypes.create": true, "incometypes.edit": true, "incometypes.delete": true, "expenditures.index": true, "expenditures.create": true, "expenditures.edit": true, "expenditures.delete": true, "expendituretypes.index": true, "expendituretypes.create": true, "expendituretypes.edit": true, "expendituretypes.delete": true, "patients.index": true, "patients.create": true, "patients.edit": true, "patients.delete": true, "patients.bill": true, "patients.search": true, "downloads.index": true, "storein.index": false, "storein.create": false, "settings.index": true, "types.index": true, "types.create": true, "types.edit": true, "types.delete": true, "services.index": true, "services.create": true, "services.edit": true, "services.delete": true, "department.index": true, "department.create": true, "department.edit": true, "department.delete": true, "profile.index": true}	create	2020-07-14 16:19:40.839283+05:45	2	1
5	NURSE	004	{"dashboard.index": true, "account.index": true, "profile.index": true}	create	2020-07-14 16:20:46.497011+05:45	2	1
6	SAMITI	005	{"dashboard.index": true, "profile.index": true}	create	2020-07-14 16:21:22.682961+05:45	2	1
7	LAB	006	{"dashboard.index": true, "profile.index": true}	create	2020-07-14 16:21:38.577188+05:45	2	1
8	RADIOGRAPHER	007	{"dashboard.index": true, "profile.index": true}	create	2020-07-14 16:21:47.879929+05:45	2	1
9	OFFICE ASSISTANT	008	{"dashboard.index": true, "profile.index": true}	create	2020-07-14 16:21:58.605906+05:45	2	1
10	PARAMED 	009	{"dashboard.index": true, "profile.index": true}	create	2020-07-14 16:22:44.234673+05:45	2	1
11	ALL STAFF	010	{"dashboard.index": true, "profile.index": true}	create	2020-07-14 16:23:50.115532+05:45	2	1
12	AMBULANCE STAFF	011	{"profile.index": true}	create	2020-07-14 16:24:38.845806+05:45	2	1
\.


--
-- Data for Name: staff_staffhistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.staff_staffhistory (id, scode, sname, sphone, semail, action, "actionDate", "actionBy_id", hospital_id, sdepart_id, stype_id, year_id) FROM stdin;
1	2020071	DIPESH DHITAL	9860279876	melamchiphc@gmail.com	create	2020-07-14 15:54:38.01658+05:45	3	1	1	1	77
2	2020072	BINDESHWORI GYALANG	9845149081	melamchiphc@gmail.com	create	2020-07-14 15:55:21.606624+05:45	4	1	2	2	77
3	2020073	JAY PRAKASH SHAH	9840729198	shahjayprakash978@gmail.com	create	2020-07-14 15:56:35.154793+05:45	5	1	2	2	77
4	2020071	DIPESH DHITAL	9860279876	melamchiphc@gmail.com	edit	2020-07-14 16:02:01.47174+05:45	3	1	1	1	77
\.


--
-- Data for Name: staff_type; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.staff_type (id, tname, code, created_at, updated_at, created_by, updated_by, hospital_id, permissions) FROM stdin;
2	ACCOUNTANT	002	2020-07-14 15:51:34.981093+05:45	2020-07-14 15:51:34.981114+05:45	\N	\N	1	{"dashboard.index": true, "account.index": true, "incomes.index": true, "incomes.create": true, "incomes.edit": true, "incomes.delete": false, "incometypes.index": true, "incometypes.create": true, "incometypes.edit": true, "incometypes.delete": false, "expenditures.index": true, "expenditures.create": true, "expenditures.edit": true, "expendituretypes.index": true, "expendituretypes.create": true, "expendituretypes.edit": true, "patients.index": true, "patients.create": true, "patients.edit": true, "patients.delete": false, "patients.bill": true, "patients.search": true, "downloads.index": true, "storein.index": false, "storein.create": false, "storein.edit": false, "storein.delete": false, "types.index": true, "types.create": true, "types.edit": true, "services.index": true, "services.create": true, "services.edit": true, "department.index": true, "department.create": true, "department.edit": true, "profile.index": true}
1	CASHIER	001	2020-07-14 15:50:43.116204+05:45	2020-07-14 16:00:01.045425+05:45	\N	\N	1	{"dashboard.index": true, "account.index": false, "patients.index": true, "patients.create": true, "patients.edit": true, "patients.delete": true, "patients.search": true, "patients.bill": true, "downloads.index": true, "types.index": true, "types.create": true, "types.edit": true, "services.index": true, "services.create": true, "services.edit": true, "department.index": true, "department.create": true, "department.edit": true, "profile.index": true}
3	DOCTOR	003	2020-07-14 16:19:40.848391+05:45	2020-07-14 16:19:40.848415+05:45	\N	\N	1	{"dashboard.index": true, "account.index": true, "incomes.index": true, "incomes.create": true, "incomes.edit": true, "incomes.delete": true, "incometypes.index": true, "incometypes.create": true, "incometypes.edit": true, "incometypes.delete": true, "expenditures.index": true, "expenditures.create": true, "expenditures.edit": true, "expenditures.delete": true, "expendituretypes.index": true, "expendituretypes.create": true, "expendituretypes.edit": true, "expendituretypes.delete": true, "patients.index": true, "patients.create": true, "patients.edit": true, "patients.delete": true, "patients.bill": true, "patients.search": true, "downloads.index": true, "storein.index": false, "storein.create": false, "settings.index": true, "types.index": true, "types.create": true, "types.edit": true, "types.delete": true, "services.index": true, "services.create": true, "services.edit": true, "services.delete": true, "department.index": true, "department.create": true, "department.edit": true, "department.delete": true, "profile.index": true}
4	NURSE	004	2020-07-14 16:20:46.504384+05:45	2020-07-14 16:20:46.504407+05:45	\N	\N	1	{"dashboard.index": true, "account.index": true, "profile.index": true}
5	SAMITI	005	2020-07-14 16:21:22.691073+05:45	2020-07-14 16:21:22.691103+05:45	\N	\N	1	{"dashboard.index": true, "profile.index": true}
6	LAB	006	2020-07-14 16:21:38.584743+05:45	2020-07-14 16:21:38.584766+05:45	\N	\N	1	{"dashboard.index": true, "profile.index": true}
7	RADIOGRAPHER	007	2020-07-14 16:21:47.888146+05:45	2020-07-14 16:21:47.888169+05:45	\N	\N	1	{"dashboard.index": true, "profile.index": true}
8	OFFICE ASSISTANT	008	2020-07-14 16:21:58.613389+05:45	2020-07-14 16:21:58.613411+05:45	\N	\N	1	{"dashboard.index": true, "profile.index": true}
9	PARAMED	009	2020-07-14 16:22:44.249734+05:45	2020-07-14 16:22:44.249758+05:45	\N	\N	1	{"dashboard.index": true, "profile.index": true}
10	ALL STAFF	010	2020-07-14 16:23:50.125661+05:45	2020-07-14 16:23:50.125684+05:45	\N	\N	1	{"dashboard.index": true, "profile.index": true}
11	AMBULANCE STAFF	011	2020-07-14 16:24:38.85497+05:45	2020-07-14 16:24:38.855003+05:45	\N	\N	1	{"profile.index": true}
\.


--
-- Data for Name: store_productservice; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.store_productservice (id, quantity, unit_price, created_at, updated_at, product_id, batch, expiry_date, manufacture_date, packs) FROM stdin;
\.


--
-- Data for Name: store_storeinhistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.store_storeinhistory (id, name, quantity, unit_price, batch, pack, manufacture_date, expiry_date, action, "actionDate", "actionBy_id", hospital_id, year_id) FROM stdin;
\.


--
-- Data for Name: store_storeouthistory; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.store_storeouthistory (id, buyer, action, "actionDate", "actionBy_id", hospital_id, year_id) FROM stdin;
\.


--
-- Data for Name: store_storeouthistory_orders; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.store_storeouthistory_orders (id, storeouthistory_id, productservice_id) FROM stdin;
\.


--
-- Data for Name: storein; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.storein (id, name, quantity, unit_price, created_at, updated_at, created_by_id, hospital_id, year_id, batch, expiry_date, manufacture_date, pack) FROM stdin;
\.


--
-- Data for Name: storeout; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.storeout (id, created_at, updated_at, created_by_id, hospital_id, buyer, year_id) FROM stdin;
\.


--
-- Data for Name: storeout_orders; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.storeout_orders (id, storeout_id, productservice_id) FROM stdin;
\.


--
-- Data for Name: type_servicehasincentive; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.type_servicehasincentive (id, percentage, stafftype_id, type_id) FROM stdin;
\.


--
-- Data for Name: wards; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.wards (id, name) FROM stdin;
1	Ward No. 1
2	Ward No. 2
3	Ward No. 3
4	Ward No. 4
5	Ward No. 5
6	Ward No. 6
7	Ward No. 7
8	Ward No. 8
9	Ward No. 9
10	Ward No. 10
11	Ward No. 11
12	Ward No. 12
13	Ward No. 13
\.


--
-- Data for Name: years; Type: TABLE DATA; Schema: public; Owner: hos
--

COPY public.years (year) FROM stdin;
77
76
\.


--
-- Name: Doctors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public."Doctors_id_seq"', 3, true);


--
-- Name: Patient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public."Patient_id_seq"', 116, true);


--
-- Name: Services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public."Services_id_seq"', 87, true);


--
-- Name: Staff_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public."Staff_id_seq"', 3, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 5, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 220, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 8, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 8, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: bill_historicalbill_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.bill_historicalbill_history_id_seq', 40, true);


--
-- Name: bill_historicalbillservice_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.bill_historicalbillservice_history_id_seq', 61, true);


--
-- Name: bill_services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.bill_services_id_seq', 183, true);


--
-- Name: bills_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.bills_id_seq', 129, true);


--
-- Name: department_departmenthistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.department_departmenthistory_id_seq', 5, true);


--
-- Name: departments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.departments_id_seq', 3, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 55, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 110, true);


--
-- Name: doctor_doctorshistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.doctor_doctorshistory_id_seq', 3, true);


--
-- Name: expenditure_expenditurehistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.expenditure_expenditurehistory_id_seq', 4, true);


--
-- Name: expenditure_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.expenditure_type_id_seq', 7, true);


--
-- Name: expendituretype_expendituretypehistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.expendituretype_expendituretypehistory_id_seq', 8, true);


--
-- Name: expenses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.expenses_id_seq', 3, true);


--
-- Name: hospital_has_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.hospital_has_users_id_seq', 6, true);


--
-- Name: hospitals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.hospitals_id_seq', 1, true);


--
-- Name: income_incomehistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.income_incomehistory_id_seq', 8, true);


--
-- Name: income_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.income_type_id_seq', 4, true);


--
-- Name: incomes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.incomes_id_seq', 7, true);


--
-- Name: incometype_incometypehistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.incometype_incometypehistory_id_seq', 7, true);


--
-- Name: login_rolepermissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.login_rolepermissions_id_seq', 1, true);


--
-- Name: medicines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.medicines_id_seq', 1, false);


--
-- Name: metas_metas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.metas_metas_id_seq', 13, true);


--
-- Name: patients_historicalpatients_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.patients_historicalpatients_history_id_seq', 36, true);


--
-- Name: pharmacies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.pharmacies_id_seq', 1, true);


--
-- Name: productstore_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_product_id_seq', 1, false);


--
-- Name: productstore_productcategory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_productcategory_id_seq', 1, false);


--
-- Name: productstore_productstorein_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_productstorein_id_seq', 1, false);


--
-- Name: productstore_productstoreout_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_productstoreout_id_seq', 1, false);


--
-- Name: productstore_requestform_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_requestform_id_seq', 1, false);


--
-- Name: productstore_requestform_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_requestform_product_id_seq', 1, false);


--
-- Name: productstore_requestproduct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_requestproduct_id_seq', 1, false);


--
-- Name: productstore_sellerdetails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_sellerdetails_id_seq', 1, false);


--
-- Name: productstore_storeinhasproducts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_storeinhasproducts_id_seq', 1, false);


--
-- Name: productstore_storeouthasproduct_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.productstore_storeouthasproduct_id_seq', 1, false);


--
-- Name: services_serviceshistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.services_serviceshistory_id_seq', 88, true);


--
-- Name: staffType_stafftypehistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public."staffType_stafftypehistory_id_seq"', 12, true);


--
-- Name: staff_staffhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.staff_staffhistory_id_seq', 4, true);


--
-- Name: staff_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.staff_type_id_seq', 11, true);


--
-- Name: store_productservice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.store_productservice_id_seq', 1, false);


--
-- Name: store_storeinhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.store_storeinhistory_id_seq', 1, false);


--
-- Name: store_storeouthistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.store_storeouthistory_id_seq', 1, false);


--
-- Name: store_storeouthistory_orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.store_storeouthistory_orders_id_seq', 1, false);


--
-- Name: storein_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.storein_id_seq', 1, false);


--
-- Name: storeout_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.storeout_id_seq', 1, false);


--
-- Name: storeout_orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.storeout_orders_id_seq', 1, false);


--
-- Name: type_servicehasincentive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.type_servicehasincentive_id_seq', 1, false);


--
-- Name: type_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.type_type_id_seq', 13, true);


--
-- Name: wards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hos
--

SELECT pg_catalog.setval('public.wards_id_seq', 13, true);


--
-- Name: Doctors Doctors_dnmc_key; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Doctors"
    ADD CONSTRAINT "Doctors_dnmc_key" UNIQUE (dnmc);


--
-- Name: Doctors Doctors_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Doctors"
    ADD CONSTRAINT "Doctors_pkey" PRIMARY KEY (id);


--
-- Name: Patient Patient_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Patient"
    ADD CONSTRAINT "Patient_pkey" PRIMARY KEY (id);


--
-- Name: Services Services_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Services"
    ADD CONSTRAINT "Services_pkey" PRIMARY KEY (id);


--
-- Name: Services Services_sname_sprice_stype_id_hospital_id_2631d215_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Services"
    ADD CONSTRAINT "Services_sname_sprice_stype_id_hospital_id_2631d215_uniq" UNIQUE (sname, sprice, stype_id, hospital_id);


--
-- Name: Staff Staff_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Staff"
    ADD CONSTRAINT "Staff_pkey" PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: bill_historicalbill bill_historicalbill_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_historicalbill
    ADD CONSTRAINT bill_historicalbill_pkey PRIMARY KEY (history_id);


--
-- Name: bill_historicalbillservice bill_historicalbillservice_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_historicalbillservice
    ADD CONSTRAINT bill_historicalbillservice_pkey PRIMARY KEY (history_id);


--
-- Name: bill_services bill_services_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_services
    ADD CONSTRAINT bill_services_pkey PRIMARY KEY (id);


--
-- Name: bills bills_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bills
    ADD CONSTRAINT bills_pkey PRIMARY KEY (id);


--
-- Name: department_departmenthistory department_departmenthistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.department_departmenthistory
    ADD CONSTRAINT department_departmenthistory_pkey PRIMARY KEY (id);


--
-- Name: departments departments_dname_hospital_id_1ede5904_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.departments
    ADD CONSTRAINT departments_dname_hospital_id_1ede5904_uniq UNIQUE (dname, hospital_id);


--
-- Name: departments departments_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.departments
    ADD CONSTRAINT departments_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: doctor_doctorshistory doctor_doctorshistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.doctor_doctorshistory
    ADD CONSTRAINT doctor_doctorshistory_pkey PRIMARY KEY (id);


--
-- Name: expenditure_expenditurehistory expenditure_expenditurehistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_expenditurehistory
    ADD CONSTRAINT expenditure_expenditurehistory_pkey PRIMARY KEY (id);


--
-- Name: expenditure_type expenditure_type_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_type
    ADD CONSTRAINT expenditure_type_pkey PRIMARY KEY (id);


--
-- Name: expenditure_type expenditure_type_tname_hospital_id_fa908af0_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_type
    ADD CONSTRAINT expenditure_type_tname_hospital_id_fa908af0_uniq UNIQUE (tname, hospital_id);


--
-- Name: expendituretype_expendituretypehistory expendituretype_expendituretypehistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expendituretype_expendituretypehistory
    ADD CONSTRAINT expendituretype_expendituretypehistory_pkey PRIMARY KEY (id);


--
-- Name: expenses expenses_amount_paid_by_itype_id_hospital_id_ec82bc3b_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenses
    ADD CONSTRAINT expenses_amount_paid_by_itype_id_hospital_id_ec82bc3b_uniq UNIQUE (amount, paid_by, itype_id, hospital_id);


--
-- Name: expenses expenses_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenses
    ADD CONSTRAINT expenses_pkey PRIMARY KEY (id);


--
-- Name: hospital_has_users hospital_has_users_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.hospital_has_users
    ADD CONSTRAINT hospital_has_users_pkey PRIMARY KEY (id);


--
-- Name: hospitals hospitals_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.hospitals
    ADD CONSTRAINT hospitals_pkey PRIMARY KEY (id);


--
-- Name: income_incomehistory income_incomehistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_incomehistory
    ADD CONSTRAINT income_incomehistory_pkey PRIMARY KEY (id);


--
-- Name: income_type income_type_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_type
    ADD CONSTRAINT income_type_pkey PRIMARY KEY (id);


--
-- Name: income_type income_type_tname_hospital_id_ebeb7308_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_type
    ADD CONSTRAINT income_type_tname_hospital_id_ebeb7308_uniq UNIQUE (tname, hospital_id);


--
-- Name: incomes incomes_amount_received_by_itype_id_hospital_id_469c4c0a_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incomes
    ADD CONSTRAINT incomes_amount_received_by_itype_id_hospital_id_469c4c0a_uniq UNIQUE (amount, received_by, itype_id, hospital_id);


--
-- Name: incomes incomes_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incomes
    ADD CONSTRAINT incomes_pkey PRIMARY KEY (id);


--
-- Name: incometype_incometypehistory incometype_incometypehistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incometype_incometypehistory
    ADD CONSTRAINT incometype_incometypehistory_pkey PRIMARY KEY (id);


--
-- Name: login_rolepermissions login_rolepermissions_group_id_key; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.login_rolepermissions
    ADD CONSTRAINT login_rolepermissions_group_id_key UNIQUE (group_id);


--
-- Name: login_rolepermissions login_rolepermissions_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.login_rolepermissions
    ADD CONSTRAINT login_rolepermissions_pkey PRIMARY KEY (id);


--
-- Name: years login_year_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.years
    ADD CONSTRAINT login_year_pkey PRIMARY KEY (year);


--
-- Name: medicines medicines_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.medicines
    ADD CONSTRAINT medicines_pkey PRIMARY KEY (id);


--
-- Name: metas_metas metas_metas_key_value_hospital_id_b1fa534a_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.metas_metas
    ADD CONSTRAINT metas_metas_key_value_hospital_id_b1fa534a_uniq UNIQUE (key, value, hospital_id);


--
-- Name: metas_metas metas_metas_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.metas_metas
    ADD CONSTRAINT metas_metas_pkey PRIMARY KEY (id);


--
-- Name: patients_historicalpatients patients_historicalpatients_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.patients_historicalpatients
    ADD CONSTRAINT patients_historicalpatients_pkey PRIMARY KEY (history_id);


--
-- Name: pharmacies pharmacies_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.pharmacies
    ADD CONSTRAINT pharmacies_pkey PRIMARY KEY (id);


--
-- Name: productstore_product productstore_product_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_product
    ADD CONSTRAINT productstore_product_pkey PRIMARY KEY (id);


--
-- Name: productstore_productcategory productstore_productcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_productcategory
    ADD CONSTRAINT productstore_productcategory_pkey PRIMARY KEY (id);


--
-- Name: productstore_productstorein productstore_productstorein_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_productstorein
    ADD CONSTRAINT productstore_productstorein_pkey PRIMARY KEY (id);


--
-- Name: productstore_productstoreout productstore_productstoreout_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_productstoreout
    ADD CONSTRAINT productstore_productstoreout_pkey PRIMARY KEY (id);


--
-- Name: productstore_requestform productstore_requestform_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestform
    ADD CONSTRAINT productstore_requestform_pkey PRIMARY KEY (id);


--
-- Name: productstore_requestform_product productstore_requestform_product_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestform_product
    ADD CONSTRAINT productstore_requestform_product_pkey PRIMARY KEY (id);


--
-- Name: productstore_requestform_product productstore_requestform_requestform_id_requestpr_591cdecf_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestform_product
    ADD CONSTRAINT productstore_requestform_requestform_id_requestpr_591cdecf_uniq UNIQUE (requestform_id, requestproduct_id);


--
-- Name: productstore_requestproduct productstore_requestproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestproduct
    ADD CONSTRAINT productstore_requestproduct_pkey PRIMARY KEY (id);


--
-- Name: productstore_sellerdetails productstore_sellerdetails_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_sellerdetails
    ADD CONSTRAINT productstore_sellerdetails_pkey PRIMARY KEY (id);


--
-- Name: productstore_storeinhasproducts productstore_storeinhasproducts_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_storeinhasproducts
    ADD CONSTRAINT productstore_storeinhasproducts_pkey PRIMARY KEY (id);


--
-- Name: productstore_storeouthasproduct productstore_storeouthasproduct_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_storeouthasproduct
    ADD CONSTRAINT productstore_storeouthasproduct_pkey PRIMARY KEY (id);


--
-- Name: services_serviceshistory services_serviceshistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.services_serviceshistory
    ADD CONSTRAINT services_serviceshistory_pkey PRIMARY KEY (id);


--
-- Name: staffType_stafftypehistory staffType_stafftypehistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."staffType_stafftypehistory"
    ADD CONSTRAINT "staffType_stafftypehistory_pkey" PRIMARY KEY (id);


--
-- Name: staff_staffhistory staff_staffhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_staffhistory
    ADD CONSTRAINT staff_staffhistory_pkey PRIMARY KEY (id);


--
-- Name: staff_type staff_type_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_type
    ADD CONSTRAINT staff_type_pkey PRIMARY KEY (id);


--
-- Name: staff_type staff_type_tname_hospital_id_1836baf2_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_type
    ADD CONSTRAINT staff_type_tname_hospital_id_1836baf2_uniq UNIQUE (tname, hospital_id);


--
-- Name: store_productservice store_productservice_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_productservice
    ADD CONSTRAINT store_productservice_pkey PRIMARY KEY (id);


--
-- Name: store_storeinhistory store_storeinhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeinhistory
    ADD CONSTRAINT store_storeinhistory_pkey PRIMARY KEY (id);


--
-- Name: store_storeouthistory_orders store_storeouthistory_or_storeouthistory_id_produ_dca7f083_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory_orders
    ADD CONSTRAINT store_storeouthistory_or_storeouthistory_id_produ_dca7f083_uniq UNIQUE (storeouthistory_id, productservice_id);


--
-- Name: store_storeouthistory_orders store_storeouthistory_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory_orders
    ADD CONSTRAINT store_storeouthistory_orders_pkey PRIMARY KEY (id);


--
-- Name: store_storeouthistory store_storeouthistory_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory
    ADD CONSTRAINT store_storeouthistory_pkey PRIMARY KEY (id);


--
-- Name: storein storein_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storein
    ADD CONSTRAINT storein_pkey PRIMARY KEY (id);


--
-- Name: storeout_orders storeout_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout_orders
    ADD CONSTRAINT storeout_orders_pkey PRIMARY KEY (id);


--
-- Name: storeout_orders storeout_orders_storeout_id_productservice_id_5fb55663_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout_orders
    ADD CONSTRAINT storeout_orders_storeout_id_productservice_id_5fb55663_uniq UNIQUE (storeout_id, productservice_id);


--
-- Name: storeout storeout_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout
    ADD CONSTRAINT storeout_pkey PRIMARY KEY (id);


--
-- Name: type_servicehasincentive type_servicehasincentive_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.type_servicehasincentive
    ADD CONSTRAINT type_servicehasincentive_pkey PRIMARY KEY (id);


--
-- Name: service_type type_type_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.service_type
    ADD CONSTRAINT type_type_pkey PRIMARY KEY (id);


--
-- Name: service_type type_type_tname_hospital_id_fab6e54f_uniq; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.service_type
    ADD CONSTRAINT type_type_tname_hospital_id_fab6e54f_uniq UNIQUE (tname, hospital_id);


--
-- Name: wards wards_pkey; Type: CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.wards
    ADD CONSTRAINT wards_pkey PRIMARY KEY (id);


--
-- Name: Doctors_ddepart_id_4e003a65; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Doctors_ddepart_id_4e003a65" ON public."Doctors" USING btree (ddepart_id);


--
-- Name: Doctors_dnmc_b658b32f_like; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Doctors_dnmc_b658b32f_like" ON public."Doctors" USING btree (dnmc varchar_pattern_ops);


--
-- Name: Doctors_hospital_id_b36f3199; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Doctors_hospital_id_b36f3199" ON public."Doctors" USING btree (hospital_id);


--
-- Name: Doctors_user_id_8327b13f; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Doctors_user_id_8327b13f" ON public."Doctors" USING btree (user_id);


--
-- Name: Patient_hospital_id_d3194ac5; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Patient_hospital_id_d3194ac5" ON public."Patient" USING btree (hospital_id);


--
-- Name: Patient_year_id_a99746b8; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Patient_year_id_a99746b8" ON public."Patient" USING btree (year_id);


--
-- Name: Services_hospital_id_b00653d7; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Services_hospital_id_b00653d7" ON public."Services" USING btree (hospital_id);


--
-- Name: Services_stype_id_2690df0b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Services_stype_id_2690df0b" ON public."Services" USING btree (stype_id);


--
-- Name: Services_year_id_695eb979; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Services_year_id_695eb979" ON public."Services" USING btree (year_id);


--
-- Name: Staff_hospital_id_22d7c6fe; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Staff_hospital_id_22d7c6fe" ON public."Staff" USING btree (hospital_id);


--
-- Name: Staff_sdepart_id_44c4fd38; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Staff_sdepart_id_44c4fd38" ON public."Staff" USING btree (sdepart_id);


--
-- Name: Staff_stype_id_94f3b3ee; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Staff_stype_id_94f3b3ee" ON public."Staff" USING btree (stype_id);


--
-- Name: Staff_user_id_e5c5c721; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Staff_user_id_e5c5c721" ON public."Staff" USING btree (user_id);


--
-- Name: Staff_year_id_317f3ea9; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "Staff_year_id_317f3ea9" ON public."Staff" USING btree (year_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: bill_historicalbill_history_user_id_12297421; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbill_history_user_id_12297421 ON public.bill_historicalbill USING btree (history_user_id);


--
-- Name: bill_historicalbill_id_68818421; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbill_id_68818421 ON public.bill_historicalbill USING btree (id);


--
-- Name: bill_historicalbill_patient_id_f03a15ee; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbill_patient_id_f03a15ee ON public.bill_historicalbill USING btree (patient_id);


--
-- Name: bill_historicalbill_user_id_a5010d0f; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbill_user_id_a5010d0f ON public.bill_historicalbill USING btree (user_id);


--
-- Name: bill_historicalbill_year_id_e57eb782; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbill_year_id_e57eb782 ON public.bill_historicalbill USING btree (year_id);


--
-- Name: bill_historicalbillservice_bill_id_dc6e98d4; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbillservice_bill_id_dc6e98d4 ON public.bill_historicalbillservice USING btree (bill_id);


--
-- Name: bill_historicalbillservice_history_user_id_29395411; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbillservice_history_user_id_29395411 ON public.bill_historicalbillservice USING btree (history_user_id);


--
-- Name: bill_historicalbillservice_id_b2fa32f2; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbillservice_id_b2fa32f2 ON public.bill_historicalbillservice USING btree (id);


--
-- Name: bill_historicalbillservice_service_id_86adab0e; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_historicalbillservice_service_id_86adab0e ON public.bill_historicalbillservice USING btree (service_id);


--
-- Name: bill_services_bill_id_216122fe; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_services_bill_id_216122fe ON public.bill_services USING btree (bill_id);


--
-- Name: bill_services_service_id_aaa82bef; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bill_services_service_id_aaa82bef ON public.bill_services USING btree (service_id);


--
-- Name: bills_patient_id_9d20fb3f; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bills_patient_id_9d20fb3f ON public.bills USING btree (patient_id);


--
-- Name: bills_user_id_89b06570; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bills_user_id_89b06570 ON public.bills USING btree (user_id);


--
-- Name: bills_year_id_3f8954a3; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX bills_year_id_3f8954a3 ON public.bills USING btree (year_id);


--
-- Name: department_departmenthistory_actionBy_id_524776c6; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "department_departmenthistory_actionBy_id_524776c6" ON public.department_departmenthistory USING btree ("actionBy_id");


--
-- Name: department_departmenthistory_hospital_id_7a1d5401; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX department_departmenthistory_hospital_id_7a1d5401 ON public.department_departmenthistory USING btree (hospital_id);


--
-- Name: departments_hospital_id_74db31cd; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX departments_hospital_id_74db31cd ON public.departments USING btree (hospital_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: doctor_doctorshistory_actionBy_id_2e0a001c; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "doctor_doctorshistory_actionBy_id_2e0a001c" ON public.doctor_doctorshistory USING btree ("actionBy_id");


--
-- Name: doctor_doctorshistory_ddepart_id_27e77b63; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX doctor_doctorshistory_ddepart_id_27e77b63 ON public.doctor_doctorshistory USING btree (ddepart_id);


--
-- Name: doctor_doctorshistory_hospital_id_4068fe0a; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX doctor_doctorshistory_hospital_id_4068fe0a ON public.doctor_doctorshistory USING btree (hospital_id);


--
-- Name: expenditure_expenditurehistory_actionBy_id_a602fc81; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "expenditure_expenditurehistory_actionBy_id_a602fc81" ON public.expenditure_expenditurehistory USING btree ("actionBy_id");


--
-- Name: expenditure_expenditurehistory_hospital_id_6e829e10; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX expenditure_expenditurehistory_hospital_id_6e829e10 ON public.expenditure_expenditurehistory USING btree (hospital_id);


--
-- Name: expenditure_expenditurehistory_itype_id_75bb086f; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX expenditure_expenditurehistory_itype_id_75bb086f ON public.expenditure_expenditurehistory USING btree (itype_id);


--
-- Name: expenditure_type_hospital_id_f48e3ae2; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX expenditure_type_hospital_id_f48e3ae2 ON public.expenditure_type USING btree (hospital_id);


--
-- Name: expendituretype_expendituretypehistory_actionBy_id_bcb11cad; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "expendituretype_expendituretypehistory_actionBy_id_bcb11cad" ON public.expendituretype_expendituretypehistory USING btree ("actionBy_id");


--
-- Name: expendituretype_expendituretypehistory_hospital_id_eddb6cb5; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX expendituretype_expendituretypehistory_hospital_id_eddb6cb5 ON public.expendituretype_expendituretypehistory USING btree (hospital_id);


--
-- Name: expenses_hospital_id_13c87354; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX expenses_hospital_id_13c87354 ON public.expenses USING btree (hospital_id);


--
-- Name: expenses_itype_id_39b3895d; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX expenses_itype_id_39b3895d ON public.expenses USING btree (itype_id);


--
-- Name: expenses_year_id_94f150bc; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX expenses_year_id_94f150bc ON public.expenses USING btree (year_id);


--
-- Name: hospital_has_users_hospital_id_dc01ddab; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX hospital_has_users_hospital_id_dc01ddab ON public.hospital_has_users USING btree (hospital_id);


--
-- Name: hospital_has_users_user_id_e1952d5a; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX hospital_has_users_user_id_e1952d5a ON public.hospital_has_users USING btree (user_id);


--
-- Name: hospitals_user_id_b634178f; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX hospitals_user_id_b634178f ON public.hospitals USING btree (user_id);


--
-- Name: hospitals_ward_id_839ad9e5; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX hospitals_ward_id_839ad9e5 ON public.hospitals USING btree (ward_id);


--
-- Name: income_incomehistory_actionBy_id_11706f4d; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "income_incomehistory_actionBy_id_11706f4d" ON public.income_incomehistory USING btree ("actionBy_id");


--
-- Name: income_incomehistory_hospital_id_4ffb31da; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX income_incomehistory_hospital_id_4ffb31da ON public.income_incomehistory USING btree (hospital_id);


--
-- Name: income_incomehistory_itype_id_435492fb; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX income_incomehistory_itype_id_435492fb ON public.income_incomehistory USING btree (itype_id);


--
-- Name: income_type_hospital_id_6ee96a80; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX income_type_hospital_id_6ee96a80 ON public.income_type USING btree (hospital_id);


--
-- Name: incomes_hospital_id_a4b43ed9; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX incomes_hospital_id_a4b43ed9 ON public.incomes USING btree (hospital_id);


--
-- Name: incomes_itype_id_f2a2a936; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX incomes_itype_id_f2a2a936 ON public.incomes USING btree (itype_id);


--
-- Name: incomes_year_id_1184828b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX incomes_year_id_1184828b ON public.incomes USING btree (year_id);


--
-- Name: incometype_incometypehistory_actionBy_id_ccbafaa8; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "incometype_incometypehistory_actionBy_id_ccbafaa8" ON public.incometype_incometypehistory USING btree ("actionBy_id");


--
-- Name: incometype_incometypehistory_hospital_id_d5a4b886; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX incometype_incometypehistory_hospital_id_d5a4b886 ON public.incometype_incometypehistory USING btree (hospital_id);


--
-- Name: medicines_created_by_id_36427a69; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX medicines_created_by_id_36427a69 ON public.medicines USING btree (created_by_id);


--
-- Name: metas_metas_hospital_id_4c48e464; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX metas_metas_hospital_id_4c48e464 ON public.metas_metas USING btree (hospital_id);


--
-- Name: patients_historicalpatients_history_user_id_88f28c6e; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX patients_historicalpatients_history_user_id_88f28c6e ON public.patients_historicalpatients USING btree (history_user_id);


--
-- Name: patients_historicalpatients_hospital_id_95313bec; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX patients_historicalpatients_hospital_id_95313bec ON public.patients_historicalpatients USING btree (hospital_id);


--
-- Name: patients_historicalpatients_id_4e582d11; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX patients_historicalpatients_id_4e582d11 ON public.patients_historicalpatients USING btree (id);


--
-- Name: patients_historicalpatients_year_id_e7dea840; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX patients_historicalpatients_year_id_e7dea840 ON public.patients_historicalpatients USING btree (year_id);


--
-- Name: pharmacies_hospital_id_fcf916a7; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX pharmacies_hospital_id_fcf916a7 ON public.pharmacies USING btree (hospital_id);


--
-- Name: pharmacies_user_id_25af35f2; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX pharmacies_user_id_25af35f2 ON public.pharmacies USING btree (user_id);


--
-- Name: productstore_product_category_id_d3692f29; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_product_category_id_d3692f29 ON public.productstore_product USING btree (category_id);


--
-- Name: productstore_product_hospital_id_3e2ead06; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_product_hospital_id_3e2ead06 ON public.productstore_product USING btree (hospital_id);


--
-- Name: productstore_productcategory_hospital_id_f020686b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_productcategory_hospital_id_f020686b ON public.productstore_productcategory USING btree (hospital_id);


--
-- Name: productstore_productstorein_seller_details_id_b62d65ce; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_productstorein_seller_details_id_b62d65ce ON public.productstore_productstorein USING btree (seller_details_id);


--
-- Name: productstore_requestform_product_requestform_id_009efe9c; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_requestform_product_requestform_id_009efe9c ON public.productstore_requestform_product USING btree (requestform_id);


--
-- Name: productstore_requestform_product_requestproduct_id_bfa02c29; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_requestform_product_requestproduct_id_bfa02c29 ON public.productstore_requestform_product USING btree (requestproduct_id);


--
-- Name: productstore_storeinhasproducts_product_id_ff25390f; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_storeinhasproducts_product_id_ff25390f ON public.productstore_storeinhasproducts USING btree (product_id);


--
-- Name: productstore_storeinhasproducts_store_in_id_0373a5f6; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_storeinhasproducts_store_in_id_0373a5f6 ON public.productstore_storeinhasproducts USING btree (store_in_id);


--
-- Name: productstore_storeouthasproduct_product_id_107681ae; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_storeouthasproduct_product_id_107681ae ON public.productstore_storeouthasproduct USING btree (product_id);


--
-- Name: productstore_storeouthasproduct_store_out_id_5649ea59; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX productstore_storeouthasproduct_store_out_id_5649ea59 ON public.productstore_storeouthasproduct USING btree (store_out_id);


--
-- Name: services_serviceshistory_actionBy_id_853587c4; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "services_serviceshistory_actionBy_id_853587c4" ON public.services_serviceshistory USING btree ("actionBy_id");


--
-- Name: services_serviceshistory_hospital_id_73ce1297; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX services_serviceshistory_hospital_id_73ce1297 ON public.services_serviceshistory USING btree (hospital_id);


--
-- Name: services_serviceshistory_stype_id_a5f15b37; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX services_serviceshistory_stype_id_a5f15b37 ON public.services_serviceshistory USING btree (stype_id);


--
-- Name: staffType_stafftypehistory_actionBy_id_28d50395; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "staffType_stafftypehistory_actionBy_id_28d50395" ON public."staffType_stafftypehistory" USING btree ("actionBy_id");


--
-- Name: staffType_stafftypehistory_hospital_id_9bcc05db; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "staffType_stafftypehistory_hospital_id_9bcc05db" ON public."staffType_stafftypehistory" USING btree (hospital_id);


--
-- Name: staff_staffhistory_actionBy_id_1050eb0b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "staff_staffhistory_actionBy_id_1050eb0b" ON public.staff_staffhistory USING btree ("actionBy_id");


--
-- Name: staff_staffhistory_hospital_id_3f508f9c; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX staff_staffhistory_hospital_id_3f508f9c ON public.staff_staffhistory USING btree (hospital_id);


--
-- Name: staff_staffhistory_sdepart_id_4337d2a8; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX staff_staffhistory_sdepart_id_4337d2a8 ON public.staff_staffhistory USING btree (sdepart_id);


--
-- Name: staff_staffhistory_stype_id_746a3c3d; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX staff_staffhistory_stype_id_746a3c3d ON public.staff_staffhistory USING btree (stype_id);


--
-- Name: staff_staffhistory_year_id_ff7d0ae2; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX staff_staffhistory_year_id_ff7d0ae2 ON public.staff_staffhistory USING btree (year_id);


--
-- Name: staff_type_hospital_id_39b1568b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX staff_type_hospital_id_39b1568b ON public.staff_type USING btree (hospital_id);


--
-- Name: store_productservice_product_id_2e4491bb; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX store_productservice_product_id_2e4491bb ON public.store_productservice USING btree (product_id);


--
-- Name: store_storeinhistory_actionBy_id_97b107b9; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "store_storeinhistory_actionBy_id_97b107b9" ON public.store_storeinhistory USING btree ("actionBy_id");


--
-- Name: store_storeinhistory_hospital_id_92e8f20e; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX store_storeinhistory_hospital_id_92e8f20e ON public.store_storeinhistory USING btree (hospital_id);


--
-- Name: store_storeinhistory_year_id_aa224543; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX store_storeinhistory_year_id_aa224543 ON public.store_storeinhistory USING btree (year_id);


--
-- Name: store_storeouthistory_actionBy_id_3feeea09; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX "store_storeouthistory_actionBy_id_3feeea09" ON public.store_storeouthistory USING btree ("actionBy_id");


--
-- Name: store_storeouthistory_hospital_id_0fc96c2b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX store_storeouthistory_hospital_id_0fc96c2b ON public.store_storeouthistory USING btree (hospital_id);


--
-- Name: store_storeouthistory_orders_productservice_id_9597005b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX store_storeouthistory_orders_productservice_id_9597005b ON public.store_storeouthistory_orders USING btree (productservice_id);


--
-- Name: store_storeouthistory_orders_storeouthistory_id_0e8d3bf2; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX store_storeouthistory_orders_storeouthistory_id_0e8d3bf2 ON public.store_storeouthistory_orders USING btree (storeouthistory_id);


--
-- Name: store_storeouthistory_year_id_57ba24fe; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX store_storeouthistory_year_id_57ba24fe ON public.store_storeouthistory USING btree (year_id);


--
-- Name: storein_created_by_id_d3e1b5f0; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX storein_created_by_id_d3e1b5f0 ON public.storein USING btree (created_by_id);


--
-- Name: storein_hospital_id_c04ed861; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX storein_hospital_id_c04ed861 ON public.storein USING btree (hospital_id);


--
-- Name: storein_year_id_d811d07a; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX storein_year_id_d811d07a ON public.storein USING btree (year_id);


--
-- Name: storeout_created_by_id_1e2d4424; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX storeout_created_by_id_1e2d4424 ON public.storeout USING btree (created_by_id);


--
-- Name: storeout_hospital_id_72b4a323; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX storeout_hospital_id_72b4a323 ON public.storeout USING btree (hospital_id);


--
-- Name: storeout_orders_productservice_id_42863646; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX storeout_orders_productservice_id_42863646 ON public.storeout_orders USING btree (productservice_id);


--
-- Name: storeout_orders_storeout_id_993c1fa6; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX storeout_orders_storeout_id_993c1fa6 ON public.storeout_orders USING btree (storeout_id);


--
-- Name: storeout_year_id_1354c1e1; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX storeout_year_id_1354c1e1 ON public.storeout USING btree (year_id);


--
-- Name: type_servicehasincentive_stafftype_id_560af746; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX type_servicehasincentive_stafftype_id_560af746 ON public.type_servicehasincentive USING btree (stafftype_id);


--
-- Name: type_servicehasincentive_type_id_24cc402b; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX type_servicehasincentive_type_id_24cc402b ON public.type_servicehasincentive USING btree (type_id);


--
-- Name: type_type_hospital_id_30ef94ce; Type: INDEX; Schema: public; Owner: hos
--

CREATE INDEX type_type_hospital_id_30ef94ce ON public.service_type USING btree (hospital_id);


--
-- Name: Doctors Doctors_ddepart_id_4e003a65_fk_departments_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Doctors"
    ADD CONSTRAINT "Doctors_ddepart_id_4e003a65_fk_departments_id" FOREIGN KEY (ddepart_id) REFERENCES public.departments(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Doctors Doctors_hospital_id_b36f3199_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Doctors"
    ADD CONSTRAINT "Doctors_hospital_id_b36f3199_fk_hospitals_id" FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Doctors Doctors_user_id_8327b13f_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Doctors"
    ADD CONSTRAINT "Doctors_user_id_8327b13f_fk_auth_user_id" FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Patient Patient_hospital_id_d3194ac5_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Patient"
    ADD CONSTRAINT "Patient_hospital_id_d3194ac5_fk_hospitals_id" FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Patient Patient_year_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Patient"
    ADD CONSTRAINT "Patient_year_id_fkey" FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Services Services_hospital_id_b00653d7_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Services"
    ADD CONSTRAINT "Services_hospital_id_b00653d7_fk_hospitals_id" FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Services Services_stype_id_2690df0b_fk_type_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Services"
    ADD CONSTRAINT "Services_stype_id_2690df0b_fk_type_type_id" FOREIGN KEY (stype_id) REFERENCES public.service_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Services Services_year_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Services"
    ADD CONSTRAINT "Services_year_id_fkey" FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Staff Staff_hospital_id_22d7c6fe_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Staff"
    ADD CONSTRAINT "Staff_hospital_id_22d7c6fe_fk_hospitals_id" FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Staff Staff_sdepart_id_44c4fd38_fk_departments_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Staff"
    ADD CONSTRAINT "Staff_sdepart_id_44c4fd38_fk_departments_id" FOREIGN KEY (sdepart_id) REFERENCES public.departments(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Staff Staff_stype_id_94f3b3ee_fk_staff_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Staff"
    ADD CONSTRAINT "Staff_stype_id_94f3b3ee_fk_staff_type_id" FOREIGN KEY (stype_id) REFERENCES public.staff_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Staff Staff_user_id_e5c5c721_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Staff"
    ADD CONSTRAINT "Staff_user_id_e5c5c721_fk_auth_user_id" FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: Staff Staff_year_id_317f3ea9_fk_years_year; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."Staff"
    ADD CONSTRAINT "Staff_year_id_317f3ea9_fk_years_year" FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: bill_historicalbill bill_historicalbill_history_user_id_12297421_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_historicalbill
    ADD CONSTRAINT bill_historicalbill_history_user_id_12297421_fk_auth_user_id FOREIGN KEY (history_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: bill_historicalbillservice bill_historicalbills_history_user_id_29395411_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_historicalbillservice
    ADD CONSTRAINT bill_historicalbills_history_user_id_29395411_fk_auth_user FOREIGN KEY (history_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: bill_services bill_services_bill_id_216122fe_fk_bills_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_services
    ADD CONSTRAINT bill_services_bill_id_216122fe_fk_bills_id FOREIGN KEY (bill_id) REFERENCES public.bills(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: bill_services bill_services_service_id_aaa82bef_fk_Services_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bill_services
    ADD CONSTRAINT "bill_services_service_id_aaa82bef_fk_Services_id" FOREIGN KEY (service_id) REFERENCES public."Services"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: bills bills_patient_id_9d20fb3f_fk_Patient_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bills
    ADD CONSTRAINT "bills_patient_id_9d20fb3f_fk_Patient_id" FOREIGN KEY (patient_id) REFERENCES public."Patient"(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: bills bills_user_id_89b06570_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bills
    ADD CONSTRAINT bills_user_id_89b06570_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: bills bills_year_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.bills
    ADD CONSTRAINT bills_year_id_fkey FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: department_departmenthistory department_departmen_actionBy_id_524776c6_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.department_departmenthistory
    ADD CONSTRAINT "department_departmen_actionBy_id_524776c6_fk_auth_user" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: department_departmenthistory department_departmen_hospital_id_7a1d5401_fk_hospitals; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.department_departmenthistory
    ADD CONSTRAINT department_departmen_hospital_id_7a1d5401_fk_hospitals FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: departments departments_hospital_id_74db31cd_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.departments
    ADD CONSTRAINT departments_hospital_id_74db31cd_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: doctor_doctorshistory doctor_doctorshistory_actionBy_id_2e0a001c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.doctor_doctorshistory
    ADD CONSTRAINT "doctor_doctorshistory_actionBy_id_2e0a001c_fk_auth_user_id" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: doctor_doctorshistory doctor_doctorshistory_ddepart_id_27e77b63_fk_departments_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.doctor_doctorshistory
    ADD CONSTRAINT doctor_doctorshistory_ddepart_id_27e77b63_fk_departments_id FOREIGN KEY (ddepart_id) REFERENCES public.departments(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: doctor_doctorshistory doctor_doctorshistory_hospital_id_4068fe0a_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.doctor_doctorshistory
    ADD CONSTRAINT doctor_doctorshistory_hospital_id_4068fe0a_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expenditure_expenditurehistory expenditure_expendit_actionBy_id_a602fc81_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_expenditurehistory
    ADD CONSTRAINT "expenditure_expendit_actionBy_id_a602fc81_fk_auth_user" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expenditure_expenditurehistory expenditure_expendit_hospital_id_6e829e10_fk_hospitals; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_expenditurehistory
    ADD CONSTRAINT expenditure_expendit_hospital_id_6e829e10_fk_hospitals FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expenditure_expenditurehistory expenditure_expendit_itype_id_75bb086f_fk_expenditu; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_expenditurehistory
    ADD CONSTRAINT expenditure_expendit_itype_id_75bb086f_fk_expenditu FOREIGN KEY (itype_id) REFERENCES public.expenditure_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expenditure_type expenditure_type_hospital_id_f48e3ae2_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenditure_type
    ADD CONSTRAINT expenditure_type_hospital_id_f48e3ae2_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expendituretype_expendituretypehistory expendituretype_expe_actionBy_id_bcb11cad_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expendituretype_expendituretypehistory
    ADD CONSTRAINT "expendituretype_expe_actionBy_id_bcb11cad_fk_auth_user" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expendituretype_expendituretypehistory expendituretype_expe_hospital_id_eddb6cb5_fk_hospitals; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expendituretype_expendituretypehistory
    ADD CONSTRAINT expendituretype_expe_hospital_id_eddb6cb5_fk_hospitals FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expenses expenses_hospital_id_13c87354_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenses
    ADD CONSTRAINT expenses_hospital_id_13c87354_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expenses expenses_itype_id_39b3895d_fk_expenditure_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenses
    ADD CONSTRAINT expenses_itype_id_39b3895d_fk_expenditure_type_id FOREIGN KEY (itype_id) REFERENCES public.expenditure_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: expenses expenses_year_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.expenses
    ADD CONSTRAINT expenses_year_id_fkey FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: hospital_has_users hospital_has_users_hospital_id_dc01ddab_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.hospital_has_users
    ADD CONSTRAINT hospital_has_users_hospital_id_dc01ddab_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: hospital_has_users hospital_has_users_user_id_e1952d5a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.hospital_has_users
    ADD CONSTRAINT hospital_has_users_user_id_e1952d5a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: hospitals hospitals_user_id_b634178f_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.hospitals
    ADD CONSTRAINT hospitals_user_id_b634178f_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: hospitals hospitals_ward_id_839ad9e5_fk_wards_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.hospitals
    ADD CONSTRAINT hospitals_ward_id_839ad9e5_fk_wards_id FOREIGN KEY (ward_id) REFERENCES public.wards(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: income_incomehistory income_incomehistory_actionBy_id_11706f4d_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_incomehistory
    ADD CONSTRAINT "income_incomehistory_actionBy_id_11706f4d_fk_auth_user_id" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: income_incomehistory income_incomehistory_hospital_id_4ffb31da_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_incomehistory
    ADD CONSTRAINT income_incomehistory_hospital_id_4ffb31da_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: income_incomehistory income_incomehistory_itype_id_435492fb_fk_income_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_incomehistory
    ADD CONSTRAINT income_incomehistory_itype_id_435492fb_fk_income_type_id FOREIGN KEY (itype_id) REFERENCES public.income_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: income_type income_type_hospital_id_6ee96a80_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.income_type
    ADD CONSTRAINT income_type_hospital_id_6ee96a80_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incomes incomes_hospital_id_a4b43ed9_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incomes
    ADD CONSTRAINT incomes_hospital_id_a4b43ed9_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incomes incomes_itype_id_f2a2a936_fk_income_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incomes
    ADD CONSTRAINT incomes_itype_id_f2a2a936_fk_income_type_id FOREIGN KEY (itype_id) REFERENCES public.income_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incomes incomes_year_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incomes
    ADD CONSTRAINT incomes_year_id_fkey FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incometype_incometypehistory incometype_incometyp_actionBy_id_ccbafaa8_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incometype_incometypehistory
    ADD CONSTRAINT "incometype_incometyp_actionBy_id_ccbafaa8_fk_auth_user" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: incometype_incometypehistory incometype_incometyp_hospital_id_d5a4b886_fk_hospitals; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.incometype_incometypehistory
    ADD CONSTRAINT incometype_incometyp_hospital_id_d5a4b886_fk_hospitals FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: login_rolepermissions login_rolepermissions_group_id_80e7597d_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.login_rolepermissions
    ADD CONSTRAINT login_rolepermissions_group_id_80e7597d_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: medicines medicines_created_by_id_36427a69_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.medicines
    ADD CONSTRAINT medicines_created_by_id_36427a69_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: metas_metas metas_metas_hospital_id_4c48e464_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.metas_metas
    ADD CONSTRAINT metas_metas_hospital_id_4c48e464_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: patients_historicalpatients patients_historicalp_history_user_id_88f28c6e_fk_auth_user; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.patients_historicalpatients
    ADD CONSTRAINT patients_historicalp_history_user_id_88f28c6e_fk_auth_user FOREIGN KEY (history_user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pharmacies pharmacies_hospital_id_fcf916a7_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.pharmacies
    ADD CONSTRAINT pharmacies_hospital_id_fcf916a7_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pharmacies pharmacies_user_id_25af35f2_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.pharmacies
    ADD CONSTRAINT pharmacies_user_id_25af35f2_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_product productstore_product_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_product
    ADD CONSTRAINT productstore_product_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.productstore_productcategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_productcategory productstore_product_hospital_id_f020686b_fk_hospitals; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_productcategory
    ADD CONSTRAINT productstore_product_hospital_id_f020686b_fk_hospitals FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_product productstore_product_hospital_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_product
    ADD CONSTRAINT productstore_product_hospital_id_fkey FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_productstorein productstore_productstorein_seller_details_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_productstorein
    ADD CONSTRAINT productstore_productstorein_seller_details_id_fkey FOREIGN KEY (seller_details_id) REFERENCES public.productstore_sellerdetails(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_requestform_product productstore_request_requestform_id_009efe9c_fk_productst; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestform_product
    ADD CONSTRAINT productstore_request_requestform_id_009efe9c_fk_productst FOREIGN KEY (requestform_id) REFERENCES public.productstore_requestform(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_requestform_product productstore_request_requestproduct_id_bfa02c29_fk_productst; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_requestform_product
    ADD CONSTRAINT productstore_request_requestproduct_id_bfa02c29_fk_productst FOREIGN KEY (requestproduct_id) REFERENCES public.productstore_requestproduct(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_storeinhasproducts productstore_storein_product_id_ff25390f_fk_productst; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_storeinhasproducts
    ADD CONSTRAINT productstore_storein_product_id_ff25390f_fk_productst FOREIGN KEY (product_id) REFERENCES public.productstore_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_storeinhasproducts productstore_storein_store_in_id_0373a5f6_fk_productst; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_storeinhasproducts
    ADD CONSTRAINT productstore_storein_store_in_id_0373a5f6_fk_productst FOREIGN KEY (store_in_id) REFERENCES public.productstore_productstorein(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_storeouthasproduct productstore_storeou_product_id_107681ae_fk_productst; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_storeouthasproduct
    ADD CONSTRAINT productstore_storeou_product_id_107681ae_fk_productst FOREIGN KEY (product_id) REFERENCES public.productstore_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: productstore_storeouthasproduct productstore_storeou_store_out_id_5649ea59_fk_productst; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.productstore_storeouthasproduct
    ADD CONSTRAINT productstore_storeou_store_out_id_5649ea59_fk_productst FOREIGN KEY (store_out_id) REFERENCES public.productstore_productstoreout(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: services_serviceshistory services_serviceshistory_actionBy_id_853587c4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.services_serviceshistory
    ADD CONSTRAINT "services_serviceshistory_actionBy_id_853587c4_fk_auth_user_id" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: services_serviceshistory services_serviceshistory_hospital_id_73ce1297_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.services_serviceshistory
    ADD CONSTRAINT services_serviceshistory_hospital_id_73ce1297_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: services_serviceshistory services_serviceshistory_stype_id_a5f15b37_fk_service_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.services_serviceshistory
    ADD CONSTRAINT services_serviceshistory_stype_id_a5f15b37_fk_service_type_id FOREIGN KEY (stype_id) REFERENCES public.service_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: staffType_stafftypehistory staffType_stafftypehistory_actionBy_id_28d50395_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."staffType_stafftypehistory"
    ADD CONSTRAINT "staffType_stafftypehistory_actionBy_id_28d50395_fk_auth_user_id" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: staffType_stafftypehistory staffType_stafftypehistory_hospital_id_9bcc05db_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public."staffType_stafftypehistory"
    ADD CONSTRAINT "staffType_stafftypehistory_hospital_id_9bcc05db_fk_hospitals_id" FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: staff_staffhistory staff_staffhistory_actionBy_id_1050eb0b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_staffhistory
    ADD CONSTRAINT "staff_staffhistory_actionBy_id_1050eb0b_fk_auth_user_id" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: staff_staffhistory staff_staffhistory_hospital_id_3f508f9c_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_staffhistory
    ADD CONSTRAINT staff_staffhistory_hospital_id_3f508f9c_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: staff_staffhistory staff_staffhistory_sdepart_id_4337d2a8_fk_departments_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_staffhistory
    ADD CONSTRAINT staff_staffhistory_sdepart_id_4337d2a8_fk_departments_id FOREIGN KEY (sdepart_id) REFERENCES public.departments(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: staff_staffhistory staff_staffhistory_stype_id_746a3c3d_fk_staff_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_staffhistory
    ADD CONSTRAINT staff_staffhistory_stype_id_746a3c3d_fk_staff_type_id FOREIGN KEY (stype_id) REFERENCES public.staff_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: staff_staffhistory staff_staffhistory_year_id_ff7d0ae2_fk_years_year; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_staffhistory
    ADD CONSTRAINT staff_staffhistory_year_id_ff7d0ae2_fk_years_year FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: staff_type staff_type_hospital_id_39b1568b_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.staff_type
    ADD CONSTRAINT staff_type_hospital_id_39b1568b_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_productservice store_productservice_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_productservice
    ADD CONSTRAINT store_productservice_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.storein(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_storeinhistory store_storeinhistory_actionBy_id_97b107b9_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeinhistory
    ADD CONSTRAINT "store_storeinhistory_actionBy_id_97b107b9_fk_auth_user_id" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_storeinhistory store_storeinhistory_hospital_id_92e8f20e_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeinhistory
    ADD CONSTRAINT store_storeinhistory_hospital_id_92e8f20e_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_storeinhistory store_storeinhistory_year_id_aa224543_fk_years_year; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeinhistory
    ADD CONSTRAINT store_storeinhistory_year_id_aa224543_fk_years_year FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_storeouthistory_orders store_storeouthistor_productservice_id_9597005b_fk_store_pro; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory_orders
    ADD CONSTRAINT store_storeouthistor_productservice_id_9597005b_fk_store_pro FOREIGN KEY (productservice_id) REFERENCES public.store_productservice(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_storeouthistory_orders store_storeouthistor_storeouthistory_id_0e8d3bf2_fk_store_sto; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory_orders
    ADD CONSTRAINT store_storeouthistor_storeouthistory_id_0e8d3bf2_fk_store_sto FOREIGN KEY (storeouthistory_id) REFERENCES public.store_storeouthistory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_storeouthistory store_storeouthistory_actionBy_id_3feeea09_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory
    ADD CONSTRAINT "store_storeouthistory_actionBy_id_3feeea09_fk_auth_user_id" FOREIGN KEY ("actionBy_id") REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_storeouthistory store_storeouthistory_hospital_id_0fc96c2b_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory
    ADD CONSTRAINT store_storeouthistory_hospital_id_0fc96c2b_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: store_storeouthistory store_storeouthistory_year_id_57ba24fe_fk_years_year; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.store_storeouthistory
    ADD CONSTRAINT store_storeouthistory_year_id_57ba24fe_fk_years_year FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: storein storein_created_by_id_d3e1b5f0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storein
    ADD CONSTRAINT storein_created_by_id_d3e1b5f0_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: storein storein_hospital_id_c04ed861_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storein
    ADD CONSTRAINT storein_hospital_id_c04ed861_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: storein storein_year_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storein
    ADD CONSTRAINT storein_year_id_fkey FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: storeout storeout_created_by_id_1e2d4424_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout
    ADD CONSTRAINT storeout_created_by_id_1e2d4424_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: storeout storeout_hospital_id_72b4a323_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout
    ADD CONSTRAINT storeout_hospital_id_72b4a323_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: storeout_orders storeout_orders_productservice_id_42863646_fk_store_pro; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout_orders
    ADD CONSTRAINT storeout_orders_productservice_id_42863646_fk_store_pro FOREIGN KEY (productservice_id) REFERENCES public.store_productservice(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: storeout_orders storeout_orders_storeout_id_993c1fa6_fk_storeout_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout_orders
    ADD CONSTRAINT storeout_orders_storeout_id_993c1fa6_fk_storeout_id FOREIGN KEY (storeout_id) REFERENCES public.storeout(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: storeout storeout_year_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.storeout
    ADD CONSTRAINT storeout_year_id_fkey FOREIGN KEY (year_id) REFERENCES public.years(year) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: type_servicehasincentive type_servicehasincentive_stafftype_id_560af746_fk_staff_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.type_servicehasincentive
    ADD CONSTRAINT type_servicehasincentive_stafftype_id_560af746_fk_staff_type_id FOREIGN KEY (stafftype_id) REFERENCES public.staff_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: type_servicehasincentive type_servicehasincentive_type_id_24cc402b_fk_service_type_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.type_servicehasincentive
    ADD CONSTRAINT type_servicehasincentive_type_id_24cc402b_fk_service_type_id FOREIGN KEY (type_id) REFERENCES public.service_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: service_type type_type_hospital_id_30ef94ce_fk_hospitals_id; Type: FK CONSTRAINT; Schema: public; Owner: hos
--

ALTER TABLE ONLY public.service_type
    ADD CONSTRAINT type_type_hospital_id_30ef94ce_fk_hospitals_id FOREIGN KEY (hospital_id) REFERENCES public.hospitals(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

