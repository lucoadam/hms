const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout
});

var fs = require("fs");
var path = require("path");
var modelName = null;
var baseDirectory = null;
function apiContent(modelName) {
  return `import Vue from 'vue';

export default {
  index (query) {
    return Vue.http.post('api/v1/${modelName.toLowerCase()}/index', query)
  },

  save (${modelName.toLowerCase()}) {
    return Vue.http.post('api/v1/${modelName.toLowerCase()}', ${modelName.toLowerCase()})
  },

  delete (${modelName.toLowerCase()}Id) {
    return Vue.http.delete('api/v1/${modelName.toLowerCase()}/' + ${modelName.toLowerCase()}Id)
  },


  findById (${modelName.toLowerCase()}Id) {
    return Vue.http.get('api/v1/${modelName.toLowerCase()}/' + ${modelName.toLowerCase()}Id)
  },

}
  `;
}
function indexContent(model) {
  return `<template>

  <div>

    <v-layout row wrap mb40>
      <v-flex md2 mb20>
          <h2>${modelName.charAt(0).toUpperCase() + modelName.slice(1)}</h2>
      </v-flex>

      <v-flex md4>
        <v-text-field
          v-model="query.search"
          append-icon="search"
          label="Search"
          single-line
          hide-details
        ></v-text-field>
      </v-flex>
      <v-flex justify-end>
        <v-btn fab :to="{ name: 'admin.${modelName.toLowerCase()}.create' }" class="float-right"><v-icon>add</v-icon></v-btn>
      </v-flex>
    </v-layout>

    <v-dialog
      v-model="deleteDialog"
      max-width="400"
    >
      <v-card>
        <v-card-title class="headline">Are you sure?</v-card-title>
        <v-card-actions>
          <v-spacer></v-spacer>

          <v-btn
            color="green darken-1"
            flat="flat"
            @click="deleteDialog = false"
          >
            No
          </v-btn>

          <v-btn
            color="green darken-1"
            flat="flat"
            @click="deleteItem"
          >
            Yes
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>

    <v-data-table
      :headers="headers"
      :items="users"
      class="elevation-1"
      :pagination.sync="query"
      :rows-per-page-items="[20, 50, 100]"
      :total-items="totalItems"
    >
      <template slot="headerCell" slot-scope="props">
        <v-tooltip bottom>
        <span slot="activator">
          {{ props.header.text }}
        </span>
          <span>
          {{ props.header.text }}
        </span>
        </v-tooltip>
      </template>
      <template slot="items" slot-scope="props">
        <td>{{ (query.rowsPerPage * (query.page - 1)) + props.index + 1 }}</td>
        <td>
          <v-btn danger flat @click=" showDeleteDialog(props.item) "><v-icon>delete</v-icon></v-btn>
          <v-btn :to="{ name: 'admin.${modelName.toLowerCase()}.edit', params: { id: props.item.id } }" primary flat><v-icon>create</v-icon></v-btn>
        </td>
      </template>
    </v-data-table>

  </div>

</template>

<script>

import ${model}Api from '../../../api/${modelName.toLowerCase()}'

export default {
  mounted () {
    this.getDataFromApi()
  },

  watch: {
    query: {
      handler () {
        this.getDataFromApi()
      },
      deep: false
    }
  },
  data () {
    return {
      dialog: false,
      loading: true,
      query: {
        search: null,
        rowsPerPage: 20
      },
      ${modelName.toLowerCase()}: [],
      deletingItem: null,
      deleteDialog: false,
      totalItems: null,
      headers: [
        {
          text: 'SN',
          align: 'left',
          sortable: false,
          value: 'sn'
        },
        { text: 'Actions', value: 'Actions', sortable: false }
      ]
    }
  },
  methods: {
    getDataFromApi () {
      this.loading = true
      return ${model}Api.index(this.query).then((response) => {
        this.${modelName.toLowerCase()} = response.body.content
        this.totalItems = response.body.totalElements
      })
    },

    deleteItem () {
      UserApi.delete(this.deletingItem.id).then((response) => {
        let index = this.users.indexOf(this.deletingItem)
        this.${modelName.toLowerCase()}.splice(index, 1)
      })

      this.deleteDialog = false
    },
    close () {
      this.dialog = false
      setTimeout(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
      }, 300)
    },

    showDeleteDialog (item) {
      this.deletingItem = item
      this.deleteDialog = true
    }
  }
}
</script>

<style scoped>

</style>
`;
}
function editContent(model) {
  return `<template>
  <div>
    <v-layout row wrap mb40>
      <v-flex md10 mb20>
        <h2>Edit ${model}</h2>
      </v-flex>
      <v-flex md2>
        <v-btn fab :to="{ name: 'admin.${modelName.toLowerCase()}.index' }"  class="float-right" ><v-icon>arrow_back</v-icon></v-btn>
      </v-flex>
    </v-layout>

     <${model.toLowerCase()}-form :${model.toLowerCase()}="blank${model}"></${model.toLowerCase()}-form>
  </div>
</template>

<script>
import ${model}Form from './Form'
import ${model}Api from '../../../api/${modelName.toLowerCase()}'

export default {
  components: { ${model}Form },

  data () {
    return {
      ${model.toLowerCase()}: {
        id: ''
      }
    }
  },

  watch: {
    '$route'(to, from) {
      this.created()
    }
  },

  created () {
    ${model}Api.findById(this.$route.params.id).then((response) => {
      this.${model.toLowerCase()} = response.body
    })
  }
}
</script>`;
}
function formContent(model) {
  return `<template>

  <v-flex md6>
    <v-form v-model="valid" ref="${model.toLowerCase()}_form">

      <v-text-field
        v-model="${model.toLowerCase()}.title"
        :rules="titleRules"
        :counter="30"
        label="Name"
        required

      ></v-text-field>

      <v-textarea
        v-model="${model.toLowerCase()}.content"
        :rules="contentRules"
        :counter="30"
        label="Content"
        required
        rows="20"
      ></v-textarea>


      <v-btn
        :disabled="!valid"
        @click="submit"
      >
        submit
      </v-btn>
      <v-btn @click="clear">clear</v-btn>
    </v-form>
  </v-flex>

</template>

<script>
  import ${model}Api from '../../../api/${modelName.toLowerCase()}'
export default {
  name: '${model.toLowerCase()}-form',

  props: {
    ${model.toLowerCase()}: {
      type: Object,
      required: true
    }
  },

  data () {
    return {
      valid: false,
      name: '',
      id: '',
      titleRules: [
        v => !!v || 'title is required',
      ],
      contentRules: [
        v => !!v || 'Content is required',
      ]
    }
  },

  methods: {
    clear () {
      this.$refs.${model.toLowerCase()}_form.reset()
    },

    submit () {
      ${model}Api.save(this.${model.toLowerCase()}).then((response) => {
        this.$router.push({ name: 'admin.${modelName.toLowerCase()}.index' })
      })
    }
  }
}
</script>

<style scoped>

</style>
`;
}
function createContent(model) {
  return `<template>

  <div>

    <v-layout row wrap mb40>
      <v-flex md10 mb20>
        <h2>Add ${model}</h2>
      </v-flex>
      <v-flex md1>
        <v-btn fab :to="{ name: 'admin.${modelName.toLowerCase()}.index' }" ><v-icon>arrow_back</v-icon></v-btn>
      </v-flex>
    </v-layout>

    <${model.toLowerCase()}-form :${model.toLowerCase()}="blank${model}"></${model.toLowerCase()}-form>

  </div>

</template>

<script>

import ${model}Form from './Form'

export default {
  components: { ${model}Form },
  created () {
  },

  data () {
    return {
      blank${model}: {
        id: null
      }
    }
  }
}
</script>

<style scoped>

</style>`;
}
function generateFiles() {
  const readline2 = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
  });
  readline2.question(`ModelName(singular):`, name => {
    readline2.close();
    fs.writeFile(
      path.join(baseDirectory, "Index.vue"),
      indexContent(name),
      function(err) {
        if (err) throw err;
        console.log(
          "\x1b[32m%s\x1b[0m",
          path.join(baseDirectory, "Index.vue"),
          " created successfully"
        );
        fs.writeFile(
          path.join(baseDirectory, "Create.vue"),
          createContent(name),
          function(err) {
            if (err) throw err;
            console.log(
              "\x1b[32m%s\x1b[0m",
              path.join(baseDirectory, "Create.vue"),
              " created successfully"
            );
            fs.writeFile(
              path.join(baseDirectory, "Edit.vue"),
              editContent(name),
              function(err) {
                if (err) throw err;
                console.log(
                  "\x1b[32m%s\x1b[0m",
                  path.join(baseDirectory, "Edit.vue"),
                  " created successfully"
                );
                fs.writeFile(
                  path.join(baseDirectory, "Form.vue"),
                  formContent(name),
                  function(err) {
                    if (err) throw err;
                    console.log(
                      "\x1b[32m%s\x1b[0m",
                      path.join(baseDirectory, "Form.vue"),
                      " created successfully"
                    );
                    console.log(
                      "\x1b[34m%s\x1b[0m",
                      "Please add following to router is not exists",
                      "(",
                      path.join(__dirname, "router", "admin.js"),
                      "):\n"
                    );
                    console.log(`import ${name}Index from '../pages/admin/${modelName.toLowerCase()}/Index'
import ${name}Create from '../pages/admin/${modelName.toLowerCase()}/Create'
import ${name}Edit from '../pages/admin/${modelName.toLowerCase()}/Edit'`);
                    console.log(`
  {
    path: '${modelName.toLowerCase()}',
    name: '${modelName.toLowerCase()}.index',
    component: ${name}Index
  },

  {
    path: '${modelName.toLowerCase()}/create',
    name: '${modelName.toLowerCase()}.create',
    component: ${name}Create
  },

  {
    path: '${modelName.toLowerCase()}/:id/edit',
    name: '${modelName.toLowerCase()}.edit',
    component: ${name}Edit
  },\n`);
                    console.log(
                      "\x1b[34m%s\x1b[0m",
                      "And Add following to menus is not exists",
                      "(",
                      path.join(__dirname, "meuns.js"),
                      "):"
                    );
                    console.log(`
    {
      title: '${modelName.charAt(0).toUpperCase() + modelName.slice(1)}',
      route: 'admin.${modelName.toLowerCase()}.index'
    },`);
                  }
                );
              }
            );
          }
        );
      }
    );
  });
}
console.log("\x1b[32m%s\x1b[0m", "Write the model ");
readline.question(`Directory Name:`, name => {
  modelName = name;
  readline.close();
  fs.writeFile(
    path.join(__dirname, "src", "api", modelName.toLowerCase() + ".js"),
    apiContent(modelName),
    function(err) {
      if (err) throw err;
      console.log(
        "\x1b[32m%s\x1b[0m",
        "Api Created successfully",
        path.join(__dirname, "src", "api", modelName.toLowerCase() + ".js")
      );
      baseDirectory = path.join(
        __dirname,
        "src",
        "pages",
        "admin",
        modelName.toLowerCase()
      );
      if (!fs.existsSync(baseDirectory)) {
        fs.mkdir(baseDirectory, function(err) {
          if (err) throw err;
          console.log(
            "\x1b[32m%s\x1b[0m",
            baseDirectory,
            " created successfully"
          );
          generateFiles();
        });
      } else {
        console.log("\x1b[31m%s\x1b[0m", baseDirectory, " exists");
        generateFiles();
      }
    }
  );
});
