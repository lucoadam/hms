from django.shortcuts import render
from rest_framework import viewsets,generics
import datetime
import pytz
from . import serializers
from .models import Patients
from django.contrib.auth.models import User
from django.db.models import Sum
from income.views import initialMonths
from metas.models import Metas
from bill.models import Bill,BillService
from services.models import Services
from hospital.models import Hospital,UserBelongsToHospital
from rest_framework import response
from rest_framework.decorators import action
from rest_framework import parsers
from django_filters.rest_framework  import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from type.models import Type
import datetime, os, json

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
with open(os.path.join(BASE_DIR, 'apilogin', 'date.json')) as json_data:
  datemap = json.load(json_data)



# Create your views here.
# class PatientsViewset(generics.ListAPIView,viewsets.ModelViewSet):
#     queryset = Patients.objects.all()
#     serializer_class = serializers.PatientsSerializer
#     filter_backends = (DjangoFilterBackend,)
#     filterset_fields = ('Patients__pname')

from rest_framework import filters



class PatientListView(generics.ListAPIView):
    queryset = Patients.objects.all()
    serializer_class = serializers.PatientsSerializer
    filter_backends = (DjangoFilterBackend,OrderingFilter,SearchFilter)
    filter_fields = ('pcode','pname','paddress')
    ordering_fields = ('pname', 'pcode', 'paddress', 'id')
    search_fields = ('pname', 'pcode', 'paddress', 'pphone', 'bills__bcode')

    def get_queryset(self):
        queryset = Patients.objects.all()
        if not self.request.user.is_anonymous:
            hos = Hospital.objects.filter(user=self.request.user)
            if (hos.exists()):
                queryset = Patients.objects.filter(hospital_id=hos[0].id)
            else:
              usr = UserBelongsToHospital.objects.filter(user=self.request.user)
              print(self.request.user.groups.all())
              if usr.exists():
                queryset = Patients.objects.filter(hospital=usr[0].hospital)
        return queryset

    @classmethod
    def get_extra_actions(cls):
        return []


class ReportListView(generics.ListAPIView):
    serializer_class = serializers.PatientsSerializer

    def get_queryset(self):
        queryset_list = Bill.objects.filter(refunded=False)
        get_from = self.request.GET.get('from')
        get_to = self.request.GET.get('to')
        user = self.request.GET.get('user')
        if get_from and get_to:
            queryset_list= Bill.objects.filter(refunded=False,created_at__range=[ datetime.datetime(2019,3,11,15,58,52,tzinfo=pytz.UTC) , datetime.datetime(2020,3,11,15,58,52,tzinfo=pytz.UTC) ])
        return queryset_list

class PatientsViewset(viewsets.ModelViewSet):
    queryset = Patients.objects.all()
    serializer_class = serializers.PatientsSerializer
    #permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('referred_by','pcreated_at','pcreated_by','pphone','psex','page','pcode','pname','paddress')

    def get_queryset(self):
        year = Metas.objects.filter(hospital=None,key='current_year')[0].value
        queryset = Patients.objects.filter(year=year)
        if not self.request.user.is_anonymous:
            hos = Hospital.objects.filter(user=self.request.user)
            if (hos.exists()):
                year = Metas.objects.filter(hospital_id=hos[0].id,key='current_year')[0].value
                queryset = Patients.objects.filter(hospital_id=hos[0].id,year=year)
            else:
              usr = UserBelongsToHospital.objects.filter(user=self.request.user)
              if self.request.user.doctors_set.exists():
                year = Metas.objects.filter(hospital_id=usr[0].hospital.id,key='current_year')[0].value
                queryset= Patients.objects.filter(referred_by=self.request.user.doctors_set.all()[0].id,year=year)
              elif usr.exists():
                year = Metas.objects.filter(hospital_id=usr[0].hospital.id,key='current_year')[0].value
                queryset = Patients.objects.filter(hospital=usr[0].hospital,year=year)
        # year = Metas.obje
        return queryset

    def destroy(self, request, *args, **kwargs):
      instance = self.get_object()
      if request.GET['remark']:
        instance.changeReason = request.GET['remark']
      self.perform_destroy(instance)
      return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False,methods=['GET'])
    def age(self,request,*args,**kwargs):
        patients = Patients.objects.order_by('page').all()
        datas=[]
        bil={}
        serv={}
        present=[]
        for patient in patients:
            bills = patient.bills.all()
            print(patient.page)
            s=0
            total=0
            for bill in bills:
                bS = [serv for serv in bill.services]
                #print(bS)
                for b in bS:
                    total+=b.sprice*b.squantity
                #billservices=[serv for serv in bill.services]
                s +=bill.services.count()
            if (patient.page//10) in present:
                serv[present.index(patient.page//10)]['bill']+= bills.count()
            else:
                present.append(patient.page//10)
                age={
                    'age':str((patient.page//10)*10-9)+'-'+str((patient.page//10)*10),
                    'id': 0 if not present else present.index(patient.page//10)
                }
                serv[0 if not present else present.index(patient.page//10)]={'bill': bills.count(),'service':s,'total':total}
                datas.append(age)
        return Response({'age':datas,'bills':serv})

    @action(detail=False,methods=['GET'])
    def servicebillamount(self,request,*args,**kwargs):
        services =  Hospital.objects.filter(user=request.user).first().services_set.all() if Hospital.objects.filter(user=request.user).exists() else UserBelongsToHospital.objects.filter(user=request.user).first().hospital.services_set.all() if UserBelongsToHospital.objects.filter(user=request.user).exists() else Services.objects.order_by('sname').all()
        b={}
        total=[]
        servic = []
        i=0
        print(services.count())
        for service in services:
            billService = BillService.objects.filter(service=service,bill__refunded=False)
            billsCount = [(billservice.bill.bcode) for billservice in billService]
            total = [service.sprice*service.squantity for service in billService]
            b[i]={'bills':len(billsCount),'total':sum(total)}


            #total = [(t.squantity*t.sprice) for t in total]
            servic.append({
                'id':i,
                'service':service.sname
            })
            i+=1
        return Response({'bill':b,'service':servic})

    @action(detail=False, methods=['GET'])
    def userserviceamountdetails(self, request, *args, **kwargs):
      reqest_data = request.GET
      bills = Bill.objects.all()
      serviceTypelist = []
      b = {}
      users = []
      monthYearBillMapping = {}
      if 'user' in reqest_data and User.objects.filter(id=reqest_data['user']).exists():
        bills = Bill.objects.filter(user=reqest_data['user'])
        if 'to' in reqest_data and 'from' in reqest_data:
          # queryset_list= Bill.objects.filter(refunded=False,created_at__range=[ datetime.datetime(2019,3,11,15,58,52,tzinfo=pytz.UTC) , datetime.datetime(2020,3,11,15,58,52,tzinfo=pytz.UTC) ])
          gFrom = reqest_data['from'].split('/')
          gTo = reqest_data['to'].split('/')
          bills = Bill.objects.filter(user=reqest_data['user'], created_at__range=[
            datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
            datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
      else:
        if 'to' in reqest_data and 'from' in reqest_data:
          # queryset_list= Bill.objects.filter(refunded=False,created_at__range=[ datetime.datetime(2019,3,11,15,58,52,tzinfo=pytz.UTC) , datetime.datetime(2020,3,11,15,58,52,tzinfo=pytz.UTC) ])
          gFrom = reqest_data['from'].split('/')
          gTo = reqest_data['to'].split('/')
          bills = Bill.objects.filter(created_at__range=[
            datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
            datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
      if type(None) != bills:
        daystypeMapping = {}
        for bill in bills:
          dateKey = bill.created_at.strftime('%Y/%m/%d')
          if (dateKey in datemap) and (datemap[dateKey] != ''):
            converted = datemap[dateKey]
            each_date = converted.split('/')
            datas = bill.services.filter(service__stype__id=reqest_data['service']).values('service__sname',
                                                                                           'service__stype__tname').annotate(
              rate=Sum('sprice'),
              quantity=Sum('squantity')) if 'service' in reqest_data and Type.objects.filter(id=reqest_data['service']).exists() else  bill.services.values('service__sname', 'service__stype__tname').annotate(
              rate=Sum('sprice'),
              quantity=Sum('squantity'))
            for data in datas:
              key = data['service__sname'] + each_date[2] + str(bill.user.id)
              if key not in daystypeMapping:
                serviceTypelist.append(key)
                current_date = each_date[0] + '/' + each_date[1] + '/' + each_date[2]
                current_user = bill.user.first_name + ' ' + bill.user.last_name
                if current_user not in users:
                  users.append(current_user)
                  b[current_user] = {}
                  b[current_user][current_date] = []
                else:
                  if not current_date in b[current_user]:
                    b[current_user][current_date] = []
                monthYearBillMapping[key] = {
                  'user': current_user,
                  'date': current_date
                }

                daystypeMapping[key] = {
                  'amount': data['rate'] * data['quantity'],
                  'type': data['service__stype__tname'] + ' ' + data['service__sname'],
                  'received_by': bill.user.first_name + ' ' + bill.user.last_name,
                }
              else:
                daystypeMapping[key]['amount'] += data['rate'] * data['quantity']
        for key in serviceTypelist:

          if (monthYearBillMapping[key]['date'] in b[monthYearBillMapping[key]['user']]):
            b[monthYearBillMapping[key]['user']][monthYearBillMapping[key]['date']].append(
              daystypeMapping[key]) if key in daystypeMapping else ''

      return Response({'bill': b, 'user': users, })

    @action(detail=False,methods=['GET'])
    def userbillamount(self,request,*args,**kwargs):
        get_user = self.request.GET.get('user')
        users = User.objects.filter(id=get_user) if get_user and  User.objects.filter(id=get_user).exists() else User.objects.exclude(username='superuser')
        if not request.user.is_anonymous:
            hos = Hospital.objects.filter(user=request.user)
            if (hos.exists()):
                users=[usr.user for usr in UserBelongsToHospital.objects.filter(hospital_id=hos[0].id)]
                users.append(hos[0].user)
            else:
                ad=UserBelongsToHospital.objects.filter(user=request.user)
                if ad.exists():
                    users=[usr.user for usr in UserBelongsToHospital.objects.filter(hospital=ad[0].hospital)]
                    users.append(ad[0].hospital.user)
        get_from = self.request.GET.get('from')
        get_to = self.request.GET.get('to')
        datas =[]
        b={}

        servic = []
        i=0
        for user in users:
            total=0
            if get_from and get_to:
                gFrom= get_from.split('/')
                gTo = get_to.split('/')
                bills = Bill.objects.filter(refunded=False,user=user,created_at__range=[datetime.datetime(int(gFrom[0]),int(gFrom[1]),int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                                   datetime.datetime(int(gTo[0]), int(gTo[1]),int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
            else:
                bills = Bill.objects.filter(refunded=False,user=user)
            j=0
            for bill in bills:
              each = 0
              for serv in bill.services:
                each += serv.squantity * serv.sprice
              if each == bill.total:
                datas.append({'code': bill.bcode, 'services': len(bill.services),
                              'service': ','.join([serv.service.sname for serv in bill.services]), 'total': each,
                              'amount': bill.total, 'created': bill.created_at.strftime('%Y/%m/%d')})
              for serv in bill.services:
                total += serv.squantity * serv.sprice
              # total+=bill.total
            b[i]={'bills':len(bills),'total':total,'billdetails':datas}
            datas=[]
            servic.append({
                'id':i,
                'user':user.username,
                'name':user.first_name+' '+user.last_name
            })
            i+=1

        return Response({'bill':b,'users':servic})


    @action(detail=False,methods=['GET'])
    def service_date(self,request,*args,**kwargs):
        users = User.objects.order_by('-id').all()
        datas =[]
        b={}
        date=0
        usr = []
        i=0
        for user in users:
            bills = Bill.objects.filter(refunded=False,user=user)
            for bill in bills:
                date = str(bill.created_at)



            b[i]={'bills':len(bills),'date':date}

            usr.append({
                'id':i,
                'user':user.username
            })
            i+=1


        return Response({'bill':b,'users':usr})








def unique(it):
    s =[]
    for el in it:
        if el not in s:
            s.append(el)
            yield el





#     # for pdf download
# from django.http import HttpResponse
# from django.views.generic import View

# from .utils import render_to_pdf
# import datetime

# class GeneratePdf(View):
#     def get(self, request, *args, **kwargs):
#         data = {
#              'today': datetime.date.today(),
#              'amount': 39.99,
#             'customer_name': 'Cooper Mann',
#             'order_id': 1233434,
#         }
#         pdf = render_to_pdf('pdf/invoice.html', data)
#         return HttpResponse(pdf, content_type='application/pdf')

