import django_filters

class PatientsFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='iexact')
    

    class Meta:
        model = Patient
        fields = ['pname']

