from rest_framework import serializers
from bill.serializers import BillSerializer
from . import models
from bill.models import BillService,Bill

class PatientsSerializer(serializers.ModelSerializer):
    bills =  BillSerializer(many=True)
    doctor = serializers.StringRelatedField()
    created_at = serializers.StringRelatedField()
    hospital_name = serializers.CharField(required=False)
    pphone = serializers.IntegerField(required=False)

    class Meta:
        model = models.Patients
        fields = [
            "id",
            "pcode",
            "pname",
            "page",
            "psex",
            "paddress",
            "pphone",
            "pcreated_by",
            "pcreated_at",
            "referred_by",
            "bills",
            'doctor',
            'created_at',
            'hospital',
            'hospital_name',
            'year'
        ]

    def create(self,validated_data):
        patient = models.Patients(
            pname=validated_data.get('pname'),
            page=validated_data.get('page'),
            psex=validated_data.get('psex'),
            paddress=validated_data.get('paddress'),
            referred_by=validated_data.get('referred_by'),
            pcreated_by=validated_data.get('pcreated_by'),
            hospital=validated_data.get('hospital'),
            year=validated_data.get('year'),
        )
        if validated_data.get('pphone'):
          patient.pphone=validated_data.get('pphone')
        patient.save()
        datas = validated_data.get('bills')[0]
        bill = Bill.objects.create(patient=patient,user=datas.get('user'),total=datas.get('total'),year=validated_data.get('year'))
        services = datas.get('services')
        total = 0
        for service in services:
          total += service.get('sprice') * service.get('squantity')
          BillService.objects.create(
	        service=service.get('service'),
 	        sprice=service.get('sprice'),
        	squantity = service.get('squantity'),
         	bill=bill)

        bill.total = total
        bill.save()
        return patient

    def update(self,instance,validated_data):
        datas = validated_data.get('bills')[0]
        bill = Bill.objects.create(patient=instance,user=datas.get('user'),total=datas.get('total'))
        services = datas.get('services')
        total = 0
        for service in services:
            total += service.get('sprice') * service.get('squantity')
            BillService.objects.create(
                service=service.get('service'),
                sprice=service.get('sprice'),
                squantity = service.get('squantity'),
                bill=bill)
        bill.total = total
        bill.save()
        return instance
