from django.db import models
from datetime import date
from doctor.models import Doctors
from login.models import Year
from hospital.models import Hospital
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords
# from multiselectfield import MultiSelectField
import datetime

from apilogin.utils import converttothree, returnCode

def xyz():
    no = Patients.objects.count()
    if no == None:
        return 1
    else:
      if (no == 0):
        return 'P' + returnCode() + converttothree(1, 4)
      last = Patients.objects.first().pcode[-4:]
      return 'P' + returnCode() + converttothree(int(last) + 1, 4)


# Create your models here.
class Patients(models.Model):
    MALE = 'MALE'
    FEMALE = 'FEMALE'
    OTHER = 'OTHER'

    SEX_CHOICES = [
        (MALE, 'Male'),
        (FEMALE, 'Female'),
        (OTHER, 'Other'),
    ]

    pcode =  models.CharField(max_length = 20, default = xyz, editable=False)
    pname = models.CharField(max_length=50)
    page = models.IntegerField()
    psex = models.CharField(max_length=6,choices = SEX_CHOICES,default = MALE)
    paddress = models.CharField(max_length=100)
    pphone = models.BigIntegerField(null=True)
    pcreated_by = models.CharField(null=True,max_length=50)
    pcreated_at = models.DateTimeField(auto_now_add=True)
    referred_by = models.IntegerField()
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT,
    )
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    history = HistoricalRecords()
    def __str__(self):
        return self.pname

    def created_at(self):
        return self.pcreated_at.strftime('%m %d, %Y')

    def bills(self):
        return self.bills_set.all()

    def hospital_name(self):
        return self.hospital.name

    def doctor(self):
        return Doctors.objects.filter(id=self.referred_by)[0].dname if Doctors.objects.filter(id=self.referred_by).exists() else 'None'

    class Meta:
        db_table = "Patient"
        ordering = ["-id"]

    @staticmethod
    def autocomplete_search_fields():
        return 'pname', 'pcode'



