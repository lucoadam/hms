# Generated by Django 3.0.1 on 2020-07-19 10:41

from django.db import migrations, models


class Migration(migrations.Migration):
  dependencies = [
    ('patients', '0006_historicalpatients'),
  ]

  operations = [
    migrations.AddField(
      model_name='historicalpatients',
      name='remark',
      field=models.CharField(max_length=20, null=True),
    ),
    migrations.AddField(
      model_name='patients',
      name='remark',
      field=models.CharField(max_length=20, null=True),
    ),
  ]
