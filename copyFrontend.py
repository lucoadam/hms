import os,shutil

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            if os.path.exists(d):
                shutil.copy2(s, d)
            else:
                shutil.copy2(s, d)

basepath =os.path.dirname(os.path.abspath(__file__))
shutil.rmtree(basepath+'/login/templates')
os.mkdir(basepath+'/login/templates')
copytree(basepath+'/target/dist/',basepath+'/login/templates')
print("Done")
