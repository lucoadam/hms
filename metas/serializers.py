from rest_framework import serializers
from . import models


class MetasSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Metas
        fields = "__all__"

