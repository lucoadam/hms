from rest_framework import generics,response
from . import serializers
from .models import Metas
from hospital.models import Ward
from hospital.models import Hospital,UserBelongsToHospital
from rest_framework.permissions import IsAuthenticated

# Create your views here.
class MetasList(generics.ListCreateAPIView):
    queryset = Metas.objects.filter(hospital__isnull=True)
    serializer_class = serializers.MetasSerializer

    def get_queryset(self):
      queryset = Metas.objects.filter(hospital__isnull=True)
      if not self.request.user.is_anonymous:
        hos= Hospital.objects.filter(user=self.request.user)
        if hos.exists():
          queryset= Metas.objects.filter(hospital=hos[0])
        else:
          usr = UserBelongsToHospital.objects.filter(user=self.request.user)
          if usr.exists():
            queryset = Metas.objects.filter(hospital=usr[0].hospital)
      return queryset

    def list(self, request, *args, **kwargs):
      print('alish')
      queryset = self.filter_queryset(self.get_queryset())

      page = self.paginate_queryset(queryset)
      if page is not None:
        serializer = self.get_serializer(page, many=True)
        data = serializer.data
        if not Ward.objects.count():
            data.append({
              'key':'is_installed',
              'value':"0"
            })
        return self.get_paginated_response(data)

      serializer = self.get_serializer(queryset, many=True)
      return response.Response(serializer.data)

class MetasDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Metas.objects.filter(hospital__isnull=True)
    serializer_class = serializers.MetasSerializer

    def get_queryset(self):
      queryset = Metas.objects.filter(hospital__isnull=True)
      if not self.request.user.is_anonymous:
        hos= Hospital.objects.filter(user=self.request.user)
        if hos.exists():
          queryset= Metas.objects.filter(hospital=hos[0])
        else:
          usr = UserBelongsToHospital.objects.filter(user=self.request.user)
          print(usr)
          if usr.exists():
            queryset = Metas.objects.filter(hospital=usr[0].hospital)

      return queryset
