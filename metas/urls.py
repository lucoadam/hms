from django.urls import include, path
from rest_framework.routers import DefaultRouter
from . import views

urlpatterns = [
    path("", views.MetasList.as_view(), name="metas_list"),
    path("<int:pk>/",views.MetasDetail.as_view(), name="metas_detail")

]