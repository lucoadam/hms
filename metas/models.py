from django.db import models
from hospital.models import Hospital
# Create your models here.
class Metas(models.Model):
    key = models.CharField(max_length=20)
    value=models.CharField(max_length=50)
    hospital = models.ForeignKey(Hospital,blank=True,null=True,on_delete=models.CASCADE)
    def __str__(self):
        return '{"'+self.key+'":"'+self.value+'"}'

    class Meta:
      unique_together=('key','value','hospital')
