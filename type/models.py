from django.db import models
from hospital.models import Hospital
from staffType.models import StaffType
from simple_history.models import HistoricalRecords

# Create your models here.
def ids():
    no = Type.objects.count()
    if no == 0:
       return '001'
    else:
        no = Type.objects.last()
        i= str(int(no.code)+1)
        while len(i)<3:
            i='0'+i

        return i


class Type(models.Model):
    tname = models.CharField(max_length=50)
    code = models.CharField(max_length=5,null=True,editable=False)
    is_incentive = models.BooleanField(default=False)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    history=HistoricalRecords()
    def __str__(self):
        return self.tname

    def hospital_name(self):
        return self.hospital.name

    class Meta:
        db_table="service_type"
        unique_together=('tname','hospital')

class ServiceHasIncentive(models.Model):
  stafftype = models.ForeignKey(
    StaffType,
    on_delete=models.PROTECT
  )
  type = models.ForeignKey(
    Type,
    on_delete=models.PROTECT,
    default=None
  )
  percentage = models.IntegerField()
  history=HistoricalRecords()
