from rest_framework import serializers
from . import models

class TypeSerializer(serializers.ModelSerializer):
    hospital_name = serializers.CharField(required=False)
    code = serializers.CharField(required=True)
    class Meta:
        model = models.Type
        fields = [
            'id',
            'tname',
            'code',
            'hospital',
            'hospital_name',
            'is_incentive'
        ]

class ServiceTypeHasIncentive(serializers.ModelSerializer):
  class Meta:
    model = models.ServiceHasIncentive
    fields = '__all__'
