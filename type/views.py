from rest_framework import viewsets,status,filters
from . import serializers
from rest_framework.pagination import PageNumberPagination
from .models import Type,ServiceHasIncentive
from  services.models import Services
from rest_framework.response import Response
from hospital.models import Hospital
from rest_framework.permissions import IsAuthenticated

def ownGenerator(type):
    typ= Hospital.objects.filter(id=type)[0].code if Hospital.objects.filter(id=type).exists() else  '0001'
    typ = typ[2:]
    no = Type.objects.count()
    if no == 0:
        return typ+'001'
    else:
        s = int(Type.objects.filter(hospital_id=type).last().code[-3:])
        if(s == 0):
            return typ+'001'
        else:
            a='000'
            st = str(s+1)
            i = len(st)
            while i< 3:
                st='0'+st
                i=i+1

            return typ+st

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000

class TypeViewset(viewsets.ModelViewSet):
    queryset = Type.objects.all()
    serializer_class = serializers.TypeSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['tname']

    def get_queryset(self):
        queryset = Type.objects.all()
        hos= Hospital.objects.filter(user=self.request.user)
        if hos.exists():
            queryset= Type.objects.filter(hospital=hos[0])
        return queryset


    def retrieve(self, request, *args, **kwargs):
      instance = self.get_object()
      serializer = self.get_serializer(instance)
      data = serializer.data
      if instance.is_incentive:
        data['stafftypepercentage'] = [{
          'stafftype': item.stafftype.id,
          'percentage': item.percentage
        } for item in ServiceHasIncentive.objects.filter(type=instance)]
      return Response(data)

    def destroy(self, request, *args, **kwargs):
        if not Services.objects.filter(stype=kwargs.get('pk')).exists():
            if request.GET['remark']:
              Type.changeReason = request.GET['remark']
            Type.objects.filter(id=kwargs.get('pk')).delete()

        return Response({'success': True}) if not Services.objects.filter(
            stype=kwargs.get('pk')).exists() else Response({'success': False})

    def perform_destroy(self, instance):
        instance.delete()


    def create(self, request, *args, **kwargs):
        data = request.data
        data['code']=ownGenerator(data['hospital'])

        serializer = self.serializer_class(data=data)

        if(serializer.is_valid()):
            serializer.save()
            instance = serializer.data
            if instance['is_incentive']:
              d =[]
              for item in data.get('stafftypepercentage'):
                if item['stafftype']:
                  item['type'] = instance['id']
                  serial = serializers.ServiceTypeHasIncentive(data=item)
                  if serial.is_valid(True):
                    serial.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

