from django.db import models
import datetime
from login.models import Year
from incometype.models import IncomeType
from hospital.models import Hospital
from simple_history.models import HistoricalRecords
from django.contrib.auth.models import User
def ftn():
    no = Income.objects.count()
    if no == None:
        return 1
    else:
        return 'S' + str(str(datetime.date.today().year)) + str(datetime.date.today().month).zfill(2)+str(no+1)

# Create your models here.
class Income(models.Model):
    # scode = models.CharField(max_length = 20,unique=True)
    amount = models.FloatField()
    itype = models.ForeignKey(IncomeType, on_delete=models.CASCADE)
    remarks = models.TextField(null=True)
    received_by = models.CharField(null=True,max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.CharField(null=True, max_length=50)
    updated_by = models.CharField(null=True, max_length=50)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    year = models.ForeignKey(Year,on_delete=models.PROTECT,default=77)
    history=HistoricalRecords()
    def __str__(self):
        return self.amount

    def screated_at(self):
        return self.created_at.strftime('%Y/%m/%d')

    def type(self):
        return self.itype.tname

    def hospital_name(self):
        return self.hospital.name


    class Meta:
        db_table = "incomes"
        unique_together = ('amount','received_by','itype','hospital')


class IncomeHistory(models.Model):
    amount = models.FloatField()
    itype = models.ForeignKey(IncomeType, on_delete=models.CASCADE)
    received_by = models.CharField(null=True,max_length=50)
    hospital = models.ForeignKey(
        Hospital,
        on_delete=models.PROTECT
    )
    action=models.CharField(max_length=20)
    actionBy=models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )
    actionDate=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.actionBy.username
