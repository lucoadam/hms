
from rest_framework import viewsets,generics,filters
from django.db.models import Max,Min,Sum
import json,os
import csv
from rest_framework.response import Response
from django.http import HttpResponse
from . import serializers
from django.views.generic import View
from . import models
from rest_framework.pagination import PageNumberPagination
from .models import Income,IncomeHistory
from expenditure.models import Expenditure
from bill.models import Bill
from incometype.models import IncomeType
import datetime
import pytz
from hospital.models import Hospital,UserBelongsToHospital
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated
from rest_framework import status,decorators
from rest_framework.settings import api_settings
import json
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
with open(os.path.join(BASE_DIR, 'apilogin', 'date.json')) as json_data:
  datemap = json.load(json_data)

class StandardResultsSetPagination(PageNumberPagination):
  page_size = 100
  page_size_query_param = 'page_size'
  max_page_size = 1000

# Create your views here.
class IncomeViewSet(viewsets.ModelViewSet):
    queryset = Income.objects.all()
    serializer_class = serializers.IncomeSerializer
    # permission_classes = (IsAuthenticated,)
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['received_by']

    @decorators.action(detail=False,methods=['GET'])
    def incomeexpensedetials(self, request, *args, **kwargs):
      request_data = request.GET
      datas = None
      t = True
      if 'dtype' in request_data and request_data['dtype'] == 'expense':
        datas = Expenditure.objects.all()
        t = False
        if 'user' in request_data and Expenditure.objects.filter(paid_by=request_data['user']).exists():
          datas = Expenditure.objects.filter(paid_by=request_data['member'])
          if 'to' in request_data and 'from' in request_data:
            gFrom = request_data['from'].split('/')
            gTo = request_data['to'].split('/')
            datas = Expenditure.objects.filter(paid_by=request_data['user'], created_at__range=[
              datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
              datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
            if 'type' in request_data and request_data['type']!='0':
              datas = Expenditure.objects.filter(itype_id=request_data['type'],paid_by=request_data['user'], created_at__range=[
                datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])

        else:
          if 'to' in request_data and 'from' in request_data:
            gFrom = request_data['from'].split('/')
            gTo = request_data['to'].split('/')
            datas = Expenditure.objects.filter(created_at__range=[
              datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
              datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
            if 'type' in request_data and request_data['type']!='0':
              datas = Expenditure.objects.filter(itype_id=request_data['type'],created_at__range=[
                datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
      else:
        datas = Income.objects.all()
        print(request_data['member'])
        user = User.objects.filter(pk=request_data['member'])
        if user.exists():
          user = user.last().first_name + ' ' + user.last().last_name
        if 'user' in request_data and Income.objects.filter(received_by=request_data['user']).exists():

          datas = Income.objects.filter(received_by=request_data['user'])
          if 'to' in request_data and 'from' in request_data:
            gFrom = request_data['from'].split('/')
            gTo = request_data['to'].split('/')
            datas = Income.objects.filter(received_by=request_data['user'], created_at__range=[
              datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
              datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
            if 'type' in request_data and request_data['type']!='0':
              datas = Income.objects.filter(itype_id=request_data['type'],received_by=request_data['user'], created_at__range=[
                datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])

        else:
          if 'to' in request_data and 'from' in request_data:
            gFrom = request_data['from'].split('/')
            gTo = request_data['to'].split('/')
            datas = Income.objects.filter( created_at__range=[
              datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
              datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
            if 'type' in request_data and request_data['type']!='0':
              datas = Income.objects.filter(itype_id=request_data['type'], created_at__range=[
                datetime.datetime(int(gFrom[0]), int(gFrom[1]), int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                datetime.datetime(int(gTo[0]), int(gTo[1]), int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
      daystypeMapping = {}
      accountTypelist = []
      monthYearBillMapping = {}
      users=[]
      b={}
      for data in datas:
        dateKey = data.created_at.strftime('%Y/%m/%d')
        if (dateKey in datemap) and (datemap[dateKey] != ''):
          converted = datemap[dateKey]
          each_date = converted.split('/')
          key = data.itype.tname + each_date[2] + (data.received_by if t else data.paid_by)
          if key not in daystypeMapping:
            accountTypelist.append(key)
            current_date = each_date[0] + '/' + each_date[1] + '/' + each_date[2]
            current_user = (data.received_by if t else data.paid_by).upper()
            if current_user not in users:
              users.append(current_user)
              b[current_user] = {}
              b[current_user][current_date] = []
            else:
              if not current_date in b[current_user]:
                b[current_user][current_date] = []
            monthYearBillMapping[key] = {
              'user': current_user,
              'date': current_date
            }
            daystypeMapping[key] = {
              'amount': data.amount,
              'type': data.itype.tname,
              ('received_by' if t else 'paid_by'): current_user,
            }
          else:
            daystypeMapping[key]['amount']+= data.amount
      for key in accountTypelist:

        if (monthYearBillMapping[key]['date'] in b[monthYearBillMapping[key]['user']]):
          b[monthYearBillMapping[key]['user']][monthYearBillMapping[key]['date']].append(
            daystypeMapping[key]) if key in daystypeMapping else ''

      return Response({'data':b,'user':users})

    def get_queryset(self):
        queryset = Income.objects.all()
        hos= Hospital.objects.filter(user=self.request.user)
        if hos.exists():
            queryset= Income.objects.filter(hospital=hos[0])
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data=request.data
        temp=models.IncomeHistory()
        temp.itype=IncomeType.objects.get(id=data['itype'])
        temp.received_by=data['received_by']
        temp.hospital=Hospital.objects.get(id=data['hospital'])
        temp.action='create'
        temp.actionBy=request.user
        temp.amount=data['amount']
        temp.save()
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        tempInc=Income.objects.get(id=kwargs.get('pk'))
        temp=models.IncomeHistory()
        temp.itype=tempInc.itype
        temp.received_by=tempInc.received_by
        temp.hospital=tempInc.hospital
        temp.action='edit'
        temp.actionBy=request.user
        temp.amount=tempInc.amount
        temp.save()
        serializer.save()
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)



    def destroy(self, request, *args, **kwargs):
            instance = self.get_object()
            if request.GET['remark']:
                instance.changeReason = request.GET['remark']
            self.perform_destroy(instance)
            return Response({'success':True})

    def perform_destroy(self, instance):
        instance.delete()



def initialMonths():
  return {
    '01':{
      'income_amount':0,
      'expense_amount':0
    },
    '02':{
      'income_amount':0,
      'expense_amount':0
    },
    '03':{
      'income_amount':0,
      'expense_amount':0
    },
    '04':{
      'income_amount':0,
      'expense_amount':0
    },
    '05':{
      'income_amount':0,
      'expense_amount':0
    },
    '06':{
      'income_amount':0,
      'expense_amount':0
    },
    '07':{
      'income_amount':0,
      'expense_amount':0
    },
    '08':{
      'income_amount':0,
      'expense_amount':0
    },
    '09':{
      'income_amount':0,
      'expense_amount':0
    },
    '10':{
      'income_amount':0,
      'expense_amount':0
    },
    '11':{
      'income_amount':0,
      'expense_amount':0
    },
    '12':{
      'income_amount':0,
      'expense_amount':0
    }
  }

class DataForAccount(generics.ListAPIView):
  queryset = Income.objects.all()

  # FIXME: Deduce and optimize codes
  def get(self, request, *args, **kwargs):
    get_user = self.request.GET.get('user')
    users = User.objects.filter(id=get_user) if get_user else User.objects.exclude(username='superuser')
    get_from = self.request.GET.get('from')
    get_to = self.request.GET.get('to')
    hospital = self.request.GET.get('hospital', 1)
    incomes = Income.objects.all()
    expenses = Expenditure.objects.all()
    bills = Bill.objects.filter(refunded=False)
    if not request.user.is_anonymous:
      hos = Hospital.objects.filter(user=request.user) if hospital == 0 else Hospital.objects.filter(id=hospital)
      if (hos.exists()):
        users=[usr.user for usr in UserBelongsToHospital.objects.filter(hospital_id=hos[0].id)]
        users.append(hos[0].user)
        if get_from and get_to:
          gFrom= get_from.split('/')
          gTo = get_to.split('/')
          incomes = Income.objects.filter(hospital_id=hos[0].id,created_at__range=[datetime.datetime(int(gFrom[0]),int(gFrom[1]),int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                                                                                   datetime.datetime(int(gTo[0]), int(gTo[1]),int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
          expenses = Expenditure.objects.filter(hospital_id=hos[0].id,created_at__range=[datetime.datetime(int(gFrom[0]),int(gFrom[1]),int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                                                                                         datetime.datetime(int(gTo[0]), int(gTo[1]),int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
        else:
          incomes = Income.objects.filter(hospital_id=hos[0].id)
          expenses = Expenditure.objects.filter(hospital_id=hos[0].id)
          # bills = Bill.objects.filter(hospital_id=hos[0].id)
      else:
        ad=UserBelongsToHospital.objects.filter(user=request.user)
        if ad.exists():
          users=[usr.user for usr in UserBelongsToHospital.objects.filter(hospital=ad[0].hospital)]
          users.append(ad[0].hospital.user)
          if get_from and get_to:
            gFrom= get_from.split('/')
            gTo = get_to.split('/')
            incomes = Income.objects.filter(hospital_id=ad[0].hospital.id,created_at__range=[datetime.datetime(int(gFrom[0]),int(gFrom[1]),int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                                                                                             datetime.datetime(int(gTo[0]), int(gTo[1]),int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
            expenses = Expenditure.objects.filter(hospital_id=ad[0].hospital.id,created_at__range=[datetime.datetime(int(gFrom[0]),int(gFrom[1]),int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                                                                                                   datetime.datetime(int(gTo[0]), int(gTo[1]),int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
          else:
            incomes = Income.objects.filter(hospital_id=ad[0].hospital.id)
            expenses = Expenditure.objects.filter(hospital_id=ad[0].hospital.id)
            # bills = Bill.objects.filter(hospital_id=ad[0].hospital.id)


    inc={}
    exp={}
    serviceTypelist = []
    b={}
    total=0
    billTotal =0
    servic = []
    i=0
    bills=None
    monthYearBillMapping = {}
    daystypeMapping = {}
    serviceTypelist = []
    for user in users:
      if get_from and get_to:

        bills = Bill.objects.filter(refunded=False,user=user,created_at__range=[datetime.datetime(int(gFrom[0]),int(gFrom[1]),int(gFrom[2]), 1, 58, 52, tzinfo=pytz.UTC),
                                                                 datetime.datetime(int(gTo[0]), int(gTo[1]),int(gTo[2]), 23, 58, 52, tzinfo=pytz.UTC)])
      else:
        bills = Bill.objects.filter(refunded=False,user=user)

      if type(None) != bills:
        t=bills.aggregate(sum=Sum('total'))['sum']
        total += (t if type(None) != type(t) else 0)


        for bill in bills:

          dateKey=bill.created_at.strftime('%Y/%m/%d')
          if (dateKey in datemap) and (datemap[dateKey]!=''):
            converted=datemap[dateKey]
            each_date = converted.split('/')
            year = each_date[0]
            month = each_date[1]
            if not year in b:
              b[year] = initialMonths()
              servic.append(year)
              if not 'income' in b[year][month]:
                b[year][month]['income'] = []
            else:
              if not 'income' in b[year][month]:
                b[year][month]['income'] = []
        #
            datas = bill.services.values('service__stype__tname').annotate(rate=Sum('sprice'),
                                                                           quantity=Sum('squantity'))
            for data in datas:
              billTotal += data['rate']*data['quantity']
              data['amount'] = data['rate']*data['quantity']
              data['received_by'] =  (bill.user.first_name + ' ' + bill.user.last_name).upper()
              data['day'] = each_date[2]
              key = data['service__stype__tname']+each_date[0]+'/'+each_date[1]+'/'+data['day']+data['received_by']
              if key not in daystypeMapping:
                serviceTypelist.append(key)
                monthYearBillMapping[key] = {
                  'year': each_date[0],
                  'month': each_date[1]
                }
                daystypeMapping[key] = {
                  'amount': data['amount'],
                  'received_by': data['received_by'],
                  'type': data['service__stype__tname'],
                  'day': data['day']
                }
              else:
                daystypeMapping[key]['amount']+= data['amount']


        #     for data in datas:

        #       key = data['service__stype__tname'] + each_date[2] + str(bill.user.id)
        #       if key not in daystypeMapping:
        #         serviceTypelist.append(key)
        #         monthYearBillMapping[key] = {
        #           'year': year,
        #           'month': month
        #         }
        #
        #         daystypeMapping[key] = {
        #           'amount': data['rate'] * data['quantity'],
        #           'type': data['service__stype__tname'],
        #           'received_by': bill.user.first_name + ' ' + bill.user.last_name,
        #           'day': each_date[2]
        #         }
        #       else:
        #         daystypeMapping[key]['amount'] += data['rate'] * data['quantity']
    # for key in serviceTypelist:
    #   if ('income' in b[monthYearBillMapping[key]['year']][monthYearBillMapping[key]['month']]):
    #     b[monthYearBillMapping[key]['year']][monthYearBillMapping[key]['month']]['income'].append(
    #       daystypeMapping[key]) if key in daystypeMapping else ''
    #     if key in daystypeMapping:
    #       b[monthYearBillMapping[key]['year']][monthYearBillMapping[key]['month']]['income_amount'] += \
    #         daystypeMapping[key]['amount']4
    for key in serviceTypelist:
      print(key,monthYearBillMapping[key],daystypeMapping[key])
      if ('income' in b[monthYearBillMapping[key]['year']][monthYearBillMapping[key]['month']]):
        b[monthYearBillMapping[key]['year']][monthYearBillMapping[key]['month']]['income'].append(daystypeMapping[key])
        b[monthYearBillMapping[key]['year']][monthYearBillMapping[key]['month']]['income_amount'] += daystypeMapping[key]['amount']


    for income in incomes:
      dateKey = income.created_at.strftime('%Y/%m/%d')
      if dateKey in datemap and datemap[dateKey] != '':
        each_date = datemap[dateKey].split('/')
        year = each_date[0]
        month = each_date[1]
        if not year in b:
          b[year] = initialMonths()
          servic.append(year)
          if not 'income' in b[year][month]:
            b[year][month]['income'] = []
        else:
          if not 'income' in b[year][month]:
            b[year][month]['income']=[]
        b[year][month]['income'].append({
          'amount': income.amount,
          'type': income.itype.tname,
          'received_by': income.received_by.upper(),
          'day': each_date[2]
        })
        b[year][month]['income_amount'] += income.amount

    for expense in expenses:
      dateKey = expense.created_at.strftime('%Y/%m/%d')
      if dateKey in datemap and datemap[dateKey] != '':
        each_date = datemap[dateKey].split('/')
        year = each_date[0]
        month = each_date[1]
        if not year in b:
          b[year] = initialMonths()
          servic.append(year)
          if not 'expense' in b[year][month]:
            b[year][month]['expense'] = []
        else:
          if not 'expense' in b[year][month]:
            b[year][month]['expense'] = []

        b[year][month]['expense'].append({
          'amount': expense.amount,
          'type': expense.itype.tname,
          'paid_by': expense.paid_by.upper(),
          'day': each_date[2]
        })
        b[year][month]['expense_amount'] += expense.amount


    incs = incomes.aggregate(sum=Sum('amount'))['sum']
    exps = expenses.aggregate(sum=Sum('amount'))['sum']
    acc = {
      'income':(incs if type(incs) != type(None) else 0)+(total if type(total) != type(None) else 0),
      'expenses':(exps if type(exps) != type(None) else 0),
    }
    dateKey=datetime.datetime.now().strftime('%Y/%m/%d')
    if (dateKey in datemap) and (datemap[dateKey]!=''):
      converted=datemap[dateKey]
      each_date = converted.split('/')
      year = each_date[0]
      month = each_date[1]
      acc['activeYear'] = year
      acc['activeMonth'] = month
    return Response({
      'accounts':b,
      'years':servic,
      'account':acc,
    })

class IncomeHistoryViewset(viewsets.ModelViewSet):
    queryset=IncomeHistory.objects.all()
    serializer_class=serializers.IncomeHistorySerializer



class DownloadDataForAccount(View):
    serializer_class = serializers.IncomeSerializer

    def get_serializer(self, queryset, many=True):
        return self.serializer_class(
            queryset,
            many=many,
        )

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment'
        filename="export.csv"

        serializer = self.get_serializer(
            Income.objects.all(),
            many=True
        )
        header = serializers.IncomeSerializer.Meta.fields

        writer = csv.DictWriter(response, fieldnames=header)
        writer.writeheader()
        for row in serializer.data:
            writer.writerow(row)

        return response

